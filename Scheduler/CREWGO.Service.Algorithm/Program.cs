﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;

namespace CREWGO.Service.Algorithm
{
    class Program
    {
        #region PRIVATE FIELDS
        private static HttpClient client = new HttpClient();
        private static Stopwatch sw;
        #endregion

        static void Main(string[] args)
        {
            client.BaseAddress = new Uri(
                ConfigurationManager
                .AppSettings["CREWGOWEBAPI_PUBLISHED_LOCAL"].ToString());
            //client.BaseAddress = new Uri(
            //  ConfigurationManager
            //  .AppSettings["CREWGOWEBAPI_PUBLISHED_STAGING"].ToString());
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(
                    "application/json"
                )
            );

            while (true)
            {
                Thread.Sleep(TimeSpan.FromMinutes(1));

                try
                {
                    sw = Stopwatch.StartNew();
                    Modules.CREWGOPartnerAlgorithm.RunPartnerAlgorithm(client).Wait();
                    sw.Stop();
                    Console.WriteLine("Successful Execution");
                    Console.WriteLine(
                        System.Environment.NewLine + "Run Algorithm [{0}] = {1}ms", 
                        DateTime.Now, sw.ElapsedMilliseconds);
                }
                catch (Exception e)
                {
                    sw.Stop();
                    Console.WriteLine(
                        "Unsuccessful Execution \n" + 
                        e.Message + "\n" + 
                        e.StackTrace);
                    //continue;
                }
                finally
                {
                    Console.ReadKey(true);
                }

            }
        }
    }
}