﻿using CREWGO.Service.Algorithm.Service;
using CREWGO.Service.BusinessService;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Algorithm.Modules
{
    /// <summary>
    /// The CrewGo Crew (CRUVA) Algorithm Class
    /// </summary>
    public class CREWGOCrewAlgorithm
    {
        #region VARIABLES
        #endregion
        #region CREW ALGORITHM LOGIC
        /// <summary>
        /// The CRUVA algorithm logic
        /// </summary>
        /// <param name="cons">HttpClient</param>
        /// <param name="shift">JobShift</param>
        public static async Task RunCrewAlgorithm(
            HttpClient cons, JobShifts shift)
        {
            // Generate potential staff list in database
            await CREWGOService
                .GeneratePotentialStaffList(cons, shift.JobShiftId);

            bool isShiftQuotaFulfilled = false;
            while(!isShiftQuotaFulfilled)
            {
                // Get potential staff count for the job
                int potentialStaffCount = await CREWGOService
                    .GetPotentialStaffCount(cons, shift.JobShiftId);

                // Compute Potential Staff Result(PSR)
                double potentialStaffResult = await Task.Run(() => 
                    CREWGOService.ComputePotentialStaffResult(
                        potentialStaffCount, shift.RequiredNumber)
                );

                // Fetch Shift Sociability Factor (SF), Sociable Hours
                int sociabilityFactor = 10;
                DateTime sociableHoursStart = DateTime.Now;
                DateTime sociableHoursEnd = DateTime.Now;

                // If shift time lies outside sociable hours
                // Compute New Potential Staff Result (PSR * SF)
                double newPSR = potentialStaffResult;
                if (shift.StartTime > sociableHoursEnd || 
                      shift.EndTime < sociableHoursStart)
                {
                    newPSR = await Task.Run(() =>
                        CREWGOService.ComputeNewPotentialStaffResult(
                            potentialStaffResult, sociabilityFactor)
                    );
                }

                // Load Activity Level table from Database
                List<ActivityLevel> activityLevels = 
                    await CREWGOService.GetAllActivityLevels(cons);

                // And Potential Staff List has not been exhausted
                while (potentialStaffCount != 0)
                {
                    // Check whether it is Single(Advanced) Request Period
                    // or Multiple Request Period
                    ActivityLevel currentAL = 
                        activityLevels
                        .FirstOrDefault(x =>
                            newPSR >= x.p_staff_result_range_from &&
                            newPSR <= x.p_staff_result_range_to);
                    ActivityLevel nextAL =
                        activityLevels
                        .FirstOrDefault(x => x.p_staff_result_range_to == 
                            currentAL.p_staff_result_range_from - 1);

                    TimeSpan timeToShift = (DateTime)shift.StartTime -
                                                DateTime.Now;
                    TimeSpan shortNoticePeriod = await CREWGOService
                                            .GetShortNoticePeriod(cons);
                    // If it is Single Request Period, send 1 request per shift 
                    // every 'X' minutes as determined by Request Rate 
                    // for this period until Multiple Request Period is reached
                    if (timeToShift > shortNoticePeriod)
                    {
                        int numberOfReq = await Task.Run(() =>
                            CREWGOService.ComputeNumberOfRequests(
                                timeToShift - shortNoticePeriod, 
                                TimeSpan.FromSeconds(
                                    currentAL.shift_time_interval
                                )
                            )
                        );

                        List<PotentialStaff> potentialStaffs = 
                            await CREWGOService.GetPotentialStaffs(
                                cons, shift.JobShiftId, numberOfReq);
                        await ScheduleNotifications(
                            potentialStaffs, 
                            shift, 
                            cons, 
                            currentAL.shift_time_interval,
                            1);
                    }
                    // If it is Multiple Request Period,
                    // Recalculate New Potential Staff Result
                    // Determine Activity Level for New Potential Staff result
                    // Get Request Rate corresponding to the Activity Level
                    // Determine the next Activity Level
                    // Determine the number of requests 'N' to be sent 
                    // until next Activity Level is reached
                    // Send 'N' requests depending upon the request rate
                    else
                    {
                        double nextPSR = await Task.Run(() =>
                            CREWGOService.CalculateNextPSR(nextAL));
                        int numberOfReq = await Task.Run(() =>
                            CREWGOService.ComputeNumberOfRequests(
                                newPSR, nextPSR));
                        List<PotentialStaff> potentialStaffs =
                            await CREWGOService.GetPotentialStaffs(
                                cons, shift.JobShiftId, numberOfReq);
                        await ScheduleNotifications(
                            potentialStaffs, 
                            shift, 
                            cons,
                            currentAL.shift_time_interval,
                            shift.RequiredNumber);
                    }

                    // Repeat above block until shift is fulfilled
                    // or potential staff list is exhausted
                }
                isShiftQuotaFulfilled = await CREWGOService
                    .IsShiftQuotaFulfilled(cons, shift.JobShiftId);
            }
        }
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Prepares notifications scheduling to potential staffs
        /// </summary>
        /// <param name="staffs">List of Potential Staffs</param>
        /// <param name="shift">The job shift</param>
        /// <param name="cons">HttpClient</param>
        /// <param name="interval">Interval to send notifications in</param>
        /// <param name="count">Number of notifications to send at once</param>
        public static async Task ScheduleNotifications(
            List<PotentialStaff> staffs, JobShifts shift, 
            HttpClient cons, int interval, int count)
        {
            if (staffs == null || staffs.Count == 0)
            {
                return;
            }

            int counter = 0;
            int time = 0;
            foreach( PotentialStaff staff in staffs)
            {
                if (staff.Devices == null || staff.Devices.Count == 0)
                {
                    continue;
                }
                ++counter;
                if (counter == count)
                {
                    counter = 0;
                    time += interval;
                    //use DateTime.Now + this time(seconds) to schedule
                }
                var iosTokens =
                    staff.Devices
                    .Where(
                        x => x.DeviceType == (int)Enums.DeviceTypeEnum.ANDROID)
                    .Select(x => x.DeviceToken)
                    .ToList();
                var androidTokens =
                    staff.Devices
                    .Where(
                        x => x.DeviceType == (int)Enums.DeviceTypeEnum.IOS)
                    .Select(x => x.DeviceToken)
                    .ToList();
                await PreScheduleNotifications(
                        androidTokens, iosTokens, shift, cons);
            }
        }
        /// <summary>
        /// Checks if shift quota is fulfilled 
        /// and schedules notifications if it is not fulfilled
        /// </summary>
        /// <param name="androidTokens">List of Android tokens</param>
        /// <param name="iosTokens">List of iOS tokens</param>
        /// <param name="shift">The shift to send notifications for</param>
        /// <param name="cons">HttpClient</param>
        /// ToDo: Make method sync for use with hangfire
        public static async Task PreScheduleNotifications(
            List<string> androidTokens, List<string> iosTokens, 
            JobShifts shift, HttpClient cons)
        {
            //ToDo: With hangfire, use sync function below 
            var isShiftQuotaFulfilled = await CREWGOService
                .IsShiftQuotaFulfilled(cons, shift.JobShiftId);
            if (isShiftQuotaFulfilled)
            {
                return;
            }
            await new PushNotifications()
                .SendPushNotification(
                1,
                "",
                Enums.NotificationTypeEnum.JOBALERTS,
                androidTokens,
                iosTokens,
                null,
                null,
                Enums.UserGroupEnum.STAFF);
        }
        /// <summary>
        /// Sync wrapper for async method IsStaffQuotaFulfilled
        /// To be used with hangfire only
        /// </summary>
        /// <param name="cons">HttpClient</param>
        /// <param name="shiftId">The Shift's ID</param>
        /// <returns>True if shift quota is fulfilled</returns>
        public static bool IsShiftQuotaFulfilled(HttpClient cons, int shiftId)
        {
            return CREWGOService.IsShiftQuotaFulfilled(cons, shiftId).Result;
        }
        #endregion
    }
}
