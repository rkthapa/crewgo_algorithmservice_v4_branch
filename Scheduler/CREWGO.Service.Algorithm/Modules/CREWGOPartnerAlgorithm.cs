﻿using CREWGO.Service.Algorithm.Service;
using CREWGO.Service.BusinessService;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CREWGO.Service.Algorithm.Modules
{
    public class CREWGOPartnerAlgorithm
    {
        #region VARIABLES
        
        #endregion

        #region ALGORITHM LOGIC
        public static async Task RunPartnerAlgorithm(HttpClient cons, JobShifts shift, int shiftNoticePeriod)
        {
            var lstStaffAndPriority = await SetAvailableStaffAndPriorityType(cons, shift, shiftNoticePeriod);
            var availableStaff = lstStaffAndPriority.Item1;
            var staffPriorityType = lstStaffAndPriority.Item2;

            //Console.WriteLine("staff = " + availableStaff.Count<User>());

            if (availableStaff != null && availableStaff.Count<User>() > 0)
            {
                foreach (var staff in availableStaff)
                {
                    //Get alert count
                    //var alertCount = await Task.Run(() => CREWGOService.GetJobAlertCount(cons, staff.UserId));
                    var alertCount = await CREWGOService.GetJobAlertCount(cons, staff.UserId);

                    if (staff.UserDevice != null && !string.IsNullOrEmpty(staff.UserDevice.DeviceToken))
                    {
                        var iosToken = new List<string>();
                        var androidToken = new List<string>();

                        if (staff.UserDevice.DeviceType == (int)Enums.DeviceTypeEnum.ANDROID)
                            androidToken.Add(staff.UserDevice.DeviceToken);
                        else
                            iosToken.Add(staff.UserDevice.DeviceToken);

                        //Send push notification
                        bool isSuccess = await new PushNotifications().SendPushNotification(alertCount, "", Enums.NotificationTypeEnum.JOBALERTS, androidToken, iosToken, null, null, (Enums.UserGroupEnum)staff.GroupId);

                        if (isSuccess)
                        {
                            //Insert job alert 
                            await CREWGOService.PostJobAlert(cons, await CREWGOService.SetJobAlert(shift.JobShiftId, staff.UserId));

                            //Get job alert details - UNUSED
                            //var jobAlert = await CREWGOService.GetJobAlert(cons, shift.JobShiftId, staff.UserId);                        

                            UpdateAlertSettings(cons, shift, staff, staffPriorityType);

                            //Log to view shfit in console
                            Console.WriteLine(string.Format("{0} | {1}-{2}-{3} | {4}-{5}-{6} | {7} | {8}",
                                              staffPriorityType.ToString(),
                                              shift.JobShiftId,
                                              shift.ParentJob.LHCUserId,
                                              shift.RequiredNumber,
                                              shift.ParentJob.JobTitle,
                                              shift.SkillId,
                                              shift.LevelId,
                                              shift.StartTime,
                                              shift.EndTime));
                            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------");

                            //Log to view staff in console
                            Console.WriteLine(string.Format("{0} | {1} | {2} | {3} | Sent On: {4}",
                                              staff.UserId,
                                              staff.Name,
                                              staff.Email,
                                              staff.UserDevice.DeviceToken,
                                              DateTime.Now.ToString()));
                            Console.WriteLine(System.Environment.NewLine);

                            //Maintain log for push notifications sent
                            await Task.Run(() => CREWGOService.MaintainAlertLog(
                                              Utility.ReplaceInvalidCharsAndSpacesWithUnderScore(shift.ParentJob.JobTitle),
                                              shift.ParentJob.JobNumber,
                                              shift.JobShiftId,
                                              staffPriorityType.ToString(),
                                              shift.ParentJob.LHCUserId,
                                              shift.RequiredNumber,
                                              shift.SkillId,
                                              shift.LevelId,
                                              shift.StartTime,
                                              shift.EndTime,
                                              staff.UserId,
                                              staff.Name,
                                              staff.Email,
                                              staff.UserDevice.DeviceToken));
                        }
                    }

                }
            }            
        }
        private static async Task<Tuple<IEnumerable<User>, Enums.StaffPriorityTypeEnum>> SetAvailableStaffAndPriorityType(HttpClient cons, JobShifts shift, int shiftNoticePeriod)
        {
            var availableStaff = (IEnumerable<User>)null;
            var staffPriorityType = Enums.StaffPriorityTypeEnum.MANUAL;

            if (await CREWGOService.OrderPriorityExists(cons, shift.JobShiftId))
            {
                //Console.WriteLine("MANUAL");
                availableStaff = await CREWGOService.GetStaffByManualPriority(
                    cons, 
                    shift.JobShiftId, 
                    shift.RequiredNumber, 
                    shift.StartTime.Value.ToString(Utility.DateTimeFormat), 
                    shift.EndTime.Value.ToString(Utility.DateTimeFormat));
                staffPriorityType = Enums.StaffPriorityTypeEnum.MANUAL;
            }
            else if (await CREWGOService.GlobalPriorityExists(cons, (int)shift.ParentJob.LHCUserId, shift.SkillId, shift.LevelId))
            {
                //Console.WriteLine("GLOBAL");
                if (DateTime.Now.Subtract(shift.ParentJob.EnteredDate).TotalMinutes >= shiftNoticePeriod)
                {
                    availableStaff = await CREWGOService.GetStaffByGlobalPriority(
                        cons, 
                        (int)shift.ParentJob.LHCUserId, 
                        shift.JobShiftId, 
                        shift.SkillId, 
                        shift.LevelId, 
                        shift.RequiredNumber, 
                        shift.StartTime.Value.ToString(Utility.DateTimeFormat), 
                        shift.EndTime.Value.ToString(Utility.DateTimeFormat));
                }

                staffPriorityType = Enums.StaffPriorityTypeEnum.GLOBAL;
            }
            else
            {
                //Console.WriteLine("GENERAL");
                if (DateTime.Now.Subtract(shift.ParentJob.EnteredDate).TotalMinutes >= shiftNoticePeriod)
                {
                    availableStaff = await CREWGOService.GetStaffByGeneralPriority(
                        cons, 
                        (int)shift.ParentJob.LHCUserId, 
                        shift.JobShiftId, 
                        shift.SkillId, 
                        shift.LevelId, 
                        shift.StartTime.Value.ToString(Utility.DateTimeFormat), 
                        shift.EndTime.Value.ToString(Utility.DateTimeFormat));
                }

                staffPriorityType = Enums.StaffPriorityTypeEnum.GENERAL;
            }

            return new Tuple<IEnumerable<User>, Enums.StaffPriorityTypeEnum>(availableStaff, staffPriorityType);
        }
        private static async void UpdateAlertSettings(HttpClient cons, JobShifts shift, User staff, Enums.StaffPriorityTypeEnum staffPriorityType)
        {
            switch (staffPriorityType)
            {
                case Enums.StaffPriorityTypeEnum.MANUAL:
                    //Update job alert setting
                    var alertSetting = await CREWGOService.GetJobAlertSettingByShiftUser(cons, shift.JobShiftId, staff.UserId);
                    await CREWGOService.PutJobAlertSetting(cons, alertSetting);

                    break;
                case Enums.StaffPriorityTypeEnum.GLOBAL:
                    //Update job alert global setting
                    var alertGlobalSetting = await CREWGOService.GetJobAlertSettingByPartnerUser(cons, (int)shift.ParentJob.LHCUserId, staff.UserId);
                    await CREWGOService.PutJobAlertSettingGlobal(cons, alertGlobalSetting);

                    break;
                case Enums.StaffPriorityTypeEnum.GENERAL:
                    await CREWGOService.PostJobAlertSetting(cons, await CREWGOService.SetJobAlertSetting(shift.JobShiftId, staff.UserId, (int)shift.ParentJob.LHCUserId));
                    
                    break;
            }
        }
        #endregion
    }
}