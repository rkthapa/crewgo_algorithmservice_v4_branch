﻿using CREWGO.Service.Algorithm.Service;
using CREWGO.Service.BusinessService;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Algorithm.Modules
{
    public class CREWGOAlgorithm
    {
        #region VARIABLES
        public static string errorPath = System.AppDomain.CurrentDomain.BaseDirectory + string.Format("{0}{1}.txt", ConfigurationManager.AppSettings["PARTNERMODULE_PATH_ERROR"].ToString(), DateTime.Now.ToString("ddMMyyyy"));
        #endregion

        #region ALGORITHM LOGIC
        public static async Task RunAlgorithm(HttpClient cons)
        {
            try
            {
                //Get list of latest shifts
                var jobShifts = CREWGOService.GetShifts(cons);
                
                //Get priority notice period, i.e time to set the priority to manual or global
                var shiftNoticePeriod = CREWGOService.GetShiftNoticePeriod(cons);

                await jobShifts;
                await shiftNoticePeriod;

                foreach (var shift in jobShifts.Result)
                {
                    //Console.WriteLine(System.Environment.NewLine);
                    //Console.WriteLine("[" + DateTime.Now + "] SHIFT NUMBER = " + shift.JobShiftId);

                    var isShiftQuotaFulfilled = CREWGOService
                        .IsShiftQuotaFulfilled(cons, shift.JobShiftId);
                    await isShiftQuotaFulfilled;

                    //Check if shift quota is fulfilled
                    if (!isShiftQuotaFulfilled.Result)
                    {
                        //Check if parent job is associated with partner or not
                        if (shift.ParentJob.LHCUserId > 0 && shift.ParentJob.LHCUserId != null)
                        {
                            //Console.WriteLine("[" + DateTime.Now + "] RUN PARTNER ALGORITHM");
                            //Run Partner Algorithm
                            var algPartner = CREWGOPartnerAlgorithm.RunPartnerAlgorithm(cons, shift, shiftNoticePeriod.Result);
                            await algPartner;
                        }
                        else
                        {
                            //Console.WriteLine("[" + DateTime.Now + "] RUN CREW ALGORITHM");
                            //Run Crew Algorithm
                            //await CREWGOCrewAlgorithm.RunCrewAlgorithm(cons, shift);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogFile(errorPath, ex.Message + "\n" + ex.StackTrace);
                //Utility.SendEmailWithAttachment(errorPath,Enums.EmailType.ERRORMAIL);
                throw;
            }
        }
        #endregion
    }
}