﻿using CREWGO.Service.BusinessService;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CREWGO.Service.Algorithm.Service
{
    public static class CREWGOService
    {
        #region GET METHODS
        public static async Task<IEnumerable<JobShifts>> GetShifts(HttpClient cons)
        {
            var serviceUrl = "api/crewgo/getlatestshifts";
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                return JsonConvert.DeserializeObject<IEnumerable<JobShifts>>(jsonString);
                //Console.WriteLine("[" + DateTime.Now + "] GET SHIFTS");
            }

            return null;
        }
        public static async Task<IEnumerable<User>> GetStaffByGeneralPriority(HttpClient cons, int partnerId, int jobShiftId, int skillId, int levelId, string startTime, string endTime)
        {
            var serviceUrl = (string.IsNullOrEmpty(startTime) || string.IsNullOrEmpty(endTime)) ? "api/crewgo/getavailablestaffbygeneralpriority/1/1/1/1/null/null" : "api/crewgo/getavailablestaffbygeneralpriority/" + partnerId + "/" + jobShiftId + "/" + skillId + "/" + levelId + "/" + startTime.ToString() + "/" + endTime;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                return JsonConvert.DeserializeObject<IEnumerable<User>>(jsonString);
            }
            return null;
        }
        public static async Task<IEnumerable<User>> GetStaffByGlobalPriority(HttpClient cons, int partnerId, int jobShiftId, int skillId, int levelId, int userLimit, string startTime, string endTime)
        {
            var serviceUrl = "api/crewgo/getavailablestaffbyglobalpriority/" + partnerId + "/" + jobShiftId + "/" + skillId + "/" + levelId + "/" + userLimit + "/" + startTime + "/" + endTime;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            
            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                return JsonConvert.DeserializeObject<IEnumerable<User>>(jsonString);
            }
            return null;
        }
        public static async Task<IEnumerable<User>> GetStaffByManualPriority(HttpClient cons, int jobShiftId, int userLimit, string startTime, string endTime)
        {
            var serviceUrl = "api/crewgo/getavailablestaffbymanualpriority/" + jobShiftId + "/" + userLimit + "/" + startTime + "/" + endTime;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            
            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                return JsonConvert.DeserializeObject<IEnumerable<User>>(jsonString);
            }
            return null;
        }
        public static async Task<Int32> GetJobAlertCount(HttpClient cons, int staffUserId)
        {
            var serviceUrl = "api/crewgo/getjobalertcount/" + staffUserId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                return Convert.ToInt32(await response.Content.ReadAsStringAsync());
            }
            return 0;
        }
        public static async Task<Boolean> OrderPriorityExists(HttpClient cons, int jobShiftId)
        {
            var serviceUrl = "api/crewgo/orderpriorityexists/" + jobShiftId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            
            if (response.IsSuccessStatusCode)
            {
                //Get the response
                return Convert.ToBoolean(await response.Content.ReadAsStringAsync());
            }
            return false;
        }
        public static async Task<Boolean> GlobalPriorityExists(HttpClient cons, int partnerId, int skillId, int levelId)
        {
            var serviceUrl = "api/crewgo/globalpriorityexists/" + partnerId + "/" + skillId + "/" + levelId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                return Convert.ToBoolean(await response.Content.ReadAsStringAsync());
            }
            return false;
        }
        public static async Task<JobAlert> GetJobAlert(HttpClient cons, int jobShiftId, int staffUserId)
        {
            var serviceUrl = "api/crewgo/getjobalertbyshiftuserid/" + jobShiftId + "/" + staffUserId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();
                //Deserialize the data
                return JsonConvert.DeserializeObject<JobAlert>(jsonString);
            }
            return null;
        }
        public static async Task<JobAlertSetting> GetJobAlertSettingByShiftUser(HttpClient cons, int jobShiftId, int staffUserId)
        {
            var serviceUrl = "api/crewgo/getjobalertsettingbyshiftuserid/" + jobShiftId + "/" + staffUserId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                return JsonConvert.DeserializeObject<JobAlertSetting>(jsonString);
            }

            return null;
        }
        public static async Task<JobAlertSettingGlobal> GetJobAlertSettingByPartnerUser(HttpClient cons, int partnerId, int staffUserId)
        {
            var serviceUrl = "api/crewgo/getjobalertsettingbypartneruserid/" + partnerId + "/" + staffUserId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                return JsonConvert.DeserializeObject<JobAlertSettingGlobal>(jsonString);
            }
            return null;
        }
        public static async Task<Boolean> IsShiftQuotaFulfilled(HttpClient cons, int jobShiftId)
        {
            var serviceUrl = "api/crewgo/isstaffachieved/" + jobShiftId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Console.WriteLine("[" + DateTime.Now + "] IS SHIFT QUOTA FULFILLED");
                //Get the response
                return Convert.ToBoolean(await response.Content.ReadAsStringAsync());
            }
            return false;
        }
        public static async Task<int> GetShiftNoticePeriod(HttpClient cons)
        {
            var value = 0;
            var serviceUrl = "api/crewgo/getshiftnoticeperiod";
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            
            if (response.IsSuccessStatusCode)
            {
                //Get the response
                value = Convert.ToInt32(await response.Content.ReadAsStringAsync());
            }
            //Console.WriteLine("[" + DateTime.Now + "] SHIFT NOTICE PERIOD");
            return value;
        }
        #endregion

        #region POST METHODS
        public static async Task PostJobAlert(HttpClient cons, tbl_job_alert obj)
        {
            await Task.Run(() => 
                {
                    var serviceUrl = "api/jobalert";
                    HttpResponseMessage response = cons.PostAsJsonAsync(serviceUrl, obj).Result;                
                });
        }
        public static async Task PostJobAlertSetting(HttpClient cons, tbl_job_manual_alert_setting obj)
        {
            await Task.Run(() =>
                {
                    var serviceUrl = "api/jobalertsetting";
                    HttpResponseMessage response = cons.PostAsJsonAsync(serviceUrl, obj).Result;
                });
        }
        #endregion

        #region PUT METHODS
        public static async Task PutJobAlertSetting(HttpClient cons, JobAlertSetting obj)
        {
            HttpResponseMessage response = await cons.GetAsync("api/jobalertsetting/" + obj.JobAlertSettingId);
            
            if (response.IsSuccessStatusCode)
            {
                tbl_job_manual_alert_setting setting = await response.Content.ReadAsAsync<tbl_job_manual_alert_setting>();
                setting.alerted_date = DateTime.UtcNow.Date;
                setting.status = true;
                await cons.PutAsJsonAsync("api/jobalertsetting/" + obj.JobAlertSettingId, setting);
            }
        }
        public static async Task PutJobAlertSettingGlobal(HttpClient cons, JobAlertSettingGlobal obj)
        {
            HttpResponseMessage response = await cons.GetAsync("api/jobalertsettingglobal/" + obj.JobAlertSettingGlobalId);

            if (response.IsSuccessStatusCode)
            {
                tbl_job_manual_alert_gb_setting setting = await response.Content.ReadAsAsync<tbl_job_manual_alert_gb_setting>();
                setting.status = 0;
                await cons.PutAsJsonAsync("api/jobalertglobalsetting/" + obj.JobAlertSettingGlobalId, setting);
            }
        }
        #endregion

        #region SET ENTITYFRAMEWORK OBJECTS
        public static async Task<tbl_job_alert> SetJobAlert(int jobShiftId, int staffUserId)
        {
            tbl_job_alert obj = new tbl_job_alert();

            await Task.Run(() =>
                {
                    obj.job_detail_id = jobShiftId;
                    obj.staff_user_id = staffUserId;
                    obj.status = (int)Enums.JobAlertStatusEnum.UNREAD;
                    obj.action_date = DateTime.Now;
                });

            return obj;
        }
        public static async Task<tbl_job_manual_alert_setting> SetJobAlertSetting(int jobShiftId, int staffUserId, int partnerId)
        {
            tbl_job_manual_alert_setting obj = new tbl_job_manual_alert_setting();

            await Task.Run(() => 
                {
                    obj.job_detail_id = jobShiftId;
                    obj.staff_user_id = staffUserId;
                    obj.lhc_user_id = partnerId;
                    obj.order_by = null;
                    obj.assigned_date = DateTime.Now;
                    obj.alerted_date = null;
                    obj.status = true;                
                });
            
            return obj;
        }
        #endregion

        #region CUSTOM METHODS
        public static async void MaintainAlertLog(string jobTitle, string jobNumber, int jobShiftId, string staffPriorityType, int? partnerId, int shiftReqNumber, int skillId, int levelId, DateTime? startTime, DateTime? endTime, int staffId, string staffName, string staffEmail, string deviceToken)
        {
            //await Task.Run(() =>
            //{
                string content = string.Empty;
                string path = System.AppDomain.CurrentDomain.BaseDirectory + string.Format("{0}{1}#{2}_{3}_{4}.txt", ConfigurationManager.AppSettings["PARTNERMODULE_PATH"].ToString(), jobTitle, jobNumber, jobShiftId, DateTime.Now.ToString("ddMMyyyy"));
                if (!File.Exists(path))
                    content = string.Format("{0} | {1}-{2}-{3} | {4}-{5}-{6} | {7} | {8}" + System.Environment.NewLine + System.Environment.NewLine, staffPriorityType, jobShiftId, partnerId, shiftReqNumber, jobTitle, skillId, levelId, startTime, endTime);
                content += string.Format("{0} | {1} | {2} | {3} | Sent On: {4}", staffId, staffName, staffEmail, deviceToken, DateTime.Now.ToString());

                Utility.WriteLogFile(path, content);
            //});
        }
        #endregion

        #region POTENTIAL STAFF ALGORITHM
        #region GET METHODS
        /// <summary>
        /// Gets the count of potential staffs.
        /// </summary>
        /// <param name="cons">HttpClient</param>
        /// <param name="jobShiftId">The Job Shift Id</param>
        /// <returns>Count of Potential Staffs(int)</returns>
        public static async Task<int> GetPotentialStaffCount(
            HttpClient cons, int jobShiftId)
        {
            var value = 0;
            var serviceUrl = "api/crewgo/getpotentialstaffcount/" + jobShiftId;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                value = Convert.ToInt32(await response.Content.ReadAsStringAsync());
            }
            return value;
        }
        /// <summary>
        /// Gets all activity levels defined in the admin portal.
        /// </summary>
        /// <param name="cons">HttpClient</param>
        /// <returns>ActivityLevelList</returns>
        public static async Task<List<ActivityLevel>> GetAllActivityLevels(
            HttpClient cons)
        {
            var serviceUrl = "api/crewgo/getallactivitylevels";
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                string jsonString = await response.Content.ReadAsStringAsync();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return serializer
                    .Deserialize<List<ActivityLevel>>(jsonString);
            }
            return null;
        }
        /// <summary>
        /// Gets list of top potential staffs upto 'count'
        /// </summary>
        /// <param name="cons">HttpClient</param>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        /// <param name="count">The number of potential staffs to fetch</param>
        /// <returns>List of PotentialStaff</returns>
        public static async Task<List<PotentialStaff>> GetPotentialStaffs(
            HttpClient cons, int jobShiftId, int count)
        {
            var serviceUrl = "api/crewgo/getpotentialstaffs/" + jobShiftId +
                             "/" + count;
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                string jsonString = await response.Content.ReadAsStringAsync();
                return (new JavaScriptSerializer())
                    .Deserialize<List<PotentialStaff>>(jsonString);
            }
            return null;
        }
        /// <summary>
        /// Gets the value of short notice period (in hours) by API call
        /// </summary>
        /// <param name="cons">HttpClient</param>
        /// <returns>Timespan in hours</returns>
        public static async Task<TimeSpan> GetShortNoticePeriod(HttpClient cons)
        {
            var serviceUrl = "api/crewgo/getshortnoticeperiod/";
            TimeSpan time = new TimeSpan();
            HttpResponseMessage response = await cons.GetAsync(serviceUrl);

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                string period = await response.Content.ReadAsStringAsync();
                time = TimeSpan.FromHours(Convert.ToInt32(period));
            }
            return time;
        }

        #endregion
        #region POST METHODS
        /// <summary>
        /// API call to generate Potential Staff List for a job in database
        /// </summary>
        /// <param name="cons">HttpClient</param>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        public static async Task GeneratePotentialStaffList(HttpClient cons, int jobShiftId)
        {
            await Task.Run(() =>
            {
                var serviceUrl = "api/crewgo/generatepotentialstafflist";
                cons.PostAsJsonAsync(serviceUrl, jobShiftId);
            });
        }
        #endregion
        #region PUT METHODS
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Calculates Potential Staff Result
        /// </summary>
        /// <param name="numberOfStaffs">The number of potential staffs</param>
        /// <param name="numberOfShifts">The number of shifts in a job</param>
        /// <returns>Potential Staff Result</returns>
        public static double ComputePotentialStaffResult(
            int numberOfStaffs, int numberOfShifts)
        {
            double result = 0;
            result = (numberOfStaffs * 10) / numberOfShifts;
            return Math.Round(result);
        }
        /// <summary>
        /// Computes New Potential Staff Result using Sociability Factor
        /// </summary>
        /// <param name="psr">Old Potential Staff Result(double)</param>
        /// <param name="sociabilityFactor">Sociability Factor(int)</param>
        /// <returns>New Potential Staff Result(double)</returns>
        public static double ComputeNewPotentialStaffResult(
            double psr, int sociabilityFactor)
        {
            return (psr * sociabilityFactor);
        }      
        /// <summary>
        /// Calculates the number of Single/Advance requests to be sent
        /// until short notice period arrives
        /// </summary>
        /// <param name="time">Time until short notice period</param>
        /// <param name="rate">The rate to send messages at</param>
        /// <returns></returns>
        public static int ComputeNumberOfRequests(
            TimeSpan time, TimeSpan rate)
        {
            return Convert.ToInt32(
                Math.Floor(time.TotalSeconds / rate.TotalSeconds));
        }
        /// <summary>
        /// Calculates the number of requests until next ActivityLevel
        /// for Multiple Request Period
        /// </summary>
        /// <param name="currentPSR">PSR of current Activity Lecvel</param>
        /// <param name="nextPSR">PSR of next Activity Level</param>
        /// <returns>Number of requests</returns>
        public static int ComputeNumberOfRequests(
            double currentPSR, double nextPSR)
        {
            return Convert.ToInt32(Math.Floor(currentPSR - nextPSR));
        }
        /// <summary>
        /// Calculates the PSR 
        /// </summary>
        /// <param name="nextAL"></param>
        /// <returns></returns>
        public static double CalculateNextPSR(ActivityLevel nextAL)
        {
            return (double)nextAL.p_staff_result_range_to / 10;
        }
        #endregion
        #endregion

        
    }
}