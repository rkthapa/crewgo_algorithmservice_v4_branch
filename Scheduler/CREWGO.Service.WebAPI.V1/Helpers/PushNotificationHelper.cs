﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Facade;
using CREWGO.Service.Common;
using CREWGO.Service.Common.RequestObject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CREWGO.Service.WebAPI.V1.Helpers
{
    /// <summary>
    /// Helper class for all Push Notifications to be sent by the API.
    /// </summary>
    public class PushNotificationHelper
    {
        /// <summary>
        /// Sends Instant (Normal) Notifications.
        /// </summary>
        /// <param name="notificationPayload">RequestNotification object</param>
        /// <returns>Notification status codes (enums)</returns>
        public async Task<NotificationStatusEnum> 
            SendInstantNotifications(RequestNotification notificationPayload)
        {
            var userIds = (notificationPayload.userId)
                            .Split(',')
                            .Select(Int32.Parse)
                            .ToList();
            var notificationType = 
                (NotificationTypeEnum)notificationPayload.notificationType;
            var notificationBody = notificationPayload.body;
            var notificationTitle = notificationPayload.title;
            if (userIds.Count == 0)
            {
                return NotificationStatusEnum.USERNOTFOUND;
            }
            var statusList = new List<NotificationStatusEnum>();
            foreach (var userId in userIds)
            {
                User responseUser = new User();
                try
                {
                    responseUser = await Task.Run(() => 
                        new CREWGOBaseServiceFacade().GetUserById(userId)
                    );
                }
                catch (DataException)
                {
                    return NotificationStatusEnum.USERNOTFOUND;
                }
                statusList.Add(
                    SendPushNotifications(
                        responseUser,
                        notificationPayload,
                        notificationBody,
                        notificationTitle,
                        notificationType,
                        false
                    )
                );
                
            }
            if (statusList.All(s => s == NotificationStatusEnum.SUCCESS))
            {
                return NotificationStatusEnum.SUCCESS;
            }
            else if (statusList.Any(s => s == NotificationStatusEnum.NODEVICEFOUND))
            {
                return NotificationStatusEnum.NODEVICEFOUND;
            }
            else if (statusList.Any(s => s == NotificationStatusEnum.USEROFFLINE))
            {
                return NotificationStatusEnum.USEROFFLINE;
            }
            else if (statusList.Any(l => l == NotificationStatusEnum.SUCCESS))
            {
                return NotificationStatusEnum.SOMEFAILED;
            }
            else
            {
                return NotificationStatusEnum.FAILED;
            }
        }
        /// <summary>
        /// Sends Call Notifications (Twilio).
        /// </summary>
        /// <param name="notificationPayload">RequestNotification object</param>
        /// <returns>Notification status codes (enums)</returns>
        public async Task<NotificationStatusEnum> 
            SendVideoCallNotifications(RequestNotification notificationPayload)
        {
            var notificationType = 
                (NotificationTypeEnum)notificationPayload.notificationType;
            var notificationBody = notificationPayload.body;
            var notificationTitle = notificationPayload.title;
            CallInfo callInfo = new CallInfo();
            var notificationRecipients = new List<User>();
            if (notificationPayload.callDetail != null)
            {
                try
                {
                    callInfo = await Task.Run(() =>
                        new CREWGOBaseServiceFacade()
                            .GetCallInfoById(
                                notificationPayload.callDetail.callId
                            )
                    );
                }
                catch (DataException)
                {
                    return NotificationStatusEnum.CALLINFONOTFOUND;
                }
            }

            switch (notificationType)
            {
                case NotificationTypeEnum.INCOMINGCALL:
                    notificationRecipients.Add(callInfo.Receiver);
                    break;

                case NotificationTypeEnum.CALLACCEPTED:
                case NotificationTypeEnum.CALLREJECTED:
                    #region Manage Caller Devices
                    switch (callInfo.Caller.GroupId)
                    {
                        case (int)UserGroupEnum.CUSTOMER:
                        case (int)UserGroupEnum.SUPERVISOR:
                            callInfo.Caller.UserDevices.Clear();
                            if (callInfo.CallerDeviceType ==
                                (int)DeviceTypeEnum.ANDROID)
                            {
                                callInfo.Caller.UserDevices.Add(
                                    new DeviceInfo()
                                    {
                                        DeviceToken = callInfo.CallerDeviceToken,
                                        DeviceType = callInfo.CallerDeviceType
                                    }
                                );
                            }
                            if (callInfo.CallerDeviceType ==
                                (int)DeviceTypeEnum.IOS)
                            {
                                callInfo.Caller.UserDevices.Add(
                                    new DeviceInfo()
                                    {
                                        VoipToken = callInfo.CallerDeviceToken,
                                        DeviceType = callInfo.CallerDeviceType
                                    }
                                );
                            }
                            break;

                        default:
                            break;
                    }
                    #endregion
                    #region Manage Receiver Devices
                    switch (callInfo.Receiver.GroupId)
                    {
                        case (int)UserGroupEnum.CUSTOMER:
                        case (int)UserGroupEnum.SUPERVISOR:
                            if (callInfo.ReceiverDeviceType ==
                                (int)DeviceTypeEnum.ANDROID)
                            {
                                var curent = callInfo.Receiver.UserDevices
                                    .FirstOrDefault(
                                        x => x.DeviceToken == 
                                            callInfo.ReceiverDeviceToken
                                    );
                                if (curent != null)
                                {
                                    callInfo.Receiver.UserDevices.Remove(curent);
                                }

                            }
                            if (callInfo.ReceiverDeviceType ==
                                (int)DeviceTypeEnum.IOS)
                            {
                                var curent = callInfo.Receiver.UserDevices
                                    .FirstOrDefault(
                                        x => x.VoipToken == 
                                            callInfo.ReceiverDeviceToken
                                    );
                                if (curent != null)
                                {
                                    callInfo.Receiver.UserDevices.Remove(curent);
                                }
                            }
                            break;
                        case (int)UserGroupEnum.STAFF:
                            callInfo.Receiver.UserDevice.VoipToken = "";
                            callInfo.Receiver.UserDevice.DeviceToken = "";
                            break;
                        default:
                            break;
                    }
                    #endregion
                    notificationRecipients.Add(callInfo.Caller);
                    notificationRecipients.Add(callInfo.Receiver);
                    break;

                case NotificationTypeEnum.CALLCANCELLED:
                    notificationRecipients.Add(callInfo.Receiver);
                    break;

                case NotificationTypeEnum.CALLCOMPLETED:
                    #region Manage Receiver Devices
                    switch (callInfo.Receiver.GroupId)
                    {
                        case (int)UserGroupEnum.CUSTOMER:
                        case (int)UserGroupEnum.SUPERVISOR:
                            callInfo.Receiver.UserDevices.Clear();
                            if (callInfo.ReceiverDeviceType ==
                                (int)DeviceTypeEnum.ANDROID)
                            {
                                callInfo.Receiver.UserDevices.Add(
                                    new DeviceInfo()
                                    {
                                        DeviceToken = callInfo.ReceiverDeviceToken,
                                        DeviceType = callInfo.ReceiverDeviceType
                                    }
                                );
                            }
                            if (callInfo.ReceiverDeviceType ==
                                (int)DeviceTypeEnum.IOS)
                            {
                                callInfo.Receiver.UserDevices.Add(
                                    new DeviceInfo()
                                    {
                                        VoipToken = callInfo.ReceiverDeviceToken,
                                        DeviceType = callInfo.ReceiverDeviceType
                                    }
                                );
                            }
                            break;
                        default:
                            break;
                    }
#endregion
                    notificationRecipients.Add(callInfo.Receiver);
                    break;
            }

            notificationPayload.callDetail.callerName = callInfo.Caller.Name;
            var status = NotificationStatusEnum.SUCCESS;
            foreach (var recipient in notificationRecipients)
            {
                var type = notificationType;
                if (callInfo.Receiver.UserId == recipient.UserId &&
                    notificationType == NotificationTypeEnum.CALLREJECTED)
                {
                    type = NotificationTypeEnum.CALLALREADYREJECTED;
                }
                if (callInfo.Receiver.UserId == recipient.UserId && 
                    notificationType == NotificationTypeEnum.CALLACCEPTED)
                {
                    type = NotificationTypeEnum.CALLALREADYACCEPTED;
                }

                if (recipient.UserId == callInfo.Receiver.UserId && 
                    (type == NotificationTypeEnum.CALLALREADYACCEPTED || 
                    type == NotificationTypeEnum.CALLALREADYREJECTED))
                {
                    SendPushNotifications(
                                recipient,
                                notificationPayload,
                                notificationBody,
                                notificationTitle,
                                type,
                                true
                            );
                }
                else
                {
                    status = SendPushNotifications(
                                recipient,
                                notificationPayload,
                                notificationBody,
                                notificationTitle,
                                notificationType,
                                true
                            );
                }
            }
            return status;
        }
        /// <summary>
        /// Handles Sinch related notificaitons.
        /// </summary>
        /// <param name="notificationPayload">RequestNotification object</param>
        /// <returns>NotificationStatusEnum</returns>
        public async Task<NotificationStatusEnum>
            SendSinchNotifications(RequestNotification notificationPayload)
        {
            var notificationType =
                (NotificationTypeEnum)notificationPayload.notificationType;
            bool isSuccess = false;

            //List of device tokens [Android|IOS]
            var iosTokens = new List<string>();
            var androidTokens = new List<string>();

            #region PushNotification Constructor Call
            var pushNotification = new PushNotifications(
                PushNotificationKeys.CustomerSenderId,
                PushNotificationKeys.CustomerAuthToken,
                PushNotificationKeys.SupervisorSenderId,
                PushNotificationKeys.SupervisorAuthToken,
                PushNotificationKeys.StaffSenderId,
                PushNotificationKeys.StaffAuthToken,
                PushNotificationKeys.CustomerAppleCert,
                PushNotificationKeys.CustomerApplePass,
                PushNotificationKeys.SupervisorAppleCert,
                PushNotificationKeys.SupervisorApplePass,
                PushNotificationKeys.StaffAppleCert,
                PushNotificationKeys.StaffApplePass,
                PushNotificationKeys.CustomerAppleCertVoip,
                PushNotificationKeys.CustomerApplePassVoip,
                PushNotificationKeys.SupervisorAppleCertVoip,
                PushNotificationKeys.SupervisorApplePassVoip,
                PushNotificationKeys.StaffAppleCertVoip,
                PushNotificationKeys.StaffApplePassVoip
            );
            #endregion

            #region MANAGE NOTIFICATION PAYLOAD
            SinchNotificationDetail sinchDetail = null;
            if (notificationPayload.sinchNotification != null)
                sinchDetail = notificationPayload.sinchNotification;
            #endregion

            #region MANAGE DEVICE TOKENS
            if (sinchDetail.deviceType == (int)DeviceTypeEnum.ANDROID && 
                sinchDetail.deviceTokens != null && 
                sinchDetail.deviceTokens.Count > 0)
            {
               androidTokens = sinchDetail.deviceTokens;
            }
            if (sinchDetail.deviceType == (int)DeviceTypeEnum.IOS && 
                sinchDetail.deviceTokens != null && 
                sinchDetail.deviceTokens.Count > 0)
            {
               iosTokens = sinchDetail.deviceTokens;
            }
            if (androidTokens.Count == 0 && iosTokens.Count == 0)
            {
                return NotificationStatusEnum.NODEVICEFOUND;
            }
            if (androidTokens.Count > 1)
            {
                androidTokens = androidTokens.Distinct().ToList();
            }
            if (iosTokens.Count > 1)
            {
                iosTokens = iosTokens.Distinct().ToList();
            }
            #endregion
            try
            {
                isSuccess = await Task.Run(() => 
                                pushNotification.SendPushNotification(
                                    1,
                                    sinchDetail.payload,
                                    androidTokens,
                                    iosTokens,
                                    sinchDetail.userGroup
                                )
                            );
            }
            catch (Exception)
            {
                throw;
            }
            if (isSuccess)
            {
                return NotificationStatusEnum.SUCCESS;
            }
            else
            {
                return NotificationStatusEnum.FAILED;
            } 
        }
        /// <summary>
        /// Sends the notification containing title, body and payload.
        /// </summary>
        /// <param name="recipient">The recipient of the notification.</param>
        /// <param name="notificationPayload">The payload being carried.</param>
        /// <param name="notificationBody">Notification body.</param>
        /// <param name="notificationType">Notification type (enum).</param>
        /// <param name="isSilentNotification">Silent notification flag.</param>
        /// <returns></returns>
        private NotificationStatusEnum SendPushNotifications(
            User recipient, 
            RequestNotification notificationPayload,
            string notificationBody,
            string notificationTitle,
            NotificationTypeEnum notificationType,
            bool isSilentNotification)
        {
            bool isSuccess = false;
            if (recipient == null)
            {
                return NotificationStatusEnum.USERNOTFOUND;
            }
            //List of device tokens [Android|IOS]
            var iosTokens = new List<string>();
            var androidTokens = new List<string>();

            #region PushNotification Constructor Call
            var pushNotification = new PushNotifications(
                PushNotificationKeys.CustomerSenderId,
                PushNotificationKeys.CustomerAuthToken,
                PushNotificationKeys.SupervisorSenderId,
                PushNotificationKeys.SupervisorAuthToken,
                PushNotificationKeys.StaffSenderId,
                PushNotificationKeys.StaffAuthToken,
                PushNotificationKeys.CustomerAppleCert,
                PushNotificationKeys.CustomerApplePass,
                PushNotificationKeys.SupervisorAppleCert,
                PushNotificationKeys.SupervisorApplePass,
                PushNotificationKeys.StaffAppleCert,
                PushNotificationKeys.StaffApplePass,
                PushNotificationKeys.CustomerAppleCertVoip,
                PushNotificationKeys.CustomerApplePassVoip,
                PushNotificationKeys.SupervisorAppleCertVoip,
                PushNotificationKeys.SupervisorApplePassVoip,
                PushNotificationKeys.StaffAppleCertVoip,
                PushNotificationKeys.StaffApplePassVoip
            );
            #endregion

            #region MANAGE NOTIFICATION PAYLOAD
            //Get message payload from JSON posted
            RequestMessage requestMessage = null;
            if (notificationPayload.message != null)
                requestMessage = notificationPayload.message;

            //Get jobdetail payload from JSON posted
            RequestJobDetail requestJobDetail = null;
            if (notificationPayload.jobDetail != null)
                requestJobDetail = notificationPayload.jobDetail;

            //Get staffposition payload from JSON posted
            RequestStaffPosition requestStaffPosition = null;
            if (notificationPayload.staffPosition != null)
                requestStaffPosition = notificationPayload.staffPosition;

            //Get CallDetails payload from JSON posted
            var requestCallDetail = new RequestCallDetail();
            if (notificationPayload.callDetail != null)
                requestCallDetail = notificationPayload.callDetail;
            #endregion

            #region MANAGE DEVICE TOKENS
            if (recipient.GroupId == (int)UserGroupEnum.STAFF)
            {
                //GeneralUser - If single device token
                if (recipient.UserDevice == null)
                {
                    return NotificationStatusEnum.NODEVICEFOUND;
                }
                if (!string.IsNullOrEmpty(recipient.UserDevice.DeviceToken) &&
                    recipient.UserDevice.DeviceType ==
                        (int)DeviceTypeEnum.ANDROID)
                {
                    androidTokens.Add(recipient.UserDevice.DeviceToken);
                }
                else if (!isSilentNotification && 
                    !string.IsNullOrEmpty(recipient.UserDevice.DeviceToken))
                {
                    iosTokens.Add(recipient.UserDevice.DeviceToken);
                }
                else if(isSilentNotification && 
                    !string.IsNullOrEmpty(recipient.UserDevice.VoipToken))
                {
                    iosTokens.Add(recipient.UserDevice.VoipToken);
                }
               
            }
            else
            {
                //GeneralUser - If multiple device token
                if (recipient.UserDevices == null || 
                    recipient.UserDevices.Count == 0)
                {
                    return NotificationStatusEnum.NODEVICEFOUND;
                }
                foreach (var userDevice in recipient.UserDevices)
                {
                    if (!string.IsNullOrEmpty(userDevice.DeviceToken) &&
                        userDevice.DeviceType == 
                            (int)DeviceTypeEnum.ANDROID)
                    {
                        androidTokens.Add(userDevice.DeviceToken);
                    }
                    else if (!isSilentNotification &&
                        !string.IsNullOrEmpty(userDevice.DeviceToken))
                    {
                        iosTokens.Add(userDevice.DeviceToken);
                    }
                    else if (isSilentNotification &&
                        !string.IsNullOrEmpty(userDevice.VoipToken))
                    {
                        iosTokens.Add(userDevice.VoipToken);
                    }
                }
            }
            if(androidTokens.Count == 0 && iosTokens.Count == 0)
            {
                return NotificationStatusEnum.NODEVICEFOUND;
            }
            if(androidTokens.Count > 1)
            {
                androidTokens = androidTokens.Distinct().ToList();
            }
            if (iosTokens.Count > 1)
            {
                iosTokens = iosTokens.Distinct().ToList();
            }
            #endregion
            try
            {
                isSuccess = pushNotification.SendPushNotification(
                                1,
                                notificationBody,
                                notificationTitle,
                                notificationType,
                                androidTokens,
                                iosTokens,
                                requestMessage,
                                requestJobDetail,
                                requestStaffPosition,
                                requestCallDetail,
                                (UserGroupEnum)recipient.GroupId,
                                string.Empty,
                                string.Empty
                            );
            }
            catch (Exception)
            {
                throw; 
            }
            if (isSuccess)
            {
                return NotificationStatusEnum.SUCCESS;
            }
            else
            {
                return NotificationStatusEnum.FAILED;
            } 
        }
    }
}