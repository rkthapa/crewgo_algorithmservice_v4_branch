﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CREWGO.Service.WebAPI.V1.Helpers
{
    /// <summary>
    /// Gets push notification keys from AppSettings
    /// </summary>
    public static class PushNotificationKeys
    {
        public static string CustomerSenderId =
            ConfigurationManager.AppSettings["PRODUCTION_SENDER_ID_CUSTOMER"];
        public static string CustomerAuthToken =
            ConfigurationManager.AppSettings["PRODUCTION_AUTHTOKEN_CUSTOMER"];
        public static string SupervisorSenderId =
            ConfigurationManager.AppSettings["PRODUCTION_SENDER_ID_SUPERVISOR"];
        public static string SupervisorAuthToken =
            ConfigurationManager.AppSettings["PRODUCTION_AUTHTOKEN_SUPERVISOR"];
        public static string StaffSenderId =
            ConfigurationManager.AppSettings["PRODUCTION_SENDER_ID_STAFF"];
        public static string StaffAuthToken =
            ConfigurationManager.AppSettings["PRODUCTION_AUTHTOKEN_STAFF"];
        public static string CustomerAppleCert = 
            ConfigurationManager.AppSettings["PRODUCTION_CERT_CUSTOMER"];
        public static string CustomerApplePass = 
            ConfigurationManager.AppSettings["PRODUCTION_PASS_CUSTOMER"];
        public static string SupervisorAppleCert = 
            ConfigurationManager.AppSettings["PRODUCTION_CERT_SUPERVISOR"];
        public static string SupervisorApplePass = 
            ConfigurationManager.AppSettings["PRODUCTION_PASS_SUPERVISOR"];
        public static string StaffAppleCert = 
            ConfigurationManager.AppSettings["PRODUCTION_CERT_STAFF"];
        public static string StaffApplePass = 
            ConfigurationManager.AppSettings["PRODUCTION_PASS_STAFF"];

        public static string CustomerAppleCertVoip =
            ConfigurationManager.AppSettings["PRODUCTION_CERT_CUSTOMER_VOIP"];
        public static string CustomerApplePassVoip =
            ConfigurationManager.AppSettings["PRODUCTION_PASS_CUSTOMER_VOIP"];
        public static string SupervisorAppleCertVoip =
            ConfigurationManager.AppSettings["PRODUCTION_CERT_SUPERVISOR_VOIP"];
        public static string SupervisorApplePassVoip =
            ConfigurationManager.AppSettings["PRODUCTION_PASS_SUPERVISOR_VOIP"];
        public static string StaffAppleCertVoip =
            ConfigurationManager.AppSettings["PRODUCTION_CERT_STAFF_VOIP"];
        public static string StaffApplePassVoip =
            ConfigurationManager.AppSettings["PRODUCTION_PASS_STAFF_VOIP"];
    }
}