﻿using CREWGO.Service.BusinessService.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using CREWGO.Service.BusinessService.Facade;
using CREWGO.Service.BusinessService;
using CREWGO.Service.WebAPI.V1.Filters;

namespace CREWGO.Service.WebAPI.V1.Helpers
{
    /// <summary>
    /// Handles cryptography related tasks for the API
    /// </summary>
    public class CryptographyHelper
    {
        private const int keyLength = 64;
        private const int idLength = 32;
        private Random random = new Random();
        /// <summary>
        /// Computes SHA512 hash for the imput byte stream.
        /// </summary>
        /// <param name="byteStream">Array of bytes</param>
        /// <returns>Hash string for input byte stream</returns>
        public static string ComputeHash(byte[] byteStream)
        {
            using (SHA512 sha = SHA512.Create())
            {
                byte[] hash = null;
                if (byteStream.Length != 0)
                {
                    hash = sha.ComputeHash(byteStream);
                }
                return BitConverter.ToString(hash).Replace("-", "");
            }
        }
        /// <summary>
        /// Generates and stores credentials for API Clients
        /// </summary>
        /// <returns>AuthElements object (AppID, ApiKey)</returns>
        public async Task<AuthElements> GetCredentials()
        {
            var credentials = await GenerateCredentials();
            await StoreCredentials(credentials.Item1.AppID, credentials.Item2);
            AdminAuthFilter.RefreshAuthData(
                credentials.Item1.AppID, 
                credentials.Item2, 
                credentials.Item1.IsAdmin
            );
            return credentials.Item1;
        }
        /// <summary>
        /// Generates AppID and ApiKey
        /// </summary>
        /// <returns>tuple of AuthElements object and hashed string</returns>
        private async Task<Tuple<AuthElements, string>> GenerateCredentials()
        {
            var id = Task.Run(() => GenerateRandomKey(idLength));
            var key = Task.Run(() => GenerateRandomKey(keyLength));
            var data = String.Format("{0}{1}", await id, await key);
            var dataBytes = Encoding.UTF8.GetBytes(data);
            var token = await Task.Run(() => ComputeHash(dataBytes));
            AuthElements authElements = new AuthElements();
            authElements.AppID = await id;
            authElements.ApiKey = await key;
            authElements.IsAdmin = false;
            
            return new Tuple<AuthElements, string>(authElements, token);
        }
        /// <summary>
        /// Stores generated authentication credentials to database
        /// </summary>
        /// <param name="appId">The generated AppID</param>
        /// <param name="token">The generated hash of ApiKey</param>
        /// <returns></returns>
        private async Task StoreCredentials(string appId, string token)
        {
            await Task.Run(() => new CREWGOBaseServiceFacade()
                .SaveAuthData(
                    new NotificationAuth()
                    {
                        Id = 0,
                        AppId = appId, 
                        KeyHash = token,
                        IsAdmin = false
                    }
                )
            );
        }
        /// <summary>
        /// Generated random string of fixed length.
        /// </summary>
        /// <param name="length">The length of key to generate.</param>
        /// <returns>Random string of fixed length.</returns>
        private string GenerateRandomKey(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(
                Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray()
            );
        }
        /// <summary>
        /// Authenticates user as admin. Uses BCrypt.
        /// This is subject to change because the encryption should match the 
        /// encryption used in the the CrewGo admin dashboard aulthentication.
        /// </summary>
        /// <param name="email">User input email</param>
        /// <param name="password">User input password</param>
        /// <returns>True if credentials are valid, false otherwise</returns>
        public async Task<bool> AuthenticateDashboardUse(
            string email, string password)
        {
            var user = new User();
            try
            {
                user = await new CREWGOBaseServiceFacade()
                    .GetAdminUserByEmail(email);
            }
            catch(Exception)
            {
                return false;
            }
            if (email == user.Email)
            {
                // Crewgo Admin Portal uses BCrypt encryption to store passwords
                // So, hash the user input password using BCrypt and validate
                return BCrypt.Net.BCrypt.Verify(password, user.Password);
            }
            else
            {
                return false;
            }
        }
    }
}