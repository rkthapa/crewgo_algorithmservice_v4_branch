﻿using CREWGO.Service.BusinessService.Facade;
using CREWGO.Service.WebAPI.V1.Helpers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace CREWGO.Service.WebAPI.V1.Filters
{
    /// <summary>
    /// Attribute filter to authenticate API use by admin.
    /// Use: [AdminAuth] over Controllers or Individual Actions.
    /// The authentication header for each request must contain 
    /// type/scheme: "apiKey"
    /// content/parameter: "app_id:apikey"
    /// </summary>
    public class AdminAuthFilter : APIAuthFilter
    {
        private static Dictionary<string, bool> clientGroups =
            new Dictionary<string, bool>();
        
        /// <summary>
        /// Load allowed app list if not already loaded
        /// </summary>
        public AdminAuthFilter()
        {
            if (allowedApps.Count == 0)
            {
                var authList = new CREWGOBaseServiceFacade().GetAuthData();
                foreach (var auth in authList)
                {
                    allowedApps.Add( auth.AppId, auth.KeyHash);
                }
            }
            if (clientGroups.Count == 0)
            {
                var authList = new CREWGOBaseServiceFacade().GetAuthData();
                foreach (var auth in authList)
                {
                    clientGroups.Add(auth.AppId, auth.IsAdmin);
                }
            }
        }
        /// <summary>
        /// Refreshes allowedApps and clientGroups after generation.
        /// </summary>
        /// <param name="id">App ID</param>
        /// <param name="key">API Key</param>
        /// <param name="isAdmin">Admin flag</param>
        public static void RefreshAuthData(string id, string key, bool isAdmin)
        {
            allowedApps.Add(id, key);
            clientGroups.Add(id, isAdmin);
        }
        /// <summary>
        /// Check request validity.
        /// </summary>
        /// <param name="appID">The application id in the header.</param>
        /// <param name="apiKey">The api key in the header.</param>
        /// <returns></returns>
        public override bool isValidRequest(string appID, string apiKey)
        {
            if (!allowedApps.ContainsKey(appID))
            {
                return false;
            }
            var sharedKey = allowedApps[appID];
            string data = String.Format("{0}{1}", appID, apiKey);
            byte[] inputBytes = Encoding.UTF8.GetBytes(data);
            bool valid = String.Compare(
                sharedKey, 
                CryptographyHelper.ComputeHash(inputBytes), 
                false
            ) == 0 ? true : false;
            bool isAdmin = clientGroups[appID];
            return (valid && isAdmin);
        }
    }
}