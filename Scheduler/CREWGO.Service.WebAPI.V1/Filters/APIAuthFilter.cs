﻿using CREWGO.Service.BusinessService.Facade;
using CREWGO.Service.WebAPI.V1.Helpers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace CREWGO.Service.WebAPI.V1.Filters
{
    /// <summary>
    /// Attribute filter to authenticate API use by general user.
    /// Use: [APIAuthFilter] over Controllers or Individual Actions.
    /// The authentication header for each request must contain 
    /// type/scheme: "apiKey"
    /// content/parameter: "app_id:apikey"
    /// </summary>
    public class APIAuthFilter : Attribute, IAuthenticationFilter
    {
        protected static Dictionary<string, string> allowedApps = 
            new Dictionary<string, string>();
        protected readonly string authenticationScheme = "apiKey";
        
        /// <summary>
        /// Load allowed app list if not already loaded
        /// </summary>
        public APIAuthFilter()
        {
            if (allowedApps.Count == 0)
            {
                var authList = new CREWGOBaseServiceFacade().GetAuthData();
                foreach (var auth in authList)
                {
                    allowedApps.Add( auth.AppId, auth.KeyHash);
                }
            }
        }
        /// <summary>
        /// Authenticate API Client
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task AuthenticateAsync(
            HttpAuthenticationContext context, 
            CancellationToken cancellationToken)
        {
            var req = context.Request;

            if (req.Headers.Authorization != null && 
                authenticationScheme.Equals(
                    req.Headers.Authorization.Scheme, 
                    StringComparison.OrdinalIgnoreCase))
            {
                var rawAuthHeader = req.Headers.Authorization.Parameter;

                var authorizationHeaderArray = GetAutherizationHeaderValues(
                                                    rawAuthHeader);

                if (authorizationHeaderArray != null)
                {
                    var appID = authorizationHeaderArray[0];
                    var apiKey = authorizationHeaderArray[1];

                    var isValid = isValidRequest(appID, apiKey);

                    if (isValid)
                    {
                        var currentPrincipal = 
                            new GenericPrincipal(
                                new GenericIdentity(appID), 
                                null);
                        context.Principal = currentPrincipal;
                    }
                    else
                    {
                        context.ErrorResult = 
                            new UnauthorizedResult(
                                new AuthenticationHeaderValue[0],
                                context.Request);
                    }
                }
                else
                {
                    context.ErrorResult = 
                        new UnauthorizedResult(
                            new AuthenticationHeaderValue[0], 
                            context.Request);
                }
            }
            else
            {
                context.ErrorResult = 
                    new UnauthorizedResult(
                        new AuthenticationHeaderValue[0], 
                        context.Request);
            }

            return Task.FromResult(0);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task ChallengeAsync(
            HttpAuthenticationChallengeContext context, 
            CancellationToken cancellationToken)
        {
            context.Result = new ResultWithChallenge(context.Result);
            return Task.FromResult(0);
        }
        /// <summary>
        /// Multiple access disabled
        /// </summary>
        public bool AllowMultiple
        {
            get { return false; }
        }
        /// <summary>
        /// Split auth parameter into AppID and ApiKey
        /// </summary>
        /// <param name="rawAuthHeader">Raw authentication header param</param>
        /// <returns></returns>
        protected string[] GetAutherizationHeaderValues(string rawAuthHeader)
        {

            var credArray = rawAuthHeader.Split(':');

            if (credArray.Length == 2)
            {
                return credArray;
            }
            else
            {
                return null;
            }

        }
        /// <summary>
        /// Check request validity.
        /// </summary>
        /// <param name="appID">The application id in the header.</param>
        /// <param name="apiKey">The api key in the header.</param>
        /// <returns></returns>
        public virtual bool isValidRequest(string appID, string apiKey)
        {
            if (!allowedApps.ContainsKey(appID))
            {
                return false;
            }
            var sharedKey = allowedApps[appID];
            string data = String.Format("{0}{1}", appID, apiKey);
            byte[] inputBytes = Encoding.UTF8.GetBytes(data);
            return String.Compare(
                sharedKey, 
                CryptographyHelper.ComputeHash(inputBytes), 
                false
            ) == 0 ? true : false;
        }
    }

    /// <summary>
    /// Custom action result class for authentication actions.
    /// </summary>
    public class ResultWithChallenge : IHttpActionResult
    {
        private readonly string authenticationScheme = "amx";
        private readonly IHttpActionResult next;

        public ResultWithChallenge(IHttpActionResult next)
        {
            this.next = next;
        }

        public async Task<HttpResponseMessage> ExecuteAsync(
            CancellationToken cancellationToken)
        {
            var response = await next.ExecuteAsync(cancellationToken);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                response.Headers.WwwAuthenticate.Add(
                    new AuthenticationHeaderValue(authenticationScheme));
            }

            return response;
        }
    }
}