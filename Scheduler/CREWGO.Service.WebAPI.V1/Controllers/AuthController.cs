﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.WebAPI.V1.Filters;
using CREWGO.Service.WebAPI.V1.Helpers;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CREWGO.Service.WebAPI.V1.Controllers
{
    [RoutePrefix("api/Auth")]
    public class AuthController : ApiController
    {
        /// <summary>
        /// Generates and gets api client credentials
        /// </summary>
        /// <returns>AppID(string), ApiKey(string)</returns>
        [HttpGet]
        [Route("GetCredentials")]
        [SwaggerOperation(Tags = new[] { "Authentication" })]
        [AdminAuthFilter]
        public async Task<AuthElements> GetCredentials()
        {
            return await new CryptographyHelper().GetCredentials();
        }
        /// <summary>
        /// Authenticate user for hangfire dashboard use (admin only)
        /// </summary>
        /// <param name="emailPassword">Tuple of email and password</param>
        /// <returns>True if credentials are valid, false otherwise</returns>
        [HttpPost]
        [SwaggerOperation(Tags = new[] { "Authentication" })]
        [Route("AuthenticateDashboardUse")]
        [APIAuthFilter]
        public async Task<bool> AuthenticateDashboardUse(
            [FromBody] Tuple<string, string> emailPassword)
        {
            if (emailPassword.Item1 == null || emailPassword.Item1 == "")
            {
                return false;
            }
            if (emailPassword.Item2 == null || emailPassword.Item2 == "")
            {
                return false;
            } 
            return await new CryptographyHelper().AuthenticateDashboardUse(
                                                    emailPassword.Item1, 
                                                    emailPassword.Item2);
        }
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "Authentication" })]
        [Route("CheckAPIKeyValidity")]
        [AllowAnonymous]
        public IHttpActionResult CheckAPIKeyValidity(string key)
        {
            string authenticationScheme = "apiKey";
            var keyIdArray = key.Split(':');
            var idArray = keyIdArray[0].Split(' ');

            var auth = new APIAuthFilter();
            if (authenticationScheme.Equals(
                    idArray[0], StringComparison.OrdinalIgnoreCase))
            {
                if (auth.isValidRequest(idArray[1], keyIdArray[1]))
                {
                    return Ok("Valid Api Key");
                }
            }
            return Ok("Invalid Api Key");
        }

    }
}
