﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CREWGO.Service.BusinessService;
using CREWGO.Service.WebAPI.V1.Filters;
using System.Threading.Tasks;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Facade;
using Swashbuckle.Swagger.Annotations;

namespace CREWGO.Service.WebAPI.V1.Controllers
{
    [APIAuthFilter]
    public class JobAlertSettingGlobalController : ApiController
    {
        /// <summary>
        /// Get a JobAlertSettingGlobal object by ID.
        /// </summary>
        /// <param name="id">JobAlertSettingGlobal ID.</param>
        /// <returns>JobAlertSettingGlobal object.</returns>
        [SwaggerOperation(Tags = new[] { "JobAlertSettingGlobal" })]
        [Route("api/JobAlertSettingGlobal/getbyid/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int id)
        {
            JobAlertSettingGlobal jobAlertSetting = new JobAlertSettingGlobal();
            try
            {
                jobAlertSetting =
                    await new CREWGOBaseServiceFacade()
                    .GetJobAlertSettingGlobalById(id);
                return Ok(jobAlertSetting);
            }
            catch (Exception)
            {
                return BadRequest("Not Found or Inaccessible");
            }
        }
        /// <summary>
        /// Puts a JobAlertSettingGlobal object.
        /// </summary>
        /// <param name="jobAlertSettingGlobal">JobAlertSettingGlobal object</param>
        /// <returns>Status code.</returns>
        [SwaggerOperation(Tags = new[] { "JobAlertSettingGlobal" })]    
        [Route("api/JobAlertSettingGlobal/PutJobAlertSettingGlobal")]
        [HttpPut]
        public async Task<IHttpActionResult> PutJobAlertSettingGlobal(
            JobAlertSettingGlobal jobAlertSettingGlobal)
        {
            try
            {
                int count = await new CREWGOBaseServiceFacade()
                    .UpdateJobAlertSettingGlobal(jobAlertSettingGlobal);
                if (count > 0)
                {
                    return Ok("Successfully updated");
                }
                return BadRequest("Error while updating");
            }
            catch (Exception)
            {
                return BadRequest("Error while updating");
            }
        }
    }
}