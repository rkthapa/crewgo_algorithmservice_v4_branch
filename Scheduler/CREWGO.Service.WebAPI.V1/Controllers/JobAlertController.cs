﻿using System.Web.Http;
using CREWGO.Service.WebAPI.V1.Filters;
using CREWGO.Service.BusinessService.Common;
using Swashbuckle.Swagger.Annotations;
using CREWGO.Service.BusinessService.Facade;
using System.Threading.Tasks;
using System;

namespace CREWGO.Service.WebAPI.V1.Controllers
{
    [APIAuthFilter]
    public class JobAlertController : ApiController
    {
        /// <summary>
        /// Post a JobAlert.
        /// </summary>
        /// <param name="jobAlert">Job Alert Object</param>
        /// <returns>Status code.</returns>
        [SwaggerOperation(Tags = new[] { "JobAlert" })]
        [Route("api/JobAlert/PostJobAlert")]
        [HttpPost]
        public async Task<IHttpActionResult> PostJobAlert(JobAlert jobAlert)
        {
            if (jobAlert.AlertDate == null)
            {
                jobAlert.AlertDate = DateTime.Now;
            }
            try
            {
                int count = 0;
                count = await new CREWGOBaseServiceFacade().PostJobAlert(jobAlert);
                if (count > 0)
                {
                    return Ok("Successfully added.");
                }
                return BadRequest("Error while adding.");
            }
            catch(Exception)
            {
                return InternalServerError();
            }
        }

    }
}