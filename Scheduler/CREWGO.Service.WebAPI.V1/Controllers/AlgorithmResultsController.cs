﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Facade;
using CREWGO.Service.Common;
using CREWGO.Service.WebAPI.V1.Filters;
using CREWGO.Service.WebAPI.V1.Helpers;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;
using System.Web.Http;

namespace CREWGO.Service.WebAPI.V1.Controllers
{
    /// <summary>
    /// Checks eligibility of staff for shift for all algorithms.
    /// Returns eligibility or reasons why the staff is not getting notification 
    /// in case of ineligibility.
    /// </summary>
    [RoutePrefix("api/AlgorithmResults")]
    public class AlgorithmResultsController : ApiController
    {
        /// <summary>
        /// Checks if am User should get job offers for CrewServer-General or not.
        /// </summary>
        /// <returns>JSON object containing why the User will not get offer</returns>
        [HttpGet]
        [Route("GetUserResultsForShiftGeneral/{shiftId}/{userId}")]
        [SwaggerOperation(Tags = new[] { "Algorithm Results" })]
        [APIAuthFilter]
        public IHttpActionResult GetUserResultsForShiftGeneral(
            long shiftId, long userId)
        {
            var result = new CrewServerResults();
            try
            {
                result = new CREWGOBaseServiceFacade()
                            .GetUserResultsForShift(shiftId, userId);
            }
            catch(Exception)
            {
                return InternalServerError();
            }
            return ConstructResult(result, AlgorithmType.GENREAL);
        }
        /// <summary>
        /// Checks if am User should get job offers for CrewServer-Global or not.
        /// </summary>
        /// <returns>JSON object containing why the User will not get offer</returns>
        [HttpGet]
        [Route("GetUserResultsForShiftGlobal/{shiftId}/{userId}")]
        [SwaggerOperation(Tags = new[] { "Algorithm Results" })]
        [APIAuthFilter]
        public IHttpActionResult GetUserResultsForShiftGlobal(
            long shiftId, long userId)
        {
            var result = new CrewServerResults();
            try
            {
                result = new CREWGOBaseServiceFacade()
                            .GetUserResultsForShift(shiftId, userId);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
            return ConstructResult(result, AlgorithmType.GLOBAL);
        }
        /// <summary>
        /// Checks if am User should get job offers for Crewgo or not.
        /// </summary>
        /// <returns>JSON object containing why the User will not get offer</returns>
        [HttpGet]
        [Route("GetUserResultsForShiftCrewgo/{shiftId}/{userId}")]
        [SwaggerOperation(Tags = new[] { "Algorithm Results" })]
        [APIAuthFilter]
        public IHttpActionResult GetUserResultsForShiftCrewgo(
            long shiftId, long userId)
        {
            var result = new CrewgoResults();
            try
            {
                result = new CREWGOBaseServiceFacade()
                            .GetUserResultsForCrewgoShift(shiftId, userId);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
            return ConstructResult(result);
        }
        /// <summary>
        /// Construct JSON response for CrewServerResults.
        /// </summary>
        /// <param name="result">CrewServerResults object.</param>
        /// <returns>Status code with JSON object.</returns>
        private IHttpActionResult ConstructResult(
            CrewServerResults result, AlgorithmType algorithmType)
        {
            var response = new List<KeyValuePair<string, bool>>();
            if (!result.IsUserStaff)
            {
                response.Add(new KeyValuePair<string, bool>("IsUserStaff", false));
            }
            if (!result.IsActive)
            {
                response.Add(new KeyValuePair<string, bool>("IsActive", false));
            }
            if (!result.IsEmailVerified)
            {
                response.Add(new KeyValuePair<string, bool>("IsEmailVerified", false));
            }
            if (!result.IsAvailable)
            {
                response.Add(new KeyValuePair<string, bool>("IsAvailable", false));
            }
            if (!result.HasRequiredSkill)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredSkill", false));
            }
            if (!result.HasRequiredSubSkill)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredSubSkill", false));
            }
            if (!result.HasRequiredQualification)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredQualification", false));
            }
            if (result.IsAlreadyNotified)
            {
                response.Add(new KeyValuePair<string, bool>("IsAlreadyNotified", true));
            }
            if (!result.IsLoggedIn)
            {
                response.Add(new KeyValuePair<string, bool>("IsLoggedIn", false));
            }
            if (!result.IsAssociatedToSamePartner)
            {
                response.Add(new KeyValuePair<string, bool>("IsAssociatedToSamePartner", false));
            }
            if (!result.IsActiveForPartner)
            {
                response.Add(new KeyValuePair<string, bool>("IsActiveForPartner", false));
            }
            //if (!result.IsActiveForPartner)
            //{
            //    response.Add(new KeyValuePair<string, bool>("IsActiveForPartner", false));
            //}
            if (algorithmType == AlgorithmType.GENREAL)
            {
                if (result.IsStaffManual)
                {
                    response.Add(new KeyValuePair<string, bool>("IsStaffManual", true));
                }
                if (result.IsStaffGlobal)
                {
                    response.Add(new KeyValuePair<string, bool>("IsStaffGlobal", true));
                }
            }
            else if (algorithmType == AlgorithmType.GLOBAL)
            {
                if (result.IsStaffManual)
                {
                    response.Add(new KeyValuePair<string, bool>("IsStaffManual", true));
                }
                if (!result.IsStaffGlobal)
                {
                    response.Add(new KeyValuePair<string, bool>("IsStaffGlobal", false));
                }
            }
            else if (algorithmType == AlgorithmType.MANUAL)
            {
                if (!result.IsStaffManual)
                {
                    response.Add(new KeyValuePair<string, bool>("IsStaffManual", false));
                }
                if (result.IsStaffGlobal)
                {
                    response.Add(new KeyValuePair<string, bool>("IsStaffGlobal", true));
                }
            }
            if (response.Count > 0)
            {
                return Ok(response);
            }
            else
            {
                response.Add(new KeyValuePair<string, bool>(
                    "EligibleStaff",
                    true
                ));
                return Ok(response);
            }
        }
        /// <summary>
        /// Construct JSON response for CrewgoResults.
        /// </summary>
        /// <param name="result">CrewgoResults object.</param>
        /// <returns>Status code with JSON object.</returns>
        private IHttpActionResult ConstructResult(CrewgoResults result)
        {
            var response = new List<KeyValuePair<string, bool>>();
            if (!result.IsUserStaff)
            {
                response.Add(new KeyValuePair<string, bool>("IsUserStaff", false));
            }
            if (!result.IsActive)
            {
                response.Add(new KeyValuePair<string, bool>("IsActive", false));
            }
            if (!result.IsEmailVerified)
            {
                response.Add(new KeyValuePair<string, bool>("IsEmailVerified", false));
            }
            if (!result.IsAvailable && !result.IsOnCall)
            {
                response.Add(new KeyValuePair<string, bool>("IsAvailable", false));
                response.Add(new KeyValuePair<string, bool>("IsOnCall", false));
            }
            if (!result.HasRequiredSkill)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredSkill", false));
            }
            if (!result.HasRequiredSubSkill)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredSubSkill", false));
            }
            if (!result.HasRequiredQualification)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredQualification", false));
            }
            if (result.IsAlreadyNotified)
            {
                response.Add(new KeyValuePair<string, bool>("IsAlreadyNotified", true));
            }
            if (!result.IsLoggedIn)
            {
                response.Add(new KeyValuePair<string, bool>("IsLoggedIn", false));
            }
            if (!result.IsInCrewgo && !result.IsAssociatedPartnerCrewgo)
            {
                response.Add(new KeyValuePair<string, bool>("IsInCrewgo", false));
                response.Add(new KeyValuePair<string, bool>("IsAssociatedPartnerCrewgo", false));
            }
            if (result.IsAlreadyInPSList)
            {
                response.Add(new KeyValuePair<string, bool>("IsAlreadyInPSList", true));
            }
            if (!result.IsPreferedLocation)
            {
                response.Add(new KeyValuePair<string, bool>("IsPreferedLocation", false));
            }
            if (!result.HasRequiredRating)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredRating", false));
            }
            if (!result.HasRequiredAcceptance)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredAcceptance", false));
            }
            if (!result.HasRequiredExperience)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredExperience", false));
            }
            if (!result.HasRequiredHistory)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredHistory", false));
            }
            if (!result.HasRequiredLongevity)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredLongevity", false));
            }
            if (!result.HasRequiredProximity)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredProximity", false));
            }
            if (!result.HasRequiredAccuracy)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredAccuracy", false));
            }
            if (!result.HasRequiredAppearance)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredAppearance", false));
            }
            if (!result.HasRequiredPunctuality)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredPunctuality", false));
            }
            if (!result.HasRequiredRapidity)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredRapidity", false));
            }
            if (!result.HasRequiredTransport)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredTransport", false));
            }
            if (!result.HasRequiredUrgency)
            {
                response.Add(new KeyValuePair<string, bool>("HasRequiredUrgency", false));
            }

            if (response.Count > 0)
            {
                return Ok(response);
            }
            else
            {
                response.Add(new KeyValuePair<string, bool>(
                    "EligibleStaff",
                    true
                ));
                return Ok(response);
            }
        }
    }
}
