﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CREWGO.Service.BusinessService;
using CREWGO.Service.WebAPI.V1.Filters;
using System.Threading.Tasks;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Facade;
using Swashbuckle.Swagger.Annotations;

namespace CREWGO.Service.WebAPI.V1.Controllers
{
    [APIAuthFilter]
    public class JobAlertSettingController : ApiController
    {
        /// <summary>
        /// Get JobAlertSetting object by ID.
        /// </summary>
        /// <param name="id">JobAlertSetting ID.</param>
        /// <returns>JobAlertSetting object.</returns>
        [SwaggerOperation(Tags = new[] { "JobAlertSetting" })]
        [Route("api/JobAlertSetting/getbyid/{id}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(int id)
        {
            JobAlertSetting jobAlertSetting = new JobAlertSetting();
            try
            {
                jobAlertSetting = 
                    await new CREWGOBaseServiceFacade().GetJobAlertSettingById(id);
                return Ok(jobAlertSetting);
            }
            catch(Exception)
            {
                return BadRequest("Not Found or Inaccessible");
            }
        }
        /// <summary>
        /// Puts a JobAlertSertting object.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Status code.</returns>
        [SwaggerOperation(Tags = new[] { "JobAlertSetting" })]
        [Route("api/JobAlertSetting/PutJobAlertSetting")]
        [HttpPut]
        public async Task<IHttpActionResult> PutJobAlertSetting(
            JobAlertSetting jobAlertSetting)
        {
            try
            {
                int count = await new CREWGOBaseServiceFacade().UpdateJobAlertSetting(jobAlertSetting);
                if (count > 0)
                {
                    return Ok("Successfully updated");
                }
                return BadRequest("Error while updating");
            }
            catch (Exception)
            {
                return BadRequest("Error while updating");
            }
        }
        /// <summary>
        /// Posts a JobAlertSertting object.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Status code.</returns>
        [SwaggerOperation(Tags = new[] { "JobAlertSetting" })]
        [Route("api/JobAlertSetting/PostJobAlertSetting")]
        [HttpPost]
        public async Task<IHttpActionResult> PostJobAlertSetting(
            JobAlertSetting jobAlertSetting)
        {
            if(jobAlertSetting == null || jobAlertSetting.Status == null)
            {
                return BadRequest("Invalid data.");
            }
            try
            {
                int count = await new CREWGOBaseServiceFacade().InsertJobAlertSetting(jobAlertSetting);
                if (count > 0)
                {
                    return Ok("Successfully inserted");
                }
                return BadRequest("Error while inserting");
            }
            catch (Exception)
            {
                return BadRequest("Error while inserting");
            }
        }

        
    }
}