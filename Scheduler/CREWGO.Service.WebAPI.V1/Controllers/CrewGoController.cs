﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Facade;
using CREWGO.Service.Common;
using CREWGO.Service.Common.RequestObject;
using CREWGO.Service.WebAPI.V1.Filters;
using CREWGO.Service.WebAPI.V1.Helpers;
using System.Data;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;

namespace CREWGO.Service.WebAPI.V1.Controllers
{
    [APIAuthFilter]
    public class CrewGoController : ApiController
    {
        #region VARIABLES
        #endregion
        #region ACTIVITY LEVEL
        [SwaggerOperation(Tags = new[] { "ActivityLevel" })]
        [Route("api/CrewGo/GetActivityLevelByValue/{potentialStaff}")]
        [HttpGet]
        public async Task<ActivityLevel> GetActivityLevelByValue(int potentialStaff)
        {
            return await new CREWGOBaseServiceFacade()
                .GetActivityLevelByValue(potentialStaff);
        }
        [SwaggerOperation(Tags = new[] { "ActivityLevel" })]
        [Route("api/CrewGo/GetAllActivityLevels")]
        [HttpGet]
        public async Task<List<ActivityLevel>> GetAllActivityLevels()
        {
            return await new CREWGOBaseServiceFacade().GetAllActivityLevels();
        }
        [SwaggerOperation(Tags = new[] { "ActivityLevel" })]
        [Route("api/CrewGo/GetShortNoticePeriod")]
        [HttpGet]
        public async Task<int> GetShortNoticePeriod()
        {
            return await new CREWGOBaseServiceFacade().GetShortNoticePeriod();
        }
        #endregion
        #region JOB

        /// <summary>
        /// GET api/CrewGo/GetAllJobs
        /// </summary>
        /// <returns>List<Job></returns>
        [SwaggerOperation(Tags = new[] { "Job" })]
        [Route("api/CrewGo/GetAllJobs")]
        [HttpGet]
        public async Task<List<Job>> GetAllJobs()
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetAllJobs());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "Job" })]
        [Route("api/CrewGo/GetJobById/{id}")]
        [HttpGet]
        public async Task<Job> GetJobById(int id)
        {
            var job = new Job();
            try
            {
                job = await Task.Run(() => new CREWGOBaseServiceFacade().GetJobById(id));
        }
            catch(DataException)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
            return job;
        }
        #endregion
        #region JOB SHIFTS
        
        [SwaggerOperation(Tags = new[] { "JobShifts" })]
        [Route("api/CrewGo/GetShiftById/{jobShiftId}")]
        [HttpGet]
        public async Task<JobShift> GetShiftById(long jobShiftId)
        {
            var shift = new JobShift();
            try
            {
                shift = await Task.Run(() => 
                    new CREWGOBaseServiceFacade().GetShiftById(jobShiftId)
                );
        }
            catch (DataException)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
            return shift;
        }
        /// <summary>
        /// GET api/CrewGo/GetAllJobShifts
        /// </summary>
        /// <returns>List<JobShifts></returns>
        [SwaggerOperation(Tags = new[] { "JobShifts" })]
        [Route("api/CrewGo/GetAllShifts")]
        [HttpGet]
        public async Task<List<JobShift>> GetAllShifts()
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetAllShifts());
        }
        /// <summary>
        /// GET api/CrewGo/GetLatestShifts
        /// </summary>
        /// <returns>List<JobShifts></returns>
        [SwaggerOperation(Tags = new[] { "JobShifts" })]
        [Route("api/CrewGo/GetLatestShifts")]
        [HttpGet]
        public async Task<List<JobShift>> GetLatestShifts()
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetLatestShifts());
        }

        #endregion
        #region JOB STAFF
        /// <summary>
        /// Post a JobStaff.
        /// </summary>
        /// <param name="jobStaff">JobStaff Object</param>
        /// <returns>Status code.</returns>
        [SwaggerOperation(Tags = new[] { "JobStaff" })]
        [Route("api/CrewGo/PostJobStaff")]
        [HttpPost]
        public async Task<IHttpActionResult> PostJobStaff(JobStaff jobStaff)
        {
            if (jobStaff.AcceptedDate == null)
            {
                jobStaff.AcceptedDate = DateTime.Now;
            }
            try
            {
                int count = 0;
                count = await new CREWGOBaseServiceFacade().AddJobStaff(jobStaff);
                if (count > 0)
                {
                    return Ok("Successfully added.");
                }
                return BadRequest("Error while adding.");
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
        #endregion
        #region JOB ALERT

        /// <summary>
        /// GET api/CrewGo/GetJobAlertByShiftUserId/jobShiftId/staffUserId
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <param name="staffUserId"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "JobAlert" })]
        [Route("api/CrewGo/GetJobAlertByShiftUserId/{jobShiftId}/{staffUserId}")]
        [HttpGet]
        public async Task<JobAlert> GetJobAlertByShiftUserId(long jobShiftId, long staffUserId)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetAlertByShiftIdAndUserId(jobShiftId, staffUserId));
        }

        #endregion
        #region JOB ALERT SETTING

        /// <summary>
        /// GET api/CrewGo/GetJobAlertSettingByShiftUserId/jobShiftId/staffUserId
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <param name="staffUserId"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "JobAlertSetting" })]
        [Route("api/CrewGo/GetJobAlertSettingByShiftUserId/{jobShiftId}/{staffUserId}")]
        [HttpGet]
        public async Task<JobAlertSetting> GetJobAlertSettingByShiftUserId(long jobShiftId, long staffUserId)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetAlertSettingByShiftIdAndUserId(jobShiftId, staffUserId));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="staffUserId"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "JobAlertSetting" })]
        [Route("api/CrewGo/GetJobAlertSettingByPartnerUserId/{partnerId}/{staffUserId}")]
        [HttpGet]
        public async Task<JobAlertSettingGlobal> GetJobAlertSettingByPartnerUserId(long partnerId, long staffUserId)
        {
            var response = new JobAlertSettingGlobal();
            try
            {
                response = await Task.Run(() => new CREWGOBaseServiceFacade().GetAlertSettingByPartnerIdAndUserId(partnerId, staffUserId));
        }
            catch(DataException)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
            return response;
        }

        #endregion
        #region POTENTIAL STAFF
        /// <summary>
        /// Generates a list of potential staffs
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        [SwaggerOperation(Tags = new[] { "Potential Staff" })]
        [Route("api/CrewGo/GeneratePotentialStaffList/")]
        [HttpPost]
        public async Task GeneratePotentialStaffList([FromBody] long jobShiftId)
        {
            await new CREWGOBaseServiceFacade()
                    .GeneratePotentialStaffList(jobShiftId);
        }
        /// <summary>
        /// Gets potential staff count
        /// </summary>
        /// <param name="jobShiftId">The job shift's id</param>
        /// <returns>Count of potential staffs</returns>
        [SwaggerOperation(Tags = new[] { "Potential Staff" })]
        [Route("api/CrewGo/GetPotentialStaffCount/{jobShiftId}")]
        [HttpGet]
        public async Task<int> GetPotentialStaffCount(long jobShiftId)
        {
            return await new CREWGOBaseServiceFacade()
                            .GetPotentialStaffCount(jobShiftId);
        }
        /// <summary>
        /// Gets list of top potential staffs upto 'count'
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        /// <param name="count">The number of potential staffs to fetch</param>
        /// <returns>List of PotentialStaff</returns>
        [SwaggerOperation(Tags = new[] { "Potential Staff" })]
        [Route("api/CrewGo/GetPotentialStaffs/{jobShiftId}/{count}")]
        [HttpGet]
        public async Task<List<PotentialStaff>> GetPotentialStaffs(
            long jobShiftId, int count)
        {
            return await new CREWGOBaseServiceFacade()
                            .GetPotentialStaffs(jobShiftId, count);
        }
        /// <summary>
        /// Updates the potential staff table for a particular shift-user 
        /// combination with value of is_notification_sent as true
        /// </summary>
        /// <param name="shiftUser">
        /// Tuple with first item userId, second item jobShiftId, the third item 
        /// and fouth item are the isSent and isScheduled flags respectively.
        /// </param>
        [SwaggerOperation(Tags = new[] { "Potential Staff" })]
        [Route("api/CrewGo/UpdatePotentialStaffList")]
        [HttpPost]
        public async Task UpdatePotentialStaffList(
            [FromBody]Tuple<long, long, bool, bool> shiftUserFlags)

        {
            try
            {
                await new CREWGOBaseServiceFacade()
                        .UpdatePotentialStaffList(
                            shiftUserFlags.Item1,
                            shiftUserFlags.Item2,
                            shiftUserFlags.Item3,
                            shiftUserFlags.Item4);
        }
            catch(DataException)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        /// Checks notification status for user id - shift id combination.
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <returns>True if notification is sent, false otherwise.</returns>
        [SwaggerOperation(Tags = new[] { "Potential Staff" })]
        [Route("api/CrewGo/CheckPSNotificationStatus/{shiftId}/{userId}")]
        [HttpGet]
        public async Task<bool> CheckPSNotificationStatus(
            long shiftId, long userId)
        {
            try
            {
                return await new CREWGOBaseServiceFacade()
                    .CheckNotificationStatus(userId,shiftId);
            }
            catch (Exception)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
        }
        /// <summary>
        /// Gets the shift sociability factor for particular shift
        /// </summary>
        /// <param name="jobShiftId">The job shift's ID</param>
        /// <returns>Shift sociability factor (0 <= SSF <= 1)</returns>
        [SwaggerOperation(Tags = new[] { "Potential Staff" })]
        [Route("api/CrewGo/GetShiftSociabilityFactor/{jobShiftId}")]
        [HttpGet]
        public async Task<decimal> GetShiftSociabilityFactor(long jobShiftId)
        {
            decimal ssf = 0;
            try
            {
                ssf = (decimal)await new CREWGOBaseServiceFacade()
                            .GetShiftSociabilityFactor(jobShiftId);
        }
            catch(DataException)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
            return ssf;
        }
        #endregion
        #region USER

        /// <summary>
        /// GET api/CrewGo/GetUserById/userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "User" })]
        [Route("api/CrewGo/GetUserById/{userId}")]
        [HttpGet]
        public async Task<User> GetUserById(long userId)
        {
            var user = new User();
            try
            {
                user = await Task.Run(() => 
                    new CREWGOBaseServiceFacade().GetUserById(userId)
                );
            }
            catch (DataException)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
            return user;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="skillId"></param>
        /// <param name="levelId"></param>
        /// <param name="userLimit"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "User" })]
        [Route("api/CrewGo/GetAvailableStaffByGlobalPriority/{partnerId}/{jobShiftId}/{skillId}/{levelId}/{userLimit}/{startTime}/{endTime}")]
        [HttpGet]
        public async Task<List<User>> GetAvailableStaffByGlobalPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetAvailableStaffByGlobalPriority(partnerId, jobShiftId, skillId, levelId, userLimit, startTime, endTime));
        }
        /// <summary>
        /// GET api/CrewGo/GetAvailableStaffByGeneralPriority/partnerId/jobShiftId/skillId/levelId/startTime/endTime
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="jobShiftId"></param>
        /// <param name="skillId"></param>
        /// <param name="levelId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "User" })]
        [Route("api/CrewGo/GetAvailableStaffByGeneralPriority/{partnerId}/{jobShiftId}/{skillId}/{levelId}/{userLimit}/{startTime}/{endTime}")]
        [HttpGet]
        public async Task<List<User>> GetAvailableStaffByGeneralPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetAvailableStaffByGeneralPriority(partnerId, jobShiftId, skillId, levelId, userLimit, startTime, endTime));
        }
        /// <summary>
        /// GET api/CrewGo/GetAvailableStaffByManualPriority/jobShiftId/userLimit/startTime/endTime
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <param name="userLimit"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "User" })]
        [Route("api/CrewGo/GetAvailableStaffByManualPriority/{jobShiftId}/{userLimit}/{startTime}/{endTime}")]
        [HttpGet]
        public async Task<List<User>> GetAvailableStaffByManualPriority(long jobShiftId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetAvailableStaffByManualPriority(jobShiftId, userLimit, startTime, endTime));
        }

        #endregion
        #region PRENOTIFICATION

        /// <summary>
        /// GET api/CrewGo/GetPreNotifications
        /// </summary>
        /// <returns>A list of PreNotification, i.e. List<PreNotification>.</returns>
        [SwaggerOperation(Tags = new[] { "PreNotification" })]
        [Route("api/CrewGo/GetPreNotifications")]
        [HttpGet]
        public async Task<List<PreNotification>> GetPreNotifications()
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetPreNotifications());
        }

        #endregion
        #region CUSTOM METHODS

        /// <summary>
        /// GET api/CrewGo/GetJobAlertCount/staffUserId
        /// </summary>
        /// <param name="staffUserId"></param>
        /// <returns>Integer</returns>
        [SwaggerOperation(Tags = new[] { "Custom" })]
        [Route("api/CrewGo/GetJobAlertCount/{staffUserId}")]
        [HttpGet]
        public async Task<Int32> GetJobAlertCount(int staffUserId)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetJobAlertCount(staffUserId));
        }
        /// <summary>
        /// GET api/CrewGo/OrderPriorityExists/jobShiftId
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <returns>Boolean</returns>
        [SwaggerOperation(Tags = new[] { "Custom" })]
        [Route("api/CrewGo/OrderPriorityExists/{jobShiftId}")]
        [HttpGet]
        public async Task<Boolean> OrderPriorityExists(long jobShiftId)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade()
                                        .OrderPriorityExists(jobShiftId));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="skillId"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "Custom" })]
        [Route("api/CrewGo/GlobalPriorityExists/{partnerId}/{skillId}/{levelId}")]
        [HttpGet]
        public async Task<Boolean> GlobalPriorityExists(long partnerId, int skillId, int levelId)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade()
                        .GlobalPriorityExists(partnerId, skillId, levelId));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "Custom" })]
        [Route("api/CrewGo/IsShiftQuotaFulfilled/{jobShiftId}")]
        [HttpGet]
        public async Task<Boolean> IsShiftQuotaFulfilled(long jobShiftId)
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade()
                                       .IsShiftQuotaFulfilled(jobShiftId));
        }
        /// <summary>
        /// Checks if staff has autobookable enabled ot not.
        /// </summary>
        /// <param name="staffId">Staff's userId.</param>
        /// <returns>True if autobookable, False otherwise.</returns>
        [SwaggerOperation(Tags = new[] { "Custom" })]
        [Route("api/CrewGo/IsStaffAutobookable/{staffId}")]
        [HttpGet]
        public async Task<Boolean> IsStaffAutobookable(long staffId)
        {
            return await new CREWGOBaseServiceFacade()
                                .IsStaffAutobookable(staffId);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "Custom" })]
        [Route("api/CrewGo/GetCurrentDate")]
        [HttpGet]
        public string GetCurrentDate()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "Custom" })]
        [Route("api/CrewGo/GetShiftNoticePeriod")]
        [HttpGet]
        public async Task<int> GetShiftNoticePeriod()
        {
            return await Task.Run(() => new CREWGOBaseServiceFacade().GetShiftNoticePeriod());
        }
        #endregion
        #region PUSH NOTIFICATION
        /// <summary>
        /// Generic Method for Push Notification. Used for instant api calls.
        /// </summary>
        /// <returns></returns>
        [SwaggerOperation(Tags = new[] { "PushNotification" })]
        [Route("api/CrewGo/PushNotification")]
        [HttpPost]
        public async Task<HttpResponseMessage> 
            PushNotification([FromBody] RequestNotification notificationPayload)
        {
            var notificationType = 
                (NotificationTypeEnum)notificationPayload.notificationType;
            NotificationStatusEnum notificationStatus = 
                NotificationStatusEnum.UNKNOWNFAILURE;

            #region CALL HELPER FUNCTIONS
            switch (notificationType)
            {
                case NotificationTypeEnum.INCOMINGCALL:
                case NotificationTypeEnum.CALLACCEPTED:
                case NotificationTypeEnum.CALLREJECTED:
                case NotificationTypeEnum.CALLCANCELLED:
                case NotificationTypeEnum.CALLCOMPLETED:
                    try
                    {
                        notificationStatus = await new PushNotificationHelper()
                            .SendVideoCallNotifications(notificationPayload);
                    }
                    catch(Exception)
                    {
                        notificationStatus = 
                            NotificationStatusEnum.UNKNOWNFAILURE;
                    }
                    break;
                case NotificationTypeEnum.SINCHNOTIFICATION:
                    try
                    {
                        notificationStatus = await new PushNotificationHelper()
                            .SendSinchNotifications(notificationPayload);
                    }
                    catch(Exception)
                    {
                        notificationStatus = 
                            NotificationStatusEnum.UNKNOWNFAILURE;
                    }
                    break;
                default:
                    try
                    {
                        notificationStatus = await new PushNotificationHelper()
                            .SendInstantNotifications(notificationPayload);
                    }
                    catch(Exception)
                    {
                        notificationStatus = 
                            NotificationStatusEnum.UNKNOWNFAILURE;
                    }
                    break;
            }
            #endregion

            #region RETURN RESPONSE MESSAGES
            if (notificationStatus == NotificationStatusEnum.SUCCESS)
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "Push Notification(s) Successfully Sent." 
                });
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(message);
                return response;
            }
            else if (notificationStatus == NotificationStatusEnum.FAILED)
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "Push Notification(s) not sent."
                });
                var response = 
                    new HttpResponseMessage(HttpStatusCode.BadRequest);
                response.Content = new StringContent(message);
                return response;
            }
            else if (notificationStatus == 
                NotificationStatusEnum.USERNOTFOUND)
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "Requested user(s) could not be found."
                });
                var response = new HttpResponseMessage(HttpStatusCode.NotFound);
                response.Content = new StringContent(message);
                return response;
            }
            else if (notificationStatus == 
                NotificationStatusEnum.CALLINFONOTFOUND)
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "Requested call record could not be found."
                });
                var response = new HttpResponseMessage(HttpStatusCode.NotFound);
                response.Content = new StringContent(message);
                return response;
            }
            else if (notificationStatus == 
                NotificationStatusEnum.USEROFFLINE)
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "Requestes user is offline."
                });
                var response = new HttpResponseMessage(HttpStatusCode.NotFound);
                response.Content = new StringContent(message);
                return response;
            }
            else if (notificationStatus == 
                NotificationStatusEnum.NODEVICEFOUND)
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "User is not signed in."
                });
                var response = new HttpResponseMessage(HttpStatusCode.NotFound);
                response.Content = new StringContent(message);
                return response;
            }
            else if(notificationStatus == 
                NotificationStatusEnum.UNKNOWNFAILURE)
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "Failed for some unknown reason."
                });
                var response = 
                    new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(message);
                return response;
            }
            else
            {
                var message = JsonConvert.SerializeObject(new
                {
                    statusCode = (int)notificationStatus,
                    message = "Internal server error."
                });
                var response = 
                    new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(message);
                return response;
            }
            #endregion
        }
        #endregion
    }
}