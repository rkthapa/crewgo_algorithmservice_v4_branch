﻿using System;
using System.Data;
using System.Web.Http;
using CREWGO.Service.WebAPI.V1.Filters;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Facade;
using System.Threading.Tasks;
using Swashbuckle.Swagger.Annotations;

namespace CREWGO.Service.WebAPI.V1.Controllers
{
    [APIAuthFilter]
    public class PreNotificationController : ApiController
    {
        /// <summary>
        /// Gets a PreNotification object by ID.
        /// </summary>
        /// <param name="id">PreNotification ID.</param>
        /// <returns>PreNotification object.</returns>
        [SwaggerOperation(Tags = new[] { "PreNotification" })]
        [Route("api/prenotification/getbyid/{id}")]
        [HttpGet]
        public async Task<PreNotification> GetById(long id)
        {
            var preNotification = new PreNotification();
            try
            {
                preNotification = await Task.Run(() =>
                    new CREWGOBaseServiceFacade().GetPrenotificationById(id)
                );
            }
            catch (DataException)
            {
                throw new HttpResponseException(
                    System.Net.HttpStatusCode.NotFound);
            }
            return preNotification;
        }
        /// <summary>
        /// Puts a PreNotification object.
        /// </summary>
        /// <param name="preNotification">PreNotification object.</param>
        /// <returns>Status code.</returns>
        [SwaggerOperation(Tags = new[] { "PreNotification" })]
        [Route("api/prenotification/PutPrenotification")]
        [HttpPut]
        public async Task<IHttpActionResult> PutPrenotification(
            PreNotification preNotification)
        {
            if (preNotification == null)
            {
                return BadRequest();
            }
            try
            {
                int count = 0;
                count = await Task.Run(() =>
                    new CREWGOBaseServiceFacade()
                        .UpdatePreNotification(preNotification)
                );
                if (count > 0)
                {
                    return Ok("Successfully updated");
                }
                return BadRequest("Error while updating");
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
    }
}