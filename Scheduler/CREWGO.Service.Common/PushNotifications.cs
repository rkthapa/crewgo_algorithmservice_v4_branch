﻿using CREWGO.Service.Common.RequestObject;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using PushSharp.Apple;
using PushSharp.Core;
using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CREWGO.Service.Common
{
    /// <summary>
    /// Handles notification sending using PushSharp.
    /// </summary>
    public class PushNotifications : IPushNotifications
    {
        #region PUBLIC VARIABLES

        public static string path = System.AppDomain.CurrentDomain.BaseDirectory + "/Logs/Log_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";

        #endregion PUBLIC VARIABLES

        #region PRIVATE VARIABLES

        private const string NOTIFICATION_JOBALERT_TITLE = "New Job Alert";
        private const string NOTIFICATION_JOBPOSTED_TITLE = "New Job Posted";
        private const string NOTIFICATION_JOBACCEPTED_TITLE = "Job Accepted";
        private const string NOTIFICATION_JOBCOMPLETED_TITLE = "Job Completed";
        private const string NOTIFICATION_JOBCANCELLED_TITLE = "Job Cancelled";
        private const string NOTIFICATION_JOBUPDATED_TITLE = "Job Updated";
        private const string NOTIFICATION_MESSAGE_TITLE = "New Message";
        private const string NOTIFICATION_BEFOREJOBSTART_TITLE = "Job About To Start";
        private const string NOTIFICATION_STAFFLEFT_TITLE = "Staff Has Left Site";
        private const string NOTIFICATION_MISSEDTOCOMPLETE_TITLE = "Staff Missed To Complete Job";
        private const string NOTIFICATION_PAYMENTMADE_TITLE = "Payment Made";
        private const string NOTIFICATION_TIMESHEETAPPROVED_TITLE = "Timesheet Approved";
        private const string NOTIFICATION_TIMESHEETCOMPLETED_TITLE = "Timesheet Completed";
        private const string NOTIFICATION_ONGOINGJOB_TITLE = "Ongoing Job";
        private const string NOTIFICATION_INDUCTION_TITLE = "Induction";
        private const string NOTIFICATION_STAFFADDED_TITLE = "Staff Added";
        private const string NOTIFICATION_STAFFREMOVED_TITLE = "Staff Removed";
        private const string NOTIFICATION_SHIFTUPDATED_TITLE = "Shift Updated";
        private const string NOTIFICATION_SUPERVISORADDED_TITLE = "Supervisor Added";
        private const string NOTIFICATION_SUPERVISORREMOVED_TITLE = "Supervisor Removed";
        private const string NOTIFICATION_JOBADDED_TITLE = "Job Added";
        private const string NOTIFICATION_JOBREMOVED_TITLE = "Job Removed";
        private const string NOTIFICATION_CALLACCEPTED_TITLE = "Call Accepted";
        private const string NOTIFICATION_CALLINITIATED_TITLE = "Incoming Call";
        private const string NOTIFICATION_CALLREJECTED_TITLE = "Call Rejected";
        private const string NOTIFICATION_CALLCANCELLED_TITLE = "Call Cancelled";
        private const string NOTIFICATION_CALLCOMPLETED_TITLE = "Call Completed";
        private const string NOTIFICATION_CALLALREADYACCEPTED_TITLE = "Call Already Accepted";
        private const string NOTIFICATION_CALLALREADYREJECTED_TITLE = "Call Already Rejected";
        private const string NOTIFICATION_SCHEDULE_CHANGED_TITLE = "Schedule Changed";
        private const string NOTIFICATION_STAFF_DISSOCIATED_TITLE = "Staff Dissociated";
        private const string NOTIFICATION_STAFF_ASSOCIATION_TITLE = "Invitation For Association";
        private const string NOTIFICATION_AUTOASSIGNED_TITLE = "Job Assigned";

        private const string NOTIFICATION_JOBALERT_BODY = "You have new job alerts pending your response.";
        private const string NOTIFICATION_JOBCANCELLED_BODY = "Job has been cancelled.";
        private const string NOTIFICATION_MESSAGE_BODY = "You have new messages.";
        private const string NOTIFICATION_AUTOASSIGNED_BODY = "You have been assigned to a job.";

        private const string NOTIFICATION_JOBALERT_TYPE = "JobAlerts";
        private const string NOTIFICATION_JOBPOSTED_TYPE = "JobPosted";
        private const string NOTIFICATION_JOBACCEPTED_TYPE = "JobAccepted";
        private const string NOTIFICATION_JOBCOMPLETED_TYPE = "JobCompleted";
        private const string NOTIFICATION_JOBCANCELLED_TYPE = "JobCancelled";
        private const string NOTIFICATION_JOBUPDATED_TYPE = "JobUpdated";
        private const string NOTIFICATION_MESSAGE_TYPE = "Messages";
        private const string NOTIFICATION_BEFOREJOBSTART_TYPE = "JobStart";
        private const string NOTIFICATION_STAFFLEFT_TYPE = "StaffLeaves";
        private const string NOTIFICATION_MISSEDTOCOMPLETE_TYPE = "MissToCompleteJob";
        private const string NOTIFICATION_PAYMENTMADE_TYPE = "PaymentMade";
        private const string NOTIFICATION_TIMESHEETAPPROVED_TYPE = "TimesheetApproved";
        private const string NOTIFICATION_TIMESHEETCOMPLETED_TYPE = "TimesheetCompleted";
        private const string NOTIFICATION_ONGOINGJOB_TYPE = "OnGoingJob";
        private const string NOTIFICATION_INDUCTION_TYPE = "InductionRequired";
        private const string NOTIFICATION_STAFFADDED_TYPE = "StaffAdded";
        private const string NOTIFICATION_STAFFREMOVED_TYPE = "StaffRemoved";
        private const string NOTIFICATION_SHIFTUPDATED_TYPE = "ShiftUpdated";
        private const string NOTIFICATION_SUPERVISORADDED_TYPE = "SupervisorAdded";
        private const string NOTIFICATION_SUPERVISORREMOVED_TYPE = "SupervisorRemoved";
        private const string NOTIFICATION_JOBADDED_TYPE = "JobAdded";
        private const string NOTIFICATION_JOBREMOVED_TYPE = "JobRemoved";
        private const string NOTIFICATION_ADMINJOBOFFER_TYPE = "AdminJobOffer";
        private const string NOTIFICATION_CALLACCEPTED_TYPE = "CallAccepted";
        private const string NOTIFICATION_CALLINITIATED_TYPE = "IncomingCall";
        private const string NOTIFICATION_CALLREJECTED_TYPE = "CallRejected";
        private const string NOTIFICATION_CALLCANCELLED_TYPE = "CallCancelled";
        private const string NOTIFICATION_CALLCOMPLETED_TYPE = "CallCompleted";
        private const string NOTIFICATION_CALLALREADYACCEPTED_TYPE = "CallAlreadyAccepted";
        private const string NOTIFICATION_CALLALREADYREJECTED_TYPE = "CallAlreadyRejected";
        private const string NOTIFICATION_SCHEDULE_CHANGED_TYPE = "ScheduleChanged";
        private const string NOTIFICATION_STAFF_DISSOCIATED_TYPE = "StaffDissociated";
        private const string NOTIFICATION_STAFF_ASSOCIATION_TYPE = "AssociationInvitation";
        private const string NOTIFICATION_AUTOASSIGNED_TYPE = "StaffAutoAssigned";

        private string notificationPayload = string.Empty;
        private string staffpositionPayload = string.Empty;
        private string jobdetailPayload = string.Empty;
        private string messagePayload = string.Empty;
        private string customPayload = string.Empty;
        private string alertTitle = string.Empty;
        private string alertBody = string.Empty;
        private string alertType = string.Empty;
        private string callType = string.Empty;
        private bool isSilentNotification = false;

        private readonly string _customerSenderId;
        private readonly string _customerAuthToken;
        private readonly string _supervisorSenderId;
        private readonly string _supervisorAuthToken;
        private readonly string _staffSenderId;
        private readonly string _staffAuthToken;
        private readonly string _appleCertCustomer;
        private readonly string _applePassCustomer;
        private readonly string _appleCertSupervisor;
        private readonly string _applePassSupervisor;
        private readonly string _appleCertStaff;
        private readonly string _applePassStaff;
        private readonly string _appleCertCustomerSilent;
        private readonly string _applePassCustomerSilent;
        private readonly string _appleCertSupervisorSilent;
        private readonly string _applePassSupervisorSilent;
        private readonly string _appleCertStaffSilent;
        private readonly string _applePassStaffSilent;

        #endregion PRIVATE VARIABLES

        #region CONSTRUCTOR

        [Inject]
        public PushNotifications(
            string customerSenderId,
            string customerAuthToken,
            string supervisorSenderId,
            string supervisorAuthToken,
            string staffSenderId,
            string staffAuthToken,
            string appleCertCustomer,
            string applePassCustomer,
            string appleCertSupervisor,
            string applePassSupervisor,
            string appleCertStaff,
            string applePassStaff)
        {
            _customerSenderId = customerSenderId;
            _customerAuthToken = customerAuthToken;
            _supervisorSenderId = supervisorSenderId;
            _supervisorAuthToken = supervisorAuthToken;
            _staffSenderId = staffSenderId;
            _staffAuthToken = staffAuthToken;
            _appleCertCustomer = appleCertCustomer;
            _applePassCustomer = applePassCustomer;
            _appleCertSupervisor = appleCertSupervisor;
            _applePassSupervisor = applePassSupervisor;
            _appleCertStaff = appleCertStaff;
            _applePassStaff = applePassStaff;
        }

        public PushNotifications(
            string customerSenderId,
            string customerAuthToken,
            string supervisorSenderId,
            string supervisorAuthToken,
            string staffSenderId,
            string staffAuthToken,
            string appleCertCustomer,
            string applePassCustomer,
            string appleCertSupervisor,
            string applePassSupervisor,
            string appleCertStaff,
            string applePassStaff,
            string appleCertCustomerSilent,
            string applePassCustomerSilent,
            string appleCertSupervisorSilent,
            string applePassSupervisorSilent,
            string appleCertStaffSilent,
            string applePassStaffSilent)
        {
            _customerSenderId = customerSenderId;
            _customerAuthToken = customerAuthToken;
            _supervisorSenderId = supervisorSenderId;
            _supervisorAuthToken = supervisorAuthToken;
            _staffSenderId = staffSenderId;
            _staffAuthToken = staffAuthToken;
            _appleCertCustomer = appleCertCustomer;
            _applePassCustomer = applePassCustomer;
            _appleCertSupervisor = appleCertSupervisor;
            _applePassSupervisor = applePassSupervisor;
            _appleCertStaff = appleCertStaff;
            _applePassStaff = applePassStaff;
            _appleCertCustomerSilent = appleCertCustomerSilent;
            _applePassCustomerSilent = applePassCustomerSilent;
            _appleCertSupervisorSilent = appleCertSupervisorSilent;
            _applePassSupervisorSilent = applePassSupervisorSilent;
            _appleCertStaffSilent = appleCertStaffSilent;
            _applePassStaffSilent = applePassStaffSilent;
        }

        #endregion CONSTRUCTOR

        #region PUBLIC METHODS

        /// <summary>
        /// Invokes SendAndroidPushNotification/SendApplePushNotification
        /// depending upon the user device type.
        /// </summary>
        /// <param name="badgeCount"></param>
        /// <param name="notificationBody"></param>
        /// <param name="notificationType"></param>
        /// <param name="androidTokens">List of Android tokens.</param>
        /// <param name="iosTokens">List of iOS tokens.</param>
        /// <param name="requestMessage"></param>
        /// <param name="requestJobDetail"></param>
        /// <param name="groupType"></param>
        /// <param name="title">JobTitle for DisplayName Attribute only</param>
        /// <param name="id">ParentJobID for DisplayName Attribute only</param>
        /// <returns>True if methods successfully queues notification</returns>
        [DisplayName("Send Notification: {2} , Titile: {10} , ID: {11} ")]
        public bool SendPushNotification(
            int badgeCount,
            string notificationBody,
            string notificaitonTitle,
            NotificationTypeEnum notificationType,
            List<string> androidTokens,
            List<string> iosTokens,
            RequestMessage requestMessage,
            RequestJobDetail requestJobDetail,
            RequestStaffPosition requestStaffPosition,
            RequestCallDetail requestCallDetail,
            UserGroupEnum groupType,
            string title,
            string id)
        {
            bool isSuccess = false;
            bool isSuccessAndroid = false;
            bool isSuccessIOS = false;

            messagePayload = LoadMessagePayload(requestMessage);
            jobdetailPayload = LoadJobDetailPayload(requestJobDetail);
            staffpositionPayload = LoadStaffPositionPayload(requestStaffPosition);

            alertBody = notificationBody;

            SetNotificationAttributes(notificationType, notificaitonTitle);

            if (androidTokens != null && androidTokens.Count > 0)
            {
                var notificationPayload = SetAndroidPayload(
                                            badgeCount,
                                            notificationType,
                                            requestCallDetail);
                isSuccessAndroid = SendAndroidPushNotification(
                                        androidTokens,
                                        notificationPayload.Item1,
                                        notificationPayload.Item2,
                                        groupType
                                    );
            }

            if (iosTokens != null && iosTokens.Count > 0)
            {
                isSuccessIOS = SendApplePushNotification(
                                    iosTokens,
                                    SetApplePayload(
                                        badgeCount,
                                        notificationType,
                                        requestCallDetail
                                    ),
                                    groupType
                                );
            }

            if (isSuccessAndroid || isSuccessIOS)
                isSuccess = true;

            return isSuccess;
        }

        /// <summary>
        /// Invokes SendAndroidPushNotification/SendApplePushNotification
        /// depending upon the user device type.
        /// </summary>
        /// <param name="badgeCount">The badge count to display</param>
        /// <param name="payload">Notification payload</param>
        /// <param name="notificationType">Type of notification to send</param>
        /// <param name="androidTokens">List of Android tokens.</param>
        /// <param name="iosTokens">List of iOS tokens.</param>
        /// <param name="requestJobDetail"></param>
        /// <param name="groupType"></param>
        /// <returns>True if methods successfully queues notification</returns>
        [DisplayName("Send Notification: Type: {4} ")]
        public bool SendPushNotification(
            int badgeCount,
            string payload,
            List<string> androidTokens,
            List<string> iosTokens,
            UserGroupEnum groupType)
        {
            bool isSuccess = false;
            bool isSuccessAndroid = false;
            bool isSuccessIOS = false;
            isSilentNotification = true;

            if (androidTokens != null && androidTokens.Count > 0)
            {
                isSuccessAndroid = SendAndroidPushNotification(
                                        androidTokens,
                                        payload,
                                        string.Empty,
                                        groupType
                                    );
            }
            if (iosTokens != null && iosTokens.Count > 0)
            {
                isSuccessIOS = SendApplePushNotification(
                                    iosTokens,
                                    payload,
                                    groupType
                                );
            }

            if (isSuccessAndroid || isSuccessIOS)
                isSuccess = true;

            return isSuccess;
        }

        /// <summary>
        /// Sends Android push notifications.
        /// </summary>
        /// <param name="deviceToken">List of Android tokens.</param>
        /// <param name="notificationPayload">Notification Payload.</param>
        /// <param name="customPayload">Custom payload.</param>
        /// <param name="groupType">User group type enum.</param>
        /// <returns></returns>
        public bool SendAndroidPushNotification(
            List<string> deviceToken,
            string notificationPayload,
            string customPayload,
            UserGroupEnum groupType)
        {
            bool isSuccess = false;
            var SENDER_ID = string.Empty;
            var AUTHTOKEN = string.Empty;

            switch (groupType)
            {
                case UserGroupEnum.CUSTOMER:
                    // Production Configuration [Customer]
                    SENDER_ID = _customerSenderId;
                    AUTHTOKEN = _customerAuthToken;
                    break;

                case UserGroupEnum.SUPERVISOR:
                    // Production Configuration [Supervisor]
                    SENDER_ID = _supervisorSenderId;
                    AUTHTOKEN = _supervisorAuthToken;
                    break;

                case UserGroupEnum.STAFF:
                    // Production Configuration [Staff]
                    SENDER_ID = _staffSenderId;
                    AUTHTOKEN = _staffAuthToken;
                    break;

                default:
                    SENDER_ID = "";
                    AUTHTOKEN = "";
                    break;
            }

            var config = new GcmConfiguration(SENDER_ID, AUTHTOKEN, null);
            config.GcmUrl = "https://fcm.googleapis.com/fcm/send";

            // Create a new broker
            var gcmBroker = new GcmServiceBroker(config);

            // Wire up events

            #region OnNotificationFailed & OnNotificationSucceeded

            gcmBroker.OnNotificationFailed += (notification, aggregateEx) =>
            {
                aggregateEx.Handle(ex =>
                {
                    //See what kind of exception it was to further diagnose
                    if (ex is GcmNotificationException)
                    {
                        var notificationException = (GcmNotificationException)ex;

                        //Deal with the failed notification
                        var gcmNotification = notificationException.Notification;
                        var description = notificationException.Description;

                        Utility.WriteLogToFileExclusively(path, string.Format("FCM Notification Failed: ID={0}, Desc={1}", gcmNotification.MessageId, description));
                    }
                    else if (ex is GcmMulticastResultException)
                    {
                        var multicastException = (GcmMulticastResultException)ex;

                        foreach (var succeededNotification in multicastException.Succeeded)
                        {
                            Utility.WriteLogToFileExclusively(path, string.Format("FCM Notification Failed: ID={0}", succeededNotification.MessageId));
                        }

                        foreach (var failedKvp in multicastException.Failed)
                        {
                            var x = failedKvp.Key;
                            var y = failedKvp.Value;

                            Utility.WriteLogToFileExclusively(path, string.Format("MulticastException: FCM Notification Failed: ID={0}, Desc={1}", x.MessageId, ""));
                        }
                    }
                    else if (ex is DeviceSubscriptionExpiredException)
                    {
                        var expiredException = (DeviceSubscriptionExpiredException)ex;

                        var oldId = expiredException.OldSubscriptionId;
                        var newId = expiredException.NewSubscriptionId;

                        Utility.WriteLogToFileExclusively(path, string.Format("Device RegistrationId Expired: {0}", oldId));

                        if (!string.IsNullOrWhiteSpace(newId))
                        {
                            // If this value isn't null our subscription
                            // changed and we should update our database
                            Utility.WriteLogToFileExclusively(path, string.Format("Device RegistrationId Expired: {0}", newId));
                        }
                    }
                    else if (ex is RetryAfterException)
                    {
                        var retryException = (RetryAfterException)ex;

                        // If you get rate limited, you should stop sending
                        //messages until after the RetryAfterUtc date
                        Utility.WriteLogToFileExclusively(path, string.Format("FCM Rate Limited, don't send more until after {0}", retryException.RetryAfterUtc));
                    }
                    else
                    {
                        Utility.WriteLogToFileExclusively(path, string.Format("FCM Notification Failed for some unknown reason: {0}", ex));
                    }

                    //Mark it as handled
                    return true;
                });
            };

            gcmBroker.OnNotificationSucceeded += (notification) =>
            {
                isSuccess = true;
                Utility.WriteLogToFileExclusively(path, "FCM Notification Sent!");
            };

            #endregion OnNotificationFailed & OnNotificationSucceeded

            // Start the broker
            gcmBroker.Start();

            // Queue a notification to send
            foreach (var token in deviceToken)
            {
                gcmBroker.QueueNotification(new GcmNotification
                {
                    RegistrationIds = new List<string> { token },
                    Notification = JObject.Parse(notificationPayload),
                    Data = JObject.Parse(customPayload),
                    TimeToLive = isSilentNotification ? 60 : 86400
                });
            }
            // Stop the broker, wait for it to finish.
            // This isn't done after every message, but after you're done with the broker
            gcmBroker.Stop();

            return isSuccess;
        }

        /// <summary>
        /// Sends notifications to Apple devices.
        /// </summary>
        /// <param name="deviceToken">List of iOS tokens.</param>
        /// <param name="notificationPayload">Notification Payload.</param>
        /// <param name="groupType">User group type enum.</param>
        /// <returns></returns>
        public bool SendApplePushNotification(
            List<string> deviceToken,
            string notificationPayload,
            UserGroupEnum groupType)
        {
            bool isSuccess = false;
            var APPLE_PRODUCTION_CERT = string.Empty;
            var APPLE_PRODUCTION_PASS = string.Empty;
            List<string> invalidTokens = new List<string>();

            #region Set Certificates ans Passwords

            switch (groupType)
            {
                case UserGroupEnum.CUSTOMER:
                    if (isSilentNotification)
                    {
                        APPLE_PRODUCTION_CERT = _appleCertCustomerSilent;
                        APPLE_PRODUCTION_PASS = _applePassCustomerSilent;
                    }
                    else
                    {
                        APPLE_PRODUCTION_CERT = _appleCertCustomer;
                        APPLE_PRODUCTION_PASS = _applePassCustomer;
                    }
                    break;

                case UserGroupEnum.SUPERVISOR:
                    if (isSilentNotification)
                    {
                        APPLE_PRODUCTION_CERT = _appleCertSupervisorSilent;
                        APPLE_PRODUCTION_PASS = _applePassSupervisorSilent;
                    }
                    else
                    {
                        APPLE_PRODUCTION_CERT = _appleCertSupervisor;
                        APPLE_PRODUCTION_PASS = _applePassSupervisor;
                    }
                    break;

                case UserGroupEnum.STAFF:
                    if (isSilentNotification)
                    {
                        APPLE_PRODUCTION_CERT = _appleCertStaffSilent;
                        APPLE_PRODUCTION_PASS = _applePassStaffSilent;
                    }
                    else
                    {
                        APPLE_PRODUCTION_CERT = _appleCertStaff;
                        APPLE_PRODUCTION_PASS = _applePassStaff;
                    }
                    break;

                default:
                    break;
            }

            #endregion Set Certificates ans Passwords

            var config = new ApnsConfiguration(
                            ApnsConfiguration.ApnsServerEnvironment.Sandbox,
                            APPLE_PRODUCTION_CERT,
                            APPLE_PRODUCTION_PASS,
                            false);
            if (!isSilentNotification)
            {
                config = new ApnsConfiguration(
                            ApnsConfiguration.ApnsServerEnvironment.Sandbox,
                            APPLE_PRODUCTION_CERT,
                            APPLE_PRODUCTION_PASS);
            }
            // Create a new broker
            var apnsBroker = new ApnsServiceBroker(config);

            // Wire up events

            #region OnNotificationFailed & OnNotificationSucceeded

            apnsBroker.OnNotificationFailed += (notification, aggregateEx) =>
            {
                aggregateEx.Handle(ex =>
                {
                    // See what kind of exception it was to further diagnose
                    if (ex is ApnsNotificationException)
                    {
                        var notificationException = (ApnsNotificationException)ex;

                        // Deal with the failed notification
                        var apnsNotification = notificationException.Notification;
                        var statusCode = notificationException.ErrorStatusCode;

                        if (statusCode == ApnsNotificationErrorStatusCode.InvalidToken)
                        {
                            invalidTokens.Add(notificationException.Notification.DeviceToken);
                        }

                        Utility.WriteLogToFileExclusively(path, string.Format("Apple Notification Failed: ID={0}, Code={1}", apnsNotification.Identifier, statusCode));
                    }
                    else
                    {
                        // Inner exception might hold more useful information like an ApnsConnectionException
                        Utility.WriteLogToFileExclusively(path, string.Format("Apple Notification Failed for some unknown reason : {0}", ex.InnerException));
                    }

                    // Mark it as handled
                    return true;
                });
            };

            apnsBroker.OnNotificationSucceeded += (notification) =>
            {
                isSuccess = true;
                Utility.WriteLogToFileExclusively(path, "Apple Notification Sent!");
            };

            #endregion OnNotificationFailed & OnNotificationSucceeded

            // Start the broker
            apnsBroker.Start();

            foreach (var token in deviceToken)
            {
                // Queue a notification to send
                apnsBroker.QueueNotification(new ApnsNotification
                {
                    DeviceToken = token,
                    Payload = JObject.Parse(notificationPayload)
                });
            }

            // Stop the broker, wait for it to finish.
            // This isn't done after every message,
            // But only after you're done with the broker
            apnsBroker.Stop();

            if (invalidTokens.Count > 0)
            {
                //remove tokens from DB
            }

            return isSuccess;
        }

        /// <summary>
        /// Sets notification type of notifications to be sent.
        /// </summary>
        /// <param name="notificationType">NotificationTypeEnum value</param>
        public void SetNotificationAttributes(
            NotificationTypeEnum notificationType, string notificationTitle)
        {
            switch (notificationType)
            {
                case NotificationTypeEnum.JOBALERTS:
                    alertTitle = NOTIFICATION_JOBALERT_TITLE;
                    alertBody = NOTIFICATION_JOBALERT_BODY;
                    alertType = NOTIFICATION_JOBALERT_TYPE;
                    break;

                case NotificationTypeEnum.JOBPOSTED:
                    alertTitle = NOTIFICATION_JOBPOSTED_TITLE;
                    alertType = NOTIFICATION_JOBPOSTED_TYPE;
                    break;

                case NotificationTypeEnum.JOBACCEPTED:
                    alertTitle = NOTIFICATION_JOBACCEPTED_TITLE;
                    alertType = NOTIFICATION_JOBACCEPTED_TYPE;
                    break;

                case NotificationTypeEnum.JOBCOMPLETED:
                    alertTitle = NOTIFICATION_JOBCOMPLETED_TITLE;
                    alertType = NOTIFICATION_JOBCOMPLETED_TYPE;
                    break;

                case NotificationTypeEnum.JOBCANCELLED:
                    alertTitle = NOTIFICATION_JOBCANCELLED_TITLE;
                    //alertBody = NOTIFICATION_JOBCANCELLED_BODY;
                    alertType = NOTIFICATION_JOBCANCELLED_TYPE;
                    break;

                case NotificationTypeEnum.JOBUPDATED:
                    alertTitle = NOTIFICATION_JOBUPDATED_TITLE;
                    alertType = NOTIFICATION_JOBUPDATED_TYPE;
                    break;

                case NotificationTypeEnum.MESSAGES:
                    alertTitle = NOTIFICATION_MESSAGE_TITLE;
                    alertBody = NOTIFICATION_MESSAGE_BODY;
                    alertType = NOTIFICATION_MESSAGE_TYPE;
                    break;

                case NotificationTypeEnum.BEFOREJOBSTART:
                    alertTitle = NOTIFICATION_BEFOREJOBSTART_TITLE;
                    alertType = NOTIFICATION_BEFOREJOBSTART_TYPE;
                    break;

                case NotificationTypeEnum.STAFFLEFT:
                    alertTitle = NOTIFICATION_STAFFLEFT_TITLE;
                    alertType = NOTIFICATION_STAFFLEFT_TYPE;
                    break;

                case NotificationTypeEnum.MISSEDTOCOMPLETE:
                    alertTitle = NOTIFICATION_MISSEDTOCOMPLETE_TITLE;
                    alertType = NOTIFICATION_MISSEDTOCOMPLETE_TYPE;
                    break;

                case NotificationTypeEnum.PAYMENTMADE:
                    alertTitle = NOTIFICATION_PAYMENTMADE_TITLE;
                    alertType = NOTIFICATION_PAYMENTMADE_TYPE;
                    break;

                case NotificationTypeEnum.TIMESHEETAPPROVED:
                    alertTitle = NOTIFICATION_TIMESHEETAPPROVED_TITLE;
                    alertType = NOTIFICATION_TIMESHEETAPPROVED_TYPE;
                    break;

                case NotificationTypeEnum.TIMESHEETCOMPLETED:
                    alertTitle = NOTIFICATION_TIMESHEETCOMPLETED_TITLE;
                    alertType = NOTIFICATION_TIMESHEETCOMPLETED_TYPE;
                    break;

                case NotificationTypeEnum.INDUCTIONREQUIRED:
                    alertTitle = NOTIFICATION_INDUCTION_TITLE;
                    alertType = NOTIFICATION_INDUCTION_TYPE;
                    break;

                case NotificationTypeEnum.STAFFADDED:
                    alertTitle = NOTIFICATION_STAFFADDED_TITLE;
                    alertType = NOTIFICATION_STAFFADDED_TYPE;
                    break;

                case NotificationTypeEnum.STAFFREMOVED:
                    alertTitle = NOTIFICATION_STAFFREMOVED_TITLE;
                    alertType = NOTIFICATION_STAFFREMOVED_TYPE;
                    break;

                case NotificationTypeEnum.SHIFTUPDATED:
                    alertTitle = NOTIFICATION_SHIFTUPDATED_TITLE;
                    alertType = NOTIFICATION_SHIFTUPDATED_TYPE;
                    break;

                case NotificationTypeEnum.SUPERVISORADDED:
                    alertTitle = NOTIFICATION_SUPERVISORADDED_TITLE;
                    alertType = NOTIFICATION_SUPERVISORADDED_TYPE;
                    break;

                case NotificationTypeEnum.SUPERVISORREMOVED:
                    alertTitle = NOTIFICATION_SUPERVISORREMOVED_TITLE;
                    alertType = NOTIFICATION_SUPERVISORREMOVED_TYPE;
                    break;

                case NotificationTypeEnum.JOBADDED:
                    alertTitle = NOTIFICATION_JOBADDED_TITLE;
                    alertType = NOTIFICATION_JOBADDED_TYPE;
                    break;

                case NotificationTypeEnum.JOBREMOVED:
                    alertTitle = NOTIFICATION_JOBREMOVED_TITLE;
                    alertType = NOTIFICATION_JOBREMOVED_TYPE;
                    break;

                case NotificationTypeEnum.ADMINJOBOFFER:
                    alertType = NOTIFICATION_ADMINJOBOFFER_TYPE;
                    alertTitle = notificationTitle;
                    break;

                case NotificationTypeEnum.INCOMINGCALL:
                    alertType = NOTIFICATION_CALLINITIATED_TYPE;
                    alertTitle = NOTIFICATION_CALLINITIATED_TITLE;
                    isSilentNotification = true;
                    break;

                case NotificationTypeEnum.CALLACCEPTED:
                    alertType = NOTIFICATION_CALLACCEPTED_TYPE;
                    alertTitle = NOTIFICATION_CALLACCEPTED_TITLE;
                    isSilentNotification = true;
                    break;

                case NotificationTypeEnum.CALLREJECTED:
                    alertType = NOTIFICATION_CALLREJECTED_TYPE;
                    alertTitle = NOTIFICATION_CALLREJECTED_TITLE;
                    isSilentNotification = true;
                    break;

                case NotificationTypeEnum.CALLCANCELLED:
                    alertType = NOTIFICATION_CALLCANCELLED_TYPE;
                    alertTitle = NOTIFICATION_CALLCANCELLED_TITLE;
                    isSilentNotification = true;
                    break;

                case NotificationTypeEnum.CALLCOMPLETED:
                    alertType = NOTIFICATION_CALLCOMPLETED_TYPE;
                    alertTitle = NOTIFICATION_CALLCOMPLETED_TITLE;
                    isSilentNotification = true;
                    break;

                case NotificationTypeEnum.CALLALREADYACCEPTED:
                    alertTitle = NOTIFICATION_CALLALREADYACCEPTED_TITLE;
                    alertType = NOTIFICATION_CALLALREADYACCEPTED_TYPE;
                    isSilentNotification = true;
                    break;

                case NotificationTypeEnum.CALLALREADYREJECTED:
                    alertType = NOTIFICATION_CALLALREADYREJECTED_TYPE;
                    alertTitle = NOTIFICATION_CALLALREADYREJECTED_TITLE;
                    isSilentNotification = true;
                    break;

                case NotificationTypeEnum.ONGOINGJOB:
                    alertTitle = NOTIFICATION_ONGOINGJOB_TITLE;
                    alertType = NOTIFICATION_ONGOINGJOB_TYPE;
                    break;

                case NotificationTypeEnum.SCHEDULECHANGED:
                    alertTitle = NOTIFICATION_SCHEDULE_CHANGED_TITLE;
                    alertType = NOTIFICATION_SCHEDULE_CHANGED_TYPE;
                    break;

                case NotificationTypeEnum.STAFFDISSOCIATEDBYPARTNER:
                    alertTitle = notificationTitle;
                    alertType = NOTIFICATION_STAFF_DISSOCIATED_TYPE;
                    break;

                case NotificationTypeEnum.STAFFASSOCIATIONINVITATION:
                    alertTitle = notificationTitle;
                    alertType = NOTIFICATION_STAFF_ASSOCIATION_TYPE;
                    break;

                case NotificationTypeEnum.STAFFAUTOASSIGNED:
                    alertTitle = NOTIFICATION_AUTOASSIGNED_TITLE;
                    alertType = NOTIFICATION_AUTOASSIGNED_TYPE;
                    alertBody = NOTIFICATION_AUTOASSIGNED_BODY;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Prepares the Android notification payload.
        /// </summary>
        /// <param name="badgeCount"></param>
        /// <param name="messagePayload">The notification content.</param>
        /// <param name="jobdetailPayload">Custom details to be sent.</param>
        /// <returns></returns>
        public Tuple<string, string> SetAndroidPayload(
            int badgeCount,
            NotificationTypeEnum notificationType,
            RequestCallDetail requestCallDetail)
        {
            switch (notificationType)
            {
                case NotificationTypeEnum.CALLCOMPLETED:
                case NotificationTypeEnum.CALLACCEPTED:
                case NotificationTypeEnum.INCOMINGCALL:
                case NotificationTypeEnum.CALLREJECTED:
                case NotificationTypeEnum.CALLCANCELLED:
                case NotificationTypeEnum.CALLALREADYREJECTED:
                case NotificationTypeEnum.CALLALREADYACCEPTED:
                    // Empty payload for silent notification
                    notificationPayload = JsonConvert.SerializeObject(new
                    {
                    });
                    // Serialize the custom payload for push notification
                    customPayload = JsonConvert.SerializeObject(new
                    {
                        title = alertTitle,
                        body = alertBody,
                        type = requestCallDetail.callType,
                        action = notificationType,
                        callDetails = new
                        {
                            callId = requestCallDetail.callId,
                            token = requestCallDetail.token,
                            roomName = requestCallDetail.roomName,
                            callerName = requestCallDetail.callerName
                        }
                    });
                    break;

                default:
                    // Serialize the notification payload for push notification
                    notificationPayload = JsonConvert.SerializeObject(new
                    {
                        title = alertTitle,
                        body = alertBody,
                        icon = ""
                    });

                    // Serialize the custom payload for push notification
                    customPayload = JsonConvert.SerializeObject(new
                    {
                        type = alertType,
                        alertCount = badgeCount,
                        message = messagePayload,
                        jobdetail = jobdetailPayload,
                        staffPosition = staffpositionPayload,
                    });
                    break;
            }

            return new Tuple<string, string>(
                        notificationPayload, customPayload);
        }

        /// <summary>
        /// Prepares the iOS notification Payload.
        /// </summary>
        /// <param name="badgeCount"></param>
        /// <param name="messagePayload">The notification content.</param>
        /// <param name="jobdetailPayload">Custom details to be sent.</param>
        /// <returns>Serialized JSON Payload</returns>
        public string SetApplePayload(
            int badgeCount,
            NotificationTypeEnum notificationType,
            RequestCallDetail requestCallDetail)
        {
            switch (notificationType)
            {
                case NotificationTypeEnum.CALLCOMPLETED:
                case NotificationTypeEnum.CALLACCEPTED:
                case NotificationTypeEnum.INCOMINGCALL:
                case NotificationTypeEnum.CALLREJECTED:
                case NotificationTypeEnum.CALLCANCELLED:
                    // Serialize the payload for push notification
                    notificationPayload = JsonConvert.SerializeObject(new
                    {
                        aps = new
                        {
                            content_available = 1,
                            data = new
                            {
                                title = alertTitle,
                                body = alertBody,
                                type = requestCallDetail.callType,
                                action = notificationType,
                                callDetails = new
                                {
                                    callId = requestCallDetail.callId,
                                    token = requestCallDetail.token,
                                    roomName = requestCallDetail.roomName,
                                    callerName = requestCallDetail.callerName
                                }
                            }
                        }
                    });
                    notificationPayload = notificationPayload.Replace(
                                            "content_available",
                                            "content-available");
                    break;

                default:
                    // Serialize the payload for push notification
                    notificationPayload = JsonConvert.SerializeObject(new
                    {
                        aps = new
                        {
                            alert = new
                            {
                                title = alertTitle,
                                body = alertBody
                            },
                            badge = 1,
                            content_available = 1,
                            sound = "default",
                            data = new
                            {
                                type = alertType,
                                alertCount = badgeCount,
                                message = messagePayload,
                                jobdetail = jobdetailPayload,
                                staffPosition = staffpositionPayload,
                            }
                        }
                    });
                    notificationPayload = notificationPayload.Replace(
                                            "content_available",
                                            "content-available");
                    break;
            }

            return notificationPayload;
        }

        /// <summary>
        /// Loads the message payload.
        /// </summary>
        /// <param name="requestMessage">RequestMessage object</param>
        /// <returns></returns>
        public string LoadMessagePayload(RequestMessage requestMessage)
        {
            // Load Message Payload
            if (requestMessage != null)
            {
                messagePayload = JsonConvert.SerializeObject(new
                {
                    msgId = requestMessage.msgId,
                    msgTitle = requestMessage.msgTitle,
                    msgBody = requestMessage.msgBody,
                    msgDateTime = requestMessage
                                    .msgDateTime
                                    .ToString("yyyy-MM-dd HH:mm:ss"),
                    jobId = requestMessage.jobId,
                    senderId = requestMessage.senderId,
                    senderName = requestMessage.senderName,
                    deleteOption = requestMessage.deleteOption,
                    canReply = requestMessage.canReply
                });
            }
            else { messagePayload = "null"; }
            return messagePayload;
        }

        /// <summary>
        /// Loads the job detail payload.
        /// </summary>
        /// <param name="requestJobDetail">RequestJobDetail object</param>
        /// <returns></returns>
        public string LoadJobDetailPayload(RequestJobDetail requestJobDetail)
        {
            // Load JobDetail Payload
            if (requestJobDetail != null)
            {
                jobdetailPayload = JsonConvert.SerializeObject(new
                {
                    alertId = requestJobDetail.alertId,
                    alertMessage = requestJobDetail.alertMessage,
                    jobId = requestJobDetail.jobId,
                    jobTitle = requestJobDetail.jobTitle,
                    jobAddress = requestJobDetail.jobAddress,
                    sentDate = requestJobDetail
                                .sentDate.ToString("yyyy-MM-dd HH:mm:ss")
                });
            }
            else { jobdetailPayload = "null"; }
            return jobdetailPayload;
        }

        /// <summary>
        /// Loads the staff position payload.
        /// </summary>
        /// <param name="requestStaffPosition"></param>
        /// <returns></returns>
        public string LoadStaffPositionPayload(
            RequestStaffPosition requestStaffPosition)
        {
            // Load Staff Position Payload
            if (requestStaffPosition != null)
            {
                staffpositionPayload = JsonConvert.SerializeObject(new
                {
                    jobId = requestStaffPosition.jobId,
                    jobShiftId = requestStaffPosition.jobShiftId,
                    customerId = requestStaffPosition.customerId,
                    supervisorId = requestStaffPosition.supervisorId
                });
            }
            else { staffpositionPayload = "null"; }

            return staffpositionPayload;
        }

        #endregion PUBLIC METHODS
    }
}