﻿using System.Configuration;

namespace CREWGO.Service.Common
{
    public class Global
    {
        public static ConnectionStringSettings MySQLConnection
        {
            get { return ConfigurationManager.ConnectionStrings["MySQLConnection"]; }
        }
    }
}
