﻿using System;
using System.Data;

namespace CREWGO.Service.Common
{
    public class Validator
    {
        public static bool DataSetHasData(DataSet dst)
        {
            if (dst != null && 
                dst.Tables.Count > 0 && 
                dst.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
    }
}