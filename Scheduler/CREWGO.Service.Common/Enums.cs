﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common
{
    public enum DeviceTypeEnum : int
    {
        IOS                         = 1,    // IOS
        ANDROID                     = 2     // ANDROID
    }
    public enum JobAlertStatusEnum : int
    {
        PENDING                     = 0,    // PENDING
        ACCEPTED                    = 1,    // ACCEPTED
        IGNORED                     = 2,    // IGNORED
        UNAVAILABLE                 = 3,    // MAKE UNAVAILABLE
        EXPIRED                     = 4,    // EXPIRED
        FILLED                      = 5,    // POSITION FILLED
        ENGAGED                     = 6,    // STAFF ALREADY ENGAGED
        DELETEDBYSTAFF              = 7,    // JOB DELETED BY STAFF
        COMPLETED                   = 8,    // JOB COMPLETED
        UNREAD                      = 9     // ALERT NOT VIEWED
    }
    public enum NotificationTypeEnum : int
    {
        JOBALERTS                   = 0,    // ALERTS
        JOBPOSTED                   = 1,    // POSTED
        JOBACCEPTED                 = 2,    // ACCEPTED
        JOBCOMPLETED                = 3,    // COMPLETED
        JOBCANCELLED                = 4,    // CANCELLED
        JOBUPDATED                  = 5,    // UPDATED
        MESSAGES                    = 6,    // MESSAGE
        BEFOREJOBSTART              = 7,    // BEFORE JOB START
        STAFFLEFT                   = 8,    // STAFF LEFT SITE
        MISSEDTOCOMPLETE            = 9,    // STAFF MISSED TO COMPLETE JOB
        PAYMENTMADE                 = 10,   // PAYMENT
        TIMESHEETAPPROVED           = 11,   // TIMESHEET APPROVED
        TIMESHEETCOMPLETED          = 12,   // TIMESHEET COMPLETED
        ONGOINGJOB                  = 13,   // ONGOING JOB
        INDUCTIONREQUIRED           = 14,   // INDUCTION REQUIRED
        STAFFADDED                  = 15,   // STAFF ADDED
        STAFFREMOVED                = 16,   // STAFF REMOVED
        SHIFTUPDATED                = 17,   // SHIFT UPDATED
        SUPERVISORADDED             = 18,   // SUPERVISOR ADDED
        SUPERVISORREMOVED           = 19,   // SUPERVISOR REMOVED
        JOBADDED                    = 20,   // JOB ADDED
        JOBREMOVED                  = 21,   // JOB REMOVED
        ADMINJOBOFFER               = 22,   // STAFF ADDED BY ADMIN
        INCOMINGCALL                = 23,   // CALL INITIATED BY CLIENT
        CALLACCEPTED                = 24,   // CALL ACCEPTED BY CLIENT
        CALLREJECTED                = 25,   // CALL REJECTED BY CLIENT
        CALLCANCELLED               = 26,   // CALL CANCELLED BY CALLER
        CALLCOMPLETED               = 27,   // CALL CCOMPLETED
        CALLALREADYACCEPTED         = 28,   // CALL ACCEPTED BY ANOTHER DEVICE
        CALLALREADYREJECTED         = 29,   // CALL REJECTED BY ANOTHER DEVICE
        SINCHNOTIFICATION           = 30,   // SINCH NOTIFICATIONS
        SCHEDULECHANGED             = 31,   // JOB SCHEDULE CHANGED
        STAFFDISSOCIATEDBYPARTNER   = 32,   // STAFF DISSOCIATED BY PARTNER
        STAFFASSOCIATIONINVITATION  = 33,   // STAFF ASSOCIATION INVITATION
        STAFFAUTOASSIGNED           = 34    // AUTOBOOKABLE STAFF AUTO ASSIGNED
    }
    public enum CallTypeEnum : int
    {
        AUDIOCALL = 1,  //FOR ALL AUDIO CALL RELATED NOTIFICATIONS
        VIDEOCALL = 2   //FOR ALL VIDEO CALL RELATED NOTIFICATIONS
    }
    public enum NotificationStatusEnum : int
    {
        FAILED                  = 0,    //CALL NOTIFICATION SENDING FAILED
        SUCCESS                 = 1,    //CALL NOTIFICATION SUCCESFULLY SENT
        USEROFFLINE             = 2,    //RECIPIENT OFFLINE
        USERNOTFOUND            = 3,    //USER NOT FOUND
        CALLINFONOTFOUND        = 4,    //CALL RECORD NOT FOUND
        UNKNOWNFAILURE          = 5,    //UNEXPECTED EXCEPTION OCCURED
        NODEVICEFOUND           = 6,    //USER IS NOT LOGGEN IN
        SOMEFAILED              = 7     //SENDING SOME NOTIFICATION FAILED
    }
    public enum StaffPriorityTypeEnum : int
    {
        GLOBAL                      = 0,    // GLOBAL ORDER
        MANUAL                      = 1,    // SHIFT ORDER
        GENERAL                     = 3     // DEFAULT ORDER
    }
    public enum UserGroupEnum : int
    {
        ADMIN                       = 1,
        PARTNER                     = 2,
        CUSTOMER                    = 3,
        STAFF                       = 4,
        SUPERVISOR                  = 5
    }
    public enum EmailType : int
    {
        ERRORMAIL                   = 1,
        CONFIRMATIONMAIL            = 2
    }
    public enum AlgorithmType : int
    {
        GENREAL     = 1,
        GLOBAL      = 2,
        MANUAL      = 3,
        CREWGO      = 4,
        CREWSERVER  = 5
    }
}