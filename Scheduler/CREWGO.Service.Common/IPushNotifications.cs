﻿using System.Collections.Generic;
using CREWGO.Service.Common.RequestObject;
using System.ComponentModel;
using System;

namespace CREWGO.Service.Common
{
    public interface IPushNotifications
    {
        /// <summary>
        /// Invokes SendAndroidPushNotification/SendApplePushNotification
        /// depending upon the user device type.
        /// </summary>
        /// <param name="badgeCount"></param>
        /// <param name="notificationBody"></param>
        /// <param name="notificationType"></param>
        /// <param name="androidTokens">List of Android tokens.</param>
        /// <param name="iosTokens">List of iOS tokens.</param>
        /// <param name="requestMessage"></param>
        /// <param name="requestJobDetail"></param>
        /// <param name="groupType"></param>
        /// <param name="title">JobTitle for DisplayName Attribute only</param>
        /// <param name="id">ParentJobID for DisplayName Attribute only</param>
        /// <returns>True if methods successfully queues notification</returns>
        [DisplayName("Send Notification: {2} , Titile: {10} , ID: {11} ")]
        bool SendPushNotification(
            int badgeCount, 
            string notificationBody, 
            string  notificationTitle,
            NotificationTypeEnum notificationType, 
            List<string> androidTokens, 
            List<string> iosTokens, 
            RequestMessage requestMessage, 
            RequestJobDetail requestJobDetail, 
            RequestStaffPosition requestStaffPosition, 
            RequestCallDetail rquestCallDetail,
            UserGroupEnum groupType, 
            string title, 
            string id);
        /// <summary>
        /// Sends Android push notifications.
        /// </summary>
        /// <param name="deviceToken">List of Android tokens.</param>
        /// <param name="notificationPayload">Notification Payload.</param>
        /// <param name="customPayload">Custom payload.</param>
        /// <param name="groupType">User group type enum.</param>
        /// <returns></returns>
        bool SendAndroidPushNotification(
            List<string> deviceToken, 
            string notificationPayload, 
            string customPayload, 
            UserGroupEnum groupType);

        /// <summary>
        /// Sends notifications to Apple devices.
        /// </summary>
        /// <param name="deviceToken">List of iOS tokens.</param>
        /// <param name="notificationPayload">Notification Payload.</param>
        /// <param name="groupType">User group type enum.</param>
        /// <returns></returns>
        bool SendApplePushNotification(
            List<string> deviceToken,
            string notificationPayload,
            UserGroupEnum groupType);

        /// <summary>
        /// Sets notification type of notifications to be sent.
        /// </summary>
        /// <param name="notificationType">NotificationTypeEnum value</param>
        void SetNotificationAttributes(
            NotificationTypeEnum notificationType, string notificationTitle);

        /// <summary>
        /// Prepares the Android notification payload.
        /// </summary>
        /// <param name="badgeCount"></param>
        /// <param name="messagePayload">The notification content.</param>
        /// <param name="jobdetailPayload">Custom details to be sent.</param>
        /// <returns></returns>
        Tuple<string, string> SetAndroidPayload(
            int badgeCount,
            NotificationTypeEnum notificationType,
            RequestCallDetail requestCallDetail);

        /// <summary>
        /// Prepares the iOS notification Payload.
        /// </summary>
        /// <param name="badgeCount"></param>
        /// <param name="messagePayload">The notification content.</param>
        /// <param name="jobdetailPayload">Custom details to be sent.</param>
        /// <returns>Serialized JSON Payload</returns>
        string SetApplePayload(
            int badgeCount,
            NotificationTypeEnum notificationType,
            RequestCallDetail requestCallDetail);

        /// <summary>
        /// Loads the message payload.
        /// </summary>
        /// <param name="requestMessage">RequestMessage object</param>
        /// <returns></returns>
        string LoadMessagePayload(RequestMessage requestMessage);

        /// <summary>
        /// Loads the job detail payload.
        /// </summary>
        /// <param name="requestJobDetail">RequestJobDetail object</param>
        /// <returns></returns>
        string LoadJobDetailPayload(RequestJobDetail requestJobDetail);

        /// <summary>
        /// Loads the staff position payload.
        /// </summary>
        /// <param name="requestStaffPosition"></param>
        /// <returns></returns>
        string LoadStaffPositionPayload(
            RequestStaffPosition requestStaffPosition);
    }
}
