﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common.RequestObject
{
    public class RequestCallDetail
    {
        public long callId { get; set; }
        public string token { get; set; }
        public string roomName { get; set; }
        public int callType { get; set; }
        public string callerName { get; set; }
    }
}
