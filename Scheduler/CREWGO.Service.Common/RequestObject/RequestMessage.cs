﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common.RequestObject
{
    public class RequestMessage
    {
        public int msgId { get; set; }
        public string msgTitle { get; set; }
        public string msgBody { get; set; }
        public DateTime msgDateTime { get; set; }
        public long jobId { get; set; }
        public int senderId { get; set; }
        public string senderName { get; set; }
        public bool deleteOption { get; set; }
        public bool canReply { get; set; }
    }
}