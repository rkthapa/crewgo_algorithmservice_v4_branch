﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common.RequestObject
{
    public class RequestStaffPosition
    {
        public int jobId { get; set; }
        public int jobShiftId { get; set; }
        public int customerId { get; set; }
        public int supervisorId { get; set; }
    }
}