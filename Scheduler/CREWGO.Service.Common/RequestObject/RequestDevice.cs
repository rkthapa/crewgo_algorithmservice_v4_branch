﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common.RequestObject
{
    public class RequestDevice
    {
        public int DeviceType { get; set; }
        public string DeviceToken { get; set; }
    }
}
