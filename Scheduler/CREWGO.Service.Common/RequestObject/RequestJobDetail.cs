﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common.RequestObject
{
    public class RequestJobDetail
    {
        public long alertId { get; set; }
        public string alertMessage { get; set; }
        public long jobId { get; set; }
        public long jobShiftId { get; set; }
        public int invoiceId { get; set; }
        public string jobTitle { get; set; }
        public string jobAddress { get; set; }
        public DateTime sentDate { get; set; }
    }
}