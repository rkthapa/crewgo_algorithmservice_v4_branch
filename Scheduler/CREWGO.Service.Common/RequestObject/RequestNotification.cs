﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common.RequestObject
{
    public class RequestNotification
    {
        public string title { get; set; }
        public string body { get; set; }
        public string icon { get; set; }
        public NotificationTypeEnum notificationType { get; set; }
        //public int userId { get; set; }
        public string userId { get; set; }
        public RequestMessage message { get; set; }
        public RequestJobDetail jobDetail { get; set; }
        public RequestStaffPosition staffPosition { get; set; }
        public RequestCallDetail callDetail { get; set; }
        public SinchNotificationDetail sinchNotification { get; set; }
    }
}