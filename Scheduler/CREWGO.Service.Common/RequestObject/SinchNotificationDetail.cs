﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.Common.RequestObject
{
    public class SinchNotificationDetail
    {
        public UserGroupEnum userGroup { get; set; }
        public int deviceType { get; set; }
        public List<string> deviceTokens { get; set; }
        public string payload { get; set; }
    }
}
