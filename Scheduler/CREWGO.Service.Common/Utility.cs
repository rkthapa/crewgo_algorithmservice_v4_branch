﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using HTN.Common;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using System.Net.Mail;
using System.Net;
using System.Threading;
using System.Security.AccessControl;
using System.Security.Principal;

namespace CREWGO.Service.Common
{
    public class Utility
    {
        public static T GetFieldValue<T>(string columnname, DataRow row)
        {
            if (row.Table.Columns.Contains(columnname) && 
                !row.IsNull(columnname))
            {
                return (T)Convert.ChangeType(row[columnname], typeof(T));
            }
            return default(T);
        }

        public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        public const string DateTimeDisplayFormat = "dd/MM/yyyy HH:mm:ss";
        public const string DateFormat = "yyyy-MM-dd";
        private const long MaxFileSize = 5242880;

        public static DateTime ConvertTime(
            DateTime localTime, string timezoneid)
        {
            TimeZoneInfo est = TimeZoneInfo.FindSystemTimeZoneById(timezoneid);
            DateTime targetTime = TimeZoneInfo.ConvertTime(localTime, est);
            return targetTime;
        }
        public static string ReplaceInvalidCharsWithUnderScore(
            string Value)
        {
            Value = Regex.Replace(Value, @"[?:;\/*""<>|&'+\s]", "_");  
            while (Value.Contains("__"))
            {
                Value = Value.Replace("__", "_");
            }
            return Value;
        }
        public static string ConverListToSQLWhereIN<T>(
            List<T> liste, string columnName, 
            bool isIn, bool hasOtherConditions)
        {
            var sb = new StringBuilder();
            if (liste.Any() && 
                    (typeof(T) == typeof(int) || 
                     typeof(T) == typeof(string)))
            {
                if (hasOtherConditions) sb.Append(" AND ");
                sb.AppendFormat(" {0} ", columnName);
                if (!isIn) sb.Append(" Not ");
                sb.Append(" IN (");
                foreach (var item in liste)
                {
                    sb.AppendFormat("'{0}',", item.ToString());
                }
                var index = sb.ToString().LastIndexOf(',');
                sb.Remove(index, 1);
                sb.Append(")");
                return sb.ToString();
            }
            else if (typeof(T).IsEnum && liste.Any())
            {
                if (hasOtherConditions) sb.Append(" AND ");
                sb.AppendFormat(" {0} ", columnName);
                if (!isIn) sb.Append(" Not ");
                sb.Append(" IN (");
                foreach (var item in liste)
                {
                    sb.AppendFormat("'{0}',", (int)Enum.Parse(typeof(T), 
                                        item.ToString()));
                }
                var index = sb.ToString().LastIndexOf(',');
                sb.Remove(index, 1);
                sb.Append(")");
                return sb.ToString();
            }
            return null;
        }
        public static List<string> GetEnumDesc<T>()
        {
            if (typeof(T).IsEnum)
            {
                var results = new List<string>();
                //var listEnum = Enum
                //                .GetValues(typeof(T))
                //                .Cast<T>()
                //                .Select( x => HelperFunctions.GetEnumDesc(x))
                //                .ToList<T>();
                return results;
            }
            return null;
        }
        public static List<int> 
            ConvertCommaSeperatedEncryptedStringToIntegerList(string source)
        {
            List<int> results = new List<int>();
            string[] encryptedIntegers = source.Split(new char[] { ',' }, 
                                        StringSplitOptions.RemoveEmptyEntries);
            if (encryptedIntegers != null && encryptedIntegers.Length > 0)
            {
                try
                {
                    foreach (string item in encryptedIntegers)
                    {
                        results.Add(Crypto.DecryptInt(item.Trim()));
                    }
                }
                catch { }
            }

            return results;
        }
        public static string ConvertArrayIntoCommaSeparatedString<T>(T[] array)
        {
            string result = string.Empty;

            if (array != null && array.Length > 0)
            {
                foreach (T item in array)
                {
                    result = string.Format("{0}{1}{2}", result, 
                      string.IsNullOrEmpty(result) ? string.Empty : ",", item);
                }
            }
            return result;
        }
        public static string ConvertIntArrayToDelimitedString(
            int[] intArray, 
            char delimiter)
        {
            string result = string.Empty;

            if (intArray != null && intArray.Length > 0)
            {
                foreach (int item in intArray)
                {
                    result = string.Format("{0}{1}{2}", result, 
                             string.IsNullOrEmpty(result) ? 
                                 string.Empty : delimiter.ToString(), 
                             item);
                }
            }
            return result;
        }
        /// <summary>
        /// Serializes an object to Xml as a string.
        /// </summary>
        /// <typeparam name="T">Datatype T.</typeparam>
        /// <param name="source">Object of type T to be serialized.</param>
        /// <returns>Xml string of serialized type T object.</returns>
        public static string SerializeToXmlString<T>(T source)
        {
            string xmlstream = String.Empty;

            using (MemoryStream memstream = new MemoryStream())
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                XmlTextWriter xmlWriter = new XmlTextWriter(
                                                    memstream, 
                                                    Encoding.UTF8
                                                );

                xmlSerializer.Serialize(xmlWriter, source);
                xmlstream = UTF8ByteArrayToString(
                            ((MemoryStream)xmlWriter.BaseStream).ToArray());
            }

            return xmlstream;
        }
        /// <summary>
        /// Deserializes Xml string of type T.
        /// </summary>
        /// <typeparam name="T">Datatype T.</typeparam>
        /// <param name="XmlString">Input Xml string to read from.</param>
        /// <returns>Returns rehydrated object of type T.</returns>
        public static T DeserializeXmlString<T>(string xmlString)
        {
            T tempObject = default(T);
            XmlSerializer xs = new XmlSerializer(typeof(T));
            byte[] byteArray = Encoding.Unicode.GetBytes(xmlString);
            var ms = new MemoryStream(byteArray);
            tempObject = (T)xs.Deserialize(ms);
            ms.Close();
            return tempObject;

        }
        public static uint ConvertToUnixDatetime(DateTime value)
        {
            var minUnixDate = new DateTime(1970, 1, 1);
            value = value < minUnixDate ? minUnixDate : value;
            long ticks = (value.ToUniversalTime().Ticks - 
                            DateTime.Parse("01/01/1970 00:00:00").Ticks);
            ticks = ticks < 0 ? 0 : ticks;
            return (uint)Convert.ToUInt32(ticks / 10000000);
        }
        private static string UTF8ByteArrayToString(Byte[] ArrBytes)
        {
            return new UTF8Encoding().GetString(ArrBytes);
        }
        private static Byte[] StringToUTF8ByteArray(String XmlString)
        {
            return new UTF8Encoding().GetBytes(XmlString);
        }
        public static bool IsStandardString(string s)
        {
            for (int x = 0; x < s.Length; x++)
            {
                if (char.GetUnicodeCategory(s[x]) == 
                    System.Globalization.UnicodeCategory.OtherLetter)
                {
                    return false;
                }
            }
            return true;
        }
        public static void MapObjects(object source, object destination)
        {
            Type sourcetype = source.GetType();
            Type destinationtype = destination.GetType();

            var sourceProperties = sourcetype.GetProperties();
            var destionationProperties = destinationtype.GetProperties();

            var commonproperties = from sp in sourceProperties
                                   join dp in destionationProperties 
                                   on new { sp.Name, sp.PropertyType } equals
                                       new { dp.Name, dp.PropertyType }
                                   select new { sp, dp };

            foreach (var match in commonproperties)
            {
                match.dp.SetValue(destination, 
                                    match.sp.GetValue(source, null), 
                                    null);
            }
        }

        /// <summary>
        /// Function to maintain logs.
        /// </summary>
        /// <param name="path">The path to store the log file.</param>
        /// <param name="text">Content to be written in the log file.</param>
        public static void WriteLogFile(string path, string text)
        {
            //try
            //{
            //    Directory.CreateDirectory(Path.GetDirectoryName(path));
            //    using (StreamWriter writer = new StreamWriter(path, true))
            //    {
            //        writer.WriteLine(DateTime.Now + " : " + text);
            //    }
            //}
            //catch(Exception)
            //{
            //    throw;
            //}
        }
        /// <summary>
        /// Sets up a system-wide lock, writes to file and releases the lock.
        /// Waits for lock to be released if it has not been released already.
        /// </summary>
        /// <param name="path">The filepath to write to.</param>
        /// <param name="text">The text to append to file.</param>
        public static void WriteLogToFileExclusively(string path, string text)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                try
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(path));
                }
                catch (ArgumentException)
                {
                    throw;
                }
                catch (PathTooLongException)
                {
                    throw;
                }

                // Mutex access control rules and identifier
                bool createdNew;
                var allowEveryoneRule =
                    new MutexAccessRule(
                        new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                        MutexRights.FullControl, 
                        AccessControlType.Allow);
                var securitySettings = new MutexSecurity();
                securitySettings.AddAccessRule(allowEveryoneRule);
                string mutexID = "LogMutex.CrewGo.Co";

                using (var mutex = new Mutex(
                        false, mutexID, out createdNew, securitySettings))
                {
                    mutex.WaitOne();
                    try
                    {
                        if (File.Exists(path) &&
                        new FileInfo(path).Length > MaxFileSize)
                        {
                            File.Delete(path);
                        }
                        File.AppendAllText(path, DateTime.Now + " : " + text);
                    }
                    catch (DirectoryNotFoundException)
                    {
                        throw;
                    }
                    catch (FileNotFoundException)
                    {
                        throw;
                    }
                    catch(Exception)
                    {
                        throw;
                    }
                    mutex.ReleaseMutex();
                }
            }).Start();
        }

        /// <summary>
        /// Send mail.        
        /// </summary>
        /// <param name="mailType">The type of mail to send.</param>
        /// <param name="content">Email Message</param>
        public static void SendEmail(EmailType mailType, string content)
        {
            string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            string xmlPath = 
                Path.Combine(applicationPath, @"Resources\EmailTemplate.xml");

            XPathDocument mailTemplate = new XPathDocument(xmlPath);
            XPathNavigator navigator = mailTemplate.CreateNavigator();
            MailMessage mail = new MailMessage();

            switch (mailType)
            {
                case EmailType.ERRORMAIL:
                    mail.From = new MailAddress(
                        navigator.SelectSingleNode("email/error/from").Value);
                    XPathNodeIterator node = 
                        navigator.Select("email/error/to/receiver");
                    while (node.MoveNext())
                    {
                        mail.To.Add(new MailAddress(node.Current.Value));
                    }
                    mail.Subject = 
                        navigator.SelectSingleNode("email/error/subject").Value;
                    mail.Body = String.Concat(
                            navigator
                                .SelectSingleNode("email/error/body").InnerXml, 
                            content
                        );

                    break;
                case EmailType.CONFIRMATIONMAIL:
                    //prepare confirmation body
                    break;
            }

            mail.IsBodyHtml = true;
            //mail.Attachments.Add(new Attachment(attachmentPath));
            SmtpClient client = new SmtpClient
            {
                Host = "smtp.mailtrap.io",
                Port = 465,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(
                    "1b84449f9333b2", 
                    "a930ac092a52bd"
                ),
                Timeout = 20000
            };
            client.Send(mail);
    }
    }
}