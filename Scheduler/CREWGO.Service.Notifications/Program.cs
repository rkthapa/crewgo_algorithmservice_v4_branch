﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;

namespace CREWGO.Service.Notifications
{
    class Program
    {
        #region PRIVATE FIELDS
        private static HttpClient client = new HttpClient();
        private static Stopwatch sw;
        #endregion

        static void Main(string[] args)
        {
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["CREWGOWEBAPI_PUBLISHED_STAGING"].ToString());
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            while (true)
            {
                Thread.Sleep(TimeSpan.FromMinutes(1));

                try
                {
                    sw = Stopwatch.StartNew();
                    Modules.CREWGONotifications.ScheduleNotifications(client).Wait();
                    sw.Stop();

                    Modules.CREWGONotifications.MaintainLog(sw);
                    Console.WriteLine("");
                    Console.WriteLine("Successful Execution");
                    Console.WriteLine("Notification Algorithm TimeLapse [{0}] = {1}ms", DateTime.Now, sw.ElapsedMilliseconds);
                }
                catch (Exception ex)
                {
                    sw.Stop();
                    Console.WriteLine(System.Environment.NewLine);
                    Console.WriteLine("Unsuccessful Execution - " + DateTime.Now + "\n" + ex.Message + "\n" + ex.StackTrace);
                }

                //Console.ReadKey(true);
            }
        }
    }
}