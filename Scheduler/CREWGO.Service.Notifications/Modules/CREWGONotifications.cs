﻿using CREWGO.Service.BusinessService;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using CREWGO.Service.Common.RequestObject;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CREWGO.Service.Notifications.Modules
{
    public class CREWGONotifications
    {
        public static string errorPath = System.AppDomain.CurrentDomain.BaseDirectory + "/Logs/ErrorLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";

        #region PRIVATE VARIABLES
        private static string alertBody = string.Empty;
        private static Enums.NotificationTypeEnum alertType = 0;
        private static RequestJobDetail requestJobDetail = null;
        #endregion

        #region PUSHNOTIFICATION LOGIC
        public static async Task ScheduleNotifications(HttpClient cons)
        {
            try
            {
                bool isSuccess = false;

                //Get list of notifications from database.
                var preNotifications = await GetPreNotifications(cons);

                Console.WriteLine("[" + DateTime.Now + "] NOTIFICATION COUNT = " + preNotifications.Count<PreNotification>() + "\n");


                if (preNotifications.Count<PreNotification>() >= 0)
                {
                    foreach (var notification in preNotifications)
                    {
                        if (notification.ActiveDate >= DateTime.Now)
                        {
                            Console.WriteLine("[" + DateTime.Now + "] NOTIFICATION [" + notification.NotificationId + "] " + notification.Type.ToUpper());
                        }
                        else
                        {
                            Console.WriteLine("[" + DateTime.Now + "] NOTIFICATION [" + notification.NotificationId + "] " + notification.Type.ToUpper());

                            //Get respective user
                            var responseUser = await GetUserById(cons, notification.UserId);

                            if (responseUser != null)
                            {
                                //Set notification attributes
                                SetNotificationAttributes(cons, notification);
                                Console.WriteLine("[" + DateTime.Now + "] NOTIFICATION [" + requestJobDetail.alertId + "] " + alertType + " [" + responseUser.Email + "]");

                                //List of device tokens [Android|IOS]
                                var iosTokens = new List<string>();
                                var androidTokens = new List<string>();

                                //If single device token
                                if (responseUser.UserDevice != null && !string.IsNullOrEmpty(responseUser.UserDevice.DeviceToken))
                                {
                                    Console.WriteLine("[" + DateTime.Now + "] NOTIFICATION [" + notification.NotificationId + "] STAFF");
                                    if ((Enums.DeviceTypeEnum)responseUser.UserDevice.DeviceType == Enums.DeviceTypeEnum.ANDROID)
                                        androidTokens.Add(responseUser.UserDevice.DeviceToken);
                                    else
                                        iosTokens.Add(responseUser.UserDevice.DeviceToken);
                                }

                                //If multiple device token
                                if (responseUser.UserDevices != null && responseUser.UserDevices.Count > 0)
                                {
                                    Console.WriteLine("[" + DateTime.Now + "] NOTIFICATION [" + notification.NotificationId + "] CUSTOMER/SUPERVISOR");
                                    foreach (var userDevice in responseUser.UserDevices)
                                    {
                                        if (!string.IsNullOrEmpty(userDevice.DeviceToken))
                                        {
                                            if ((Enums.DeviceTypeEnum)userDevice.DeviceType == Enums.DeviceTypeEnum.ANDROID)
                                                androidTokens.Add(userDevice.DeviceToken);
                                            else
                                                iosTokens.Add(userDevice.DeviceToken);
                                        }
                                    }
                                }

                                isSuccess = await new PushNotifications().SendPushNotification(1, alertBody, alertType, androidTokens, iosTokens, null, requestJobDetail, (Enums.UserGroupEnum)responseUser.GroupId);
                                Console.WriteLine("[" + DateTime.Now + "] NOTIFICATION [" + notification.NotificationId + "] PUSH STATUS [" + isSuccess.ToString().ToUpper() + "]");

                                if (isSuccess)
                                    await PutPreNotification(cons, notification);
                            }
                        }
                        Console.WriteLine("");
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.WriteLogToFileExclusively(errorPath, "Unsuccessful Execution \r\n" + ex.Message + "\r\n" + ex.StackTrace);
            }
        }
        #endregion

        #region GET METHODS
        static async Task<IEnumerable<PreNotification>> GetPreNotifications(HttpClient cons)
        {
            var serviceUrl = "api/crewgo/getprenotifications";

            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                var responseNotification = JsonConvert.DeserializeObject<IEnumerable<PreNotification>>(jsonString);

                return responseNotification;
            }

            return null;
        }
        static async Task<User> GetUserById(HttpClient cons, int userId)
        {
            var serviceUrl = "api/crewgo/getuserbyid/" + userId;

            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            

            if (response.IsSuccessStatusCode)
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                var responseUser = JsonConvert.DeserializeObject<User>(jsonString);

                return responseUser;
            }

            return null;
        }
        static async Task<Job> GetJobById(HttpClient cons, int jobId)
        {
            var serviceUrl = "api/crewgo/getjobbyid/" + jobId;

            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            

            if (response.IsSuccessStatusCode)
            { 
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                var responseJob = JsonConvert.DeserializeObject<Job>(jsonString);

                return responseJob;
            }

            return null;
        }
        static async Task<JobShifts> GetJobShiftById(HttpClient cons, int jobShiftId)
        {
            var serviceUrl = "api/crewgo/getshiftbyid/" + jobShiftId;

            HttpResponseMessage response = await cons.GetAsync(serviceUrl);
            

            if (response.IsSuccessStatusCode) 
            {
                //Get the response
                var jsonString = await response.Content.ReadAsStringAsync();

                //Deserialize the data
                var responseJob = JsonConvert.DeserializeObject<JobShifts>(jsonString);

                return responseJob;
            }

            return null;
        }
        #endregion

        #region PUT METHODS
        static async Task PutPreNotification(HttpClient cons, PreNotification obj)
        {
            HttpResponseMessage response = await cons.GetAsync("api/prenotification/" + obj.NotificationId);
            

            if (response.IsSuccessStatusCode)
            {
                tbl_pre_notification notification = await response.Content.ReadAsAsync<tbl_pre_notification>();
                notification.id = obj.NotificationId;
                notification.sent_date = DateTime.Now;
                notification.is_sent = 1;
                notification.is_scheduled = true;

                response = await cons.PutAsJsonAsync("api/prenotification/" + obj.NotificationId, notification);
            }
        }
        #endregion

        #region CUSTOM METHODS
        public static async void MaintainLog(Stopwatch sw)
        {
            await Task.Run(() =>
            {
                string content = string.Empty;
                string path = System.AppDomain.CurrentDomain.BaseDirectory + "/Logs/" + DateTime.Now.ToString("ddMMyyyy") + ".txt";

                content = string.Format(System.Environment.NewLine + "Notification Algorithm Runtime [{0}] = {1}ms", DateTime.Now, sw.ElapsedMilliseconds);

                Utility.WriteLogToFileExclusively(path, content);
            });
        }
        #endregion

        public static async void SetNotificationAttributes(HttpClient cons, PreNotification notification)
        {
            alertBody = notification.Message;

            requestJobDetail = new RequestJobDetail();
            requestJobDetail.alertId = notification.NotificationId;
            requestJobDetail.alertMessage = notification.Message;
            requestJobDetail.sentDate = DateTime.Now;

            // Temporary 
            switch (notification.Type)
            {
                case "JobPosted":
                case "job_posted":
                    alertType = Enums.NotificationTypeEnum.JOBPOSTED;
                    requestJobDetail.jobId = notification.SourceId;

                    break;
                case "JobAccepted":
                case "job_accepted":
                    alertType = Enums.NotificationTypeEnum.JOBACCEPTED;
                    requestJobDetail.jobShiftId = notification.SourceId;

                    break;
                case "JobCompleted":
                case "job_completed":
                    alertType = Enums.NotificationTypeEnum.JOBCOMPLETED;
                    requestJobDetail.jobId = notification.SourceId;

                    break;
                case "JobCancelled":
                case "job_canceled":
                    alertType = Enums.NotificationTypeEnum.JOBCANCELLED;
                    requestJobDetail.jobId = notification.SourceId;

                    break;
                case "JobUpdated":
                case "job_updated":
                    alertType = Enums.NotificationTypeEnum.JOBUPDATED;
                    requestJobDetail.jobId = notification.SourceId;

                    break;
                case "JobStart":
                case "before_job_start":
                    alertType = Enums.NotificationTypeEnum.BEFOREJOBSTART;
                    requestJobDetail.jobId = notification.SourceId;

                    break;
                case "StaffLeavesJobSite":
                case "staff_leaves_site":
                    alertType = Enums.NotificationTypeEnum.STAFFLEFT;
                    requestJobDetail.jobShiftId = notification.SourceId;

                    break;
                case "MissToCompleteJob":
                case "miss_to_complete_job":
                    alertType = Enums.NotificationTypeEnum.MISSEDTOCOMPLETE;
                    requestJobDetail.jobShiftId = notification.SourceId;

                    break;
                case "PaymentMade":
                case "payment_made":
                    alertType = Enums.NotificationTypeEnum.PAYMENTMADE;
                    requestJobDetail.jobId = notification.SourceId;

                    break;
                case "TimesheetApproved":
                    alertType = Enums.NotificationTypeEnum.TIMESHEETAPPROVED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;
                case "TimesheetCompleted":
                    alertType = Enums.NotificationTypeEnum.TIMESHEETCOMPLETED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;
                default:
                    alertType = Enums.NotificationTypeEnum.JOBALERTS;

                    break;
            }

            if (notification.Type == "JobAccepted" || notification.Type == "StaffLeavesJobSite" || notification.Type == "MissToCompleteJob" || notification.Type == "job_accepted" || notification.Type == "staff_leaves_site" || notification.Type == "miss_to_complete_job")
            {
                var responseShift = await GetJobShiftById(cons, requestJobDetail.jobShiftId);
                if (responseShift != null)
                {
                    requestJobDetail.jobId = responseShift.JobId;
                    requestJobDetail.jobTitle = responseShift.ParentJob.JobTitle;
                    requestJobDetail.jobAddress = responseShift.ParentJob.JobFullAddress;
                }
            }
            else
            {
                var responseJob = await GetJobById(cons, requestJobDetail.jobId);
                if (responseJob != null)
                {
                    requestJobDetail.jobTitle = responseJob.JobTitle;
                    requestJobDetail.jobAddress = responseJob.JobFullAddress;
                }
            }        
        }
    }
}