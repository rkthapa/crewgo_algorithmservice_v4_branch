﻿using System;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using CREWGO.Service.Common;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.TaskSchedulerFront.Modules;

namespace CREWGO.Service.UnitTests.TaskSchedulerTests.ModuleTests
{
    [TestFixture]
    public class CREWGONotificationsTest
    {
        #region Test Data
        private ICREWGOService serviceMock;
        private List<PreNotification> prenotifications;
        private User firstUser, secondUser;
        private JobShift firstShift, secondShift;
        private Job firstJob, secondJob;
        private DeviceInfo firstDevice, secondDevice;
        private List<DeviceInfo> devices;
        #endregion
        /// <summary>
        /// Initialize test data
        /// </summary>
        [SetUp]
        public void TestInit()
        {
            #region Prenotifications Mock Object
            var firstPN = new PreNotification()
            {
                NotificationId = 8672,
                SourceId = 1657,
                GeneratedBy = 10,
                Type = "JobAccepted",
                UserId = 10,
                GroupId = 4,
                Email = "bipin@spearls.com",
                Message = "#JD-1151 :   is accepted by Bipin Spearls just now.",
                Detail = null,
                ActiveDate = new DateTime(2017, 07, 19, 11, 31,22),
                IsSent = false,
                SentDate = new DateTime(2017, 07, 19, 11, 16, 22),
                IsScheduled = false
            };
            var secondPN = new PreNotification()
            {
                NotificationId = 8718,
                SourceId = 31955,
                GeneratedBy = 0,
                Type = "OnGoingJob",
                UserId = 170,
                GroupId = 4,
                Email = "anak@grr.la",
                Message = "Your scheduled Job will begin at 05:00am. Check In.",
                Detail = null,
                ActiveDate = new DateTime(2017, 07, 16, 17, 30, 0),
                IsSent = false,
                SentDate = new DateTime(2017, 07, 19, 11, 25, 01),
                IsScheduled = false
            };
            prenotifications = new List<PreNotification>();
            prenotifications.Add(firstPN);
            prenotifications.Add(secondPN);
            #endregion
            #region Device Mock Object
            firstDevice = new DeviceInfo()
            {
                DeviceInfoId = 8010,
                UserId = 350,
                DeviceId = "c59781677ad83e06",
                HashCode = "JodouHRN90iz7QZrUgCiRpuYO0Tm01jK",
                DeviceType = 2,
                Token = "12254202-76b6-11e7-989d-001dd8b73106",
                TokenFlag = null,
                DeviceName = "",
                DeviceModel = "",
                OsVersion = "",
                LastActivity = null,
                CreatedDate = new DateTime(2017, 07, 17, 11, 5, 2),
                GroupId = 3,
                DeviceToken = "fODfpQYeXdw:APA91bEoPOXsVxWWU7bWv-P8MeVmoWvX3Fq1CjXEcrWSCS0vlPZ6rAE74Ey5IzW_7xDFasc2-py1gE7LCdaNf_VWvNaMMW2GA5VOECuf7p6pqiSx9goYHBcPlmOW28of4QNt5uXOHcGX"
            };
            secondDevice = new DeviceInfo()
            {
                DeviceInfoId = 8068,
                UserId = 350,
                DeviceId = "403eeb1878641b8a",
                HashCode = "K4tP9oEd0OYN0n7OmGyjMWl7uRDqWw6H",
                DeviceType = 2,
                Token = "4d2193b0-776a-11e7-989d-001dd8b73106",
                TokenFlag = null,
                DeviceName = "",
                DeviceModel = "",
                OsVersion = "",
                LastActivity = null,
                CreatedDate = new DateTime(2017, 07, 17, 11, 5, 2),
                GroupId = 3,
                DeviceToken = "czujRPE_1-s:APA91bH6baLmM12lt9jjLCLyR0-NpHytU_c-kSbvOy8Hg4c_9fVtnNC7Yr7TonYd2jHDVKTEm3dyk_AGT6TgT9h0TeztzyoUlHcjmKm4XxVSOwfkWMFCe0VmtHcOA2aTllG0fwcBPUZb"
            };
            devices = new List<DeviceInfo>();
            devices.Add(firstDevice);
            devices.Add(secondDevice);
            #endregion
            #region Users Mock Object
            firstUser = new User()
            {
                ActivationCode = "",
                Active = 1,
                CreatedOn = new DateTime(2017, 1, 1, 12, 0, 0),
                CustomerUserId = 0,
                Description = "",
                Email = "customer1@grr.la",
                EmailVerified = true,
                EnteredDate = new DateTime(2017, 8, 3, 5, 51, 56),
                FacebookId = "0",
                ForgottenPasswordCode = "",
                ForgottenPasswordTime = null,
                FullAddress = "Melbourne Street, South Brisbane, Queensland, Australia",
                GoogleId = "0",
                GroupId = 3,
                InactivatedCode = new DateTime(2001, 1, 1, 12, 0, 0),
                IPAddress = "",
                LastLogin = 1501739516,
                LHCUserId = 208,
                LoginFrom = "APP",
                Name = "Umesh1 Tandukar",
                Password = "$2y$10$gfp51h.XveFt/09KOXMjnuZaLHVtgru8SPFjfEYfZ54H8F4bh8b86",
                PhoneNumber = "0417 121 212",
                PostCodeId = null,
                ProfileImage = "",
                RegisterFrom = "LHC",
                RememberCode = "",
                Salt = "",
                Street = "",
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                UserDevice = firstDevice,
                UserDevices = null,
                UserId = 227,
                Username = ""
            };
            secondUser = new User() 
            {
                ActivationCode = "",
                Active = 1,
                CreatedOn = new DateTime(2017, 1, 1, 12, 0, 0),
                CustomerUserId = 0,
                Description = "",
                Email = "customer2@grr.la",
                EmailVerified = true,
                EnteredDate = new DateTime(2017, 8, 3, 5, 51, 56),
                FacebookId = "0",
                ForgottenPasswordCode = "",
                ForgottenPasswordTime = null,
                FullAddress = "New Street, Queensland, Australia",
                GoogleId = "0",
                GroupId = 3,
                InactivatedCode = new DateTime(2001, 1, 1, 12, 0, 0),
                IPAddress = "",
                LastLogin = 1501648547,
                LHCUserId = 209,
                LoginFrom = "APP",
                Name = "Umesh1 Tandukar",
                Password = "$2y$10$1BGl65hX1sLUqv9lsnzVvevJB.wQIptktqBwtdnE6sxj5BE6WLc6m",
                PhoneNumber = "0417 121 212",
                PostCodeId = null,
                ProfileImage = "",
                RegisterFrom = "LHC",
                RememberCode = "",
                Salt = "",
                Street = "",
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                UserDevice = null,
                UserDevices = devices,
                UserId = 227,
                Username = ""
            };
            #endregion
            #region Job Mock Object
            firstJob = new Job()
            {
                BookAmount = (decimal)18.33,
                BookingNumber = "",
                CustomerUserId = 21,
                Description = "",
                EnteredBy = 21,
                EnteredDate = new DateTime(2017, 07, 17, 11, 5, 2),
                HasPeakPrice = false,
                InductionRequired = false,
                JobFullAddress = "Kupandole, Nepal",
                JobId = 1151,
                JobLocationLatitude = (decimal)27.686913,
                JobLocationLongitude = (decimal)85.315082,
                JobNumber = "JD-1151",
                JobPostCodeId = 14179,
                JobStreet = "",
                JobTitle = "",
                LHCUserId = 14,
                MeetingFullAddress = "Kupandole, Nepal",
                MeetingLatitude = (decimal)27.686913,
                MeetingLongitude = (decimal)85.315082,
                MeetingPostCodeId = null,
                MeetingStreet = "",
                PeakPriceReason = "",
                QuoteId = "ECENEDDVDU",
                ShiftStatus = 3,
                Status = 2,
                SupervisorUserId = null,
                UpdatedBy = null,
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0),
            };
            secondJob = new Job()
            {
                BookAmount = (decimal)24800,
                BookingNumber = "0767910530",
                CustomerUserId = 21,
                Description = "",
                EnteredBy = 21,
                EnteredDate = new DateTime(2017, 07, 14, 6, 45, 1),
                HasPeakPrice = false,
                InductionRequired = false,
                JobFullAddress = "Haymarket, New South Wales, Australia",
                JobId = 1903,
                JobLocationLatitude = (decimal)-33.88092,
                JobLocationLongitude = (decimal)151.20294,
                JobNumber = "JD-1903",
                JobPostCodeId = 718,
                JobStreet = "",
                JobTitle = "",
                LHCUserId = 14,
                MeetingFullAddress = "681 George Street Haymarket NSW 2000 ",
                MeetingLatitude = (decimal)-33.879817312,
                MeetingLongitude = (decimal)151.20498687,
                MeetingPostCodeId = null,
                MeetingStreet = "",
                PeakPriceReason = "",
                QuoteId = "VTNOEUHANV",
                ShiftStatus = 4,
                Status = 2,
                SupervisorUserId = null,
                UpdatedBy = null,
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0)
            };
            #endregion
            #region Shift Mock Object
            firstShift = new JobShift()
            {
                CompletedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2017, 7, 20, 11, 45, 0),
                EndTimeBreak = null,
                HasPeakPrice = false,
                HourlyRate = null,
                IsCompleted = false,
                IsFilled = false,
                JobId = 1151,
                JobShiftId = 1657,
                LevelId = 5,
                ParentJob = firstJob,
                PeakPrice = null,
                Qualification = "",
                RequiredNumber = 2,
                ShiftStatus = 4,
                SkillId = 1,
                StartTime = new DateTime(2017, 7, 20, 10, 50, 0),
                TotalCost = (decimal)18,
                TotalHour = "0.5",
                UpdatedBy = null,
                UpdatedDate = new DateTime(2017, 1, 1, 12, 0, 0)
            };
            secondShift = new JobShift() 
            {
                CompletedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2017, 7, 17, 3, 45, 0),
                EndTimeBreak = null,
                HasPeakPrice = false,
                HourlyRate = null,
                IsCompleted = false,
                IsFilled = false,
                JobId = 1903,
                JobShiftId = 31955,
                LevelId = 3,
                ParentJob = secondJob,
                PeakPrice = null,
                Qualification = "",
                RequiredNumber = 6,
                ShiftStatus = 4,
                SkillId = 13,
                StartTime = new DateTime(2017, 7, 16, 6, 30, 0),
                TotalCost = (decimal)1086,
                TotalHour = "9.25",
                UpdatedBy = null,
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0)
            };
            #endregion
            // Construct a Mock Object of the ICREWGOService Interface
            serviceMock = Substitute.For<ICREWGOService>();
        }
        [Test]
        public void ErrorPathCheck()
        {
            var notifications = new CREWGONotifications();
            Assert.DoesNotThrow(() =>
                Utility.WriteLogToFileExclusively(
                    notifications.ErrorPath, 
                    "Test Write" 
                )
            );
        }
        [Test]
        public void LogPathCheck()
        {
            var notifications = new CREWGONotifications();
            Assert.DoesNotThrow(() =>
                Utility.WriteLogToFileExclusively(
                    notifications.LogPath, 
                    "Test Write" 
                )
            );
        }
        [Test]
        public void ScheduleNotificationsTest()
        {
            // Return Mock Objects For Service Calls
            //serviceMock.GetPreNotifications().Returns(prenotifications);
            serviceMock.GetUserById(227).Returns(firstUser);
            serviceMock.GetUserById(228).Returns(secondUser);
            serviceMock.GetJobShiftById(1657).Returns(firstShift);
            serviceMock.GetJobShiftById(31955).Returns(secondShift);

            Assert.DoesNotThrowAsync(() =>
                new CREWGONotifications(serviceMock).ScheduleNotifications());
        }
        [TearDown]
        public void EndTest()
        {
            prenotifications = null;
            firstUser = null;
            secondUser = null;
            firstShift = null;
            secondShift = null;
            firstJob = null; 
            secondJob = null;
            firstDevice = null; 
            secondDevice = null;
            devices = null;
        }
    }
}