﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using NSubstitute;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.TaskSchedulerFront.Modules;

namespace CREWGO.Service.UnitTests.TaskSchedulerTests.ModuleTests
{
    [TestFixture]
    public class CREWGOPartnerAlgorithmTest
    {
        #region Test Data
        private ICREWGOService serviceMock;
        private int shiftNoticePeriod;
        private JobShift firstShift, secondShift;
        private DeviceInfo firstDevice, secondDevice;
        private List<DeviceInfo> devices;
        private User firstUser, secondUser;
        private List<User> users;
        private Job firstJob, secondJob;
        private int jobAlertCount;

        #endregion
        /// <summary>
        /// Initialize test data
        /// </summary>
        [SetUp]
        public void TestInit()
        {
            shiftNoticePeriod = 2;
            jobAlertCount = 1;
            #region Device Mock Object
            firstDevice = new DeviceInfo()
            {
                DeviceInfoId = 8010,
                UserId = 350,
                DeviceId = "c59781677ad83e06",
                HashCode = "JodouHRN90iz7QZrUgCiRpuYO0Tm01jK",
                DeviceType = 2,
                Token = "12254202-76b6-11e7-989d-001dd8b73106",
                TokenFlag = null,
                DeviceName = "",
                DeviceModel = "",
                OsVersion = "",
                LastActivity = null,
                CreatedDate = new DateTime(2017, 07, 17, 11, 5, 2),
                GroupId = 3,
                DeviceToken = "fODfpQYeXdw:APA91bEoPOXsVxWWU7bWv-P8MeVmoWvX3Fq1CjXEcrWSCS0vlPZ6rAE74Ey5IzW_7xDFasc2-py1gE7LCdaNf_VWvNaMMW2GA5VOECuf7p6pqiSx9goYHBcPlmOW28of4QNt5uXOHcGX"
            };
            secondDevice = new DeviceInfo()
            {
                DeviceInfoId = 8068,
                UserId = 350,
                DeviceId = "403eeb1878641b8a",
                HashCode = "K4tP9oEd0OYN0n7OmGyjMWl7uRDqWw6H",
                DeviceType = 2,
                Token = "4d2193b0-776a-11e7-989d-001dd8b73106",
                TokenFlag = null,
                DeviceName = "",
                DeviceModel = "",
                OsVersion = "",
                LastActivity = null,
                CreatedDate = new DateTime(2017, 07, 17, 11, 5, 2),
                GroupId = 3,
                DeviceToken = "czujRPE_1-s:APA91bH6baLmM12lt9jjLCLyR0-NpHytU_c-kSbvOy8Hg4c_9fVtnNC7Yr7TonYd2jHDVKTEm3dyk_AGT6TgT9h0TeztzyoUlHcjmKm4XxVSOwfkWMFCe0VmtHcOA2aTllG0fwcBPUZb"
            };
            devices = new List<DeviceInfo>();
            devices.Add(firstDevice);
            devices.Add(secondDevice);
            #endregion
            #region Users Mock Object
            firstUser = new User()
            {
                ActivationCode = "",
                Active = 1,
                CreatedOn = new DateTime(2017, 1, 1, 12, 0, 0),
                CustomerUserId = 0,
                Description = "",
                Email = "customer1@grr.la",
                EmailVerified = true,
                EnteredDate = new DateTime(2017, 8, 3, 5, 51, 56),
                FacebookId = "0",
                ForgottenPasswordCode = "",
                ForgottenPasswordTime = null,
                FullAddress = "Melbourne Street, South Brisbane, Queensland, Australia",
                GoogleId = "0",
                GroupId = 3,
                InactivatedCode = new DateTime(2001, 1, 1, 12, 0, 0),
                IPAddress = "",
                LastLogin = 1501739516,
                LHCUserId = 208,
                LoginFrom = "APP",
                Name = "Umesh1 Tandukar",
                Password = "$2y$10$gfp51h.XveFt/09KOXMjnuZaLHVtgru8SPFjfEYfZ54H8F4bh8b86",
                PhoneNumber = "0417 121 212",
                PostCodeId = null,
                ProfileImage = "",
                RegisterFrom = "LHC",
                RememberCode = "",
                Salt = "",
                Street = "",
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                UserDevice = firstDevice,
                UserDevices = null,
                UserId = 227,
                Username = ""
            };
            secondUser = new User()
            {
                ActivationCode = "",
                Active = 1,
                CreatedOn = new DateTime(2017, 1, 1, 12, 0, 0),
                CustomerUserId = 0,
                Description = "",
                Email = "customer2@grr.la",
                EmailVerified = true,
                EnteredDate = new DateTime(2017, 8, 3, 5, 51, 56),
                FacebookId = "0",
                ForgottenPasswordCode = "",
                ForgottenPasswordTime = null,
                FullAddress = "New Street, Queensland, Australia",
                GoogleId = "0",
                GroupId = 3,
                InactivatedCode = new DateTime(2001, 1, 1, 12, 0, 0),
                IPAddress = "",
                LastLogin = 1501648547,
                LHCUserId = 209,
                LoginFrom = "APP",
                Name = "Umesh1 Tandukar",
                Password = "$2y$10$1BGl65hX1sLUqv9lsnzVvevJB.wQIptktqBwtdnE6sxj5BE6WLc6m",
                PhoneNumber = "0417 121 212",
                PostCodeId = null,
                ProfileImage = "",
                RegisterFrom = "LHC",
                RememberCode = "",
                Salt = "",
                Street = "",
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                UserDevice = null,
                UserDevices = devices,
                UserId = 227,
                Username = ""
            };
            users = new List<User>();
            users.Add(firstUser);
            users.Add(secondUser);
            #endregion
            #region Job Mock Object
            firstJob = new Job()
            {
                BookAmount = (decimal)18.33,
                BookingNumber = "",
                CustomerUserId = 21,
                Description = "",
                EnteredBy = 21,
                EnteredDate = new DateTime(2017, 07, 17, 11, 5, 2),
                HasPeakPrice = false,
                InductionRequired = false,
                JobFullAddress = "Kupandole, Nepal",
                JobId = 1151,
                JobLocationLatitude = (decimal)27.686913,
                JobLocationLongitude = (decimal)85.315082,
                JobNumber = "JD-1151",
                JobPostCodeId = 14179,
                JobStreet = "",
                JobTitle = "",
                LHCUserId = 14,
                MeetingFullAddress = "Kupandole, Nepal",
                MeetingLatitude = (decimal)27.686913,
                MeetingLongitude = (decimal)85.315082,
                MeetingPostCodeId = null,
                MeetingStreet = "",
                PeakPriceReason = "",
                QuoteId = "ECENEDDVDU",
                ShiftStatus = 3,
                Status = 2,
                SupervisorUserId = null,
                UpdatedBy = null,
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0),
            };
            secondJob = new Job()
            {
                BookAmount = (decimal)24800,
                BookingNumber = "0767910530",
                CustomerUserId = 21,
                Description = "",
                EnteredBy = 21,
                EnteredDate = new DateTime(2017, 07, 14, 6, 45, 1),
                HasPeakPrice = false,
                InductionRequired = false,
                JobFullAddress = "Haymarket, New South Wales, Australia",
                JobId = 1903,
                JobLocationLatitude = (decimal)-33.88092,
                JobLocationLongitude = (decimal)151.20294,
                JobNumber = "JD-1903",
                JobPostCodeId = 718,
                JobStreet = "",
                JobTitle = "",
                LHCUserId = 14,
                MeetingFullAddress = "681 George Street Haymarket NSW 2000 ",
                MeetingLatitude = (decimal)-33.879817312,
                MeetingLongitude = (decimal)151.20498687,
                MeetingPostCodeId = null,
                MeetingStreet = "",
                PeakPriceReason = "",
                QuoteId = "VTNOEUHANV",
                ShiftStatus = 4,
                Status = 2,
                SupervisorUserId = null,
                UpdatedBy = null,
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0)
            };
            #endregion
            #region Shift Mock Object
            firstShift = new JobShift()
            {
                CompletedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2017, 7, 20, 11, 45, 0),
                EndTimeBreak = null,
                HasPeakPrice = false,
                HourlyRate = null,
                IsCompleted = false,
                IsFilled = false,
                JobId = 1151,
                JobShiftId = 1657,
                LevelId = 5,
                ParentJob = firstJob,
                PeakPrice = null,
                Qualification = "",
                RequiredNumber = 2,
                ShiftStatus = 4,
                SkillId = 1,
                StartTime = new DateTime(2017, 7, 20, 10, 50, 0),
                TotalCost = (decimal)18,
                TotalHour = "0.5",
                UpdatedBy = null,
                UpdatedDate = new DateTime(2017, 1, 1, 12, 0, 0)
            };
            secondShift = new JobShift()
            {
                CompletedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2017, 7, 17, 3, 45, 0),
                EndTimeBreak = null,
                HasPeakPrice = false,
                HourlyRate = null,
                IsCompleted = false,
                IsFilled = false,
                JobId = 1903,
                JobShiftId = 31955,
                LevelId = 3,
                ParentJob = secondJob,
                PeakPrice = null,
                Qualification = "",
                RequiredNumber = 6,
                ShiftStatus = 4,
                SkillId = 13,
                StartTime = new DateTime(2017, 7, 16, 6, 30, 0),
                TotalCost = (decimal)1086,
                TotalHour = "9.25",
                UpdatedBy = null,
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0)
            };
            #endregion

            #region Initialize Service Mock
            // Construct a Mock Object of the ICREWGOService Interface
            serviceMock = Substitute.For<ICREWGOService>();
            // Return Mock Objects For Service Calls
            serviceMock.GetStaffByManualPriority(1,1, null, null)
                .ReturnsForAnyArgs(users);
            serviceMock.GetStaffByGlobalPriority(1, 1, 1, 1, 1, null, null)
                .ReturnsForAnyArgs(users);
            serviceMock.GetStaffByGeneralPriority(1, 1, 1, 1, 1, null, null)
                .ReturnsForAnyArgs(users);
            serviceMock.OrderPriorityExists(1657).Returns(true);
            serviceMock.OrderPriorityExists(31955).Returns(false);
            serviceMock.GlobalPriorityExists(14,13,3).Returns(true);
            serviceMock.GlobalPriorityExists(14,1,5).Returns(false);
            serviceMock.GetJobAlertCount(1).ReturnsForAnyArgs(jobAlertCount);
            #endregion
        }
        [Test]
        public void PartnerAlgorithmManualTest()
        {
            Assert.DoesNotThrowAsync(() =>
                new CREWGOPartnerAlgorithm(serviceMock)
                    .RunPartnerAlgorithm(firstShift, shiftNoticePeriod)
            );
        }
        [Test]
        public void PartnerAlgorithmGlobalTest()
        {
            Assert.DoesNotThrowAsync(() =>
                new CREWGOPartnerAlgorithm(serviceMock)
                    .RunPartnerAlgorithm(secondShift, shiftNoticePeriod)
            );
        }
        [Test]
        public void PartnerAlgorithmGeneralTest()
        {
            serviceMock.GlobalPriorityExists(14, 13, 3).Returns(false);
            Assert.DoesNotThrowAsync(() =>
                new CREWGOPartnerAlgorithm(serviceMock)
                    .RunPartnerAlgorithm(secondShift, shiftNoticePeriod)
            );
        }
        [Test]
        public void PartnerAlgorithmManualNoUserTest()
        {
            users = null;
            serviceMock.GetStaffByManualPriority(1, 1, null, null)
                .ReturnsForAnyArgs(users);
            Assert.DoesNotThrowAsync(() =>
                new CREWGOPartnerAlgorithm(serviceMock)
                    .RunPartnerAlgorithm(firstShift, shiftNoticePeriod)
            );
        }
        [Test]
        public void PartnerAlgorithmGlobalNoUserTest()
        {
            users = null;
            serviceMock.GetStaffByGlobalPriority(1, 1, 1, 1, 1, null, null)
                .ReturnsForAnyArgs(users);
            serviceMock.GetStaffByGeneralPriority(
                14, 31955, 13, 3, 6, new DateTime(2017, 7, 16, 6, 30, 0), 
                new DateTime(2017, 7, 17, 3, 45, 0)).ReturnsForAnyArgs(users);
            Assert.DoesNotThrowAsync(() =>
                new CREWGOPartnerAlgorithm(serviceMock)
                    .RunPartnerAlgorithm(secondShift, shiftNoticePeriod)
            );
        }
        [Test]
        public void PartnerAlgorithmGeneralNoUserTest()
        {
            serviceMock.GlobalPriorityExists(14, 13, 3).Returns(false);
            users = null;
            serviceMock.GetStaffByGeneralPriority(1, 1, 1, 1, 1, null, null)
                .ReturnsForAnyArgs(users);
            Assert.DoesNotThrowAsync(() =>
                new CREWGOPartnerAlgorithm(serviceMock)
                    .RunPartnerAlgorithm(secondShift, shiftNoticePeriod)
            );
        }
        [TearDown]
        public void EndTest()
        {
            serviceMock = null;
            firstUser = null;
            secondUser = null;
            users = null;
            firstShift = null;
            secondShift = null;
            firstJob = null;
            secondJob = null;
            firstDevice = null;
            secondDevice = null;
            devices = null;
        }
    }
}