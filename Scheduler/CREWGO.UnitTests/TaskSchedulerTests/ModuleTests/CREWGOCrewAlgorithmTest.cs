﻿using System.Collections.Generic;
using NUnit.Framework;
using CREWGO.Service.TaskSchedulerFront.Modules;
using CREWGO.Service.BusinessService.Common;
using System;
using NSubstitute;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using System.Threading;
using CREWGO.Service.TaskSchedulerFront.Services;
using System.Threading.Tasks;
using CREWGO.Service.Common;
using CREWGO.Service.TaskSchedulerFront.Helpers;

namespace CREWGO.Service.UnitTests.TaskSchedulerTests.ModuleTests
{
    [TestFixture]
    public class CREWGOCrewAlgorithmTest
    {
        #region Test Data
        private ICREWGOService serviceMock;
        private IPushNotifications pushNotifications;
        private JobShift firstShift;
        private DeviceInfo firstDevice, secondDevice;
        private List<DeviceInfo> devices;
        private Job firstJob;
        private List<ActivityLevel> activityLevels;
        private PotentialStaff firstStaff, secondStaff;
        private List<PotentialStaff> potentialStaffs;

        #endregion
        /// <summary>
        /// Initialize test data
        /// </summary>
        [SetUp]
        public void TestInit()
        {
            #region Device Mock Object
            firstDevice = new DeviceInfo()
            {
                DeviceInfoId = 8010,
                UserId = 350,
                DeviceId = "c59781677ad83e06",
                HashCode = "JodouHRN90iz7QZrUgCiRpuYO0Tm01jK",
                DeviceType = 2,
                Token = "12254202-76b6-11e7-989d-001dd8b73106",
                TokenFlag = null,
                DeviceName = "",
                DeviceModel = "",
                OsVersion = "",
                LastActivity = null,
                CreatedDate = new DateTime(2017, 07, 17, 11, 5, 2),
                GroupId = 3,
                DeviceToken = "fODfpQYeXdw:APA91bEoPOXsVxWWU7bWv-P8MeVmoWvX3Fq1CjXEcrWSCS0vlPZ6rAE74Ey5IzW_7xDFasc2-py1gE7LCdaNf_VWvNaMMW2GA5VOECuf7p6pqiSx9goYHBcPlmOW28of4QNt5uXOHcGX"
            };
            secondDevice = new DeviceInfo()
            {
                DeviceInfoId = 8068,
                UserId = 350,
                DeviceId = "403eeb1878641b8a",
                HashCode = "K4tP9oEd0OYN0n7OmGyjMWl7uRDqWw6H",
                DeviceType = 2,
                Token = "4d2193b0-776a-11e7-989d-001dd8b73106",
                TokenFlag = null,
                DeviceName = "",
                DeviceModel = "",
                OsVersion = "",
                LastActivity = null,
                CreatedDate = new DateTime(2017, 07, 17, 11, 5, 2),
                GroupId = 3,
                DeviceToken = "czujRPE_1-s:APA91bH6baLmM12lt9jjLCLyR0-NpHytU_c-kSbvOy8Hg4c_9fVtnNC7Yr7TonYd2jHDVKTEm3dyk_AGT6TgT9h0TeztzyoUlHcjmKm4XxVSOwfkWMFCe0VmtHcOA2aTllG0fwcBPUZb"
            };
            devices = new List<DeviceInfo>();
            devices.Add(firstDevice);
            devices.Add(secondDevice);
            #endregion
            #region Job Mock Object
            firstJob = new Job()
            {
                BookAmount = (decimal)18.33,
                BookingNumber = "",
                CustomerUserId = 21,
                Description = "",
                EnteredBy = 21,
                EnteredDate = new DateTime(2017, 07, 17, 11, 5, 2),
                HasPeakPrice = false,
                InductionRequired = false,
                JobFullAddress = "Kupandole, Nepal",
                JobId = 1151,
                JobLocationLatitude = (decimal)27.686913,
                JobLocationLongitude = (decimal)85.315082,
                JobNumber = "JD-1151",
                JobPostCodeId = 14179,
                JobStreet = "",
                JobTitle = "",
                LHCUserId = 14,
                MeetingFullAddress = "Kupandole, Nepal",
                MeetingLatitude = (decimal)27.686913,
                MeetingLongitude = (decimal)85.315082,
                MeetingPostCodeId = null,
                MeetingStreet = "",
                PeakPriceReason = "",
                QuoteId = "ECENEDDVDU",
                ShiftStatus = 3,
                Status = 2,
                SupervisorUserId = null,
                UpdatedBy = null,
                UpdatedDate = new DateTime(2001, 1, 1, 12, 0, 0),
            };
            #endregion
            #region Shift Mock Object
            firstShift = new JobShift()
            {
                CompletedDate = new DateTime(2001, 1, 1, 12, 0, 0),
                EndTime = new DateTime(2017, 9, 20, 11, 45, 0),
                EndTimeBreak = null,
                HasPeakPrice = false,
                HourlyRate = null,
                IsCompleted = false,
                IsFilled = false,
                JobId = 1151,
                JobShiftId = 1657,
                LevelId = 5,
                ParentJob = firstJob,
                PeakPrice = null,
                Qualification = "",
                RequiredNumber = 2,
                ShiftStatus = 4,
                SkillId = 1,
                StartTime = new DateTime(2017, 9, 20, 10, 50, 0),
                TotalCost = (decimal)18,
                TotalHour = "0.5",
                UpdatedBy = null,
                UpdatedDate = new DateTime(2017, 1, 1, 12, 0, 0)
            };
            #endregion
            #region PotentialStaff Mock Object
            firstStaff = new PotentialStaff()
            {
                ID = 1,
                UserId = 1,
                JobId = 1151,
                JobShiftId = 1657, 
                TotalScore = 91.87,
                Devices = devices
            };
            secondStaff = new PotentialStaff()
            {
                ID = 2,
                UserId = 1,
                JobId = 1151,
                JobShiftId = 1657,
                TotalScore = 89.04,
                Devices = devices
            };
            potentialStaffs = new List<PotentialStaff>();
            potentialStaffs.Add(firstStaff);
            potentialStaffs.Add(secondStaff);
            #endregion
            #region Activity Level Mock Object
            activityLevels = new List<ActivityLevel>();
            activityLevels.Add(new ActivityLevel()
                {
                    entered_by = 1,
                    entered_date = DateTime.Now,
                    filling_probability = 0,
                    id = 1,
                    name = "Activity Level 1",
                    p_staff_result_range_from = 201,
                    p_staff_result_range_to = 0,
                    peak_price_factor = 0,
                    shift_notice_time = 240,
                    shift_request_number = 1,
                    shift_time_interval = 90,
                    type = "Mild",
                    updated_by = 1,
                    updated_date = DateTime.Now,
                });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 0,
                id = 3,
                name = "Activity Level 2",
                p_staff_result_range_from = 175,
                p_staff_result_range_to = 200,
                peak_price_factor = 0,
                shift_notice_time = 240,
                shift_request_number = 2,
                shift_time_interval = 90,
                type = "Mild",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 0,
                id = 4,
                name = "Activity Level 3",
                p_staff_result_range_from = 150,
                p_staff_result_range_to = 174,
                peak_price_factor = 0,
                shift_notice_time = 240,
                shift_request_number = 3,
                shift_time_interval = 90,
                type = "Mild",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 0,
                id = 5,
                name = "Activity Level 4",
                p_staff_result_range_from = 125,
                p_staff_result_range_to = 149,
                peak_price_factor = 0,
                shift_notice_time = 240,
                shift_request_number = 4,
                shift_time_interval = 90,
                type = "Mild",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 0,
                id = 6,
                name = "Activity Level 1",
                p_staff_result_range_from = 100,
                p_staff_result_range_to = 124,
                peak_price_factor = 1.1M,
                shift_notice_time = 240,
                shift_request_number = 5,
                shift_time_interval = 60,
                type = "Peak",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 0,
                id = 7,
                name = "Activity Level 2",
                p_staff_result_range_from = 75,
                p_staff_result_range_to = 99,
                peak_price_factor = 1.2M,
                shift_notice_time = 240,
                shift_request_number = 10,
                shift_time_interval = 60,
                type = "Mild",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 50,
                id = 8,
                name = "Activity Level 3",
                p_staff_result_range_from = 50,
                p_staff_result_range_to = 74,
                peak_price_factor = 1.3M,
                shift_notice_time = 240,
                shift_request_number = 1,
                shift_time_interval = 60,
                type = "Peak",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 0,
                id = 9,
                name = "Activity Level 4",
                p_staff_result_range_from = 201,
                p_staff_result_range_to = 0,
                peak_price_factor = 0,
                shift_notice_time = 240,
                shift_request_number = 25,
                shift_time_interval = 30,
                type = "Peak",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            activityLevels.Add(new ActivityLevel()
            {
                entered_by = 1,
                entered_date = DateTime.Now,
                filling_probability = 90,
                id = 10,
                name = "Activity Level 5",
                p_staff_result_range_from = 0,
                p_staff_result_range_to = 24,
                peak_price_factor = 1.5M,
                shift_notice_time = 240,
                shift_request_number = 40,
                shift_time_interval = 30,
                type = "Mild",
                updated_by = 1,
                updated_date = DateTime.Now,
            });
            #endregion
            #region PushNotification initialization
            pushNotifications = new PushNotifications(
                        PushNotificationKeys.CustomerSenderId,
                        PushNotificationKeys.CustomerAuthToken,
                        PushNotificationKeys.SupervisorSenderId,
                        PushNotificationKeys.SupervisorAuthToken,
                        PushNotificationKeys.StaffSenderId,
                        PushNotificationKeys.StaffAuthToken,
                        PushNotificationKeys.CustomerAppleCert,
                        PushNotificationKeys.CustomerApplePass,
                        PushNotificationKeys.SupervisorAppleCert,
                        PushNotificationKeys.SupervisorApplePass,
                        PushNotificationKeys.StaffAppleCert,
                        PushNotificationKeys.StaffApplePass
                    );
            #endregion
            #region Initialize Service Mock
            // Construct a Mock Object of the ICREWGOService Interface
            serviceMock = Substitute.For<ICREWGOService>();
            // Return Mock Objects For Service Calls
            serviceMock
                .When(x => x.GeneratePotentialStaffList(Arg.Any<long>()))
                .Do(x => Thread.Sleep(TimeSpan.FromMilliseconds(0)));
            serviceMock.GetShiftSociabilityFactor(Arg.Any<long>())
                .ReturnsForAnyArgs(1);
            serviceMock.GetShiftNoticePeriod().Returns(5);
            serviceMock.GetShortNoticePeriod().Returns(TimeSpan.FromHours(5));
            serviceMock.GetPotentialStaffCount(Arg.Any<long>())
                .ReturnsForAnyArgs(2);
            serviceMock
                .ComputePotentialStaffResult(
                    Arg.Any<int>(), Arg.Any<int>(), Arg.Any<float>())
                .Returns(callinfo => 
                    (double)((callinfo.ArgAt<int>(0) *
                        callinfo.ArgAt<float>(2) * 10) /
                        callinfo.ArgAt<int>(1))
                );
            serviceMock.GetAllActivityLevels().Returns(activityLevels);
            serviceMock
                .ComputeNumberOfRequests(
                    Arg.Any<TimeSpan>(), Arg.Any<TimeSpan>(), Arg.Any<int>())
                .Returns(callinfo => callinfo.ArgAt<int>(2) * Convert.ToInt32(
                        callinfo.ArgAt<TimeSpan>(0).TotalSeconds / 
                        callinfo.ArgAt<TimeSpan>(1).TotalSeconds)
                );
            serviceMock.GetPotentialStaffs(1, 1)
                .ReturnsForAnyArgs(potentialStaffs);
            serviceMock.CalculateNextPSR(Arg.Any<ActivityLevel>())
                .Returns(callinfo => 
                (double)callinfo.ArgAt<ActivityLevel>(0)
                    .p_staff_result_range_to / 10
                );
            serviceMock
                .ComputeNumberOfRequests(Arg.Any<double>(), Arg.Any<double>())
                .Returns(callinfo => Convert.ToInt32(Math.Floor
                    (callinfo.ArgAt<double>(0) - callinfo.ArgAt<double>(1)))
                );
            #endregion
        }
        [Test]
        public void RunCrewAlgorithmSuccessTest()
        {
            serviceMock.IsShiftQuotaFulfilled(Arg.Any<long>()).Returns(true);
            Assert.DoesNotThrowAsync(() =>
                new CREWGOCrewAlgorithm(serviceMock)
                    .RunCrewAlgorithm(firstShift)
            );
        }
        [Test]
        public void PreScheduleNotificationsSuccessTest()
        {
            serviceMock.IsShiftQuotaFulfilled(Arg.Any<long>()).Returns(false);
            var androidTokens = new List<string>();
            androidTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            androidTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            var iosTokens = new List<string>();
            iosTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            iosTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            var jobTitle = "This Is Title";
            var jobNumber = "#JD-11231";
            var userId = 123;
            var shiftID = 12314;
            var result = new CREWGOCrewAlgorithm(serviceMock)
                            .CheckJobStatusAndScheduleNotifications(
                                androidTokens, 
                                iosTokens, 
                                jobTitle,
                                jobNumber,
                                shiftID,
                                userId,
                                NotificationTypeEnum.JOBALERTS
                            );
            Assert.True(result); 
        }
        [Test]
        public void PreScheduleNotificationsQuotaFulfilledTest()
        {
            serviceMock.IsShiftQuotaFulfilled(Arg.Any<long>()).Returns(true);
            var androidTokens = new List<string>();
            androidTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            androidTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            var iosTokens = new List<string>();
            iosTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            iosTokens.Add("ab87ca87c98d79d7f987df123123fafafaee988");
            var jobTitle = "This Is Title";
            var jobNumber = "#JD-11231";
            var userId = 123;
            var shiftID = 12314;
            var result = new CREWGOCrewAlgorithm(serviceMock)
                            .CheckJobStatusAndScheduleNotifications(
                                androidTokens,
                                iosTokens,
                                jobTitle,
                                jobNumber,
                                shiftID,
                                userId,
                                NotificationTypeEnum.JOBALERTS
                            );
            Assert.False(result); 
        }
        [TearDown]
        public void EndTest()
        {
            serviceMock = null;
            firstShift = null;
            firstJob = null;
            firstDevice = null;
            secondDevice = null;
            devices = null;
            firstStaff = null;
            secondStaff = null;
            potentialStaffs = null;
            activityLevels = null;
        }
    }
}