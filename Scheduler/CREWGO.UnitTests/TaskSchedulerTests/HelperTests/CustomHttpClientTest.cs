﻿using System.Net.Http;
using NUnit.Framework;
using CREWGO.Service.TaskSchedulerFront.Helpers;

namespace CREWGO.Service.UnitTests.TaskSchedulerTests.HelperTests
{
    [TestFixture]
    public class CustomHttpClientTest
    {
        private HttpClient customClient;
        [SetUp]
        public void CustomHttpClientTestSetUp()
        {
            customClient = CustomHttpClient.CustomHttpClientObj;
        }
        [Test]
        public void CustomHttpClientObjNotNullTest()
        {
            Assert.IsNotNull(customClient);
        }
        [Test]
        public void CustomHttpClientObjBaseAddressTest()
        {
            var expectedLocalAddress = "http://192.168.0.145:8087/api/";
            var expectedStagingAddress = "http://apistaging.crewgo.co/api/";
            var expectedLiveAddress = "http://api.crewgo.co/api/";
            Assert.That(
                customClient.BaseAddress.ToString() == expectedLiveAddress ||
                customClient.BaseAddress.ToString() == expectedStagingAddress ||
                customClient.BaseAddress.ToString() == expectedLocalAddress);
        }
        [Test]
        public void CustomHttpClientObjAuthParameterTest()
        {
            var expectedAuthParameters = 
                "apiKey CB6BDLPLWIWF8HZD6207SENO30CHBVZO:" + 
                "W61AE1SSDV0QFAS3TFTH3FD8XRHMJIJATHTCQY9FJI46N6J3HIY6CSVQ23NWTGI5";
            Assert.AreEqual(
                customClient.DefaultRequestHeaders.Authorization.ToString(), 
                expectedAuthParameters
            );
        }
        [Test]
        public void CustomHttpClientObjHeaderAcceptTest()
        {
            var expectedHeaderAccept = "application/json";
            Assert.AreEqual(
                customClient.DefaultRequestHeaders.Accept.ToString(),
                expectedHeaderAccept
            );
        }
    }
}