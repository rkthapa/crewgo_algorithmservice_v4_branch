﻿using NUnit.Framework;
using CREWGO.Service.TaskSchedulerFront.Helpers;

namespace CREWGO.Service.UnitTests.TaskSchedulerTests.HelperTests
{
    [TestFixture]
    public class PushNotificationKeysTest
    {
        [Test]
        public void CustomerKeysValidityTest()
        {
            string customerSenderId = "696785742113";
            string customerAuthToken = "AAAAojuqqSE:APA91bHA5Qm7cCBHe3mcDW_3c5sehbm2QoDniQyv3yql2vZ7eNyljAUoN5M5OTka5g-YQMOyt__4BoozAVrdk-1yUyy1fDMv6wtxai6GeB1q9Yp7GrvRDuQxWPGfGr6g9gv7mMQBCsE6";
            Assert.AreEqual(customerSenderId, PushNotificationKeys.CustomerSenderId);
            Assert.AreEqual(customerAuthToken, PushNotificationKeys.CustomerAuthToken);
        }
        [Test]
        public void SupervisorKeysValidityTest()
        {
            string supervisorSenderId = "712702800560";
            string supervisorAuthToken = "AAAApfBlsrA:APA91bGE8ZCTBWzsQX57unSbFYAytB2dtQ9klqAJAlJs4aI8-nKUJGysrAVCQVm31fnyhRQSAyG7_CRuQpguV-KitadyVzVVyMMhcqyvESFNldukF4lOsRzzhMs_gGdN9l-hWmGlH2Kg";
            Assert.AreEqual(supervisorSenderId, PushNotificationKeys.SupervisorSenderId);
            Assert.AreEqual(supervisorAuthToken, PushNotificationKeys.SupervisorAuthToken);
        }
        [Test]
        public void StaffKeysValidityTest()
        {
            string staffSenderId = "455124601829";
            string staffAuthToken = "AAAAafeKS-U:APA91bGkBcwW9-EyhAOM18eeRCSX-o-8UfQe5PZ61j1Cy4V-Yu_sE05yv2miY5FwPTrfHlMUUmyeTx_dLnpTzbvNhmbk-sM6cwRIQrokZ0g-KD_Z2liiwdK3YfOTfuaSBcq1ZXZxUpUV";
            Assert.AreEqual(staffSenderId, PushNotificationKeys.StaffSenderId);
            Assert.AreEqual(staffAuthToken, PushNotificationKeys.StaffAuthToken);
        }
    }
}