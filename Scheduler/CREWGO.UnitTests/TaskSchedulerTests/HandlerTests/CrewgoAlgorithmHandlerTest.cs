﻿using System;
using System.Threading;
using NUnit.Framework;
using NSubstitute;
using CREWGO.Service.TaskSchedulerFront.Handlers;
using CREWGO.Service.TaskSchedulerFront.Interfaces;

namespace CREWGO.Service.UnitTests.TaskSchedulerTests.HandlerTests
{
    [TestFixture]
    public class CrewgoAlgorithmHandlerTest
    {
        private ICREWGOAlgorithm algorithmMock;
        [SetUp]
        public void TestSetup()
        {
            algorithmMock = Substitute.For<ICREWGOAlgorithm>();
        }
        [Test]
        public void RunCrewgoAlgorithmNoExceptionTest()
        {
            algorithmMock.When(x => x.RunAlgorithm())
                .Do(x => Thread.Sleep(TimeSpan.FromMilliseconds(0)));
            Assert.DoesNotThrow(
                () => new AlgorithmHandler().InitiateAlgorithm()
            );
        }
        [Test]
        public void RunCrewgoAlgorithmExceptionTest()
        {
            algorithmMock.When(x => x.RunAlgorithm())
                .Do(x => { throw (new Exception()); });
            Assert.DoesNotThrow(
                () => new AlgorithmHandler().InitiateAlgorithm()
            );
        }
        [TearDown]
        public void EndTest()
        {
            algorithmMock = null;
        }
    }
}