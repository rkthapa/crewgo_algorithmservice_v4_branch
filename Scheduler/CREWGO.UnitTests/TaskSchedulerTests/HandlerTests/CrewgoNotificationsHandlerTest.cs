﻿using System;
using System.Threading;
using NUnit.Framework;
using NSubstitute;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.TaskSchedulerFront.Handlers;

namespace CREWGOCREWGO.Service.UnitTests.TaskSchedulerTests.HandlerTests
{
    [TestFixture]
    public class CrewgoNotificationsHandlerTest
    {
        private ICREWGONotifications notificationsMock;
        [SetUp]
        public void TestSetup()
        {
            notificationsMock = Substitute.For<ICREWGONotifications>();
        }
        [Test]
        public void ScheduleNotificationsNoExceptionTest()
        {
            notificationsMock.When(x => x.ScheduleNotifications())
                .Do(x => Thread.Sleep(TimeSpan.FromMilliseconds(0)));
            Assert.DoesNotThrow(
                () => new NotificationHandler().SendNotifications()
            );
        }
        [Test]
        public void ScheduleNotificationsExceptionTest()
        {
            notificationsMock.When(x => x.ScheduleNotifications())
                .Do(x => { throw (new Exception()); });
            Assert.DoesNotThrow(
                () => new NotificationHandler().SendNotifications()
            );
        }
        [TearDown]
        public void EndTest()
        {
            notificationsMock = null;
        }
    }
}