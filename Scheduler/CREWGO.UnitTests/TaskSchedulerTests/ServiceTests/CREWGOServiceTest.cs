﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using CREWGO.Service.TaskSchedulerFront.Services;
using CREWGO.Service.BusinessService.Common;
using System.Threading.Tasks;
using CREWGO.Service.BusinessService;
using CREWGO.Service.Common;
using System.Net.Http;
using System.Diagnostics;
using System.Threading;

namespace CREWGO.Service.UnitTests.TaskSchedulerTests.ServiceTests
{
    [TestFixture]
    public class CREWGOServiceTest
    {
        private CREWGOService crewgoService;
        [SetUp]
        public void TestSetUp()
        {
            crewgoService = new CREWGOService();
        }
        #region CREWGO Algorithm Service Tests
        #region Get Method Tests
        #region GetShifts
        [Test]
        public async Task GetShiftsTest()
        {
            var response = crewgoService.GetShifts();
            Assert.IsNotNull(await response); 
        }
        #endregion
        #region GetStaffByGeneralPriority
        [Test]
        public async Task GetStaffByGeneralPriorityTest()
        {
            long partnerId = 1;
            long jobShiftId = 1;
            int skillId = 1;
            int levelId = 1;
            int userLimit = 1;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await crewgoService.GetStaffByGeneralPriority(
                partnerId,
                jobShiftId,
                skillId,
                levelId,
                userLimit,
                startTime,
                endTime
            );
            Assert.IsNotNull(response);
            Assert.IsInstanceOf<List<User>>(response);
        }
        [Test]
        public async Task GetStaffByGeneralPriorityNullTest()
        {
            long partnerId = 0;
            long jobShiftId = 0;
            int skillId = 0;
            int levelId = 0;
            int userLimit = 0;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await crewgoService.GetStaffByGeneralPriority(
                partnerId,
                jobShiftId,
                skillId,
                levelId,
                userLimit,
                startTime,
                endTime
            );
            Assert.IsEmpty(response);
        }
        [Test]
        public void GetStaffByGeneralPriorityArgumentNullExceptionTest()
        {
            long partnerId = 0;
            long jobShiftId = 0;
            int skillId = 0;
            int levelId = 0;
            int userLimit = 0;
            DateTime? startTime = null;
            DateTime? endTime = null;
            Assert.ThrowsAsync<ArgumentNullException>(() =>
                crewgoService.GetStaffByGeneralPriority(
                    partnerId,
                    jobShiftId,
                    skillId,
                    levelId,
                    userLimit,
                    startTime,
                    endTime
                )
            );
        }
        #endregion
        #region GetStaffByGlobalPriority
        [Test]
        public async Task GetStaffByGlobalPriorityTest()
        {
            long partnerId = 1;
            long jobShiftId = 1;
            int skillId = 1;
            int levelId = 1;
            int userLimit = 5;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await crewgoService.GetStaffByGlobalPriority(
                partnerId,
                jobShiftId,
                skillId,
                levelId,
                userLimit,
                startTime,
                endTime
            );
            Assert.IsNotNull(response);
            Assert.IsInstanceOf<List<User>>(response);
        }
        [Test]
        public async Task GetStaffByGlobalPriorityNullTest()
        {
            long partnerId = 0;
            long jobShiftId = 0;
            int skillId = 0;
            int levelId = 0;
            int userLimit = 0;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await crewgoService.GetStaffByGlobalPriority(
                partnerId,
                jobShiftId,
                skillId,
                levelId,
                userLimit,
                startTime,
                endTime
            );
            Assert.IsEmpty(response);
        }
        [Test]
        public void GetStaffByGlobalPriorityArgumentNullExceptionTest()
        {
            long partnerId = 0;
            long jobShiftId = 0;
            int skillId = 0;
            int levelId = 0;
            int userLimit = 0;
            DateTime? startTime = null;
            DateTime? endTime = null;
            Assert.ThrowsAsync<ArgumentNullException>(() => 
                crewgoService.GetStaffByGlobalPriority(
                    partnerId,
                    jobShiftId,
                    skillId,
                    levelId,
                    userLimit,
                    startTime,
                    endTime
                )
            );
        }
        #endregion
        #region GetStaffByManualPriority
        [Test]
        public async Task GetStaffByManualPriorityTest()
        {
            long jobShiftId = 1;
            int userLimit = 5;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await crewgoService.GetStaffByManualPriority(
                jobShiftId,
                userLimit,
                startTime,
                endTime
            );
            Assert.IsNotNull(response);
            Assert.IsInstanceOf<List<User>>(response);
        }
        [Test]
        public async Task GetStaffByManualPriorityNullTest()
        {
            long jobShiftId = 0;
            int userLimit = 0;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await crewgoService.GetStaffByManualPriority(
                jobShiftId,
                userLimit,
                startTime,
                endTime
            );
            Assert.IsEmpty(response);
        }
        [Test]
        public void GetStaffByManualPriorityArgumentNullExceptionTest()
        {
            long jobShiftId = 0;
            int userLimit = 0;
            DateTime? startTime = null;
            DateTime? endTime = null;
            Assert.ThrowsAsync<ArgumentNullException>(() => 
                crewgoService.GetStaffByManualPriority(
                    jobShiftId,
                    userLimit,
                    startTime,
                    endTime
                )
            );
        }
        #endregion
        #region GetJobAlertCount
        [Test]
        public async Task GetJobAlertCountTest()
        {
            long staffUserId = 227;
            var response = await crewgoService.GetJobAlertCount(staffUserId);
            Assert.IsNotNull(response);
            Assert.GreaterOrEqual(response, 0);
        }
        #endregion
        #region OrderPriorityExists
        [Test]
        public async Task OrderPriorityExistsTest()
        {
            long jobShiftId = 26424;
            var response = 
                await crewgoService.OrderPriorityExists(jobShiftId);
            Assert.IsNotNull(response);
            //Assert.True(response);
        }
        #endregion
        #region GlobalPriorityExists
        [Test]
        public async Task GlobalPriorityExistsTest()
        {
            long partnerId = 14;
            int skillId = 21;
            int levelID = 1;
            var response = await crewgoService
                .GlobalPriorityExists(partnerId, skillId, levelID);
            Assert.IsNotNull(response);
            //Assert.True(response);
        }
        #endregion
        #region GetJobAlert
        [Test]
        public void GetJobAlertTest()
        {
            long jobShiftId = 123;
            long staffUserId = 227;
            var response = crewgoService.GetJobAlert(jobShiftId, staffUserId);
            Assert.IsInstanceOf<JobAlert>(response);
        }
        #endregion
        #region GetJobAlertSetting Manual/Global
        [Test]
        public void GetJobAlertSettingByShiftUserTest()
        {
            long jobShiftId = 123;
            long staffUserId = 123;
            var response = crewgoService.GetJobAlertSettingByShiftUser(
                                jobShiftId, staffUserId);
            Assert.IsInstanceOf<JobAlertSetting>(response);
        }
        [Test]
        public void GetJobAlertSettingByPartnerUserTest()
        {
            long partnerId = 46;
            long staffUserId = 1039;
            var response = crewgoService.GetJobAlertSettingByPartnerUser(
                                partnerId, staffUserId);
            Assert.IsInstanceOf<JobAlertSettingGlobal>(response);
        }
        [Test]
        public void GetJobManualAlertSettingTest()
        {
            var jobAlertSetting = new JobAlertSetting()
            {
                JobAlertSettingId = 12,
                JobShiftId = 1,
                StaffUserId = 1,
                LHCUserId = 1,
                OrderPriority = 1,
                AssignedDate = new DateTime(2017, 07, 23, 10, 50, 0),
                AlertedDate = new DateTime(2017, 07, 23, 10, 50, 0),
                Status = false
            };
            var response = 
                crewgoService.GetJobManualAlertSetting(jobAlertSetting);
            Assert.IsInstanceOf<JobAlertSetting>(response);
        }
        [Test]
        public void GetJobGlobalAlertSettingTest()
        {
            var jobAlertSetting = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = 1,
                LevelId = 2,
                StaffUserId = 1,
                LHCUserId = 1,
                OrderPriority = 1,
                AssignedDate = new DateTime(2017, 07, 23, 10, 50, 0),
                Status = 9
            };
            var response =
                crewgoService.GetJobGlobalAlertSetting(jobAlertSetting);
            Assert.IsInstanceOf<JobAlertSettingGlobal>(response);
        }
        #endregion
        #region IsShiftQuotaFulfilled
        [Test]
        public async Task IsShiftQuotaFulfilledTest()
        {
            long jobShiftId = 123;
            var response = 
                await crewgoService.IsShiftQuotaFulfilled(jobShiftId);
            Assert.NotNull(response);
        }
        #endregion
        #region IsStaffAutobookable
        [Test]
        public async Task IsStaffAutobookableTestPositive()
        {
            long staffId = 7;
            var response =
                await crewgoService.IsStaffAutobookable(staffId);
            Assert.True(response);
        }
        [Test]
        public async Task IsStaffAutobookableTestNegative()
        {
            long staffId = 26;
            var response =
                await crewgoService.IsStaffAutobookable(staffId);
            Assert.False(response);
        }
        #endregion
        #region GetShiftNoticePeriod
        [Test]
        public async Task GetShiftNoticePeriodTest()
        {
            var response =
                await crewgoService.GetShiftNoticePeriod();
            Assert.NotNull(response);
            Assert.GreaterOrEqual(response, 0);
            //Assert.True(response);
        }
        #endregion
        #endregion
        #region Post Method Tests
        #region PostJobAlert
        [Test]
        public void PostJobAlertSuccessTest()
        {
            var obj = new JobAlert()
            {
                JobShiftId = 648,
                StaffUserId = 25,
                Status = (int)JobAlertStatusEnum.UNREAD,
                AlertDate = DateTime.Now,
                ActionDate = DateTime.Now
            };
            string id = "2";
            var type = "CREW";
            Assert.DoesNotThrow(() =>   
                crewgoService.PostJobAlert(obj, id, type)
            );
        }
        [Test]
        public void PostJobAlertWrongJobDetailIdTest()
        {
            var obj = new JobAlert()
            {
                JobShiftId = -1,
                StaffUserId = 227,
                Status = (int)JobAlertStatusEnum.UNREAD,
                AlertDate = DateTime.Now,
                ActionDate = DateTime.Now
            };
            string id = "2";
            var type = "CREW";
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PostJobAlert(obj, id, type)
            );
        }
        [Test]
        public void PostJobAlertWrongStaffUserIdTest()
        {
            var obj = new JobAlert()
            {
                JobShiftId = 123,
                StaffUserId = -1,
                Status = (int)JobAlertStatusEnum.UNREAD,
                AlertDate = DateTime.Now,
                ActionDate = DateTime.Now
            };
            string id = "2";
            var type = "CREW";
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PostJobAlert(obj, id, type)
            );
        }
        [Test]
        public void PostJobAlertWrongPartnerIdTest()
        {
            var obj = new JobAlert()
            {
                JobShiftId = 123,
                StaffUserId = 227,
                Status = (int)JobAlertStatusEnum.UNREAD,
                AlertDate = DateTime.Now,
                ActionDate = DateTime.Now
            };
            string partnerId = "-1";
            var type = "CREW";
            Assert.DoesNotThrow(() =>
                crewgoService.PostJobAlert(obj, partnerId, type)
            );
        }
        [Test]
        public void PostJobAlertWrongTypeTest()
        {
            var obj = new JobAlert()
            {
                JobShiftId = 123,
                StaffUserId = 227,
                Status = (int)JobAlertStatusEnum.UNREAD,
                AlertDate = DateTime.Now,
                ActionDate = DateTime.Now
            };
            string partnerId = "2";
            var type = "NONE";
            Assert.DoesNotThrow(() =>
                crewgoService.PostJobAlert(obj, partnerId, type)
            );
        }
        #endregion
        #region PostJobStaff
        [Test]
        public void PostJobStaffSuccessTest()
        {
            var obj = new JobStaff()
            {
                JobShiftId = 1,
                StaffUserId = 1,
                AcceptedDate = DateTime.Now,
                JobAlertId = 1,
                JobStatus = 1
            };
            Assert.DoesNotThrowAsync(() => crewgoService.PostJobStaff(
                obj, 
                obj.JobShiftId.ToString(), 
                obj.StaffUserId.ToString(), 
                "Crew")
            );
        }
        [Test]
        public void PostJobStaffWrongJobDetailIdTest()
        {
            var obj = new JobStaff()
            {
                JobShiftId = -1,
                StaffUserId = 1,
                AcceptedDate = DateTime.Now,
                JobAlertId = 1,
                JobStatus = 1
            };
            Assert.ThrowsAsync<HttpRequestException>(() => 
                crewgoService.PostJobStaff(
                    obj,
                    obj.JobShiftId.ToString(),
                    obj.StaffUserId.ToString(),
                    "Crew")
            );
        }
        [Test]
        public void PostJobStaffWrongStaffUserIdTest()
        {
            var obj = new JobStaff()
            {
                JobShiftId = 1,
                StaffUserId = -1,
                AcceptedDate = DateTime.Now,
                JobAlertId = 1,
                JobStatus = 1
            };
            Assert.ThrowsAsync<HttpRequestException>(() =>
                crewgoService.PostJobStaff(
                    obj,
                    obj.JobShiftId.ToString(),
                    obj.StaffUserId.ToString(),
                    "Crew")
            );
        }
        [Test]
        public void PostJobStaffWrongJobAlertIdTest()
        {
            var obj = new JobStaff()
            {
                JobShiftId = 1,
                StaffUserId = 1,
                AcceptedDate = DateTime.Now,
                JobAlertId = -1,
                JobStatus = 1
            };
            Assert.ThrowsAsync<HttpRequestException>(() =>
                crewgoService.PostJobStaff(
                    obj,
                    obj.JobShiftId.ToString(),
                    obj.StaffUserId.ToString(),
                    "Crew")
            );
        }
        [Test]
        public void PostJobStaffNullAcceptedDateTest()
        {
            var obj = new JobStaff()
            {
                JobShiftId = 1,
                StaffUserId = 1,
                AcceptedDate = null,
                JobAlertId = 1,
                JobStatus = 1
            };
            Assert.DoesNotThrowAsync(() => crewgoService.PostJobStaff(
                obj,
                obj.JobShiftId.ToString(),
                obj.StaffUserId.ToString(),
                "Crew")
            );
        }
        #endregion
        #region PostJobAlertSetting
        [Test]
        public void PostJobAlertSettingSuccessTest()
        {
            var obj = new JobAlertSetting()
            {
                JobShiftId = 1,
                StaffUserId = 1,
                LHCUserId = 1,
                OrderPriority = null,
                AssignedDate = DateTime.Now,
                AlertedDate = null,
                Status = true
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PostJobAlertSetting(obj)
            );
        }
        [Test]
        public void PostJobAlertSettingWrongJobDetailIdTest()
        {
            var obj = new JobAlertSetting()
            {
                JobShiftId = -1,
                StaffUserId = 1,
                LHCUserId = 1,
                OrderPriority = null,
                AssignedDate = DateTime.Now,
                AlertedDate = null,
                Status = true
            };
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PostJobAlertSetting(obj)
            );
        }
        [Test]
        public void PostJobAlertSettingWrongStaffUserIdTest()
        {
            var obj = new JobAlertSetting()
            {
                JobShiftId = 123,
                StaffUserId = -1,
                LHCUserId = 1,
                OrderPriority = null,
                AssignedDate = DateTime.Now,
                AlertedDate = null,
                Status = true
            };
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PostJobAlertSetting(obj)
            );
        }
        [Test]
        public void PostJobAlertSettingWrongLhcUserIdTest()
        {
            var obj = new JobAlertSetting()
            {
                JobShiftId = 123,
                StaffUserId = 1,
                LHCUserId = -1,
                OrderPriority = null,
                AssignedDate = DateTime.Now,
                AlertedDate = null,
                Status = true
            };
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PostJobAlertSetting(obj)
            );
        }
        //[Test]
        //public void PostJobAlertSettingWrongOrderByTest()
        //{
        //    var obj = new JobAlertSetting()
        //    {
        //        JobShiftId = 1,
        //        StaffUserId = 1,
        //        LHCUserId = 1,
        //        OrderPriority = -1,
        //        AssignedDate = DateTime.Now,
        //        AlertedDate = null,
        //        Status = true
        //    };
        //    Assert.Throws<HttpRequestException>(() =>
        //        crewgoService.PostJobAlertSetting(obj)
        //    );
        //}
        [Test]
        public void PostJobAlertSettingWrongStatusTest()
        {
            var obj = new JobAlertSetting()
            {
                JobShiftId = 1,
                StaffUserId = 1,
                LHCUserId = 1,
                OrderPriority = null,
                AssignedDate = DateTime.Now,
                AlertedDate = null,
                Status = null
            };
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PostJobAlertSetting(obj)
            );
        }
        #endregion
        #endregion
        #region Put Method Tests
        #region PutJobAlertSetting
        [Test]
        public void PutJobAlertSettingSuccessTest()
        {
            var obj = new JobAlertSetting()
            {
                JobAlertSettingId = 501,
                JobShiftId = 227,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AlertedDate = DateTime.Now,
                AssignedDate = DateTime.Now,
                Status = true
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSetting(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingWrongJobAlertSettingIdTest()
        {
            var obj = new JobAlertSetting()
            {
                JobAlertSettingId = -1,
                JobShiftId = 227,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AlertedDate = DateTime.Now,
                AssignedDate = DateTime.Now,
                Status = true
            };
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PutJobAlertSetting(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingWrongJobShiftIdTest()
        {
            var obj = new JobAlertSetting()
            {
                JobAlertSettingId = 501,
                JobShiftId = -1,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AlertedDate = DateTime.Now,
                AssignedDate = DateTime.Now,
                Status = true
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSetting(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingWrongStaffUserIdTest()
        {
            var obj = new JobAlertSetting()
            {
                JobAlertSettingId = 501,
                JobShiftId = 227,
                StaffUserId = -1,
                LHCUserId = 2,
                OrderPriority = 1,
                AlertedDate = DateTime.Now,
                AssignedDate = DateTime.Now,
                Status = true
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSetting(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingWrongLHCUserIdTest()
        {
            var obj = new JobAlertSetting()
            {
                JobAlertSettingId = 501,
                JobShiftId = 227,
                StaffUserId = 227,
                LHCUserId = -1,
                OrderPriority = 1,
                AlertedDate = DateTime.Now,
                AssignedDate = DateTime.Now,
                Status = true
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSetting(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingWrongOrderPriorityTest()
        {
            var obj = new JobAlertSetting()
            {
                JobAlertSettingId = 501,
                JobShiftId = 227,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = -1,
                AlertedDate = DateTime.Now,
                AssignedDate = DateTime.Now,
                Status = true
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSetting(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingWrongStatusTest()
        {
            var obj = new JobAlertSetting()
            {
                JobAlertSettingId = 501,
                JobShiftId = 227,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AlertedDate = DateTime.Now,
                AssignedDate = DateTime.Now,
                Status = null
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSetting(obj)
            );
        }
        #endregion
        #region PutJobAlertSettingGlobal
        [Test]
        public void PutJobAlertSettingGlobalSuccessTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = 227,
                LevelId = 1,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AssignedDate = DateTime.Now,
                Status = 1
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingGlobalWrongJobAlertSettingGlobalIdTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = -1,
                SkillId = 227,
                LevelId = 1,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AssignedDate = DateTime.Now,
                Status = 1
            };
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingGlobalWrongSkillIdTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = -1,
                LevelId = 1,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AssignedDate = DateTime.Now,
                Status = 1
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingGlobalWrongLevelIdTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = 227,
                LevelId = -1,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = 1,
                AssignedDate = DateTime.Now,
                Status = 1
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingGlobalWrongStaffUserIdTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = 227,
                LevelId = 1,
                StaffUserId = -227,
                LHCUserId = 2,
                OrderPriority = 1,
                AssignedDate = DateTime.Now,
                Status = 1
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingGlobalWrongLHCUserIdTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = 227,
                LevelId = 1,
                StaffUserId = 227,
                LHCUserId = -2,
                OrderPriority = 1,
                AssignedDate = DateTime.Now,
                Status = 1
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingGlobalWrongOrderPriorityTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = 227,
                LevelId = 1,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = -1,
                AssignedDate = DateTime.Now,
                Status = 1
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        [Test]
        public void PutJobAlertSettingGlobalWrongStatusTest()
        {
            var obj = new JobAlertSettingGlobal()
            {
                JobAlertSettingGlobalId = 269,
                SkillId = 227,
                LevelId = 1,
                StaffUserId = 227,
                LHCUserId = 2,
                OrderPriority = -1,
                AssignedDate = DateTime.Now,
                Status = -1
            };
            Assert.DoesNotThrow(() =>
                crewgoService.PutJobAlertSettingGlobal(obj)
            );
        }
        #endregion
        #endregion
        #region Set Entity Framework Objects Tests
        #region SetJobAlert
        [Test]
        public void SetJobAlertSuccessTest()
        {
            long jobShiftId = 123;
            long staffUserId = 234;
            Assert.DoesNotThrow(() => 
                crewgoService.SetJobAlert(
                    jobShiftId, staffUserId, JobAlertStatusEnum.UNREAD)
            );
        }
        [Test]
        public void SetJobAlertInvalidJobShiftIdTest()
        {
            long jobShiftId = -123;
            long staffUserId = 234;
            Assert.DoesNotThrow(() =>
                crewgoService.SetJobAlert(
                    jobShiftId, staffUserId, JobAlertStatusEnum.UNREAD)
            );
        }
        [Test]
        public void SetJobAlertInvalidStaffUserIdTest()
        {
            long jobShiftId = 123;
            long staffUserId = -234;
            Assert.DoesNotThrow(() =>
                crewgoService.SetJobAlert(
                    jobShiftId, staffUserId, JobAlertStatusEnum.UNREAD)
            );
        }
        #endregion
        #region SetJobAlertSetting
        [Test]
        public void SetJobAlertSettingSuccessTest()
        {
            long jobShiftId = 123;
            long staffUserId = 234;
            long partnerId = 345;
            Assert.DoesNotThrow(() =>
                crewgoService.SetJobAlertSetting(
                    jobShiftId, 
                    staffUserId,
                    partnerId
                )
            );
        }
        [Test]
        public void SetJobAlertSettingInvalidJobShiftIdTest()
        {
            long jobShiftId = -123;
            long staffUserId = 234;
            long partnerId = 345;
            Assert.DoesNotThrow(() =>
                crewgoService.SetJobAlertSetting(
                    jobShiftId,
                    staffUserId,
                    partnerId
                )
            );
        }
        [Test]
        public void SetJobAlertSettingInvalidStaffUserIdTest()
        {
            long jobShiftId = 123;
            long staffUserId = -234;
            long partnerId = 345;
            Assert.DoesNotThrow(() =>
                crewgoService.SetJobAlertSetting(
                    jobShiftId,
                    staffUserId,
                    partnerId
                )
            );
        }
        [Test]
        public void SetJobAlertSettingInvalidPartnerIdTest()
        {
            long jobShiftId = 123;
            long staffUserId = 234;
            long partnerId = -345;
            Assert.DoesNotThrow(() =>
                crewgoService.SetJobAlertSetting(
                    jobShiftId,
                    staffUserId,
                    partnerId
                )
            );
        }
        #endregion
        #endregion
        #region Custom Method Tests
        #region UpdateAlertSettingsManual
        [Test]
        public void UpdateAlertSettingsManualSuccessTest()
        {
            long jobShiftId = 75;
            long staffUserId = 25;
            long parentJobId = 345;
            Assert.DoesNotThrow(() =>
                crewgoService.UpdateAlertSettingsManual(
                    jobShiftId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsManualInvalidJobShiftIdTest()
        {
            long jobShiftId = -123;
            long staffUserId = 23;
            long parentJobId = 345;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdateAlertSettingsManual(
                    jobShiftId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsManualInvalidStaffUserIdTest()
        {
            long jobShiftId = 50;
            long staffUserId = -234;
            long parentJobId = 345;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdateAlertSettingsManual(
                    jobShiftId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsManualInvalidParentJobIdTest()
        {
            long jobShiftId = 50;
            long staffUserId = 23;
            long parentJobId = -345;
            Assert.DoesNotThrow(() =>
                crewgoService.UpdateAlertSettingsManual(
                    jobShiftId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        #endregion
        #region UpdateAlertSettingsGlobal
        [Test]
        public void UpdateAlertSettingsGlobalSuccessTest()
        {
            long lhcUserId = 46;
            long staffUserId = 1039;
            long parentJobId = 345;
            Assert.DoesNotThrow(() =>
                crewgoService.UpdateAlertSettingsGlobal(
                    lhcUserId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsGlobalInvalidJobShiftIdTest()
        {
            long lhcUserId = -123;
            long staffUserId = 1039;
            long parentJobId = 345;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdateAlertSettingsGlobal(
                    lhcUserId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsGlobalInvalidStaffUserIdTest()
        {
            long lhcUserId = 46;
            long staffUserId = -234;
            long parentJobId = 345;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdateAlertSettingsGlobal(
                    lhcUserId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsGlobalInvalidParentJobIdTest()
        {
            long lhcUserId = 46;
            long staffUserId = 1039;
            long parentJobId = -345;
            Assert.DoesNotThrow(() =>
                crewgoService.UpdateAlertSettingsGlobal(
                    lhcUserId,
                    staffUserId,
                    parentJobId
                )
            );
        }
        #endregion
        #region UpdateAlertSettingsGeneral
        [Test]
        public void UpdateAlertSettingsGeneralSuccessTest()
        {
            long jobShiftId = 50;
            long staffUserId = 23;
            long lhcUserId = 14;
            long parentJobId = 345;
            Assert.DoesNotThrow(() =>
                crewgoService.UpdateAlertSettingsGeneral(
                    jobShiftId,
                    staffUserId,
                    lhcUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsGeneralInvalidJobShiftIdTest()
        {
            long jobShiftId = -123;
            long staffUserId = 23;
            long lhcUserId = 14;
            long parentJobId = 345;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdateAlertSettingsGeneral(
                    jobShiftId,
                    staffUserId,
                    lhcUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsGeneralInvalidStaffUserIdTest()
        {
            long jobShiftId = 50;
            long staffUserId = -234;
            long lhcUserId = 14;
            long parentJobId = 345;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdateAlertSettingsGeneral(
                    jobShiftId,
                    staffUserId,
                    lhcUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsGeneralInvalidLHCUserIdTest()
        {
            long jobShiftId = 50;
            long staffUserId = 23;
            long lhcUserId = -14;
            long parentJobId = 345;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdateAlertSettingsGeneral(
                    jobShiftId,
                    staffUserId,
                    lhcUserId,
                    parentJobId
                )
            );
        }
        [Test]
        public void UpdateAlertSettingsGeneralInvalidParentJobIdTest()
        {
            long jobShiftId = 50;
            long staffUserId = 23;
            long lhcUserId = 14;
            long parentJobId = -345;
            Assert.DoesNotThrow(() =>
                crewgoService.UpdateAlertSettingsGeneral(
                    jobShiftId,
                    staffUserId,
                    lhcUserId,
                    parentJobId
                )
            );
        }
        #endregion
        #region MaintainAlertLog
        [Test]
        public void MaintainAlertLogSuccessTest()
        {
            string jobTitle = "Random Title";
            string jobNumber = "#Random123";
            long jobShiftId = 1234123;
            string staffPriorityType = "Random Priority Type";
            long? partnerId = 123333;
            int shiftReqNumber = 42;
            int skillId = 234;
            int levelId = 21;
            DateTime? startTime = DateTime.Now + TimeSpan.FromDays(23);
            DateTime? endTime = DateTime.Now + TimeSpan.FromDays(203);
            long staffId = 99345;
            string staffName = "Jon Snow"; 
            string staffEmail = "KnowNothing@Winterfall.com";
            string deviceToken = "SomeRandomHexKey";
            Assert.DoesNotThrowAsync(() =>
                crewgoService.MaintainAlertLog(
                    jobTitle,
                    jobNumber,
                    jobShiftId,
                    staffPriorityType,
                    partnerId,
                    shiftReqNumber,
                    skillId,
                    levelId,
                    startTime,
                    endTime,
                    staffId,
                    staffName,
                    staffEmail,
                    deviceToken
                )
            );
        }
        #endregion
        #endregion
        #region Potential Staff Algorithm Tests
        #region Get Method Tests
        #region GetPotentialStaffCount
        [Test]
        public void GetPotentialStaffCountSuccessTest()
        {
            long jobShiftId = 1201;
            Assert.DoesNotThrowAsync(() =>
                crewgoService.GetPotentialStaffCount(jobShiftId)
            );
        }
        [Test]
        public async Task GetPotentialStaffCountInvalidJobShiftIdTest()
        {
            long jobShiftId = -1201;
            var response = 
                await crewgoService.GetPotentialStaffCount(jobShiftId);
            var expectedResponse = 0;
            Assert.AreEqual(expectedResponse, response);

        }
        [Test]
        public async Task GetPotentialStaffCountPositiveCountTest()
        {
            long jobShiftId = 17840;
            var response = 
                await crewgoService.GetPotentialStaffCount(jobShiftId);
            Assert.GreaterOrEqual(response, 0);
        }
        #endregion
        #region GetShiftSociabilityFactor
        [Test]
        public void GetShiftSociabilityFactorSuccessTest()
        {
            long jobShiftId = 1;
            Assert.DoesNotThrowAsync(() =>
                crewgoService.GetShiftSociabilityFactor(jobShiftId)
            );
        }
        [Test]
        public void GetShiftSociabilityFactorInvalidJobShiftIdTest()
        {
            long jobShiftId = -1201;
            Assert.ThrowsAsync<HttpRequestException>(() =>
                crewgoService.GetShiftSociabilityFactor(jobShiftId)
            );
        }
        [Test]
        public async Task GetShiftSociabilityFactorPositiveCountTest()
        {
            long jobShiftId = 1;
            var response =
                await crewgoService.GetShiftSociabilityFactor(jobShiftId);
            Assert.GreaterOrEqual(response, 0);
        }
        #endregion
        #region GetAllActivityLevels
        [Test]
        public void GetAllActivityLevelsSuccessTest()
        {
            Assert.DoesNotThrowAsync(() =>
                crewgoService.GetAllActivityLevels()
            );
        }
        [Test]
        public async Task GetAllActivityLevelsTypeTest()
        {
            var response = await crewgoService.GetAllActivityLevels();
            Assert.IsInstanceOf<List<ActivityLevel>>(response);
        }
        [Test]
        public async Task GetAllActivityLevelsCountTest()
        {
            var response = await crewgoService.GetAllActivityLevels();
            Assert.GreaterOrEqual(response.Count, 0);
        }
        #endregion
        #region GetPotentialStaffs
        [Test]
        public void GetPotentialStaffsSuccessTest()
        {
            long jobShiftId = 1127;
            var count = 12;
            Assert.DoesNotThrowAsync(() =>
                crewgoService.GetPotentialStaffs(jobShiftId, count)
            );
        }
        [Test]
        public async Task GetPotentialStaffsTypeTest()
        {
            long jobShiftId = 1201;
            var count = 12;
            var response = 
                await crewgoService.GetPotentialStaffs(jobShiftId, count);
            Assert.IsInstanceOf<List<PotentialStaff>>(response);
        }
        [Test]
        public async Task GetPotentialStaffsInvalidJobShiftIdTest()
        {
            long jobShiftId = -1201;
            var count = 12;
            var response =
                await crewgoService.GetPotentialStaffs(jobShiftId, count);
            Assert.IsEmpty(response);
        }
        [Test]
        public async Task GetPotentialStaffsInvalidCountTest()
        {
            long jobShiftId = 1127;
            var count = -12;
            var response = 
                await crewgoService.GetPotentialStaffs(jobShiftId, count);
            Assert.IsEmpty(response);
        }
        [Test]
        public async Task GetPotentialStaffsPositiveCountTest()
        {
            long jobShiftId = 1023;
            var count = 12;
            var response =
                await crewgoService.GetPotentialStaffs(jobShiftId, count);
            Assert.GreaterOrEqual(response.Count, 0);
        }
        #endregion
        #region GetShortNoticePeriod
        [Test]
        public void GetShortNoticePeriodSuccessTest()
        {
            Assert.DoesNotThrowAsync(() =>
                crewgoService.GetShortNoticePeriod()
            );
        }
        [Test]
        public async Task GetShortNoticePeriodPositiveCountTest()
        {
            var response =
                await crewgoService.GetShortNoticePeriod();
            Assert.GreaterOrEqual(response, TimeSpan.FromHours(0));
        }
        #endregion
        #endregion
        #region Post Method Tests
        #region GeneratePotentialStaffList
        [Test]
        public void GeneratePotentialStaffListSuccessTest()
        {
            long jobShiftId = 2356;
            Assert.DoesNotThrowAsync(() => 
                crewgoService.GeneratePotentialStaffList(jobShiftId)    
            );
        }
        #endregion
        #region UpdatePotentialStaffList
        [Test]
        public void UpdatePotentialStaffListSuccessTest()
        {
            long userId = 153;
            long jobShiftId = 17840;
            bool isScheduled = false;
            bool isSent = true;
            Assert.DoesNotThrow(() =>
                crewgoService.UpdatePotentialStaffList(
                    userId, jobShiftId, isSent, isScheduled)
            );
        }
        [Test]
        public void UpdatePotentialStaffListInvalidJobShiftIdTest()
        {
            long userId = 123;
            long jobShiftId = -2356;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdatePotentialStaffList(
                    userId, jobShiftId, true, false)
            );
        }
        [Test]
        public void UpdatePotentialStaffListInvalidUserIdTest()
        {
            long userId = -123;
            long jobShiftId = 2356;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.UpdatePotentialStaffList(
                    userId, jobShiftId, false, false)
            );
        }
        #endregion
        #region UpdatePotentialStaffListAsync
        [Test]
        public void UpdatePotentialStaffListAsyncSuccessTest()
        {
            long userId = 153;
            long jobShiftId = 17840;
            Assert.DoesNotThrowAsync(() =>
                crewgoService.UpdatePotentialStaffListAsync(
                    userId, jobShiftId, true, false)
            );
        }
        [Test]
        public void UpdatePotentialStaffListAsyncInvalidJobShiftIdTest()
        {
            long userId = 123;
            long jobShiftId = -2356;
            Assert.ThrowsAsync<HttpRequestException>(() =>
                crewgoService.UpdatePotentialStaffListAsync(
                    userId, jobShiftId, false, false)
            );
        }
        [Test]
        public void UpdatePotentialStaffListAsyncInvalidUserIdTest()
        {
            long userId = -123;
            long jobShiftId = 2356;
            Assert.ThrowsAsync<HttpRequestException>(() =>
                crewgoService.UpdatePotentialStaffListAsync(
                    userId, jobShiftId, false, false)
            );
        }
        #endregion
        #endregion
        #region Put Method Tests
        #endregion
        #region Custom Method Tests
        #region ComputePotentialStaffResult
        [Test]
        public void ComputePotentialStaffResultSuccessTest()
        {
            int numberOfStaffs = 215;
            int numberOfShifts = 5;
            float ssf = 1;
            Assert.DoesNotThrow(() =>
                crewgoService.ComputePotentialStaffResult(
                    numberOfStaffs,
                    numberOfShifts,
                    ssf
                )
            );
        }
        [Test]
        public void ComputePotentialStaffResultZeroShiftTest()
        {
            int numberOfStaffs = 215;
            int numberOfShifts = 0;
            float ssf = 1;
            Assert.Throws<DivideByZeroException>(() =>
                crewgoService.ComputePotentialStaffResult(
                    numberOfStaffs,
                    numberOfShifts,
                    ssf
                )
            );
        }
        [Test]
        public void ComputePotentialStaffResultValueTest()
        {
            int numberOfStaffs = 215;
            int numberOfShifts = 5;
            float ssf = 1;
            int expectedResult = 430;
            var result = crewgoService.ComputePotentialStaffResult(
                            numberOfStaffs,
                            numberOfShifts,
                            ssf
                        );
            Assert.AreEqual(expectedResult, result);
        }
        #endregion
        #region ComputeNumberOfRequests (From TimeSpan)
        [Test]
        public void ComputeNumberOfRequestsSuccessTest()
        {
            TimeSpan time = TimeSpan.FromSeconds(24900);
            TimeSpan rate = TimeSpan.FromSeconds(60);
            int shiftMultiple = 2;
            Assert.DoesNotThrow(() =>
                crewgoService.ComputeNumberOfRequests(time, rate, shiftMultiple)
            );
        }
        [Test]
        public void ComputeNumberOfRequestsZeroShiftTest()
        {
            TimeSpan time = TimeSpan.FromSeconds(24900);
            TimeSpan rate = TimeSpan.FromSeconds(0);
            int shiftMultiple = 2;
            Assert.Throws<DivideByZeroException>(() =>
                crewgoService.ComputeNumberOfRequests(time, rate, shiftMultiple)
            );
        }
        [Test]
        public void ComputeNumberOfRequestsValueTest()
        {
            TimeSpan time = TimeSpan.FromSeconds(24900);
            TimeSpan rate = TimeSpan.FromSeconds(60);
            int shiftMultiple = 2;
            var expectedResult = 830;
            var result = crewgoService.ComputeNumberOfRequests(
                            time, rate, shiftMultiple);
            Assert.AreEqual(expectedResult, result);
        }
        #endregion
        #region ComputeNumberOfRequests (From PSR)
        [Test]
        public void ComputeNumberOfRequestsFromPSRSuccessTest()
        {
            double currentPSR = 213.894;
            double nextPSR = 200.178;
            Assert.DoesNotThrow(() =>
                crewgoService.ComputeNumberOfRequests(currentPSR, nextPSR)
            );
        }
        [Test]
        public void ComputeNumberOfRequestsFromPSRValueTest()
        {
            double currentPSR = 213.894;
            double nextPSR = 200.178;
            int expectedResult = 13;
            int numberOfRequests =
                crewgoService.ComputeNumberOfRequests(currentPSR, nextPSR);
            Assert.AreEqual(expectedResult, numberOfRequests);
        }
        #endregion
        #region CalculateNextPSR
        [Test]
        public void CalculateNextPSRValueTest()
        {
            var nextAL = new ActivityLevel()
            {
                id = 1,
                name = "some name",
                p_staff_result_range_from = 100,
                p_staff_result_range_to = 200,
                shift_request_number = 4,
                shift_time_interval = 60,
                shift_notice_time = 70,
                peak_price_factor = 1.79M,
                entered_date = DateTime.Now - TimeSpan.FromDays(1),
                updated_date = DateTime.Now,
                entered_by = 1,
                updated_by = 1,
                type = "Mild",
                filling_probability = 78
            };
            var expectedResult = 20;
            var result = crewgoService.CalculateNextPSR(nextAL);
            Assert.AreEqual(expectedResult, result);
        }
        #endregion
        #endregion
        #endregion
        #endregion
        #region CREWGO Notifications Service Tests
        #region Get Method Tests
        #region GetPreNotifications
        [Test]
        public void GetPreNotificationsSuccessTest()
        {
            Assert.DoesNotThrowAsync(() =>
                crewgoService.GetPreNotifications()
            );
        }
        [Test]
        public async Task GetPreNotificationsTypeTest()
        {
            var response = await crewgoService.GetPreNotifications();
            Assert.IsInstanceOf<IEnumerable<PreNotification>>(response);
        }
        [Test]
        public async Task GetPreNotificationsCountTest()
        {
            var response = (List<PreNotification>)await crewgoService.GetPreNotifications();
            Assert.GreaterOrEqual(response.Count, 0);
        }
        #endregion
        #region GetUserById
        [Test]
        public void GetUserByIdSuccessTest()
        {
            long userId = 227;
            Assert.DoesNotThrowAsync(() => crewgoService.GetUserById(userId));
        }
        [Test]
        public async Task GetUserByIdTypeTest()
        {
            long userId = 227;
            var response = await crewgoService.GetUserById(userId);
            Assert.IsInstanceOf<User>(response);
        }
        [Test]
        public void GetUserByIdInvalidUserIdTest()
        {
            long userId = -227;
            Assert.ThrowsAsync<HttpRequestException>(() => 
                crewgoService.GetUserById(userId)
            );
        }
        #endregion
        #region GetJobById
        [Test]
        public void GetJobByIdSuccessTest()
        {
            long jobId = 1;
            Assert.DoesNotThrowAsync(() => crewgoService.GetJobById(jobId));
        }
        [Test]
        public async Task GetJobByIdTypeTest()
        {
            long jobId = 1;
            var response = await crewgoService.GetJobById(jobId);
            Assert.IsInstanceOf<Job>(response);
        }
        [Test]
        public void GetJobByIdInvalidJobIdTest()
        {
            long jobId = -1;
            Assert.ThrowsAsync<HttpRequestException>(() =>
                crewgoService.GetJobById(jobId)
            );
        }
        #endregion
        #region GetJobShiftById
        [Test]
        public void GetJobShiftByIdSuccessTest()
        {
            long jobShiftId = 1;
            Assert.DoesNotThrowAsync(() => crewgoService.GetJobShiftById(jobShiftId));
        }
        [Test]
        public async Task GetJobShiftByIdTypeTest()
        {
            long jobShiftId = 1;
            var response = await crewgoService.GetJobShiftById(jobShiftId);
            Assert.IsInstanceOf<JobShift>(response);
        }
        [Test]
        public void GetJobShiftByIdInvalidJobShiftIdTest()
        {
            long jobShiftId = -1;
            Assert.ThrowsAsync<HttpRequestException>(() =>
                crewgoService.GetJobShiftById(jobShiftId)
            );
        }
        #endregion
        #endregion
        #region Put Method Tests
        #region PutPreNotification
        [Test]
        public void PutPreNotificationSuccessTest()
        {
            int notificationId = 1258;
            Assert.DoesNotThrow(() =>
                crewgoService.PutPreNotification(notificationId, true)
            );
        }
        [Test]
        public void PutPreNotificationInvalidNotificationIdTest()
        {
            int notificationId = -7248;
            Assert.Throws<HttpRequestException>(() =>
                crewgoService.PutPreNotification(notificationId, true)
            );
        }
        #endregion
        #endregion
        #region Custom Method Tests
        #region MaintainLog
        [Test]
        public void MaintainLogSuccessTest()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Thread.Sleep(TimeSpan.FromMilliseconds(12));
            sw.Stop();
            Assert.DoesNotThrowAsync(() => crewgoService.MaintainLog(sw));
        }
        #endregion
        #endregion
        #endregion
        #region Auth Service Tests
        [Test]
        public async Task AuthenticateDashboardUseSuccessTest()
        {
            string email = "crewgo_admin@gmail.com";
            string password = "password!@#";
            bool expected = true;
            Assert.AreEqual(
                expected, 
                await crewgoService.AuthenticateDashboardUse(email, password)
            );
        }
        [Test]
        public async Task AuthenticateDashboardUseInvalidEmailTest()
        {
            string email = "new_email@gmail.com";
            string password = "password!@#";
            bool expected = false;
            Assert.AreEqual(
                expected,
                await crewgoService.AuthenticateDashboardUse(email, password)
            );
        }
        [Test]
        public async Task AuthenticateDashboardUseWrongPasswordTest()
        {
            string email = "crewgo_admin@gmail.com";
            string password = "thiispassword";
            bool expected = false;
            Assert.AreEqual(
                expected,
                await crewgoService.AuthenticateDashboardUse(email, password)
            );
        }
        #endregion 
        [TearDown]
        public void EndTest()
        {
            crewgoService = null;
        }
    }
}