//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_job_staff_break_history
    {
        public int id { get; set; }
        public long job_staff_break_id { get; set; }
        public Nullable<System.DateTime> in_time { get; set; }
        public Nullable<System.DateTime> out_time { get; set; }
        public Nullable<System.DateTime> entered_date { get; set; }
    }
}
