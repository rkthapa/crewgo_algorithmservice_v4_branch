﻿using CREWGO.Service.BusinessService.Business;
using CREWGO.Service.BusinessService.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Facade
{
    public class CREWGOBaseServiceFacade
    {
        #region ACTIVITY LEVEL
        /// <summary>
        /// GetActivityLevelByValue - Determine the activity level according to inputed potential staff
        /// </summary>
        /// <param name="potentialStaff"></param>
        /// <returns>ActivityLevel</returns>
        public async Task<ActivityLevel> GetActivityLevelByValue(int potentialStaff)
        {
            return await Task.Run(() => new ActivityLevelBusiness().GetByValue(potentialStaff));
        }
        /// <summary>
        /// Gets all Activity Levels.
        /// </summary>
        /// <returns>List of Activity Level</returns>
        public async Task<List<ActivityLevel>> GetAllActivityLevels()
        {
            return await Task.Run(() => new ActivityLevelBusiness().GetAll());
        }
        /// <summary>
        /// Gets the Short Notice Period set in admin panel.
        /// </summary>
        /// <returns>Short Notice Period</returns>
        public async Task<int> GetShortNoticePeriod()
        {
            return await new ActivityLevelBusiness().GetShortNoticePeriod();
        }
        #endregion 

        #region AUTH
        /// <summary>
        /// Saves authentication data to database.
        /// </summary>
        /// <param name="auth">NotificationAuth object</param>
        /// <returns>True if Success, false otherwise</returns>
        public async Task<bool> SaveAuthData(NotificationAuth auth)
        {
            return await new AuthBusiness().SaveAuthData(auth);
        }
        /// <summary>
        /// Gets all authentication credentials from the database
        /// </summary>
        /// <returns>List of NotificationAuth objects</returns>
        public List<NotificationAuth> GetAuthData()
        {
            return new AuthBusiness().GetAuthData();
        }
        #endregion

        #region DEVICE INFO
        /// <summary>
        /// GetAllDevices - Gets a list of all registerd devices for CREWGO
        /// </summary>
        /// <returns>List<DeviceInfo></returns>
        public async Task<List<DeviceInfo>> GetAllDevices()
        {
            return await Task.Run(() => new DeviceInfoBusiness().GetAll());
        }
        #endregion

        #region JOB
        /// <summary>
        /// GetAllJobs - Gets a list of all jobs for CREWGO
        /// </summary>
        /// <returns>List<Job></returns>
        public async Task<List<Job>> GetAllJobs()
        {
            try { return await Task.Run(() => new JobBusiness().GetAll()); }
            catch (Exception) { throw; }
        }
        /// <summary>
        /// GetJobById - Gets a job associated with the inputted jobId.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Job</returns>
        public async Task<Job> GetJobById(int id)
        {
            return await Task.Run(() => new JobBusiness().GetById(id)); 
        }
        #endregion

        #region JOB SHIFT
        public async Task<JobShift> GetShiftById(long id)
        {
            return await Task.Run(() => new JobShiftBusiness().GetById(id));
        }
        /// <summary>
        /// GetAllJobShifts - Gets a list of all job shifts for CREWGO
        /// </summary>
        /// <returns>List<JobShifts></returns>
        public async Task<List<JobShift>> GetAllShifts()
        {
            return await Task.Run(() => new JobShiftBusiness().GetAll());
        }
        /// <summary>
        /// GetLatestShifts - Gets a list of latest available job shifts for CREWGO
        /// </summary>
        /// <returns>List<JobShifts></returns>
        public async Task<List<JobShift>> GetLatestShifts()
        {
            return await Task.Run(() => new JobShiftBusiness().GetLatestShifts());
        }
        #endregion

        #region JOB STAFF
        /// <summary>
        /// Add a JobStaff.
        /// </summary>
        /// <param name="jobStaff">JobStaff object.</param>
        /// <returns>Count of added JobStaff.</returns>
        public async Task<int> AddJobStaff(JobStaff jobStaff)
        {
            return await new JobStaffBusiness().AddJobStaff(jobStaff);
        }
        #endregion

        #region JOB ALERT
        /// <summary>
        /// GetAllJobAlerts - Gets a list of all job notifications for CREWGO
        /// </summary>
        /// <returns>List<JobAlert></returns>
        public async Task<List<JobAlert>> GetAllJobAlerts()
        {
            return await Task.Run(() => new JobAlertBusiness().GetAll());
        }
        /// <summary>
        /// GetAlertByShiftIdAndUserId - Gets job alerts associated with the inputted jobShiftId and staffUserId.
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <param name="staffUserId"></param>
        /// <returns></returns>
        public async Task<JobAlert> GetAlertByShiftIdAndUserId(long jobShiftId, long staffUserId)
        {
            try { return await Task.Run(() => new JobAlertBusiness().GetByShiftIdAndUserId(jobShiftId, staffUserId)); }
            catch (Exception) { throw; }
        }
        // <summary>
        /// Post a JobAlert.
        /// </summary>
        /// <param name="jobAlert">Job Alert Object</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> PostJobAlert(JobAlert jobAlert)
        {
            return await new JobAlertBusiness().InsertJobAlert(jobAlert);
        }
        #endregion

        #region JOB ALERT SETTING
        /// <summary>
        /// GetAlertSettingByShiftIdAndUserId - Gets job alert setting associated with the inputted jobShiftId and staffUserId.
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <param name="staffUserId"></param>
        /// <returns></returns>
        public async Task<JobAlertSetting> GetAlertSettingByShiftIdAndUserId(long jobShiftId, long staffUserId)
        {
            return await Task.Run(() => new JobAlertSettingBusiness().GetByShiftIdAndUserId(jobShiftId, staffUserId));
        }
        /// <summary>
        /// GetAlertSettingByPartnerIdAndUserId - Gets global job alert setting associated with inputted partnerId and staffUserId.
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="staffUserId"></param>
        /// <returns></returns>
        public async Task<JobAlertSettingGlobal> GetAlertSettingByPartnerIdAndUserId(long partnerId, long staffUserId)
        {
            return await Task.Run(() => new JobAlertSettingGlobalBusiness().GetByPartnerIdAndUserId(partnerId, staffUserId));
        }
        /// <summary>
        /// Get JobAlertSetting object by ID.
        /// </summary>
        /// <param name="id">JobAlertSetting ID.</param>
        /// <returns>JobAlertSetting object.</returns>
        public async Task<JobAlertSetting> GetJobAlertSettingById(long id)
        {
            return await new JobAlertSettingBusiness().GetById(id);
        }
        /// <summary>
        /// Inserts a JobAlertSertting object.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> InsertJobAlertSetting(JobAlertSetting jobAlertSetting)
        {
            return await new JobAlertSettingBusiness().InsertJobAlertSetting(jobAlertSetting);
        }
        /// <summary>
        /// Updates a JobAlertSertting object.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> UpdateJobAlertSetting(JobAlertSetting jobAlertSetting)
        {
            return await new JobAlertSettingBusiness().UpdateJobAlertSetting(jobAlertSetting);
        }

        #region GLOBAL
        #endregion
        /// <summary>
        /// Get a JobAlertSettingGlobal object by ID.
        /// </summary>
        /// <param name="id">JobAlertSettingGlobal ID.</param>
        /// <returns>JobAlertSettingGlobal object.</returns>
        public async Task<JobAlertSettingGlobal> GetJobAlertSettingGlobalById(long id)
        {
            return await new JobAlertSettingGlobalBusiness().GetById(id);
        }
        /// <summary>
        /// Updates a JobAlertSettingGlobal object.
        /// </summary>
        /// <param name="jobAlertSettingGlobal">JobAlertSettingGlobal object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> UpdateJobAlertSettingGlobal(JobAlertSettingGlobal jobAlertSettingGlobal)
        {
            return await new JobAlertSettingGlobalBusiness().UpdateJobAlertSettingGlobal(jobAlertSettingGlobal);
        }
        #endregion

        #region POTENTIAL STAFF
        /// <summary>
        /// Generates potential staff list in database
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        public async Task GeneratePotentialStaffList(long jobShiftId)
        {
            await Task.Run(() => new PotentialStaffBusiness()
                                    .GeneratePotentialStaffList(jobShiftId));
        }
        /// <summary>
        /// Gets Potential Staff Count
        /// </summary>
        /// <param name="jobShiftId">The Shift Id for the Job</param>
        /// <returns>Count of Potential Staff</returns>
        public async Task<int> GetPotentialStaffCount(long jobShiftId)
        {
            return await new PotentialStaffBusiness()
                                        .GetPotentialStaffCount(jobShiftId);
        }
        /// <summary>
        /// Gets top potential staffs for a job shift limited by 'count'
        /// </summary>
        /// <param name="shiftId">The Job Shift's ID</param>
        /// <param name="count">The number of potential staffs to fetch</param>
        /// <returns>List of PotentialStaff objects</returns>
        public async Task<List<PotentialStaff>> GetPotentialStaffs(
            long shiftId, int count)
        {
            return await new PotentialStaffBusiness()
                            .GetPotentialStaffs(shiftId, count);
        }
        /// <summary>
        /// Gets the shift sociability factor for particular shift
        /// </summary>
        /// <param name="jobShiftId">The job shift's ID</param>
        /// <returns>Shift sociability factor (0 <= SSF <= 1)</returns>
        public async Task<float> GetShiftSociabilityFactor(long jobShiftId)
        {
            return await new PotentialStaffBusiness()
                            .GetShiftSociabilityFactor(jobShiftId);
        }
        /// <summary>
        /// Updates the potential staff table for a particular shift-user 
        /// combination with value of is_notification_sent as true
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <param name="isSent">If notification is sent.</param>
        /// <param name="isScheduled">If notification is scheduled.</param>
        public async Task UpdatePotentialStaffList(
            long userId, long shiftId, bool isSent, bool isScheduled)
        {
            await Task.Run(() => 
                new PotentialStaffBusiness()
                    .UpdatePotentialStaffList(
                        userId, 
                        shiftId,
                        isSent,
                        isScheduled
                    )
            );
        }
        /// <summary>
        /// Checks notification status for user id - shift id combination.
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <returns>True if notification is sent, false otherwise.</returns>
        public async Task<bool> CheckNotificationStatus(
            long userId, long shiftId)
        {
            return await new PotentialStaffBusiness().CheckNotificationStatus(
                userId, shiftId);
        }
        #endregion

        #region USER

        /// <summary>
        /// GetUserById - Gets a user for CREWGO
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<User> GetUserById(long id)
        {
            return await new UserBusiness().GetById(id);
        }
        /// <summary>
        /// Gets user from email
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns>User</returns>
        public async Task<User> GetAdminUserByEmail(string email)
        {

            return await Task.Run(
                () => new UserBusiness().GetAdminByEmail(email)
            );
        }
        /// <summary>
        /// GetAvailableStaffByGlobalPriority - 
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="skillId"></param>
        /// <param name="levelId"></param>
        /// <param name="userLimit"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public async Task<List<User>> GetAvailableStaffByGlobalPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            return await Task.Run(() => new UserBusiness().GetAvailableStaffByGlobalPriority(partnerId, jobShiftId, skillId, levelId, userLimit, startTime, endTime));
        }
        /// <summary>
        /// GetAvailableStaffByGeneralPriority
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="jobShiftId"></param>
        /// <param name="skillId"></param>
        /// <param name="levelId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public async Task<List<User>> GetAvailableStaffByGeneralPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            return await Task.Run(() => new UserBusiness().GetAvailableStaffByGeneralPriority(partnerId, jobShiftId, skillId, levelId, userLimit, startTime, endTime));
        }
        /// <summary>
        /// GetAvailableStaffByManualPriority
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public async Task<List<User>> GetAvailableStaffByManualPriority(long jobShiftId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            return await Task.Run(() => new UserBusiness().GetAvailableStaffByManualPriority(jobShiftId, userLimit, startTime, endTime));
        }
        #endregion

        #region CALL INFO
        /// <summary>
        /// GetUserById - Gets a call record by ID
        /// </summary>
        /// <param name="callId">The call id</param>
        /// <returns>VideoCall object</returns>
        public async Task<CallInfo> GetCallInfoById(long callId)
        {
            return await new CallInfoBusiness().GetById(callId);
        }
        #endregion

        #region PRENOTIFICATION
        /// <summary>
        /// GetPreNotifications - Gets a list of notification that are not scheduled and already sent for CREWGO
        /// </summary>
        /// <returns>A list of PreNotification, i.e. List<PreNotification>.</returns>
        public async Task<List<PreNotification>> GetPreNotifications()
        {
            return await Task.Run(() => new PreNotificationBusiness().GetPreNotifications());
        }
        /// <summary>
        /// Gets a PreNotification object by ID.
        /// </summary>
        /// <param name="id">PreNotification ID.</param>
        /// <returns>PreNotification object.</returns>
        public async Task<PreNotification> GetPrenotificationById(long id)
        {
            return await new PreNotificationBusiness().GetById(id);
        }
        /// <summary>
        /// Updates a PreNotification object.
        /// </summary>
        /// <param name="preNotification">PreNotification object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> UpdatePreNotification(PreNotification preNotification)
        {
            return await new PreNotificationBusiness()
                .UpdatePreNotification(preNotification);
        }
        #endregion

        #region ALGORITHM RESULTS
        /// <summary>
        /// Gets results for a user-shift combination for CrewServer algorithm.
        /// </summary>
        /// <param name="shiftId">Shift ID.</param>
        /// <param name="userId">User ID.</param>
        /// <returns>List of criteria not/fulfilled by user for shift.</returns>
        public CrewServerResults GetUserResultsForShift(long shiftId, long userId)
        {
            return new CrewServerResultsBusiness().GetUserResultsForShift(
                shiftId, 
                userId
            );
        }
        /// <summary>
        /// Gets results for a user-shift combination for CrewGo algorithm.
        /// </summary>
        /// <param name="shiftId">Shift ID.</param>
        /// <param name="userId">User ID.</param>
        /// <returns>List of criteria not/fulfilled by user for shift.</returns>
        public CrewgoResults GetUserResultsForCrewgoShift(long shiftId, long userId)
        {
            return new CrewgoResultsBusiness().GetUserResultsForShift(
                shiftId,
                userId
            );
        }
        #endregion

        #region CUSTOM METHODS
        /// <summary>
        /// GetJobAlertCount - Gets the count of unread alerts sent to staff users for CREWGO
        /// </summary>
        /// <param name="staffUserId"></param>
        /// <returns>Integer</returns>
        public async Task<Int32> GetJobAlertCount(long staffUserId)
        {
            return await Task.Run(() => new JobAlertBusiness().GetAlertCount(staffUserId));
        }
        /// <summary>
        /// OrderPriorityExists - Check if staff is ordered priority wise for CREWGO job shifts
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <returns>Boolean</returns>
        public async Task<Boolean> OrderPriorityExists(long jobShiftId)
        {
            return await Task.Run(() => new JobAlertSettingBusiness().OrderPriorityExists(jobShiftId));
        }
        /// <summary>
        /// GlobalPriorityExists
        /// </summary>
        /// <param name="partnerId"></param>
        /// <param name="skillId"></param>
        /// <param name="levelId"></param>
        /// <returns></returns>
        public async Task<Boolean> GlobalPriorityExists(long partnerId, int skillId, int levelId)
        {
            return await Task.Run(() => new JobAlertSettingGlobalBusiness().GlobalPriorityExists(partnerId, skillId, levelId));
        }
        /// <summary>
        /// IsStaffAchieved
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <returns></returns>
        public async Task<Boolean> IsShiftQuotaFulfilled(long jobShiftId)
        {
            return await Task.Run(() => new UserBusiness().IsShiftQuotaFulfilled(jobShiftId));
        }
        /// <summary>
        /// Checks if staff has autobookable enabled ot not.
        /// </summary>
        /// <param name="staffId">Staff's userId.</param>
        /// <returns>True if autobookable, False otherwise.</returns>
        public async Task<Boolean> IsStaffAutobookable(long staffId)
        {
            return await Task.Run(() => new UserBusiness().IsStaffAutobookable(staffId));
        }
        /// <summary>
        /// GetShiftNoticePeriod
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetShiftNoticePeriod()
        {
            return await Task.Run(() => new JobShiftBusiness().GetShiftNoticePeriod());
        }
        #endregion
    }
}