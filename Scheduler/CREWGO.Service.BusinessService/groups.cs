//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class groups
    {
        public groups()
        {
            this.users_groups = new HashSet<users_groups>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    
        public virtual ICollection<users_groups> users_groups { get; set; }
    }
}
