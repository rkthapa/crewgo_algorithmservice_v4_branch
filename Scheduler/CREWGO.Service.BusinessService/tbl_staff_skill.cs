//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_staff_skill
    {
        public int id { get; set; }
        public int skill_id { get; set; }
        public Nullable<int> level_id { get; set; }
        public System.DateTime entered_date { get; set; }
        public Nullable<long> entered_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<long> updated_by { get; set; }
        public long staff_user_id { get; set; }
    
        public virtual tbl_user tbl_user { get; set; }
    }
}
