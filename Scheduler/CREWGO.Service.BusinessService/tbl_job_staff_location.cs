//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_job_staff_location
    {
        public long id { get; set; }
        public long job_detail_id { get; set; }
        public System.DateTime created_at { get; set; }
        public decimal gps_lat { get; set; }
        public decimal gps_lng { get; set; }
        public long staff_user_id { get; set; }
    
        public virtual tbl_job_detail tbl_job_detail { get; set; }
    }
}
