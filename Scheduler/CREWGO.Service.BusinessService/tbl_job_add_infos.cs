//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_job_add_infos
    {
        public int id { get; set; }
        public Nullable<long> job_id { get; set; }
        public Nullable<int> skill_id { get; set; }
        public Nullable<int> add_info_id { get; set; }
        public string info_value { get; set; }
        public Nullable<long> entered_by { get; set; }
        public Nullable<System.DateTime> created_at { get; set; }
    
        public virtual tbl_con_add_info tbl_con_add_info { get; set; }
        public virtual tbl_job tbl_job { get; set; }
    }
}
