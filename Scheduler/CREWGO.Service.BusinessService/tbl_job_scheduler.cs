//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_job_scheduler
    {
        public int id { get; set; }
        public long job_id { get; set; }
        public string schedule_type { get; set; }
        public Nullable<decimal> schedule_rate { get; set; }
        public Nullable<System.DateTime> start_date { get; set; }
        public Nullable<int> required_occurance { get; set; }
        public Nullable<int> created_occurance { get; set; }
        public Nullable<System.DateTime> end_date { get; set; }
        public System.DateTime entered_date { get; set; }
        public Nullable<long> entered_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<long> updated_by { get; set; }
        public Nullable<bool> status { get; set; }
        public Nullable<sbyte> is_completed { get; set; }
    
        public virtual tbl_job tbl_job { get; set; }
    }
}
