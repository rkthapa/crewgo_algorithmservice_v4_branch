﻿using CREWGO.Service.BusinessService.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace CREWGO.Service.BusinessService.Data
{
    /// <summary>
    /// Handles data layer tasks for CrewServerResults.
    /// </summary>
    public class CrewServerResultsData: BaseData
    {
        /// <summary>
        /// Gets results for a user-shift combination for CrewServer algorithm.
        /// </summary>
        /// <param name="shiftId">Shift ID.</param>
        /// <param name="userId">User ID.</param>
        /// <returns>List of criteria not/fulfilled by user for shift.</returns>
        public CrewServerResults GetUserResultsForShift(long shiftId, long userId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_CheckStaffForCrewServer";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, userId);

            CrewServerResults obj = new CrewServerResults();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindCrewServerResultsData(reader, obj);
                }
                if (obj.ShiftId != shiftId && obj.UserId != userId)
                {
                    throw new DataException("Error executing query.");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        /// <summary>
        /// Binds query output to CrewServerResults object.
        /// </summary>
        /// <param name="reader">IDatareader.</param>
        /// <param name="obj">CrewServerResults object.</param>
        private void BindCrewServerResultsData(IDataReader reader, CrewServerResults obj)
        {
            obj.UserId = GetInt64(reader, "userId");
            obj.ShiftId = GetInt64(reader, "jobShiftId");
            obj.IsUserStaff = GetBoolean(reader, "isStaff");
            obj.IsAlreadyNotified = GetBoolean(reader, "isAlreadyNotified");
            obj.IsStaffManual = GetBoolean(reader, "isStaffManual");
            obj.IsStaffGlobal = GetBoolean(reader, "isStaffGlobal");
            obj.IsActive = GetBoolean(reader, "isStaffActive");
            obj.IsEmailVerified = GetBoolean(reader, "isEmailVerified");
            obj.HasRequiredSkill = GetBoolean(reader, "hasSkill");
            obj.HasRequiredSubSkill = GetBoolean(reader, "hasSubSkill");
            obj.HasRequiredQualification = GetBoolean(reader, "hasQualification");
            obj.IsLoggedIn = GetBoolean(reader, "isLoggedIn");
            obj.IsAssociatedToSamePartner = GetBoolean(reader, "arePartnersSame");
            obj.IsActiveForPartner = GetBoolean(reader, "isActiveForPartner");
            obj.IsAvailable = GetBoolean(reader, "isAvailable");
        }
    }
}
