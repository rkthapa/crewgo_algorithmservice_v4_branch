﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class UserData : BaseData
    {
        #region BASE METHODS

        public async Task<List<User>> GetAll()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetUsersAll";
            DbCommand dbCommand = db.GetStoredProcCommand(query);

            List<User> lstUser = new List<User>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        User obj = new User();
                        BindUserData(reader, obj);
                        obj.UserDevice = await Task.Run(() => 
                            new DeviceInfoData().GetById(obj.UserId)
                        );
                        lstUser.Add(obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return lstUser;
        }
        public async Task<User> GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetUserById";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, id);

            User obj = new User();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindUserData(reader, obj);

                        if (obj.GroupId == (int)UserGroupEnum.STAFF)
                            obj.UserDevice = await Task.Run(() => 
                                new DeviceInfoData().GetById(obj.UserId)
                            );
                        else
                            obj.UserDevices = await Task.Run(() => 
                                new DeviceInfoData().GetDevicesById(obj.UserId)
                            );
                    }
                }
                if (obj.UserId != id)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        /// <summary>
        /// Gets user from email
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns>User</returns>
        public User GetAdminByEmail(string email)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetAdminUserByEmail";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@userEmail", DbType.String, email);

            User obj = new User();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindUserData(reader, obj);
                    }
                }
                if (obj.Email != email)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        /// <summary>
        /// Checks if staff has autobookable enabled ot not.
        /// </summary>
        /// <param name="staffId">Staff's userId.</param>
        /// <returns>True if autobookable, False otherwise.</returns>
        public bool CheckIfStaffIsAutobookable(long staffId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_CheckStaffAutobookableStatus";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@staffId", DbType.Int64, staffId);

            bool isAutobookableEnabled = false;
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        isAutobookableEnabled = GetBoolean(
                            reader, "is_autobookable");
                    }
                }
            }
            catch (Exception) { throw; }

            return isAutobookableEnabled;
        }
        public void BindUserData(IDataReader reader, User obj)
        {
            obj.UserId = GetInt64(reader, "id");
            obj.Name = GetString(reader, "name");
            obj.Email = GetString(reader, "email");
            obj.Password = GetString(reader, "password");
            obj.PhoneNumber = GetString(reader, "phone_number");
            obj.FullAddress = GetString(reader, "full_address");
            obj.Street = GetString(reader, "street");
            obj.PostCodeId = GetNullableInt32(reader, "postcode_id");
            obj.ProfileImage = GetString(reader, "profile_image");
            obj.Description = GetString(reader, "description");
            obj.EmailVerified = GetBoolean(reader, "email_verified");
            obj.Active = GetNullableInt32(reader, "active");
            obj.LoginFrom = GetString(reader, "login_from");
            obj.ForgottenPasswordCode = GetString(reader, "forgotten_password_code");
            obj.InactivatedCode = GetNullableDateTime(reader, "inactivated_date");
            obj.EnteredDate = GetDateTime(reader, "entered_date");
            obj.UpdatedDate = GetNullableDateTime(reader, "updated_date");
            obj.LastLogin = GetNullableInt64(reader, "last_login");
            obj.Username = GetString(reader, "username");
            obj.IPAddress = GetString(reader, "ip_address");
            obj.Salt = GetString(reader, "salt");
            obj.ActivationCode = GetString(reader, "activation_code");
            obj.ForgottenPasswordTime = GetNullableInt32(reader, "forgotten_password_time");
            obj.RememberCode = GetString(reader, "remember_code");
            obj.FacebookId = GetString(reader, "facebook_id");
            obj.GoogleId = GetString(reader, "google_id");
            obj.CreatedOn = GetDateTime(reader, "created_on");
            obj.CustomerUserId = GetNullableInt32(reader, "customer_user_id");
            obj.RegisterFrom = GetString(reader, "register_from");
            obj.LHCUserId = GetNullableInt64(reader, "lhc_user_id");
            obj.GroupId = GetInt32(reader, "group_id");
        }
        public void BindUserData(DataRow dr, User obj)
        {
            obj.UserId = Utility.GetFieldValue<Int64>("id", dr);
            obj.Name = Utility.GetFieldValue<string>("name", dr);
            obj.Email = Utility.GetFieldValue<string>("email", dr);
            obj.Password = Utility.GetFieldValue<string>("password", dr);
            obj.PhoneNumber = Utility.GetFieldValue<string>("phone_number", dr);
            obj.FullAddress = Utility.GetFieldValue<string>("full_address", dr);
            obj.Street = Utility.GetFieldValue<string>("street", dr);
            obj.PostCodeId = Utility.GetFieldValue<Int32>("postcode_id", dr);
            obj.ProfileImage = Utility.GetFieldValue<string>("profile_image", dr);
            obj.Description = Utility.GetFieldValue<string>("description", dr);
            obj.EmailVerified = Utility.GetFieldValue<Boolean>("email_verified", dr);
            obj.Active = Utility.GetFieldValue<Int32>("active", dr);
            obj.LoginFrom = Utility.GetFieldValue<string>("login_from", dr);
            obj.ForgottenPasswordCode = Utility.GetFieldValue<string>("forgotten_password_code", dr);
            obj.InactivatedCode = Utility.GetFieldValue<DateTime>("inactivated_date", dr);
            obj.EnteredDate = Utility.GetFieldValue<DateTime>("entered_date", dr);
            obj.UpdatedDate = Utility.GetFieldValue<DateTime>("updated_date", dr);
            obj.LastLogin = Utility.GetFieldValue<Int64>("last_login", dr);
            obj.Username = Utility.GetFieldValue<string>("username", dr);
            obj.IPAddress = Utility.GetFieldValue<string>("ip_address", dr);
            obj.Salt = Utility.GetFieldValue<string>("salt", dr);
            obj.ActivationCode = Utility.GetFieldValue<string>("activation_code", dr);
            obj.ForgottenPasswordTime = Utility.GetFieldValue<Int32>("forgotten_password_time", dr);
            obj.RememberCode = Utility.GetFieldValue<string>("remember_code", dr);
            obj.FacebookId = Utility.GetFieldValue<string>("facebook_id", dr);
            obj.GoogleId = Utility.GetFieldValue<string>("google_id", dr);
            obj.CreatedOn = Utility.GetFieldValue<DateTime>("created_on", dr);
            obj.CustomerUserId = Utility.GetFieldValue<Int32>("customer_user_id", dr);
            obj.RegisterFrom = Utility.GetFieldValue<string>("register_from", dr);
            obj.LHCUserId = Utility.GetFieldValue<Int64>("lhc_user_id", dr);
            obj.GroupId = Utility.GetFieldValue<Int32>("group_id", dr);
        }
        
        #endregion

        #region CUSTOM METHODS

        public DataSet GetAvailableStaffByGlobalPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetStaffAvailableByGlobalPriority";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@partnerId", DbType.Int64, partnerId);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, jobShiftId);
            db.AddInParameter(dbCommand, "@skillId", DbType.Int32, skillId);
            db.AddInParameter(dbCommand, "@levelId", DbType.Int32, levelId);
            db.AddInParameter(dbCommand, "@userLimit", DbType.Int32, userLimit);
            db.AddInParameter(dbCommand, "@shiftST", DbType.DateTime, startTime);
            db.AddInParameter(dbCommand, "@shiftET", DbType.DateTime, endTime);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }
        public DataSet GetAvailableStaffByGeneralPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetStaffAvailableByGeneralPriority";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@partnerId", DbType.Int64, partnerId);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, jobShiftId);
            db.AddInParameter(dbCommand, "@skillId", DbType.Int32, skillId);
            db.AddInParameter(dbCommand, "@levelId", DbType.Int32, levelId);
            db.AddInParameter(dbCommand, "@userLimit", DbType.Int32, userLimit);
            db.AddInParameter(dbCommand, "@shiftST", DbType.DateTime, startTime);
            db.AddInParameter(dbCommand, "@shiftET", DbType.DateTime, endTime);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }
        public DataSet GetAvailableStaffByManualPriority(long jobShiftId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetStaffAvailableByManualPriority";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, jobShiftId);
            db.AddInParameter(dbCommand, "@userLimit", DbType.Int32, userLimit);
            db.AddInParameter(dbCommand, "@shiftST", DbType.DateTime, startTime);
            db.AddInParameter(dbCommand, "@shiftET", DbType.DateTime, endTime);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }
        public DataSet CheckRequiredStaff(long jobShiftId)
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_CheckRequiredStaff";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, jobShiftId);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }

        #endregion
    }
}