﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    class PotentialStaffData: BaseData
    {
        private DatabaseProviderFactory factory;
        private Database db;

        public PotentialStaffData() : base()
        {
            this.factory = new DatabaseProviderFactory();
            this.db = factory.Create(MYSQLCONNECTION);
        }
        /// <summary>
        /// Calls the stored procedure that populates 
        /// tbl_potential_staff_with_scores with potential staffs and scores
        /// </summary>
        /// <param name="shiftId">The Job Shift's ID</param>
        public void GeneratePotentialStaffList(long shiftId)
        {
            string query = "usp_SVC_GetPotentialStaffOrderedByPriority";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);
            try
            {
                db.ExecuteDataSet(dbCommand);
            }
            catch (Exception) 
            { 
                throw; 
            }
        }
        /// <summary>
        /// Gets the count of Potential Staffs from database.
        /// </summary>
        /// <param name="shiftId">The Job Shift ID</param>
        /// <returns>scalar dataset(1 row, 1 column) having 'count'</returns>
        public DataSet GetPotentialStaffCount(long shiftId)
        {
            DataSet dataSet = null;
            string query = "usp_SVC_GetPotentialStaffCount";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);

            try 
            {
                dataSet = db.ExecuteDataSet(dbCommand);
            }
            catch (Exception) 
            { 
                throw; 
            }
            return dataSet;
        }
        /// <summary>
        /// Gets the shift sociability factor from database
        /// </summary>
        /// <param name="shiftId">The Job Shift ID</param>
        /// <returns>
        /// scalar dataset(1 row, 1 column) having 'sociability_factor'
        /// </returns>
        public DataSet GetShiftSociabilityFactor(long shiftId)
        {
            DataSet dataSet = null;
            string query = "usp_SVC_GetShiftSociabilityFactor";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);

            try
            {
                dataSet = db.ExecuteDataSet(dbCommand);
                if (dataSet.Tables[0].Rows[0].IsNull("sociability_factor"))
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dataSet;
        }
        /// <summary>
        /// Gets top potential staffs for a job shift limited by 'count'
        /// </summary>
        /// <param name="shiftId">The Job Shift's ID</param>
        /// <param name="count">The number of potential staffs to fetch</param>
        /// <returns>Dataset of potential staffs</returns>
        public DataSet GetPotentialStaffs(long shiftId, int count)
        {
            DataSet dst = new DataSet();
            string query = "usp_SVC_GetPotentialStaffs";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);
            db.AddInParameter(dbCommand, "@totalCount", DbType.Int32, count);
            try 
            { 
                dst = db.ExecuteDataSet(dbCommand); 
            }
            catch (Exception) 
            { 
                throw; 
            }
            return dst;
        }
        /// <summary>
        /// Sets the value of "is_notification_sent" column to true
        /// in table "potential_staff_with_scores" for given userId, jobShiftId
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <param name="isScheduled">If notification is scheduled.</param>
        /// <param name="isSent">If notification is sent.</param>
        public void SetNotificationStatus(
            long userId, long shiftId, bool isSent, bool isScheduled)
        {
            string query = "usp_SVC_SetPotentialStaffNotificationSent";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, userId);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);
            db.AddInParameter(dbCommand, "@sent", DbType.Boolean, isSent);
            db.AddInParameter(dbCommand, "@scheduled", DbType.Boolean, isScheduled);
            try
            {
                var count = db.ExecuteNonQuery(dbCommand);
                if (count <= 0)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Checks notification status for user id - shift id combination.
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <returns>Dataset containing notification status</returns>
        public DataSet CheckNotificationStatus(long userId, long shiftId)
        {
            string query = "usp_SVC_CheckPotentialStaffNotificationStatus";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, userId);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);
            var dst = new DataSet();
            try
            {
                dst = db.ExecuteDataSet(dbCommand);
                return dst;
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Binds potential user dataset to PotentialStaff class.
        /// </summary>
        /// <param name="reader">IDataReader</param>
        /// <param name="staff">Staff class object</param>
        public void BindPStaffData (DataRow dr, PotentialStaff staff)
        {
            staff.ID = Utility.GetFieldValue<Int64>("id", dr);
            staff.UserId = Utility.GetFieldValue<Int64>("user_id", dr);
            staff.JobId = Utility.GetFieldValue<Int64>("job_id", dr);
            staff.JobShiftId = Utility.GetFieldValue<Int64>("shift_id", dr);
            staff.TotalScore = Utility.GetFieldValue<float>("total_score", dr);
        }
        /// <summary>
        /// Binds value of column 'count' as count
        /// </summary>
        /// <param name="dataSet">Dataset containing count column</param>
        /// <returns>Count of staffs (int)</returns>
        public int BindStaffCount (DataSet dataSet)
        {
            return Utility
                .GetFieldValue<Int32>("count", dataSet.Tables[0].Rows[0]);
        }
        /// <summary>
        /// Binds value of notification status to tuple.
        /// </summary>
        /// <param name="dataSet">Dataset containing notification status</param>
        /// <returns>Tuple <isSent, isScheduled></returns>
        public bool BindNotificationStatus(DataSet dataSet)
        {
            //var isScheduled = Utility.GetFieldValue<bool>(
            //    "is_notification_scheduled", dataSet.Tables[0].Rows[0]);
            var isSent = Utility.GetFieldValue<bool>(
                "is_notification_sent", dataSet.Tables[0].Rows[0]);
            return isSent;
        }
        /// <summary>
        /// Binds value of column 'sociability_factor' as ssf (decimal)
        /// </summary>
        /// <param name="dataSet">Dataset w/ sociability_factor columnn</param>
        /// <returns>Shift sociability factor (decimal)</returns>
        public float BindSSF(DataSet dataSet)
        {
            return Utility
                .GetFieldValue<float>(
                    "sociability_factor", 
                    dataSet.Tables[0].Rows[0]
                );
        }
    }
}
