﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class JobData : BaseData
    {
        #region BASE METHODS

        public List<Job> GetAll()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAll";
            DbCommand dbCommand = db.GetStoredProcCommand(query);

            List<Job> lstJobs = new List<Job>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        Job obj = new Job();
                        BindJobData(reader, obj);
                        lstJobs.Add(obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return lstJobs;
        }
        public Job GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobById";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@job_id", DbType.Int64, id);

            Job obj = new Job();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindJobData(reader, obj);
                }
                if (obj.JobId != id)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        public void BindJobData(IDataReader reader, Job obj)
        {
            obj.JobId = GetInt64(reader, "id");
            obj.JobFullAddress = GetString(reader, "job_full_address");
            obj.JobStreet = GetString(reader, "job_street");
            obj.JobPostCodeId = GetNullableInt32(reader, "job_postcode_id");
            obj.MeetingFullAddress = GetString(reader, "meeting_full_address");
            obj.MeetingStreet = GetString(reader, "meeting_street");
            obj.MeetingPostCodeId = GetNullableInt32(reader, "meeting_postcode_id");
            obj.MeetingLatitude = GetNullableDecimal(reader, "meeting_lat");
            obj.MeetingLongitude = GetNullableDecimal(reader, "meeting_long");
            obj.Description = GetString(reader, "description");
            obj.QuoteId = GetString(reader, "quote_id");
            obj.BookingNumber = GetString(reader, "booking_number");
            obj.CustomerUserId = GetNullableInt32(reader, "customer_user_id");
            obj.SupervisorUserId = GetNullableInt32(reader, "supervisor_user_id");
            obj.Status = GetNullableInt32(reader, "status");
            obj.ShiftStatus = GetNullableInt32(reader, "shift_status");
            obj.EnteredDate = GetDateTime(reader, "entered_date");
            obj.EnteredBy = GetNullableInt32(reader, "entered_by");
            obj.UpdatedDate = GetNullableDateTime(reader, "updated_date");
            obj.UpdatedBy = GetNullableInt32(reader, "updated_by");
            obj.HasPeakPrice = GetNullableBoolean(reader, "has_peak_price");
            obj.PeakPriceReason = GetString(reader, "peak_price_reason");
            obj.InductionRequired = GetNullableBoolean(reader, "induction_required");
            obj.BookAmount = GetNullableDecimal(reader, "book_amount");
            obj.LHCUserId = GetNullableInt64(reader, "lhc_user_id");
            obj.JobLocationLatitude = GetNullableDecimal(reader, "job_location_lat");
            obj.JobLocationLongitude = GetNullableDecimal(reader, "job_location_lng");
            obj.JobNumber = GetString(reader, "job_number");
            obj.JobTitle = GetString(reader, "job_title");
        }
        public void BindJobData(DataRow dr, Job obj)
        {
            obj.JobId = Utility.GetFieldValue<Int64>("id", dr);
            obj.JobFullAddress = Utility.GetFieldValue<string>("job_full_address", dr);
            obj.JobStreet = Utility.GetFieldValue<string>("job_street", dr);
            obj.JobPostCodeId = Utility.GetFieldValue<Int32>("job_postcode_id", dr);
            obj.MeetingFullAddress = Utility.GetFieldValue<string>("meeting_full_address", dr);
            obj.MeetingStreet = Utility.GetFieldValue<string>("meeting_street", dr);
            obj.MeetingPostCodeId = Utility.GetFieldValue<Int32>("meeting_postcode_id", dr);
            obj.MeetingLatitude = Utility.GetFieldValue<Decimal>("meeting_lat", dr);
            obj.MeetingLongitude = Utility.GetFieldValue<Decimal>("meeting_long", dr);
            obj.Description = Utility.GetFieldValue<string>("description", dr);
            obj.QuoteId = Utility.GetFieldValue<string>("quote_id", dr);
            obj.BookingNumber = Utility.GetFieldValue<string>("booking_number", dr);
            obj.CustomerUserId = Utility.GetFieldValue<Int32>("customer_user_id", dr);
            obj.SupervisorUserId = Utility.GetFieldValue<Int32>("supervisor_user_id", dr);
            obj.Status = Utility.GetFieldValue<Int32>("status", dr);
            obj.ShiftStatus = Utility.GetFieldValue<Int32>("shift_status", dr);
            obj.EnteredDate = Utility.GetFieldValue<DateTime>("entered_date", dr);
            obj.EnteredBy = Utility.GetFieldValue<Int32>("entered_by", dr);
            obj.UpdatedDate = Utility.GetFieldValue<DateTime>("updated_date", dr);
            obj.UpdatedBy = Utility.GetFieldValue<Int32>("updated_by", dr);
            obj.HasPeakPrice = Utility.GetFieldValue<Boolean>("has_peak_price", dr);
            obj.PeakPriceReason = Utility.GetFieldValue<string>("peak_price_reason", dr);
            obj.InductionRequired = Utility.GetFieldValue<Boolean>("induction_required", dr);
            obj.BookAmount = Utility.GetFieldValue<Decimal>("book_amount", dr);
            obj.LHCUserId = Utility.GetFieldValue<Int64>("lhc_user_id", dr);
            obj.JobLocationLatitude = Utility.GetFieldValue<Decimal>("job_location_lat", dr);
            obj.JobLocationLongitude = Utility.GetFieldValue<Decimal>("job_location_lng", dr);
            obj.JobNumber = Utility.GetFieldValue<string>("job_number", dr);
            obj.JobTitle = Utility.GetFieldValue<string>("job_title", dr);
        }
        
        #endregion
    }
}