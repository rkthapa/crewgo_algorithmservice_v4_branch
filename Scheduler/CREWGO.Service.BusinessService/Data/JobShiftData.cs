﻿using CREWGO.Service.BusinessService.Business;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class JobShiftData : BaseData
    {
        #region BASE METHODS
        
        public List<JobShift> GetAll()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobShiftsAll";
            DbCommand dbCommand = db.GetStoredProcCommand(query);

            List<JobShift> lstJobShifts = new List<JobShift>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        JobShift obj = new JobShift();

                        BindJobShiftsData(reader, obj);

                        obj.ParentJob = new JobBusiness().GetById(obj.JobId);

                        lstJobShifts.Add(obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return lstJobShifts;
        }
        public JobShift GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobShiftById";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@job_shift_id", DbType.Int64, id);

            JobShift obj = new JobShift();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindJobShiftsData(reader, obj);

                        obj.ParentJob = new JobBusiness().GetById(obj.JobId);
                    }
                }
                if (obj.JobShiftId != id)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        public void BindJobShiftsData(IDataReader reader, JobShift obj)
        {
            obj.JobShiftId = GetInt64(reader, "id");
            obj.JobId = GetInt64(reader, "job_id");
            obj.SkillId = GetInt32(reader, "skill_id");
            obj.LevelId = GetInt32(reader, "level_id");
            obj.RequiredNumber = GetInt32(reader, "required_number");
            obj.CompletedDate = GetNullableDateTime(reader, "completed_date");
            obj.StartTime = GetNullableDateTime(reader, "start_time");
            obj.EndTime = GetNullableDateTime(reader, "end_time");
            obj.HourlyRate = GetNullableDecimal(reader, "hourly_rate");
            obj.TotalHour = GetString(reader, "total_hour");
            obj.TotalCost = GetNullableDecimal(reader, "total_cost");
            obj.Qualification = GetString(reader, "qualification");
            obj.IsFilled = GetBoolean(reader, "is_filled");
            obj.IsCompleted = GetBoolean(reader, "is_completed");
            obj.UpdatedDate = GetNullableDateTime(reader, "updated_date");
            obj.UpdatedBy = GetNullableInt32(reader, "updated_by");
            obj.HasPeakPrice = GetBoolean(reader, "has_peak_price");
            obj.PeakPrice = GetNullableDecimal(reader, "peak_price");
            obj.ShiftStatus = GetNullableInt32(reader, "shift_status");
        }
        public void BindJobShiftsData(DataRow dr, JobShift obj)
        {
            obj.JobShiftId = Utility.GetFieldValue<Int64>("id", dr);
            obj.JobId = Utility.GetFieldValue<Int64>("job_id", dr);
            obj.SkillId = Utility.GetFieldValue<Int32>("skill_id", dr);
            obj.LevelId = Utility.GetFieldValue<Int32>("level_id", dr);
            obj.RequiredNumber = Utility.GetFieldValue<Int32>("required_number", dr);
            obj.CompletedDate = Utility.GetFieldValue<DateTime>("completed_date", dr);
            obj.StartTime = Utility.GetFieldValue<DateTime>("start_time", dr);
            obj.EndTime = Utility.GetFieldValue<DateTime>("end_time", dr);
            obj.EndTimeBreak = Utility.GetFieldValue<TimeSpan>("end_time_bk", dr);
            obj.HourlyRate = Utility.GetFieldValue<Decimal>("hourly_rate", dr);
            obj.TotalHour = Utility.GetFieldValue<string>("total_hour", dr);
            obj.TotalCost = Utility.GetFieldValue<Decimal>("total_cost", dr);
            obj.Qualification = Utility.GetFieldValue<string>("qualification", dr);
            obj.IsFilled = Utility.GetFieldValue<Boolean>("is_filled", dr);
            obj.IsCompleted = Utility.GetFieldValue<Boolean>("is_completed", dr);
            obj.UpdatedDate = Utility.GetFieldValue<DateTime>("updated_date", dr);
            obj.UpdatedBy = Utility.GetFieldValue<Int32>("updated_by", dr);
            obj.HasPeakPrice = Utility.GetFieldValue<Boolean>("has_peak_price", dr);
            obj.PeakPrice = Utility.GetFieldValue<Decimal>("peak_price", dr);
            obj.ShiftStatus = Utility.GetFieldValue<Int32>("shift_status", dr);
            obj.PartnerGroup = Utility.GetFieldValue<string>("partner_group", dr);
        }
        
        #endregion

        #region CUSTOM METHODS

        public DataSet GetLatestShifts()
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobShiftsLatest";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(
                dbCommand, "@shiftST", DbType.DateTime, 
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            
            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }
        public DataSet GetShiftNoticePeriod()
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetShiftNoticePeriod";

            DbCommand dbCommand = db.GetStoredProcCommand(query);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }

        #endregion
    }
}