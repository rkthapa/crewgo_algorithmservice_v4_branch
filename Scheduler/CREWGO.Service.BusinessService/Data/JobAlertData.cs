﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class JobAlertData : BaseData
    {
        #region BASE METHODS

        public List<JobAlert> GetAll()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertsAll";
            DbCommand dbCommand = db.GetStoredProcCommand(query);

            List<JobAlert> lstJobAlerts = new List<JobAlert>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        JobAlert obj = new JobAlert();
                        BindJobAlertData(reader, obj);
                        lstJobAlerts.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }

            return lstJobAlerts;
        }
        public JobAlert GetByShiftIdAndUserId(long jobShiftId, long staffUserId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "SELECT * FROM tbl_job_alert" + 
                           " WHERE job_detail_id = " + jobShiftId + 
                           " AND staff_user_id = " + staffUserId + ";";
            DbCommand dbCommand = db.GetSqlStringCommand(query);

            JobAlert obj = new JobAlert();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindJobAlertData(reader, obj);
                }
            }
            catch (Exception) 
            { 
                throw; 
            }

            return obj;
        }
        /// <summary>
        /// Adds a new JobAlert entry to tbl_job_alert.
        /// </summary>
        /// <param name="jobAlert">Job Alert Object</param>
        /// <returns>Number of rows affected.</returns>
        public int InsertJobAlert(JobAlert jobAlert)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_InsertJobAlert";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand,
                "@id", DbType.Int64, null);
            db.AddInParameter(dbCommand,
                "@jobDetailID", DbType.Int64, jobAlert.JobShiftId);
            db.AddInParameter(dbCommand,
                "@staffUserID", DbType.Int64, jobAlert.StaffUserId);
            db.AddInParameter(dbCommand,
                "@actionDate", DbType.DateTime, jobAlert.ActionDate);
            db.AddInParameter(dbCommand,
                "@alertDate", DbType.DateTime, jobAlert.AlertDate);
            db.AddInParameter(dbCommand,
                "@alertStatus", DbType.Int32, jobAlert.Status);

            try
            {
                return db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void BindJobAlertData(IDataReader reader, JobAlert obj)
        {
            obj.JobAlertId = GetInt64(reader, "id");
            obj.JobShiftId = GetInt64(reader, "job_detail_id");
            obj.StaffUserId = GetInt64(reader, "staff_user_id");
            obj.Status = GetNullableInt32(reader, "status");
            obj.ActionDate = GetNullableDateTime(reader, "action_date");
            obj.AlertDate = GetNullableDateTime(reader, "alert_date");
        }
        public void BindJobAlertData(DataRow dr, JobAlert obj)
        {
            obj.JobAlertId = Utility.GetFieldValue<Int64>("id", dr);
            obj.JobShiftId = Utility.GetFieldValue<Int64>("job_detail_id", dr);
            obj.StaffUserId = Utility.GetFieldValue<Int64>("staff_user_id", dr);
            obj.Status = Utility.GetFieldValue<Int32>("status", dr);
            obj.ActionDate = Utility.GetFieldValue<DateTime>("action_date", dr);
            obj.AlertDate = Utility.GetFieldValue<DateTime>("alert_date", dr);
        }
        
        #endregion 

        #region CUSTOM METHODS

        public DataSet GetAlertCount(long staffUserId)
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertCount";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(
                dbCommand, "@staffUserId", DbType.Int32, staffUserId);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }

        #endregion
    }
}