﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace CREWGO.Service.BusinessService.Data
{
    public class BaseData
    {
        public const string MYSQLCONNECTION = "MySQLConnection";
        protected bool HasField(IDataReader reader, string field)
        {
            field = field.ToLower();
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToLower() == field)
                {
                    return true;
                }
            }
            return false;
        }
        protected object GetValue(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? null : reader.GetValue(ordinal);
            }
            return null;
        }
        protected bool IsNullValue(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? true : false;
            }
            return true;
        }
        protected byte GetByte(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? (byte)0 : reader.GetByte(ordinal);
            }
            return 0;
        }
        protected short GetInt16(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? (short)0 : reader.GetInt16(ordinal);
            }
            return (short)0;
        }
        protected int? GetNullableInt16(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetInt16(ordinal);
            }
            return null;
        }
        protected int GetInt32(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? 0 : reader.GetInt32(ordinal);
            }
            return 0;
        }
        protected int? GetNullableInt32(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetInt32(ordinal);
            }
            return null;
        }
        protected long GetInt64(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? 0 : reader.GetInt64(ordinal);
            }
            return 0;
        }
        protected byte? GetNullableByte(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetByte(ordinal);
            }
            return null;
        }
        protected long? GetNullableInt64(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetInt64(ordinal);
            }
            return null;
        }
        protected decimal? GetNullableDecimal(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetDecimal(ordinal);
            }
            return null;
        }
        protected string GetString(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? string.Empty : reader.GetString(ordinal);
            }
            return null;
        }
        protected DateTime GetDateTime(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? DateTime.MinValue : reader.GetDateTime(ordinal);
            }
            return DateTime.MinValue;
        }
        protected DateTime? GetNullableDateTime(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetDateTime(ordinal);
            }
            return null;
        }
        protected DateTime? GetNullableDateTimeFromVarchar(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return Convert.ToDateTime(reader.GetString(ordinal));
            }
            return null;
        }
        protected TimeSpan GetTimeSpan(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? TimeSpan.MinValue : reader.GetDateTime(ordinal).TimeOfDay;;
            }
            return TimeSpan.MinValue;
        }
        protected TimeSpan? GetNullableTimeSpan(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetDateTime(ordinal).TimeOfDay;
            }
            return null;
        }
        protected bool GetBoolean(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? false : reader.GetBoolean(ordinal);
            }
            return false;
        }
        protected bool? GetNullableBoolean(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetBoolean(ordinal);
            }
            return null;
        }
        protected double GetFloat(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? 0.0f : reader.GetDouble(ordinal);
            }
            return 0.0f;
        }
        protected double? GetNullableDouble(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                if (reader.IsDBNull(ordinal))
                    return null;
                else
                    return reader.GetDouble(ordinal);
            }
            return null;
        }
        protected double GetDouble(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? 0.0d : reader.GetDouble(ordinal);
            }
            return 0.0d;
        }
        protected decimal GetDecimal(IDataReader reader, string field)
        {
            if (HasField(reader, field))
            {
                int ordinal = reader.GetOrdinal(field);
                return reader.IsDBNull(ordinal) ? 0.0m : reader.GetDecimal(ordinal);
            }
            return 0.0m;
        }
        public static bool IsNull<T>(T value)
        {
            if (typeof(T) == typeof(DateTime))
            {
                return (DateTime)Convert.ChangeType(value, typeof(DateTime)) == DateTime.MinValue;
            }
            else if (typeof(T) == typeof(int))
            {
                return (int)Convert.ChangeType(value, typeof(int)) == 0;
            }
            else if (typeof(T) == typeof(bool))
            {
                return (bool)Convert.ChangeType(value, typeof(bool)) == false;
            }
            else if (typeof(T) == typeof(float))
            {
                return (float)Convert.ChangeType(value, typeof(float)) == 0.0f;
            }
            else if (typeof(T) == typeof(double))
            {
                return (double)Convert.ChangeType(value, typeof(double)) == 0.0d;
            }
            else if (typeof(T) == typeof(decimal))
            {
                return (decimal)Convert.ChangeType(value, typeof(decimal)) == 0.0m;
            }
            else
            {
                return false;
            }
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}