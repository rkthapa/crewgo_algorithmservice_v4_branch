﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class PreNotificationData : BaseData
    {
        #region BASE METHODS

        public List<PreNotification> GetAll()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "";
            DbCommand dbCommand = db.GetStoredProcCommand(query);

            List<PreNotification> lstPreNotifications = new List<PreNotification>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        PreNotification obj = new PreNotification();
                        BindPreNotificationData(reader, obj);
                        lstPreNotifications.Add(obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return lstPreNotifications;
        }
        /// <summary>
        /// Gets an entry from tbl_pre_notification having the given ID.
        /// Binds the result of this query to a PreNotification object.
        /// </summary>
        /// <param name="id">PreNotification ID.</param>
        /// <returns>PreNotification object.</returns>
        public PreNotification GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetPreNotificationByID";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@notificationID", DbType.Int64, id);

            PreNotification obj = new PreNotification();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindPreNotificationData(reader, obj);
                    }
                }
                if (obj.NotificationId != id)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        /// <summary>
        /// Updates an entry in tbl_pre_notification.
        /// </summary>
        /// <param name="preNotification">PreNotification object.</param>
        /// <returns>Number of rows affected.</returns>
        public int UpdatePreNotification(PreNotification preNotification)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_UpdatePreNotification";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand,
                "@notificationID", DbType.Int64, preNotification.NotificationId);
            db.AddInParameter(dbCommand, 
                "@isSent", DbType.Boolean, preNotification.IsSent);
            db.AddInParameter(dbCommand, 
                "@isScheduled", DbType.Boolean, preNotification.IsScheduled);
            db.AddInParameter(dbCommand, 
                "@sentDate", DbType.DateTime, preNotification.SentDate);


            PreNotification obj = new PreNotification();
            try
            {
                return db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception) { throw; }
        }
        public void BindPreNotificationData(IDataReader reader, PreNotification obj)
        {
            obj.NotificationId = GetInt32(reader, "id");
            obj.SourceId = GetInt64(reader, "source_id");
            obj.GeneratedBy = GetNullableInt32(reader, "generated_by");
            obj.Type = GetString(reader, "type");
            obj.UserId = GetInt64(reader, "user_id");
            obj.GroupId = GetInt64(reader, "group_id");
            obj.Email = GetString(reader, "email");
            obj.Message = GetString(reader, "message");
            obj.Detail = GetString(reader, "detail");
            obj.ActiveDate = GetDateTime(reader, "active_date");
            obj.IsSent = GetBoolean(reader, "is_sent");
            obj.SentDate = GetDateTime(reader, "sent_date");
            obj.IsScheduled = GetBoolean(reader, "is_scheduled");
        }
        public void BindPreNotificationData(DataRow dr, PreNotification obj)
        {
            obj.NotificationId = Utility.GetFieldValue<Int32>("id", dr);
            obj.SourceId = Utility.GetFieldValue<Int64>("source_id", dr);
            obj.GeneratedBy = Utility.GetFieldValue<Int32>("generated_by", dr);
            obj.Type = Utility.GetFieldValue<string>("type", dr);
            obj.UserId = Utility.GetFieldValue<Int64>("user_id", dr);
            obj.GroupId = Utility.GetFieldValue<Int64>("group_id", dr);
            obj.Email = Utility.GetFieldValue<string>("email", dr);
            obj.Message = Utility.GetFieldValue<string>("message", dr);
            obj.Detail = Utility.GetFieldValue<string>("detail", dr);
            obj.ActiveDate = Utility.GetFieldValue<DateTime>("active_date", dr);
            obj.IsSent = Utility.GetFieldValue<bool>("is_sent", dr);
            obj.SentDate = Utility.GetFieldValue<DateTime>("sent_date", dr);
            obj.IsScheduled = Utility.GetFieldValue<bool>("is_scheduled", dr);
        }

        #endregion

        #region CUSTOM METHODS

        public DataSet GetPreNotifications()
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetPreNotifications";

            DbCommand dbCommand = db.GetStoredProcCommand(query);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }

        #endregion
    }
}