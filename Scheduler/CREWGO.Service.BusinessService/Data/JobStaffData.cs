﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class JobStaffData : BaseData
    {
        #region BASE METHODS
        /// <summary>
        /// Binds DataReader to JobStaff object.
        /// </summary>
        /// <param name="reader">IDataReader.</param>
        /// <param name="obj">JobStaff object.</param>
        public void BindJobStaffData(IDataReader reader, JobStaff obj)
        {
            obj.JobStaffId = GetInt64(reader, "id");
            obj.JobShiftId = GetInt64(reader, "job_detail_id");
            obj.StaffUserId = GetInt64(reader, "staff_user_id");
            obj.TotalProductionTime = GetString(reader, "total_production_time");
            obj.TotalBreakTime = GetString(reader, "total_break_time");
            obj.TotalTime = GetString(reader, "total_time");
            obj.InTimeBreak = GetNullableTimeSpan(reader, "in_time_bk");
            obj.InTime = GetDateTime(reader, "in_time");
            obj.OutTime = GetDateTime(reader, "out_time");
            obj.GpsInTime = GetDateTime(reader, "gps_in_time");
            obj.GpsOutTime = GetDateTime(reader, "gps_out_time");
            obj.OutTimeBreak = GetNullableTimeSpan(reader, "out_time_bk");
            obj.GpsInTimeBreak = GetNullableTimeSpan(reader, "gps_in_time_bk");
            obj.GpsOutTimeBreak = GetNullableTimeSpan(reader, "gps_out_time_bk");
            obj.AcceptedDate = GetNullableDateTime(reader, "accepted_date");
            obj.CompletedDate = GetNullableDateTime(reader, "completed_date");
            obj.Description = GetString(reader, "description");
            obj.IsApproved = GetBoolean(reader, "is_approved");
            obj.ApprovedBy = GetNullableInt32(reader, "approved_by");
            obj.ApprovedDate = GetDateTime(reader, "approved_date");
            obj.PinCode = GetString(reader, "pin_code");
            obj.JobAlertId = GetInt32(reader, "job_alert_id");
            obj.JobSiteLocation = GetString(reader, "job_site_location");
            obj.JobSiteLatitude = GetNullableDecimal(reader, "job_site_lat");
            obj.JobSiteLongitude = GetNullableDecimal(reader, "job_site_lng");
            obj.JobStatus = GetInt16(reader, "job_status");
            obj.IsBilled = GetNullableBoolean(reader, "is_billed");
            obj.BilledDate = GetNullableDateTime(reader, "billed_date");
            obj.TotalWage = GetNullableDecimal(reader, "total_wage");
            obj.StaffInitial = GetString(reader, "staff_initial");
            obj.NotificationStatus = GetInt32(reader, "notification_status");
            obj.InductionDaysStatus = GetInt32(reader, "induction_days_status");
            obj.InductionHoursStatus = GetInt32(reader, "induction_hrs_status");
        }
        /// <summary>
        /// Binds DataRow from query to JobStaff object.
        /// </summary>
        /// <param name="dr">Single DataRow from object.</param>
        /// <param name="obj">JobStaff object.</param>
        public void BindJobStaffData(DataRow dr, JobStaff obj)
        {
            obj.JobStaffId = Utility.GetFieldValue<Int64>("id", dr);
            obj.JobShiftId = Utility.GetFieldValue<Int64>("job_detail_id", dr);
            obj.StaffUserId = Utility.GetFieldValue<Int64>("staff_user_id", dr);
            obj.TotalProductionTime = Utility.GetFieldValue<string>("total_production_time", dr);
            obj.TotalBreakTime = Utility.GetFieldValue<string>("total_break_time", dr);
            obj.TotalTime = Utility.GetFieldValue<string>("total_time", dr);
            obj.InTimeBreak = Utility.GetFieldValue<TimeSpan>("in_time_bk", dr);
            obj.InTime = Utility.GetFieldValue<DateTime>("in_time", dr);
            obj.OutTime = Utility.GetFieldValue<DateTime>("out_time", dr);
            obj.GpsInTime = Utility.GetFieldValue<DateTime>("gps_in_time", dr);
            obj.GpsOutTime = Utility.GetFieldValue<DateTime>("gps_out_time", dr);
            obj.OutTimeBreak = Utility.GetFieldValue<TimeSpan>("out_time_bk", dr);
            obj.GpsInTimeBreak = Utility.GetFieldValue<TimeSpan>("gps_in_time_bk", dr);
            obj.GpsOutTimeBreak = Utility.GetFieldValue<TimeSpan>("gps_out_time_bk", dr);
            obj.AcceptedDate = Utility.GetFieldValue<DateTime>("accepted_date", dr);
            obj.CompletedDate = Utility.GetFieldValue<DateTime>("completed_date", dr);
            obj.Description = Utility.GetFieldValue<string>("description", dr);
            obj.IsApproved = Utility.GetFieldValue<Boolean>("is_approved", dr);
            obj.ApprovedBy = Utility.GetFieldValue<Int32>("approved_by", dr);
            obj.ApprovedDate = Utility.GetFieldValue<DateTime>("approved_date", dr);
            obj.PinCode = Utility.GetFieldValue<string>("pin_code", dr);
            obj.JobAlertId = Utility.GetFieldValue<Int32>("job_alert_id", dr);
            obj.JobSiteLocation = Utility.GetFieldValue<string>("job_site_location", dr);
            obj.JobSiteLatitude = Utility.GetFieldValue<Decimal>("job_site_lat", dr);
            obj.JobSiteLongitude = Utility.GetFieldValue<Decimal>("job_site_lng", dr);
            obj.JobStatus = Utility.GetFieldValue<Int16>("job_status", dr);
            obj.IsBilled = Utility.GetFieldValue<Boolean>("is_billed", dr);
            obj.BilledDate = Utility.GetFieldValue<DateTime>("billed_date", dr);
            obj.TotalWage = Utility.GetFieldValue<Decimal>("total_wage", dr);
            obj.StaffInitial = Utility.GetFieldValue<string>("staff_initial", dr);
            obj.NotificationStatus = Utility.GetFieldValue<Int32>("notification_status", dr);
            obj.InductionDaysStatus = Utility.GetFieldValue<Int32>("induction_days_status", dr);
            obj.InductionHoursStatus = Utility.GetFieldValue<Int32>("induction_hrs_status", dr);
        }
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Insert a JobStaff object to tbl_job_staff.
        /// </summary>
        /// <param name="jobStaff">JobStaff object.</param>
        /// <returns>Count of inserted rows.</returns>
        public int InsertJobStaff(JobStaff jobStaff)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_InsertJobStaff";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand,
                "@shiftId", DbType.Int64, jobStaff.JobShiftId);
            db.AddInParameter(dbCommand,
                "@userId", DbType.Int64, jobStaff.StaffUserId);
            db.AddInParameter(dbCommand,
                "@acceptedDate", DbType.DateTime, jobStaff.AcceptedDate);
            db.AddInParameter(dbCommand,
                "@jobAlertId", DbType.Int64, jobStaff.JobAlertId);
            db.AddInParameter(dbCommand,
                "@jobStatus", DbType.Int16, jobStaff.JobStatus);

            int count = 0;
            try
            {
                count = db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception)
            {
                throw;
            }
            return count;
        }
        #endregion

    }
}