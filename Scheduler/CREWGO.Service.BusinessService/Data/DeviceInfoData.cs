﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class DeviceInfoData : BaseData
    {
        #region BASE METHODS

        public List<DeviceInfo> GetAll()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetDevicesAll";
            DbCommand dbCommand = db.GetStoredProcCommand(query);

            List<DeviceInfo> devices = new List<DeviceInfo>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        DeviceInfo device = new DeviceInfo();
                        BindDeviceInfoData(reader, device);
                        devices.Add(device);
                    }
                }
            }
            catch (Exception) { throw; }

            return devices;
        }
        public DeviceInfo GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetDeviceByUserId";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, id);

            DeviceInfo obj = new DeviceInfo();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindDeviceInfoData(reader, obj);
                    }
                }
            }
            catch (Exception) { throw; }

            if (obj.DeviceInfoId <= 0)
                return null;
            
            return obj;
        }
        public List<DeviceInfo> GetDevicesById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetDeviceByUserId";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, id);

            List<DeviceInfo> lstDevices = new List<DeviceInfo>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        DeviceInfo obj = new DeviceInfo();
                        BindDeviceInfoData(reader, obj);
                        lstDevices.Add(obj);
                    }
                }
            }
            catch (Exception) { throw; }

            if (lstDevices.Count <= 0)
                return null;

            return lstDevices;
        }
        public DeviceInfo GetByUserIdGroupId(long userId, int groupId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetDeviceByUserGroupId";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, userId);
            db.AddInParameter(dbCommand, "@groupId", DbType.Int32, groupId);

            DeviceInfo obj = new DeviceInfo();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindDeviceInfoData(reader, obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        public void BindDeviceInfoData(IDataReader reader, DeviceInfo obj)
        {
            obj.DeviceInfoId = GetInt32(reader, "id");
            obj.UserId = GetInt64(reader, "user_id");
            obj.DeviceId = GetString(reader, "device_id");
            obj.HashCode = GetString(reader, "hash_code");
            obj.DeviceType = GetInt32(reader, "device_type");
            obj.Token = GetString(reader, "token");
            obj.TokenFlag = GetNullableBoolean(reader, "token_flag");
            obj.DeviceName = GetString(reader, "device_name");
            obj.DeviceModel = GetString(reader, "device_model");
            obj.OsVersion = GetString(reader, "os_version");
            obj.LastActivity = GetNullableDateTime(reader, "last_activity");
            obj.CreatedDate = GetDateTime(reader, "created_date");
            obj.GroupId = GetNullableInt32(reader, "group_id");
            obj.DeviceToken = GetString(reader, "device_token");
            obj.VoipToken = GetString(reader, "voip_token");
        }
        public void BindDeviceInfoData(DataRow dr, DeviceInfo obj)
        {
            obj.DeviceInfoId = Utility.GetFieldValue<Int32>("id", dr);
            obj.UserId = Utility.GetFieldValue<Int64>("user_id", dr);
            obj.DeviceId = Utility.GetFieldValue<string>("device_id", dr);
            obj.HashCode = Utility.GetFieldValue<string>("hash_code", dr);
            obj.DeviceType = Utility.GetFieldValue<Int32>("device_type", dr);
            obj.Token = Utility.GetFieldValue<string>("token", dr);
            obj.TokenFlag = Utility.GetFieldValue<Boolean>("token_flag", dr);
            obj.DeviceName = Utility.GetFieldValue<string>("device_name", dr);
            obj.DeviceModel = Utility.GetFieldValue<string>("device_model", dr);
            obj.OsVersion = Utility.GetFieldValue<string>("os_version", dr);
            obj.LastActivity = Utility.GetFieldValue<DateTime>("last_activity", dr);
            obj.CreatedDate = Utility.GetFieldValue<DateTime>("created_date", dr);
            obj.GroupId = Utility.GetFieldValue<Int32>("group_id", dr);
            obj.DeviceToken = Utility.GetFieldValue<string>("device_token", dr);
            obj.VoipToken = Utility.GetFieldValue<string>("voip_token", dr);
        }
        
        #endregion
    }
}