﻿using CREWGO.Service.BusinessService.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    /// <summary>
    /// Gets and updates authentication cresentials
    /// </summary>
    public class AuthData : BaseData
    {
        /// <summary>
        /// Saves authentication data to database.
        /// </summary>
        /// <param name="auth">NotificationAuth object</param>
        /// <returns>True if Success, false otherwise</returns>
        public bool SaveAuthData(NotificationAuth auth)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_InsertNotificationAuthData";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            
            db.AddInParameter(dbCommand, "@appID", DbType.String, auth.AppId);
            db.AddInParameter(dbCommand, "@keyHash", DbType.String, auth.KeyHash);
            db.AddInParameter(dbCommand, "@isAdmin", DbType.Boolean, auth.IsAdmin);
            
            int count = 0;
            try
            {
                count = db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception)
            {
                throw;
            }
            return count > 0 ? true : false;
        }
        /// <summary>
        /// Gets all authentication credentials from the database
        /// </summary>
        /// <returns>List of NotificationAuth objects</returns>
        public List<NotificationAuth> GetAllAuthData()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetAllNotificationAuthData";
            DbCommand dbCommand = db.GetStoredProcCommand(query);

            List<NotificationAuth> authList = new List<NotificationAuth>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        NotificationAuth obj = new NotificationAuth();
                        BindNotificationAuthData(reader, obj);
                        authList.Add(obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return authList;
        }
        /// <summary>
        /// Binds notification auth data to NotificationAuth object
        /// </summary>
        /// <param name="reader">Data reader</param>
        /// <param name="obj">NotificationAuth object to bind data to</param>
        public void BindNotificationAuthData(IDataReader reader, NotificationAuth obj)
        {
            obj.Id = GetInt32(reader, "id");
            obj.AppId = GetString(reader, "app_id");
            obj.KeyHash = GetString(reader, "key_hash");
            obj.IsAdmin = GetBoolean(reader, "is_admin");
        }
    }
}
