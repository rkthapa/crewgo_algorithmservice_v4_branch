﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class JobAlertSettingGlobalData : BaseData
    {
        #region BASE METHODS

        public void BindJobAlertGlobalSetting(IDataReader reader, JobAlertSettingGlobal obj)
        {
            obj.JobAlertSettingGlobalId = GetInt64(reader, "id");
            obj.SkillId = GetInt32(reader, "skill_id");
            obj.LevelId = GetInt32(reader, "level_id");
            obj.StaffUserId = GetInt64(reader, "staff_user_id");
            obj.LHCUserId = GetInt64(reader, "lhc_user_id");
            obj.OrderPriority = GetNullableInt32(reader, "order_by");
            obj.AssignedDate = GetDateTime(reader, "assigned_date");
            obj.Status = (sbyte)GetByte(reader, "status");
        }
        public void BindJobAlertGlobalSetting(DataRow dr, JobAlertSettingGlobal obj)
        {
            obj.JobAlertSettingGlobalId = Utility.GetFieldValue<Int64>("id", dr);
            obj.SkillId = Utility.GetFieldValue<Int32>("skill_id", dr);
            obj.LevelId = Utility.GetFieldValue<Int32>("level_id", dr);
            obj.StaffUserId = Utility.GetFieldValue<Int64>("staff_user_id", dr);
            obj.LHCUserId = Utility.GetFieldValue<Int64>("lhc_user_id", dr);
            obj.OrderPriority = Utility.GetFieldValue<Int32>("order_by", dr);
            obj.AssignedDate = Utility.GetFieldValue<DateTime>("assigned_date", dr);
            obj.Status = Utility.GetFieldValue<sbyte>("status", dr);
        }

        #endregion

        #region CUSTOM METHODS

        public JobAlertSettingGlobal GetByPartnerIdAndUserId(long partnerId, long staffUserId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertGlobalSettingByPartnerUserId";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@partnerId", DbType.Int64, partnerId);
            db.AddInParameter(dbCommand, "@staffUserId", DbType.Int64, staffUserId);

            JobAlertSettingGlobal obj = new JobAlertSettingGlobal();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindJobAlertGlobalSetting(reader, obj);
                }
                if (obj.LHCUserId != partnerId && obj.StaffUserId != staffUserId)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        public DataSet GetGlobalSettingCount(long partnerId, int skillId, int levelId)
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertGlobalSettingCount";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@partnerId", DbType.Int64, partnerId);
            db.AddInParameter(dbCommand, "@skillId", DbType.Int32, skillId);
            db.AddInParameter(dbCommand, "@levelId", DbType.Int32, levelId);

            try { dst = db.ExecuteDataSet(dbCommand); }
            catch (Exception) { throw; }

            return dst;
        }
        /// <summary>
        /// Gets an entry from tbl_job_manual_alert_gb_setting having given ID.
        /// Binds the result of the query to JobAlertSettingGlobal object.
        /// </summary>
        /// <param name="id">JobAlertSettingGlobal ID.</param>
        /// <returns>JobAlertSettingGlobal object.</returns>
        public JobAlertSettingGlobal GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertSettingGlobalByID";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@alertID", DbType.Int64, id);
            JobAlertSettingGlobal obj = new JobAlertSettingGlobal();

            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindJobAlertGlobalSetting(reader, obj); ;
                }
                if (obj.JobAlertSettingGlobalId != id)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception)
            {
                throw;
            }

            return obj;
        }
        /// <summary>
        /// Updates an entry in tbl_job_manual_alert_gb_setting.
        /// </summary>
        /// <param name="jobAlertSettingGlobal">JobAlertSettingGlobal object.</param>
        /// <returns>Number of rows affected.</returns>
        public int UpdateJobAlertSettingGlobal(JobAlertSettingGlobal jobAlertSetting)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_UpdateJobAlertSettingGlobal";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand,
                "@settingID", DbType.Int64, jobAlertSetting.JobAlertSettingGlobalId);
            db.AddInParameter(dbCommand,
                "@skillID", DbType.Int64, jobAlertSetting.SkillId);
            db.AddInParameter(dbCommand,
                "@levelID", DbType.Int64, jobAlertSetting.LevelId);
            db.AddInParameter(dbCommand,
                "@staffUserID", DbType.Int64, jobAlertSetting.StaffUserId);
            db.AddInParameter(dbCommand,
                "@lhcUserID", DbType.Int32, jobAlertSetting.LHCUserId);
            db.AddInParameter(dbCommand,
                "@orderBy", DbType.Int32, jobAlertSetting.OrderPriority);
            db.AddInParameter(dbCommand,
                "@assignedDate", DbType.DateTime, jobAlertSetting.AssignedDate);
            db.AddInParameter(dbCommand,
                "@alertStatus", DbType.Int32, jobAlertSetting.Status);

            int count = 0;
            try
            {
                count = db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception)
            {
                throw;
            }
            return count;
        }


        #endregion
    }
}
