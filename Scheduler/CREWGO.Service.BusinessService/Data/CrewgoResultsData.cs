﻿using CREWGO.Service.BusinessService.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.Common;

namespace CREWGO.Service.BusinessService.Data
{
    /// <summary>
    /// Handles data layer tasks for CrewgoResults.
    /// </summary>
    public class CrewgoResultsData : BaseData
    {
        /// <summary>
        /// Gets results for a user-shift combination for Crewgo algorithm.
        /// </summary>
        /// <param name="shiftId">Shift ID.</param>
        /// <param name="userId">User ID.</param>
        /// <returns>List of criteria not/fulfilled by user for shift.</returns>
        public CrewgoResults GetUserResultsForShift(long shiftId, long userId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_CheckStaffForCrewgo";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, shiftId);
            db.AddInParameter(dbCommand, "@userId", DbType.Int64, userId);

            CrewgoResults obj = new CrewgoResults();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindCrewgoResultsData(reader, obj);
                }
                if (obj.ShiftId != shiftId && obj.UserId != userId)
                {
                    throw new DataException("Error executing query.");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        /// <summary>
        /// Binds query output to CrewgoResults object.
        /// </summary>
        /// <param name="reader">IDatareader.</param>
        /// <param name="obj">CrewgoResults object.</param>
        private void BindCrewgoResultsData(IDataReader reader, CrewgoResults obj)
        {
            obj.UserId = GetInt64(reader, "userId");
            obj.ShiftId = GetInt64(reader, "jobShiftId");
            obj.IsUserStaff = GetBoolean(reader, "isStaff");
            obj.HasRequiredSkill = GetBoolean(reader, "hasSkill");
            obj.HasRequiredSubSkill = GetBoolean(reader, "hasSubSkill");
            obj.HasRequiredQualification = GetBoolean(reader, "hasQualification");
            obj.IsOnCall = GetBoolean(reader, "isOnCall");
            obj.IsAvailable = GetBoolean(reader, "isAvailable");
            obj.IsInCrewgo = GetBoolean(reader, "isInCrewgo");
            obj.IsAssociatedPartnerCrewgo = GetBoolean(reader, "isAssociatedPartnerCrewgo");
            obj.IsActive = GetBoolean(reader, "isActive");
            obj.IsEmailVerified = GetBoolean(reader, "isEmailVerified");
            obj.IsLoggedIn = GetBoolean(reader, "isLoggedIn");
            obj.IsAlreadyInPSList = GetBoolean(reader, "isAlreadyInPSList");
            obj.IsAlreadyNotified = GetBoolean(reader, "isAlreadyNotified");
            obj.IsPreferedLocation = GetBoolean(reader, "isPreferedLocation");
            obj.HasRequiredRating = GetBoolean(reader, "hasRequiredRating");
            obj.HasRequiredAcceptance = GetBoolean(reader, "hasRequiredAcceptance");
            obj.HasRequiredExperience = GetBoolean(reader, "hasRequiredExperience");
            obj.HasRequiredHistory = GetBoolean(reader, "hasRequiredHistory");
            obj.HasRequiredLongevity = GetBoolean(reader, "hasRequiredLongevity");
            obj.HasRequiredProximity = GetBoolean(reader, "hasRequiredProximity");
            obj.HasRequiredAccuracy = GetBoolean(reader, "hasRequiredAccuracy");
            obj.HasRequiredAppearance = GetBoolean(reader, "hasRequiredAppearance");
            obj.HasRequiredPunctuality = GetBoolean(reader, "hasRequiredPunctuality");
            obj.HasRequiredRapidity = GetBoolean(reader, "hasRequiredRapidity");
            obj.HasRequiredTransport = GetBoolean(reader, "hasRequiredTransport");
            obj.HasRequiredUrgency = GetBoolean(reader, "hasRequiredUrgency");
        }
    }
}
