﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class ActivityLevelData : BaseData
    {
        private DatabaseProviderFactory factory;
        private Database db;
        public ActivityLevelData() : base ()
        {
            this.factory = new DatabaseProviderFactory();
            this.db = factory.Create(MYSQLCONNECTION);
        }
        #region BASE METHODS
        /// <summary>
        /// Gets all activity levels
        /// </summary>
        /// <returns>List of ActivityLevel</returns>
        public List<ActivityLevel> GetAll()
        {
            string query = "usp_SVC_GetActivityLevel";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            List<ActivityLevel> lstActivityLevel = new List<ActivityLevel>();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        ActivityLevel obj = new ActivityLevel();
                        BindActivityLevelData(reader, obj);
                        lstActivityLevel.Add(obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return lstActivityLevel;
        }
        /// <summary>
        /// Gets activity level by id
        /// </summary>
        /// <param name="id">ActivityLevel ID</param>
        /// <returns>ActivityLevel</returns>
        public ActivityLevel GetById(int id)
        {
            string query = "usp_SVC_GetActivityLevelById";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@activity_level_id", DbType.Int32, id);

            ActivityLevel obj = new ActivityLevel();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindActivityLevelData(reader, obj);
                    }
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        /// <summary>
        /// Binds activity level dataset to ActivityLevel object
        /// </summary>
        /// <param name="reader">IDataReader</param>
        /// <param name="obj">ActivityLevel Object</param>
        public void BindActivityLevelData(IDataReader reader, ActivityLevel obj)
        {
            obj.id = GetInt32(reader, "id");
            obj.name = GetString(reader, "name");
            obj.p_staff_result_range_from = GetInt32(reader, "p_staff_result_range_from");
            obj.p_staff_result_range_to = GetNullableInt32(reader, "p_staff_result_range_to");
            obj.shift_request_number = GetInt32(reader, "shift_request_number");
            obj.shift_time_interval = GetInt32(reader, "shift_time_interval");
            obj.shift_notice_time = GetInt32(reader, "shift_notice_time");
            obj.peak_price_factor = GetDecimal(reader, "peak_price_factor");
            obj.entered_date = GetDateTime(reader, "entered_date");
            obj.entered_by = GetNullableInt32(reader, "entered_by");
            obj.updated_date = GetNullableDateTime(reader, "updated_date");
            obj.updated_by = GetNullableInt32(reader, "updated_by");
            obj.type = GetString(reader, "type");
            obj.filling_probability = GetNullableInt32(reader, "filling_probability");
        }
        /// <summary>
        /// Binds single row of Activity Level dataset to ActivityLevel object
        /// </summary>
        /// <param name="dr">DataRow</param>
        /// <param name="obj">ActivityLevel Object</param>
        public void BindActivityLevelData(DataRow dr, ActivityLevel obj)
        {
            obj.id = Utility.GetFieldValue<Int32>("id", dr);
            obj.name = Utility.GetFieldValue<string>("name", dr);
            obj.p_staff_result_range_from = Utility.GetFieldValue<Int32>("p_staff_result_range_from", dr);
            obj.p_staff_result_range_to = Utility.GetFieldValue<Int32>("p_staff_result_range_to", dr);
            obj.shift_request_number = Utility.GetFieldValue<Int32>("shift_request_number", dr);
            obj.shift_time_interval = Utility.GetFieldValue<Int32>("shift_time_interval", dr);
            obj.shift_notice_time = Utility.GetFieldValue<Int32>("shift_notice_time", dr);
            obj.peak_price_factor = Utility.GetFieldValue<Decimal>("peak_price_factor", dr);
            obj.entered_date = Utility.GetFieldValue<DateTime>("entered_date", dr);
            obj.entered_by = Utility.GetFieldValue<Int32>("entered_by", dr);
            obj.updated_date = Utility.GetFieldValue<DateTime>("updated_date", dr);
            obj.updated_by = Utility.GetFieldValue<Int32>("updated_by", dr);
            obj.type = Utility.GetFieldValue<string>("type", dr);
            obj.filling_probability = Utility.GetFieldValue<Int32>("filling_probability", dr);
        }
        #endregion

        #region CUSTOM METHODS
        /// <summary>
        /// Gets Activity Level By Value
        /// </summary>
        /// <param name="potentialStaff"></param>
        /// <returns>Dataset of Potential Staff</returns>
        public DataSet GetByValue(int potentialStaff)
        {
            DataSet dst = null;
            string query = "usp_SVC_GetActivityLevelByValue";
            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(
                dbCommand, "@potentialStaff", DbType.Int32, potentialStaff);

            try 
            { 
                dst = db.ExecuteDataSet(dbCommand); 
            }
            catch (Exception) 
            { 
                throw; 
            }

            return dst;
        }
        /// <summary>
        /// Gets the dataset containing 'short_notice_period'
        /// from table 'tbl_con_general_setting'
        /// </summary>
        /// <returns>DataSet containing short_notice_period</returns>
        public DataSet GetShortNoticePeriod()
        {
            DataSet dataSet = new DataSet();
            DbCommand dbCommand = db.GetSqlStringCommand(
                "select short_notice_period from tbl_con_general_setting");
            try
            {
                dataSet = db.ExecuteDataSet(dbCommand);
            }
            catch (Exception)
            {
                throw;
            }
            return dataSet;
        }
        /// <summary>
        /// Reads the first row of column "short_notice_period" from dataset
        /// </summary>
        /// <param name="ds">The DataSet containing short_notice_period</param>
        /// <returns>Short Notice Period(int)</returns>
        public int BindShortNoticeCount(DataSet ds)
        {
            return Utility.GetFieldValue<Int32>(
                    "short_notice_period", ds.Tables[0].Rows[0]);
        }
        #endregion 
    }
}
