﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class CallInfoData: BaseData
    {
        public async Task<CallInfo> GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_VideoCallDataById";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@callId", DbType.Int64, id);

            CallInfo obj = new CallInfo();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                    {
                        BindVideoCallData(reader, obj);
                    }
                    var userData = new UserData();
                    obj.Caller = await userData.GetById(obj.CallerId);
                    obj.Receiver = await userData.GetById(obj.ReceiverId);

                }
                if (obj.Id != id)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception) { throw; }

            return obj;
        }
        public void BindVideoCallData(IDataReader reader, CallInfo obj)
        {
            obj.Id = GetInt64(reader, "id");
            obj.CallerId = GetInt64(reader, "caller_id");
            obj.ReceiverId = GetInt64(reader, "receiver_id");
            obj.JobId = GetInt64(reader, "job_id");
            obj.StartTime = GetDateTime(reader, "start_time");
            obj.EndTime = GetDateTime(reader, "end_time");
            obj.RoomName = GetString(reader, "room_name");
            obj.Duration = GetInt32(reader, "duration");
            obj.Price = GetInt32(reader, "price");
            obj.CallType = GetBoolean(reader, "call_type");
            obj.IsMissed = GetBoolean(reader, "is_missed");
            obj.IsBusy = GetBoolean(reader, "is_busy");
            obj.CallerDeviceType = GetInt32(reader, "sender_device_type");
            obj.ReceiverDeviceType = GetInt32(reader, "receiver_device_type");
            obj.CallerDeviceToken = GetString(reader, "sender_device_token");
            obj.ReceiverDeviceToken = GetString(reader, "receiver_device_token");
        }
    }
}
