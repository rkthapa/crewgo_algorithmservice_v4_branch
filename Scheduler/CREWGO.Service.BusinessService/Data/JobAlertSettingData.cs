﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Data
{
    public class JobAlertSettingData : BaseData
    {
        #region BASE METHODS

        public void BindJobAlertSetting(IDataReader reader, JobAlertSetting obj) 
        {
            obj.JobAlertSettingId = GetInt64(reader, "id");
            obj.JobShiftId = GetInt64(reader, "job_detail_id");
            obj.StaffUserId = GetInt64(reader, "staff_user_id");
            obj.LHCUserId = GetInt64(reader, "lhc_user_id");
            obj.OrderPriority = GetNullableInt32(reader, "order_by");
            obj.AssignedDate = GetDateTime(reader, "assigned_date");
            obj.AlertedDate = GetNullableDateTime(reader, "alerted_date");
            obj.Status = GetNullableBoolean(reader, "status");
        }
        public void BindJobAlertSetting(DataRow dr, JobAlertSetting obj)
        {
            obj.JobAlertSettingId = Utility.GetFieldValue<Int64>("id", dr);
            obj.JobShiftId = Utility.GetFieldValue<Int64>("job_detail_id", dr);
            obj.StaffUserId = Utility.GetFieldValue<Int64>("staff_user_id", dr);
            obj.LHCUserId = Utility.GetFieldValue<Int64>("lhc_user_id", dr);
            obj.OrderPriority = Utility.GetFieldValue<Int32>("order_by", dr);
            obj.AssignedDate = Utility.GetFieldValue<DateTime>("assigned_date", dr);
            obj.AlertedDate = Utility.GetFieldValue<DateTime>("alerted_date", dr);
            obj.Status = Utility.GetFieldValue<bool>("status", dr);
        }
        
        #endregion

        #region CUSTOM METHODS
        /// <summary>
        /// Gets tbl_job_manual_alert_setting entry by id 
        /// and binds it to JobAlertSetting object. 
        /// </summary>
        /// <param name="jobShiftId">Shift ID.</param>
        /// <param name="staffUserId">Staff user ID.</param>
        /// <returns></returns>
        public JobAlertSetting GetByShiftIdAndUserId(long jobShiftId, long staffUserId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertSettingByShiftUserId";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, jobShiftId);
            db.AddInParameter(dbCommand, "@staffUserId", DbType.Int64, staffUserId);

            JobAlertSetting obj = new JobAlertSetting();
            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindJobAlertSetting(reader, obj); ;
                }
            }
            catch (Exception) 
            { 
                throw; 
            }

            return obj;
        }
        public DataSet GetAlertSettingCount(long jobShiftId)
        {
            DataSet dst = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertSettingCount";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@jobShiftId", DbType.Int64, jobShiftId);

            try 
            { 
                dst = db.ExecuteDataSet(dbCommand); 
            }
            catch (Exception) 
            { 
                throw; 
            }

            return dst;
        }
        /// <summary>
        /// Get an entry from tbl_job_manual_alert_setting for the given ID.
        /// Binds the result of query to JobAlertSetting object.
        /// </summary>
        /// <param name="id">JobAlertSetting ID.</param>
        /// <returns>JobAlertSetting object.</returns>
        public JobAlertSetting GetById(long id)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_GetJobAlertSettingByID";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, "@alertID", DbType.Int64, id);
            JobAlertSetting obj = new JobAlertSetting();

            try
            {
                using (IDataReader reader = db.ExecuteReader(dbCommand))
                {
                    while (reader.Read())
                        BindJobAlertSetting(reader, obj); ;
                }
                if (obj.JobAlertSettingId != id)
                {
                    throw new DataException("Record Not Found");
                }
            }
            catch (Exception)
            {
                throw;
            }

            return obj;
        }
        /// <summary>
        /// Inserts a JobAlertSertting object into tbl_job_manual_alert_setting.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Number of rows affected.</returns>
        public int InsertJobAlertSetting(JobAlertSetting jobAlertSetting)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_InsertJobAlertSetting";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand, 
                "@jobDetailID", DbType.Int64, jobAlertSetting.JobShiftId);
            db.AddInParameter(dbCommand,
                "@staffUserID", DbType.Int64, jobAlertSetting.StaffUserId);
            db.AddInParameter(dbCommand,
                "@lhcUserID", DbType.Int64, jobAlertSetting.LHCUserId);
            db.AddInParameter(dbCommand,
                "@alertStatus", DbType.Int32, jobAlertSetting.Status);
            db.AddInParameter(dbCommand,
                "@orderBy", DbType.Int32, jobAlertSetting.OrderPriority);
            db.AddInParameter(dbCommand,
                "@assignedDate", DbType.DateTime, jobAlertSetting.AssignedDate);
            db.AddInParameter(dbCommand,
                "@alertedDate", DbType.DateTime, jobAlertSetting.AlertedDate);

            int count = 0;
            try
            {
                count = db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception)
            {
                throw;
            }
            return count;
        }
        /// <summary>
        /// Updates the JobAlertSertting object in tbl_job_manual_alert_setting.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Number of rows affected.</returns>
        public int UpdateJobAlertSetting(JobAlertSetting jobAlertSetting)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(MYSQLCONNECTION);

            string query = "usp_SVC_UpdateJobAlertSetting";

            DbCommand dbCommand = db.GetStoredProcCommand(query);
            db.AddInParameter(dbCommand,
                "@settingID", DbType.Int64, jobAlertSetting.JobAlertSettingId);
            db.AddInParameter(dbCommand,
                "@jobDetailID", DbType.Int64, jobAlertSetting.JobShiftId);
            db.AddInParameter(dbCommand,
                "@staffUserID", DbType.Int64, jobAlertSetting.StaffUserId);
            db.AddInParameter(dbCommand,
                "@lhcUserID", DbType.Int64, jobAlertSetting.LHCUserId);
            db.AddInParameter(dbCommand,
                "@alertStatus", DbType.Int32, jobAlertSetting.Status);
            db.AddInParameter(dbCommand,
                "@orderBy", DbType.Int32, jobAlertSetting.OrderPriority);
            db.AddInParameter(dbCommand,
                "@assignedDate", DbType.DateTime, jobAlertSetting.AssignedDate);
            db.AddInParameter(dbCommand,
                "@alertedDate", DbType.DateTime, jobAlertSetting.AlertedDate);
            
            int count = 0;
            try
            {
                count = db.ExecuteNonQuery(dbCommand);
            }
            catch (Exception)
            {
                throw;
            }
            return count;
        }
        #endregion
    }
}