﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class JobAlertBusiness
    {
        public List<JobAlert> GetAll()
        {
            return new JobAlertData().GetAll();
        }
        public JobAlert GetByShiftIdAndUserId(long jobShiftId, long staffUserId)
        {
            try { return new JobAlertData().GetByShiftIdAndUserId(jobShiftId, staffUserId); }
            catch (Exception) { throw; }
        }
        public Int32 GetAlertCount(long staffUserId)
        {
            int alertCount = 0;
            DataSet dst = new JobAlertData().GetAlertCount(staffUserId);

            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    if (dst.Tables[0].Rows.Count > 0)
                        alertCount = Convert.ToInt32(dst.Tables[0].Rows[0][0].ToString());
                }
                catch (Exception) { throw; }
            }

            return alertCount;
        }
        /// <summary>
        /// Adds a new JobAlert entry.
        /// </summary>
        /// <param name="jobAlert">Job Alert Object</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> InsertJobAlert(JobAlert jobAlert)
        {
            return await Task.Run(() => new JobAlertData().InsertJobAlert(jobAlert));
        }
    }
}
