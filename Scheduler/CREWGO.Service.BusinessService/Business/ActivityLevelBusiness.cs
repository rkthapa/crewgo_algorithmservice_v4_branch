﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class ActivityLevelBusiness
    {
        /// <summary>
        /// Gets all activity levels
        /// </summary>
        /// <returns>List of ActivityLevel</returns>
        public List<ActivityLevel> GetAll()
        {
            return new ActivityLevelData().GetAll();
        }
        /// <summary>
        /// Gets activity level by id
        /// </summary>
        /// <param name="id">ActivityLevel ID</param>
        /// <returns>ActivityLevel</returns>
        public ActivityLevel GetById(int id)
        {
            return new ActivityLevelData().GetById(id);
        }
        /// <summary>
        /// Gets Activity Level By Value
        /// </summary>
        /// <param name="potentialStaff"></param>
        /// <returns>Dataset of Potential Staff</returns>
        public ActivityLevel GetByValue(int potentialStaff)
        {
            DataSet dst = new ActivityLevelData().GetByValue(potentialStaff);
            ActivityLevel obj = new ActivityLevel();

            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    foreach (DataRow dr in dst.Tables[0].Rows)
                    {
                        new ActivityLevelData().BindActivityLevelData(dr, obj);
                    }
                }
                catch (Exception) { throw; }
            }

            return obj;
        }
        public async Task<int> GetShortNoticePeriod()
        {
            ActivityLevelData al = new ActivityLevelData();
            return await Task.Run(() => 
                al.BindShortNoticeCount(al.GetShortNoticePeriod()));
        }
    }
}