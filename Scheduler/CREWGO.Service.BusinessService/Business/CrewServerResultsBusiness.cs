﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;

namespace CREWGO.Service.BusinessService.Business
{
    /// <summary>
    /// Handles business layer operations on CrewServerResults data
    /// </summary>
    public class CrewServerResultsBusiness
    {
        /// <summary>
        /// Gets results for a user-shift combination for CrewServer algorithm.
        /// </summary>
        /// <param name="shiftId">Shift ID.</param>
        /// <param name="userId">User ID.</param>
        /// <returns>List of criteria not/fulfilled by user for shift.</returns>
        public CrewServerResults GetUserResultsForShift(long shiftId, long userId)
        {
            return new CrewServerResultsData().GetUserResultsForShift(shiftId, userId);
        }
    }
}
