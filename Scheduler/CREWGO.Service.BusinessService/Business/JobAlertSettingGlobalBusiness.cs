﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class JobAlertSettingGlobalBusiness
    {
        public JobAlertSettingGlobal GetByPartnerIdAndUserId(long partnerId, long staffUserId)
        {
            return new JobAlertSettingGlobalData().GetByPartnerIdAndUserId(partnerId, staffUserId);
        }
        public Boolean GlobalPriorityExists(long partnerId, int skillId, int levelId)
        {
            int alertSetting = 0;
            DataSet dst = new JobAlertSettingGlobalData().GetGlobalSettingCount(partnerId, skillId, levelId);

            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        alertSetting = Convert.ToInt32(dst.Tables[0].Rows[0][0].ToString());
                        if (alertSetting > 0)
                            return true;
                    }
                }
                catch (Exception) { throw; }
            }

            return false;
        }
        /// <summary>
        /// Get a JobAlertSettingGlobal object by ID.
        /// </summary>
        /// <param name="id">JobAlertSettingGlobal ID.</param>
        /// <returns>JobAlertSettingGlobal object.</returns>
        public async Task<JobAlertSettingGlobal> GetById(long id)
        {
            return await Task.Run(() => new JobAlertSettingGlobalData().GetById(id));
        }
        /// <summary>
        /// Updates a JobAlertSettingGlobal object.
        /// </summary>
        /// <param name="jobAlertSettingGlobal">JobAlertSettingGlobal object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> UpdateJobAlertSettingGlobal(JobAlertSettingGlobal jobAlertSettingGlobal)
        {
            return await Task.Run(() =>
                new JobAlertSettingGlobalData().UpdateJobAlertSettingGlobal(jobAlertSettingGlobal)
            );
        }
    }   
}