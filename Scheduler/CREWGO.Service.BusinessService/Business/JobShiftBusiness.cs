﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class JobShiftBusiness
    {
        public List<JobShift> GetAll()
        {
            return new JobShiftData().GetAll();
        }
        public JobShift GetById(long id)
        {
            return new JobShiftData().GetById(id);
        }
        public List<JobShift> GetLatestShifts()
        { 
            DataSet dst = new JobShiftData().GetLatestShifts();

            List<JobShift> lstJobShifts = new List<JobShift>();
            if(Validator.DataSetHasData(dst))
            {
                try
                {
                    foreach (DataRow dr in dst.Tables[0].Rows)
                    {
                        JobShift obj = new JobShift();

                        new JobShiftData().BindJobShiftsData(dr, obj);

                        obj.ParentJob = new JobBusiness().GetById(obj.JobId);

                        lstJobShifts.Add(obj);
                    }
                }
                catch (Exception) { throw; }
            }

            return lstJobShifts;
        }
        public int GetShiftNoticePeriod()
        {
            int value = 0;
            DataSet dst = new JobShiftData().GetShiftNoticePeriod();

            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        value = Convert.ToInt32(dst.Tables[0].Rows[0][0].ToString());
                        if (value > 0)
                            return value;
                    }
                }
                catch (Exception) { throw; }
            }

            return value;
        }
    }
}