﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class CallInfoBusiness
    {
        public async Task<CallInfo> GetById(long callId)
        {
            return await new CallInfoData().GetById(callId);
        }
    }
}
