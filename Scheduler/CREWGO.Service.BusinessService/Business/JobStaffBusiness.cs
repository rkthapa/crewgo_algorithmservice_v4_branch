﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class JobStaffBusiness
    {
        /// <summary>
        /// Add a JobStaff.
        /// </summary>
        /// <param name="jobStaff">JobStaff object.</param>
        /// <returns>Count of added JobStaff.</returns>
        public async Task<int> AddJobStaff(JobStaff jobStaff)
        {
            return await Task.Run(() => new JobStaffData().InsertJobStaff(jobStaff));
        }
    }
}
