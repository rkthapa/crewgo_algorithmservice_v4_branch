﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class UserBusiness
    {
        public async Task<List<User>> GetAll()
        {
            return await new UserData().GetAll();
        }
        public async Task<User> GetById(long id)
        {
            return await new UserData().GetById(id);
        }
        /// <summary>
        /// Gets user from email
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns>User</returns>
        public User GetAdminByEmail(string email)
        {
            return new UserData().GetAdminByEmail(email);
        }
        public async Task<List<User>> GetAvailableStaffByGlobalPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            DataSet dst = await Task.Run(() => new UserData().GetAvailableStaffByGlobalPriority(partnerId, jobShiftId, skillId, levelId, userLimit, startTime, endTime));

            List<User> lstUsers = new List<User>();
            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    foreach (DataRow dr in dst.Tables[0].Rows)
                    {
                        User obj = new User();

                        new UserData().BindUserData(dr, obj);                       

                        obj.UserDevice = await Task.Run(() => new DeviceInfoData().GetById(obj.UserId));

                        lstUsers.Add(obj);
                    }
                }
                catch (Exception) { throw; }
            }

            return lstUsers;
        }
        public async Task<List<User>> GetAvailableStaffByGeneralPriority(long partnerId, long jobShiftId, int skillId, int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            DataSet dst = await Task.Run(() => new UserData().GetAvailableStaffByGeneralPriority(partnerId, jobShiftId, skillId, levelId, userLimit, startTime, endTime));

            List<User> lstUsers = new List<User>();
            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    foreach (DataRow dr in dst.Tables[0].Rows)
                    {
                        User obj = new User();
                        
                        new UserData().BindUserData(dr, obj);

                        obj.UserDevice = await Task.Run(() => new DeviceInfoData().GetById(obj.UserId));

                        lstUsers.Add(obj);
                    }
                }
                catch (Exception) { throw; }
            }

            return lstUsers;
        }
        public async Task<List<User>> GetAvailableStaffByManualPriority(long jobShiftId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            DataSet dst = await Task.Run(() => new UserData().GetAvailableStaffByManualPriority(jobShiftId, userLimit, startTime, endTime));

            List<User> lstUsers = new List<User>();
            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    foreach (DataRow dr in dst.Tables[0].Rows)
                    {
                        User obj = new User();

                        new UserData().BindUserData(dr, obj);

                        obj.UserDevice = await Task.Run(() => new DeviceInfoData().GetById(obj.UserId));

                        lstUsers.Add(obj);
                    }
                }
                catch (Exception) { throw; }
            }

            return lstUsers;
        }

        public async Task<Boolean> IsShiftQuotaFulfilled(long jobShiftId)
        {
            int value = 0;
            DataSet dst = await Task.Run(() => new UserData().CheckRequiredStaff(jobShiftId));

            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        value = Convert.ToInt32(dst.Tables[0].Rows[0][0].ToString());
                        if (value > 0)
                            return true;
                    }
                }
                catch (Exception) { throw; }
            }

            return false;
        }
        /// <summary>
        /// Checks if staff has autobookable enabled ot not.
        /// </summary>
        /// <param name="staffId">Staff's userId.</param>
        /// <returns>True if autobookable, False otherwise.</returns>
        public bool IsStaffAutobookable(long staffId)
        {
            return new UserData().CheckIfStaffIsAutobookable(staffId);
        }
    }
}