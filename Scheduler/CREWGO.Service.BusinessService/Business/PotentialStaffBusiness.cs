﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    class PotentialStaffBusiness
    {
        /// <summary>
        /// Calls the Data method in PotentialStaff data to 
        /// generate list of potential staffs
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        public async Task GeneratePotentialStaffList(long jobShiftId)
        {
            PotentialStaffData psd = new PotentialStaffData();
            await Task.Run(() => psd.GeneratePotentialStaffList(jobShiftId));
        }
        /// <summary>
        /// Calls the Data method in PotentialStaff data and reads the count
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <returns></returns>
        public async Task<int> GetPotentialStaffCount(long jobShiftId)
        {
            PotentialStaffData psd = new PotentialStaffData();
            return await Task.Run(() => 
                psd.BindStaffCount(psd.GetPotentialStaffCount(jobShiftId)));
        }
        /// <summary>
        /// Calls the Data method to get shift sociability factor in 
        /// PotentialStaffData class and reads the sociabilityFactor as decimal
        /// </summary>
        /// <param name="jobShiftId"></param>
        /// <returns>Shift sociability factor (decimal)</returns>
        public async Task<float> GetShiftSociabilityFactor(long jobShiftId)
        {
            PotentialStaffData psd = new PotentialStaffData();
            return await Task.Run(() =>
                psd.BindSSF(psd.GetShiftSociabilityFactor(jobShiftId)));
        }
        /// <summary>
        /// Gets top potential staffs for a job shift limited by 'count'
        /// </summary>
        /// <param name="shiftId">The Job Shift's ID</param>
        /// <param name="count">The number of potential staffs to fetch</param>
        /// <returns>List of PotentialStaff objects</returns>
        public async Task<List<PotentialStaff>> GetPotentialStaffs(
            long shiftId, int count)
        {

            DataSet dst = await Task.Run(() => 
                new PotentialStaffData().GetPotentialStaffs(shiftId, count));

            List<PotentialStaff> potentialStaffs = new List<PotentialStaff>();
            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    foreach (DataRow dr in dst.Tables[0].Rows)
                    {
                        PotentialStaff staff = new PotentialStaff();
                        new PotentialStaffData().BindPStaffData(dr, staff);
                        staff.Devices = await Task.Run(() => 
                            new DeviceInfoData().GetDevicesById(staff.UserId));
                        potentialStaffs.Add(staff);
                    }
                }
                catch (Exception) 
                { 
                    throw; 
                }
            }
            return potentialStaffs;
        }
        /// <summary>
        /// Updates the potential staff table for a particular shift-user 
        /// combination with value of is_notification_sent as true
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        public async Task UpdatePotentialStaffList(
            long userId, long shiftId, bool isSent, bool isScheduled)
        {
            await Task.Run(() =>
                new PotentialStaffData().SetNotificationStatus(
                    userId, shiftId, isSent, isScheduled)
            );
        }
        /// <summary>
        /// Checks notification status for user id - shift id combination.
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <returns>Tuple <isSent, isScheduled></returns>
        public async Task<bool> CheckNotificationStatus(
            long userId, long shiftId)
        {
            var psData = new PotentialStaffData();
            var dst = await Task.Run(() =>
                psData.CheckNotificationStatus(userId, shiftId)
            );
            var result = psData.BindNotificationStatus(dst);
            return result;
        }
    }
}
