﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class JobBusiness
    {
        public List<Job> GetAll()
        {
            try { return new JobData().GetAll(); }
            catch (Exception) { throw; }
        }
        public Job GetById(long id)
        {
           return new JobData().GetById(id); 
        }
    }
}