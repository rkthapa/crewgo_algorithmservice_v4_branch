﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;

namespace CREWGO.Service.BusinessService.Business
{
    /// <summary>
    /// Handles business layer operations on CrewgoResults data
    /// </summary>
    public class CrewgoResultsBusiness
    {
        /// <summary>
        /// Gets results for a user-shift combination for Crewgo algorithm.
        /// </summary>
        /// <param name="shiftId">Shift ID.</param>
        /// <param name="userId">User ID.</param>
        /// <returns>List of criteria not/fulfilled by user for shift.</returns>
        public CrewgoResults GetUserResultsForShift(long shiftId, long userId)
        {
            return new CrewgoResultsData().GetUserResultsForShift(shiftId, userId);
        }
    }
}
