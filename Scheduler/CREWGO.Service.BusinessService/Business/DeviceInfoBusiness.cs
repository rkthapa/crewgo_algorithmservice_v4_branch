﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class DeviceInfoBusiness
    {
        public List<DeviceInfo> GetAll()
        {
            return new DeviceInfoData().GetAll();
        }
    }
}