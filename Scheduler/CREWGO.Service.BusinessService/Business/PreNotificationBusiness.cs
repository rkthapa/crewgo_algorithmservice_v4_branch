﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class PreNotificationBusiness
    {
        public List<PreNotification> GetAll()
        {
            return new PreNotificationData().GetAll();
        }
        public List<PreNotification> GetPreNotifications()
        {
            DataSet dst = new PreNotificationData().GetPreNotifications();

            List<PreNotification> lstPreNotifications = new List<PreNotification>();
            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    foreach (DataRow dr in dst.Tables[0].Rows)
                    {
                        PreNotification obj = new PreNotification();

                        new PreNotificationData().BindPreNotificationData(dr, obj);
                        lstPreNotifications.Add(obj);
                    }
                }
                catch (Exception) { throw; }
            }

            return lstPreNotifications;
        }
        /// <summary>
        /// Gets a PreNotification object by ID.
        /// </summary>
        /// <param name="id">PreNotification ID.</param>
        /// <returns>PreNotification object.</returns>
        public async Task<PreNotification> GetById(long id)
        {
            return await Task.Run(() => new PreNotificationData().GetById(id));
        }
        /// <summary>
        /// Updates a PreNotification object.
        /// </summary>
        /// <param name="preNotification">PreNotification object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> UpdatePreNotification(PreNotification preNotification)
        {
            return await Task.Run(() => 
                new PreNotificationData().UpdatePreNotification(preNotification)
            );
        }
    }
}