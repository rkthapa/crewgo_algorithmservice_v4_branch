﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class AuthBusiness
    {
        /// <summary>
        /// Saves authentication data to database.
        /// </summary>
        /// <param name="auth">tbl_notifications_api_auth object</param>
        /// <returns>True if Success, false otherwise</returns>
        public async Task<bool> SaveAuthData(NotificationAuth auth)
        {
            return await Task.Run(() => new AuthData().SaveAuthData(auth));
        }
        /// <summary>
        /// Gets all authentication credentials from the database
        /// </summary>
        /// <returns>List of _notifications_api_auth objects</returns>
        public List<NotificationAuth> GetAuthData()
        {
            return new AuthData().GetAllAuthData();
        }
    }
}
