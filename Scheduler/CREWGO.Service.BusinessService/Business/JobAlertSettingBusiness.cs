﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.BusinessService.Data;
using CREWGO.Service.Common;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Business
{
    public class JobAlertSettingBusiness
    {
        public JobAlertSetting GetByShiftIdAndUserId(long jobShiftId, long staffUserId)
        {
            return new JobAlertSettingData().GetByShiftIdAndUserId(jobShiftId, staffUserId);
        }
        public Boolean OrderPriorityExists(long jobShiftId)
        {
            int alertSetting = 0;
            DataSet dst = new JobAlertSettingData().GetAlertSettingCount(jobShiftId);

            if (Validator.DataSetHasData(dst))
            {
                try
                {
                    if (dst.Tables[0].Rows.Count > 0)
                    {
                        alertSetting = Convert.ToInt32(dst.Tables[0].Rows[0][0].ToString());
                        if (alertSetting > 0)
                            return true;
                    }
                }
                catch (Exception) { throw; }
            }

            return false;
        }
        /// <summary>
        /// Get JobAlertSetting object by ID.
        /// </summary>
        /// <param name="id">JobAlertSetting ID.</param>
        /// <returns>JobAlertSetting object.</returns>
        public async Task<JobAlertSetting> GetById(long id)
        {
            return await Task.Run(() => new JobAlertSettingData().GetById(id));
        }
        /// <summary>
        /// Inserts a JobAlertSertting object.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> InsertJobAlertSetting(JobAlertSetting jobAlertSetting)
        {
            return await Task.Run(() =>
                new JobAlertSettingData().InsertJobAlertSetting(jobAlertSetting)
            );
        }
        /// <summary>
        /// Updates a JobAlertSertting object.
        /// </summary>
        /// <param name="jobAlertSetting">JobAlertSertting object.</param>
        /// <returns>Number of rows affected.</returns>
        public async Task<int> UpdateJobAlertSetting(JobAlertSetting jobAlertSetting)
        {
            return await Task.Run(() =>
                new JobAlertSettingData().UpdateJobAlertSetting(jobAlertSetting)
            );
        }
    }
}
