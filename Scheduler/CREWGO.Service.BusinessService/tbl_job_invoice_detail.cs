//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_job_invoice_detail
    {
        public int id { get; set; }
        public Nullable<long> job_id { get; set; }
        public Nullable<long> job_detail_id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string skill { get; set; }
        public Nullable<System.DateTime> start_time { get; set; }
        public Nullable<System.DateTime> end_time { get; set; }
        public Nullable<System.TimeSpan> @break { get; set; }
        public Nullable<System.TimeSpan> hours { get; set; }
        public Nullable<System.TimeSpan> rule_hours { get; set; }
        public Nullable<int> required_number { get; set; }
        public Nullable<decimal> rule_rate { get; set; }
        public Nullable<decimal> wage { get; set; }
        public string wr_rule_remark { get; set; }
        public Nullable<decimal> lhc_charge_percentage { get; set; }
        public Nullable<decimal> lhc_charge { get; set; }
    }
}
