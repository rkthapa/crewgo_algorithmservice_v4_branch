//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_con_skill_level
    {
        public int id { get; set; }
        public int skill_id { get; set; }
        public int level_id { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<System.DateTime> entered_date { get; set; }
        public Nullable<int> entered_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<int> updated_by { get; set; }
        public Nullable<int> industry_id { get; set; }
    
        public virtual tbl_con_level tbl_con_level { get; set; }
        public virtual tbl_con_skill tbl_con_skill { get; set; }
    }
}
