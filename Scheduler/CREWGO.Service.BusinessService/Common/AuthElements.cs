﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CREWGO.Service.BusinessService.Common
{
    /// <summary>
    /// Authentication elements class
    /// </summary>
    public class AuthElements
    {
        /// <summary>
        /// Unique Application ID for API Client
        /// </summary>
        public string AppID { get; set; }
        /// <summary>
        /// Unique API key for API Client
        /// </summary>
        public string ApiKey { get; set; }
        /// <summary>
        /// Determines whether admin or not.
        /// </summary>
        public bool IsAdmin { get; set; }
    }
}