﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Common
{
    public class PotentialStaff
    {
        public long ID { get; set; }
        public long UserId { get; set; }
        public long JobId { get; set; }
        public long JobShiftId { get; set; }
        public double TotalScore { get; set; }
        public List<DeviceInfo> Devices { get; set; }
    }
}
