﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class JobAlert
    {
        public long JobAlertId { get; set; }
        public long JobShiftId { get; set; }
        public long StaffUserId { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> AlertDate { get; set; }
        public Nullable<System.DateTime> ActionDate { get; set; }
    }
}
