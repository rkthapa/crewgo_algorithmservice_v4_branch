﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class Job
    {
        public long JobId { get; set; }
        public string JobFullAddress { get; set; }
        public string JobStreet { get; set; }
        public Nullable<int> JobPostCodeId { get; set; }
        public string MeetingFullAddress { get; set; }
        public string MeetingStreet { get; set; }
        public Nullable<int> MeetingPostCodeId { get; set; }
        public Nullable<decimal> MeetingLatitude { get; set; }
        public Nullable<decimal> MeetingLongitude { get; set; }
        public string Description { get; set; }
        public string QuoteId { get; set; }
        public string BookingNumber { get; set; }
        public Nullable<int> CustomerUserId { get; set; }
        public Nullable<int> SupervisorUserId { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<int> ShiftStatus { get; set; }
        public System.DateTime EnteredDate { get; set; }
        public Nullable<int> EnteredBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> HasPeakPrice { get; set; }
        public string PeakPriceReason { get; set; }
        public Nullable<bool> InductionRequired { get; set; }
        public Nullable<decimal> BookAmount { get; set; }
        public Nullable<long> LHCUserId { get; set; }
        public Nullable<decimal> JobLocationLatitude { get; set; }
        public Nullable<decimal> JobLocationLongitude { get; set; }
        public string JobNumber { get; set; }
        public string JobTitle { get; set; }
    }
}