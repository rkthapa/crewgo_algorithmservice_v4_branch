﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class ActivityLevel
    {
        public int id { get; set; }
        public string name { get; set; }
        public int p_staff_result_range_from { get; set; }
        public Nullable<int> p_staff_result_range_to { get; set; }
        public int shift_request_number { get; set; }
        public int shift_time_interval { get; set; }
        public int shift_notice_time { get; set; }
        public decimal peak_price_factor { get; set; }
        public System.DateTime entered_date { get; set; }
        public Nullable<int> entered_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<int> updated_by { get; set; }
        public string type { get; set; }
        public Nullable<int> filling_probability { get; set; }
    }
}
