﻿using System;
using System.Collections.Generic;


namespace CREWGO.Service.BusinessService.Common
{
    public class User
    {
        public long UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string FullAddress { get; set; }
        public string Street { get; set; }
        public Nullable<int> PostCodeId { get; set; }
        public string ProfileImage { get; set; }
        public string Description { get; set; }
        public Nullable<bool> EmailVerified { get; set; }
        public Nullable<int> Active { get; set; }
        public string LoginFrom { get; set; }
        public string ForgottenPasswordCode { get; set; }
        public Nullable<System.DateTime> InactivatedCode { get; set; }
        public System.DateTime EnteredDate { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<long> LastLogin { get; set; }
        public string Username { get; set; }
        public string IPAddress { get; set; }
        public string Salt { get; set; }
        public string ActivationCode { get; set; }
        public Nullable<int> ForgottenPasswordTime { get; set; }
        public string RememberCode { get; set; }
        public string FacebookId { get; set; }
        public string GoogleId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> CustomerUserId { get; set; }
        public string RegisterFrom { get; set; }
        public Nullable<long> LHCUserId { get; set; }
        public int GroupId { get; set; }
        public DeviceInfo UserDevice { get; set; }
        public List<DeviceInfo> UserDevices { get; set; }
    }
}