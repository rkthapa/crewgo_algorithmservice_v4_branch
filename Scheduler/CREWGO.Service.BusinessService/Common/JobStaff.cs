﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class JobStaff
    {
        public long JobStaffId { get; set; }
        public long JobShiftId { get; set; }
        public long StaffUserId { get; set; }
        public string TotalProductionTime { get; set; }
        public string TotalBreakTime { get; set; }
        public string TotalTime { get; set; }
        public Nullable<System.TimeSpan> InTimeBreak { get; set; }
        public System.DateTime InTime { get; set; }
        public System.DateTime OutTime { get; set; }
        public System.DateTime GpsInTime { get; set; }
        public System.DateTime GpsOutTime { get; set; }
        public Nullable<System.TimeSpan> OutTimeBreak { get; set; }
        public Nullable<System.TimeSpan> GpsInTimeBreak { get; set; }
        public Nullable<System.TimeSpan> GpsOutTimeBreak { get; set; }
        public Nullable<System.DateTime> AcceptedDate { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public string Description { get; set; }
        public Nullable<bool> IsApproved { get; set; }
        public Nullable<long> ApprovedBy { get; set; }
        public System.DateTime ApprovedDate { get; set; }
        public string PinCode { get; set; }
        public long JobAlertId { get; set; }
        public string JobSiteLocation { get; set; }
        public Nullable<decimal> JobSiteLatitude { get; set; }
        public Nullable<decimal> JobSiteLongitude { get; set; }
        public Int16 JobStatus { get; set; }
        public Nullable<bool> IsBilled { get; set; }
        public Nullable<System.DateTime> BilledDate { get; set; }
        public Nullable<decimal> TotalWage { get; set; }
        public string StaffInitial { get; set; }
        public Nullable<int> NotificationStatus { get; set; }
        public Nullable<int> InductionDaysStatus { get; set; }
        public Nullable<int> InductionHoursStatus { get; set; }
    }
}