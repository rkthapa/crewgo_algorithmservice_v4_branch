﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class StaffSkill
    {
        public int id { get; set; }
        public int skill_id { get; set; }
        public Nullable<int> level_id { get; set; }
        public System.DateTime entered_date { get; set; }
        public Nullable<int> entered_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<int> updated_by { get; set; }
        public long staff_user_id { get; set; }
    }
}
