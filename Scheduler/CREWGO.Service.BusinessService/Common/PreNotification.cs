﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Common
{
    public class PreNotification
    {
        public int NotificationId { get; set; }
        public long SourceId { get; set; }
        public Nullable<int> GeneratedBy { get; set; }
        public string Type { get; set; }
        public long UserId { get; set; }
        public long GroupId { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public string Detail { get; set; }
        public System.DateTime ActiveDate { get; set; }
        public bool IsSent { get; set; }
        public System.DateTime SentDate { get; set; }
        public bool IsScheduled { get; set; }
    }
}