﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Common
{
    public class CallInfo
    {
        public long Id { get; set; }
        public long CallerId { get; set; }
        public long ReceiverId { get; set; }
        public long JobId { get; set; }
        public System.DateTime StartTime { get; set; }
        public System.DateTime EndTime { get; set; }
        public string RoomName { get; set; }
        public int Duration { get; set; }
        public int Price { get; set; }
        public bool CallType { get; set; }
        public bool IsMissed { get; set; }
        public bool IsBusy { get; set; }
        public int CallerDeviceType { get; set; }
        public int ReceiverDeviceType { get; set; }
        public string CallerDeviceToken { get; set; }
        public string ReceiverDeviceToken { get; set; }
        public User Caller { get; set; }
        public User Receiver { get; set; }
    }
}
