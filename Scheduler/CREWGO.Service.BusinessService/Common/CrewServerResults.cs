﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Common
{
    /// <summary>
    /// Holds results data for CrewgServer Algorithm for a User-Shift combination.
    /// </summary>
    public class CrewServerResults
    {
        public long UserId { get; set; }
        public long ShiftId { get; set; }
        public bool IsUserStaff { get; set; }
        public bool IsAlreadyNotified { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool HasRequiredSkill { get; set; }
        public bool HasRequiredSubSkill { get; set; }
        public bool HasRequiredQualification { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsAssociatedToSamePartner { get; set; }
        public bool IsActiveForPartner { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsStaffManual { get; set; }
        public bool IsStaffGlobal { get; set; }
    }
}
