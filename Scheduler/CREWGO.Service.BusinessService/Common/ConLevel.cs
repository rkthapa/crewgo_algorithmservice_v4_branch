﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class ConLevel
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<bool> status { get; set; }
        public System.DateTime entered_date { get; set; }
        public Nullable<int> entered_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<int> updated_by { get; set; }
        public Nullable<int> level_position { get; set; }
    }
}