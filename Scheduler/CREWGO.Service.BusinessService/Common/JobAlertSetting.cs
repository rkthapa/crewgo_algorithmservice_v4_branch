﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class JobAlertSetting
    {
        public long JobAlertSettingId { get; set; }
        public long JobShiftId { get; set; }
        public long StaffUserId { get; set; }
        public long LHCUserId { get; set; }
        public Nullable<int> OrderPriority { get; set; }
        public System.DateTime AssignedDate { get; set; }
        public Nullable<System.DateTime> AlertedDate { get; set; }
        public Nullable<bool> Status { get; set; }
    }
}