﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Common
{
    public class NotificationAuth
    {
        public int Id { get; set; }
        public string AppId { get; set; }
        public string KeyHash { get; set; }
        public bool IsAdmin { get; set; }
    }
}
