﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class JobStaffUnavailability
    {
        public int id { get; set; }
        public long staff_user_id { get; set; }
        public Nullable<System.DateTime> from_time { get; set; }
        public Nullable<System.DateTime> to_time { get; set; }
        public System.DateTime entered_date { get; set; }
        public Nullable<bool> status { get; set; }
    }
}