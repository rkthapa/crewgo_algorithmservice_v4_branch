﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class DeviceInfo
    {
        public int DeviceInfoId { get; set; }
        public long UserId { get; set; }
        public string DeviceId { get; set; }
        public string HashCode { get; set; }
        public int DeviceType { get; set; }
        public string Token { get; set; }
        public Nullable<bool> TokenFlag { get; set; }
        public string DeviceName { get; set; }
        public string DeviceModel { get; set; }
        public string OsVersion { get; set; }
        public Nullable<System.DateTime> LastActivity { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> GroupId { get; set; }
        public string DeviceToken { get; set; }
        public string VoipToken { get; set; }
    }
}