﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CREWGO.Service.BusinessService.Common
{
    /// <summary>
    /// Holds results data for Crewgo Algorithm for a User-Shift combination.
    /// </summary>
    public class CrewgoResults
    {
        public long UserId { get; set; }
        public long ShiftId { get; set; }
        public bool IsUserStaff { get; set; }
        public bool HasRequiredSkill { get; set; }
        public bool HasRequiredSubSkill { get; set; }
        public bool HasRequiredQualification { get; set; }
        public bool IsOnCall { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsInCrewgo { get; set; }
        public bool IsAssociatedPartnerCrewgo { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsAlreadyInPSList { get; set; }
        public bool IsAlreadyNotified { get; set; }
        public bool IsPreferedLocation { get; set; }
        public bool HasRequiredRating { get; set; }
        public bool HasRequiredAcceptance { get; set; }
        public bool HasRequiredExperience { get; set; }
        public bool HasRequiredHistory { get; set; }
        public bool HasRequiredLongevity { get; set; }
        public bool HasRequiredProximity { get; set; }
        public bool HasRequiredAccuracy { get; set; }
        public bool HasRequiredAppearance { get; set; }
        public bool HasRequiredPunctuality { get; set; }
        public bool HasRequiredRapidity { get; set; }
        public bool HasRequiredTransport { get; set; }
        public bool HasRequiredUrgency { get; set; }
    }
}
