﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class JobAlertSettingGlobal
    {
        public Nullable<long> JobAlertSettingGlobalId { get; set; }
        public Nullable<int> SkillId { get; set; }
        public Nullable<int> LevelId { get; set; }
        public Nullable<long> StaffUserId { get; set; }
        public Nullable<long> LHCUserId { get; set; }
        public Nullable<int> OrderPriority { get; set; }
        public System.DateTime AssignedDate { get; set; }
        public Nullable<sbyte> Status { get; set; }
    }
}
