﻿using System;
using System.Collections.Generic;

namespace CREWGO.Service.BusinessService.Common
{
    public class JobShift
    {
        public long JobShiftId { get; set; }
        public long JobId { get; set; }
        public int SkillId { get; set; }
        public int LevelId { get; set; }
        public int RequiredNumber { get; set; }
        public Nullable<System.DateTime> CompletedDate { get; set; }
        public Nullable<System.DateTime> StartTime { get; set; }
        public Nullable<System.DateTime> EndTime { get; set; }
        public Nullable<System.TimeSpan> EndTimeBreak { get; set; }
        public Nullable<decimal> HourlyRate { get; set; }
        public string TotalHour { get; set; }
        public Nullable<decimal> TotalCost { get; set; }
        public string Qualification { get; set; }
        public Nullable<bool> IsFilled { get; set; }
        public Nullable<bool> IsCompleted { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public Nullable<bool> HasPeakPrice { get; set; }
        public Nullable<decimal> PeakPrice { get; set; }
        public Nullable<int> ShiftStatus { get; set; }
        public string PartnerGroup { get; set; }
        public Job ParentJob { get; set; }
    }
}