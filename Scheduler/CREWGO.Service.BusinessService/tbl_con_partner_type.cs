//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CREWGO.Service.BusinessService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_con_partner_type
    {
        public int id { get; set; }
        public string partner_type { get; set; }
        public string description { get; set; }
        public System.DateTime created_date { get; set; }
        public sbyte created_by { get; set; }
        public System.DateTime updated_date { get; set; }
        public sbyte updated_by { get; set; }
        public sbyte is_default { get; set; }
        public string group_type { get; set; }
    }
}
