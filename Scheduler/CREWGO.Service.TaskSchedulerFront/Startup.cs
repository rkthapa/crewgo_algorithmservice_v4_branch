﻿using System.Configuration;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.Owin;
using Owin;
using CREWGO.Service.Common;
using Ninject;
using CREWGO.Service.TaskSchedulerFront.Helpers;
using System;

[assembly: OwinStartup(typeof(CREWGO.Service.TaskSchedulerFront.Startup))]
namespace CREWGO.Service.TaskSchedulerFront
{
    /// <summary>
    /// The OWIN Startup class.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Asserts that the app uses Hangfire Dashboard at website root '~/'.
        /// Can be used to add custom Razor views and menu items.
        /// </summary>
        /// <param name="app">IAppBuilder</param>
        public void Configuration(IAppBuilder app)
        {
            // Use Ninject dependency manager to inject keys as constructor args
            GlobalConfiguration.Configuration.UseNinjectActivator(
                KernelHelper.CustomStandardKernel);

            // Log Out Menu
            NavigationMenu.Items.Add(page => new MenuItem(
                "Log Out", ("/Home/LogOut")));

            // Use custom authorization filter: CustomDashboardAuthFilter
            // Default app path: /
            app.UseHangfireDashboard("/scheduler", new DashboardOptions
            {
                Authorization = new[] { new CustomDashboardAuthFilter() },
                AppPath = "/"
            });
        }
    }
}
