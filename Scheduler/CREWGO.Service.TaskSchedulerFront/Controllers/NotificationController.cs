﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace CREWGO.Service.TaskSchedulerFront.Controllers
{
    /// <summary>
    /// Controller for Razor view 'NotificationManagement' at '~/management'.
    /// </summary>
    public class NotificationController : Controller
    {
        /// <summary>
        /// Runs the notification algorithm 
        /// Invoked on "Schedule Notifications" button press.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RunNotificationsAlgorithm()
        {
            return Redirect(Url.Content("~/jobs/enqueued"));
        }
    }
}
