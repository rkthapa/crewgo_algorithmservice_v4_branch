﻿using CREWGO.Service.TaskSchedulerFront.Services;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CREWGO.Service.TaskSchedulerFront.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        /// <summary>
        /// Login page action.
        /// </summary>
        /// <returns>Returns the view containing Log In page.</returns>
        public ActionResult Index()
        {
            if (Session == null)
            {
                return View();
            }
            else if (Convert.ToBoolean(Session["IsLoggedIn"]) == true)
            {
                return RedirectToAction("", "scheduler");
            }
            else
            {
                return View("Index");
            }
            
        }
        /// <summary>
        /// Validates login credentials
        /// </summary>
        /// <param name="email">Registered Email</param>
        /// <param name="password">Entered Password</param>
        /// <returns>View</returns>
        public async Task<ActionResult> Verify(string email, string password)
        {
            bool isValid = await new CREWGOService()
                 .AuthenticateDashboardUse(email, password);
            if (isValid)
            {
                Session["LoginFailed"] = false;
                Session["IsLoggedIn"] = true;
                return RedirectToAction("", "scheduler");
            }
            else
            {
                Session["LoginFailed"] = true;
                return RedirectToAction("Index", "Home");
            }
        }
        /// <summary>
        /// Log out action. Clears session data. 
        /// </summary>
        /// <returns>Rredirects user to login page.</returns>
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}
