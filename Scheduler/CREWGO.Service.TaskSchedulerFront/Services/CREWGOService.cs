﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using CREWGO.Service.BusinessService;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using CREWGO.Service.TaskSchedulerFront.Helpers;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using System.ComponentModel;
using System.Diagnostics;

namespace CREWGO.Service.TaskSchedulerFront.Services
{
    /// <summary>
    /// Service class for all algorithms
    /// </summary>
    public class CREWGOService : ICREWGOService
    {
        #region VARIABLES
        #endregion
        #region CONSTRUCTOR
        #endregion
        #region CREWGO ALGORITHM SERVICES
        #region GET METHODS
        public async Task<List<JobShift>> GetShifts()
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetLatestShifts;
            var serviceUrl = urlHelper.GenerateServiceUrl();
            try
            {
                var response = await CustomHttpClient
                                .CustomHttpClientObj
                                .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
               
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert
                    .DeserializeObject<List<JobShift>>(jsonString);
            }
            catch(Exception)
            {
                throw;
            }
        }
        public async Task<List<User>> GetStaffByGeneralPriority(
            long partnerId, long jobShiftId, int skillId, 
            int levelId, int userLimit, DateTime? startTime, DateTime? endTime)
        {
            if (startTime == null)
            {
                throw new ArgumentNullException("startTime");
            }
            if (endTime == null)
            {
                throw new ArgumentNullException("endTime");
            }
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.GetAvailableStaffByGeneralPriority;
            urlHelper.Parameters.Enqueue(partnerId.ToString());
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            urlHelper.Parameters.Enqueue(skillId.ToString());
            urlHelper.Parameters.Enqueue(levelId.ToString());
            urlHelper.Parameters.Enqueue(userLimit.ToString());
            urlHelper.Parameters.Enqueue(
                startTime.Value.ToString(Utility.DateTimeFormat));
            urlHelper.Parameters.Enqueue(
                endTime.Value.ToString(Utility.DateTimeFormat));
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                
                var jsonString = await response.Content.ReadAsStringAsync();
                if (jsonString != "[]")
                {

                }
                return JsonConvert.DeserializeObject<List<User>>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<List<User>> GetStaffByGlobalPriority(
            long partnerId, long jobShiftId, int skillId, int levelId, 
            int userLimit, DateTime? startTime, DateTime? endTime)
        {
            if(startTime == null)
            {
                throw new ArgumentNullException("startTime");
            }
            if (endTime == null)
            {
                throw new ArgumentNullException("endTime");
            }
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.GetAvailableStaffByGlobalPriority;
            urlHelper.Parameters.Enqueue(partnerId.ToString());
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            urlHelper.Parameters.Enqueue(skillId.ToString());
            urlHelper.Parameters.Enqueue(levelId.ToString());
            urlHelper.Parameters.Enqueue(userLimit.ToString());
            urlHelper.Parameters.Enqueue(
                startTime.Value.ToString(Utility.DateTimeFormat));
            urlHelper.Parameters.Enqueue(
                endTime.Value.ToString(Utility.DateTimeFormat));
            var serviceUrl = urlHelper.GenerateServiceUrl();
            
            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<User>>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<List<User>> GetStaffByManualPriority(
            long jobShiftId, int userLimit, 
            DateTime? startTime, DateTime? endTime)
        {
            if (startTime == null)
            {
                throw new ArgumentNullException("startTime");
            }
            if (endTime == null)
            {
                throw new ArgumentNullException("endTime");
            }
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.GetAvailableStaffByManualPriority;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            urlHelper.Parameters.Enqueue(userLimit.ToString());
            urlHelper.Parameters.Enqueue(
                startTime.Value.ToString(Utility.DateTimeFormat));
            urlHelper.Parameters.Enqueue(
                endTime.Value.ToString(Utility.DateTimeFormat));
            var serviceUrl = urlHelper.GenerateServiceUrl();
            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<User>>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<Int32> GetJobAlertCount(long staffUserId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.GetJobAlertCount;
            urlHelper.Parameters.Enqueue(staffUserId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                
                return Convert.ToInt32(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<Boolean> OrderPriorityExists(long jobShiftId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.OrderPriorityExists;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                
                
                return Convert.ToBoolean(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<Boolean>
            GlobalPriorityExists(long partnerId, int skillId, int levelId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GlobalPriorityExists;
            urlHelper.Parameters.Enqueue(partnerId.ToString());
            urlHelper.Parameters.Enqueue(skillId.ToString());
            urlHelper.Parameters.Enqueue(levelId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                
                return Convert.ToBoolean(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JobAlert GetJobAlert(long jobShiftId, long staffUserId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetJobAlertByShiftUserId;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            urlHelper.Parameters.Enqueue(staffUserId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                                .CustomHttpClientObj
                                .GetAsync(serviceUrl)
                                .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                
                var jsonString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<JobAlert>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JobAlertSetting GetJobAlertSettingByShiftUser
            (long jobShiftId, long staffUserId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.GetJobAlertSettingByShiftUserId;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            urlHelper.Parameters.Enqueue(staffUserId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                        .CustomHttpClientObj
                        .GetAsync(serviceUrl)
                        .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                
                var jsonString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert
                        .DeserializeObject<JobAlertSetting>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JobAlertSettingGlobal GetJobAlertSettingByPartnerUser(
            long partnerId, long staffUserId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.GetJobAlertSettingByPartnerUserId;
            urlHelper.Parameters.Enqueue(partnerId.ToString());
            urlHelper.Parameters.Enqueue(staffUserId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                            .CustomHttpClientObj
                            .GetAsync(serviceUrl)
                            .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                
                var jsonString = response.Content.ReadAsStringAsync().Result;
                return JsonConvert
                        .DeserializeObject<JobAlertSettingGlobal>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JobAlertSetting GetJobManualAlertSetting(
            JobAlertSetting obj)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.JobAlertSetting;
            urlHelper.ApiPath = ApiPath.GetById;
            urlHelper.Parameters.Enqueue(obj.JobAlertSettingId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();
   
            try
            {
                var response = CustomHttpClient
                            .CustomHttpClientObj
                            .GetAsync(serviceUrl)
                            .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                JobAlertSetting setting = response
                    .Content
                    .ReadAsAsync<JobAlertSetting>()
                    .Result;
                setting.AlertedDate = DateTime.UtcNow.Date;
                setting.Status = true;

                return setting;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JobAlertSettingGlobal GetJobGlobalAlertSetting(
            JobAlertSettingGlobal obj)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.JobAlertSettingGlobal;
            urlHelper.ApiPath = ApiPath.GetById;
            urlHelper.Parameters.Enqueue(
                obj.JobAlertSettingGlobalId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                                .CustomHttpClientObj
                                .GetAsync(serviceUrl)
                                .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                JobAlertSettingGlobal setting =
                       response
                       .Content
                       .ReadAsAsync<JobAlertSettingGlobal>()
                       .Result;
                setting.Status = 0;

                return setting;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<Boolean> IsShiftQuotaFulfilled(long jobShiftId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.IsShiftQuotaFulfilled;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                
                return Convert.ToBoolean(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Checks if staff has autobookable enabled ot not.
        /// </summary>
        /// <param name="staffId">Staff's userId.</param>
        /// <returns>True if autobookable, False otherwise.</returns>
        public async Task<Boolean> IsStaffAutobookable(long staffId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo; 
            urlHelper.ApiPath =
                ApiPath.IsStaffAutobookable;
            urlHelper.Parameters.Enqueue(staffId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }


                return Convert.ToBoolean(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<int> GetShiftNoticePeriod()
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath =
                ApiPath.GetShiftNoticePeriod;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                
                return Convert.ToInt32(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region POST METHODS
        /// <summary>
        /// Posts a JobStaff object to API.
        /// </summary>
        /// <param name="obj">JobStaff object.</param>
        /// <param name="shiftId">Shift ID for DisplayName attribute.</param>
        /// <param name="staffId">Staff ID for DisplayName attribute.</param>
        /// <param name="type">Algorithm type for DisplayName attribure.</param>
        [DisplayName("Post JobStaff: Shift: {1}, Staff: {2} for {3} Algorithm")]
        public async Task<bool> PostJobStaff(
            JobStaff jobStaff, string shiftId, string staffId, string algorithm)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.PostJobStaff;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                            .CustomHttpClientObj
                            .PostAsJsonAsync(serviceUrl, jobStaff);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }
        /// <summary>
        /// Posts a JobAlert object to API.
        /// </summary>
        /// <param name="obj">JobAlert object</param>
        /// <param name="id">ParentJobID for DisplayName attribute only</param>
        /// <param name="type">Algorithm type for DisplayName attribure</param>
        [DisplayName("Post Job Alert: ID: {1} for {2} Algorithm")]
        public void PostJobAlert(
            JobAlert obj, string id, string type)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.JobAlert;
            urlHelper.ApiPath = ApiPath.PostJobAlert;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                            .CustomHttpClientObj
                            .PostAsJsonAsync(serviceUrl, obj)
                            .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Post job alert settings to database
        /// </summary>
        /// <param name="obj">Manual Alert Setting Object</param>
        public void PostJobAlertSetting(JobAlertSetting obj)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.JobAlertSetting;
            urlHelper.ApiPath = ApiPath.PostJobAlertSetting;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                            .CustomHttpClientObj
                            .PostAsJsonAsync(serviceUrl, obj)
                            .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region PUT METHODS
        public void PutJobAlertSetting(JobAlertSetting obj)
        {
            var setting = new JobAlertSetting();
            try
            {
                setting = GetJobManualAlertSetting(obj);
            }
            catch(Exception)
            {
                throw;
            }

            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.JobAlertSetting;
            urlHelper.ApiPath = ApiPath.PutJobAlertSetting;
            var serviceUrl = urlHelper.GenerateServiceUrl();
            
            try
            {
                var response = CustomHttpClient
                            .CustomHttpClientObj
                            .PutAsJsonAsync(serviceUrl, setting)
                            .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void PutJobAlertSettingGlobal(JobAlertSettingGlobal obj)
        {
            var setting = new JobAlertSettingGlobal();
            try
            {
                setting = GetJobGlobalAlertSetting(obj);
            }
            catch (Exception)
            {
                throw;
            }

            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.JobAlertSettingGlobal;
            urlHelper.ApiPath = ApiPath.PutJobAlertSettingGlobal;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                            .CustomHttpClientObj
                            .PutAsJsonAsync(serviceUrl, setting)
                            .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region SET OBJECTS
        public JobAlert SetJobAlert(
            long jobShiftId, long staffUserId, JobAlertStatusEnum status)
        {
            JobAlert obj = new JobAlert();
            obj.JobShiftId = jobShiftId;
            obj.StaffUserId = staffUserId;
            obj.Status = (int)status;
            obj.AlertDate = DateTime.Now;
            return obj;
        }
        public JobAlertSetting SetJobAlertSetting(
            long jobShiftId, long staffUserId, long partnerId)
        {
            JobAlertSetting obj = new JobAlertSetting();

            obj.JobShiftId = jobShiftId;
            obj.StaffUserId = staffUserId;
            obj.LHCUserId = partnerId;
            obj.OrderPriority = null;
            obj.AssignedDate = DateTime.Now;
            obj.AlertedDate = null;
            obj.Status = true;                
            return obj;
        }
        public JobStaff SetJobStaff(
            long staffId, long shiftId, long alertId, DateTime acceptedDate)
        {
            return new JobStaff()
            {
                JobShiftId = shiftId,
                JobStatus = 0,
                StaffUserId = staffId,
                AcceptedDate = acceptedDate,
                JobAlertId = alertId
            };
        }
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Update job alert settings for case: manual
        /// </summary>
        /// <param name="shift">JobSHift</param>
        /// <param name="staff">User (Staff)</param>
        /// <param name="parentJobId">ParentJobID for Display Name Only</param>
        [DisplayName(
            "Update Alert Settings Manual: Parent JobID: {2} , " +
            "StaffID: {1} , ShiftID: {0}"
        )]
        public void UpdateAlertSettingsManual(
            long shiftId, long staffId, long parentJobId)
        {
            try
            {
                //Update job alert setting
                var alertSetting = 
                    GetJobAlertSettingByShiftUser(shiftId, staffId);
                PutJobAlertSetting(alertSetting);
            }
            catch(Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Update job alert settings for case: global
        /// </summary>
        /// <param name="shift">JobSHift</param>
        /// <param name="staff">User (Staff)</param>
        /// <param name="parentJobId">ParentJobID for Display Name Only</param>
        [DisplayName(
            "Update Alert Settings Global: Parent JobID: {2} , " +
            "StaffID: {1} , LHCUserID: {0}"
        )]
        public void UpdateAlertSettingsGlobal(
            long lhcUserId, long staffId, long parentJobId)
        {
            try
            {
                //Update job alert global setting
                var alertGlobalSetting = GetJobAlertSettingByPartnerUser(
                                            lhcUserId,
                                            staffId
                                        );
                PutJobAlertSettingGlobal(alertGlobalSetting);
            }
            catch(HttpRequestException)
            {
                throw;
            }
        }
        /// <summary>
        /// Update job alert settings for case: general
        /// </summary>
        /// <param name="shift">JobSHift</param>
        /// <param name="staffId">User (Staff) Id</param>
        /// <param name="lhcUserId">The associated LHC User ID</param>
        /// <param name="parentJobId">ParentJobID for Display Name Only</param>
        [DisplayName(
            "Update Alert Settings General: Parent JobID: {3} , " +
            "ShiftId: {0} , StaffID: {1} , LHCUserID: {2}"
        )]
        public void UpdateAlertSettingsGeneral(
            long shiftId, long staffId, long lhcUserId, long parentJobId)
        {
            try
            {
                PostJobAlertSetting(
                    SetJobAlertSetting(shiftId, staffId, lhcUserId));
            }
            catch(Exception)
            {
                throw;
            }
        }
        public async Task MaintainAlertLog(
            string jobTitle, string jobNumber, long jobShiftId,
            string staffPriorityType, long? partnerId, int shiftReqNumber, 
            int skillId, int levelId, DateTime? startTime, DateTime? endTime,
            long staffId, string staffName, string staffEmail, 
            string deviceToken)
        {
            await Task.Run(() =>
            {
                string content = string.Empty;
                string path = 
                    System.AppDomain.CurrentDomain.BaseDirectory + 
                    string.Format(
                        "{0}{1}#{2}_{3}_{4}.txt", 
                        ConfigurationManager
                            .AppSettings["PARTNERMODULE_PATH"]
                            .ToString(), 
                        jobTitle, 
                        jobNumber, 
                        jobShiftId, 
                        DateTime.Now.ToString("ddMMyyyy")
                    );
                if (!File.Exists(path))
                    content = 
                        string.Format(
                            "{0} | {1}-{2}-{3} | {4}-{5}-{6} | {7} | {8}" + 
                                System.Environment.NewLine + 
                                System.Environment.NewLine, 
                            staffPriorityType, 
                            jobShiftId, 
                            partnerId, 
                            shiftReqNumber, 
                            jobTitle, 
                            skillId,
                            levelId,
                            startTime,
                            endTime
                        );
                content += 
                    string.Format(
                        "{0} | {1} | {2} | {3} | Sent On: {4}",
                        staffId,
                        staffName,
                        staffEmail,
                        deviceToken,
                        DateTime.Now.ToString()
                    );
                Utility.WriteLogToFileExclusively(path, content);
            });
        }
        #endregion
        #region POTENTIAL STAFF ALGORITHM
        #region GET METHODS
        /// <summary>
        /// Gets the count of potential staffs.
        /// </summary>
        /// <param name="jobShiftId">The Job Shift Id</param>
        /// <returns>Count of Potential Staffs(int)</returns>
        public async Task<int> GetPotentialStaffCount(long jobShiftId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetPotentialStaffCount;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                
                return Convert.ToInt32(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Fetches shift sociability factor.
        /// </summary>
        /// <param name="jobShiftId">The job shift's Id</param>
        /// <returns>Shift sociability factor (0 <= SSF <= 1)</returns>
        public async Task<float> GetShiftSociabilityFactor(
            long jobShiftId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetShiftSociabilityFactor;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }

                return float.Parse(
                    await response.Content.ReadAsStringAsync());
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets all activity levels defined in the admin portal.
        /// </summary>
        /// <returns>ActivityLevelList</returns>
        public async Task<List<ActivityLevel>> GetAllActivityLevels()
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetAllActivityLevels;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                string jsonString = await response.Content.ReadAsStringAsync();
                return new JavaScriptSerializer()
                    .Deserialize<List<ActivityLevel>>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets list of top potential staffs upto 'count'
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        /// <param name="count">The number of potential staffs to fetch</param>
        /// <returns>List of PotentialStaff</returns>
        public async Task<List<PotentialStaff>> GetPotentialStaffs(
            long jobShiftId, int count)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetPotentialStaffs;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            urlHelper.Parameters.Enqueue(count.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                string jsonString = await response.Content.ReadAsStringAsync();
                return (new JavaScriptSerializer())
                    .Deserialize<List<PotentialStaff>>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets the value of short notice period (in hours) by API call
        /// </summary>
        /// <returns>Timespan in hours</returns>
        public async Task<TimeSpan> GetShortNoticePeriod()
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetShortNoticePeriod;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                string period = await response.Content.ReadAsStringAsync();
                return TimeSpan.FromHours(Convert.ToInt32(period));
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Checks potential staff's notification status for the job shift.
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <returns>True if notification is sent, false otherwise</returns>
        public bool CheckPSNotificationStatus(long userId, long shiftId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.CheckPSNotificationStatus;
            urlHelper.Parameters.Enqueue(shiftId.ToString());
            urlHelper.Parameters.Enqueue(userId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                                .CustomHttpClientObj
                                .GetAsync(serviceUrl)
                                .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                string jsonString = response.Content.ReadAsStringAsync().Result;
                return (new JavaScriptSerializer())
                    .Deserialize<bool>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region POST METHODS
        /// <summary>
        /// API call to generate Potential Staff List for a job in database
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        public async Task GeneratePotentialStaffList(long jobShiftId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GeneratePotentialStaffList;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                                    .CustomHttpClientObj
                                    .PostAsJsonAsync(serviceUrl, jobShiftId);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Updates the potential staff table for a particular shift-user 
        /// combination with value of is_notification_sent as true
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <param name="isScheduled">If notification is scheduled.</param>
        /// <param name="isSent">If notificaiton is sent.</param>
        [DisplayName("Set Notification Status for user: {0}, shift: {1}" +
            "Scheduled: {3}, Sent: {2}")]
        public void UpdatePotentialStaffList(
            long userId, long shiftId, bool isSent, bool isScheduled)
        {
            var requestObj = new Tuple<long, long, bool, bool>(
                                userId, shiftId, isSent, isScheduled);
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.UpdatePotentialStaffList;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = CustomHttpClient
                            .CustomHttpClientObj
                            .PostAsJsonAsync(serviceUrl, requestObj)
                            .Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Updates the potential staff table for a particular shift-user 
        /// combination with value of is_notification_sent as true
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <param name="isScheduled">If notification is scheduled.</param>
        /// <param name="isSent">If notificaiton is sent.</param>
        public async Task UpdatePotentialStaffListAsync(
            long userId, long shiftId, bool isSent, bool isScheduled)
        {
            var requestObj = new Tuple<long, long, bool, bool>(
                                userId, shiftId, isSent, isScheduled);
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.UpdatePotentialStaffList;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var response = await CustomHttpClient
                            .CustomHttpClientObj
                            .PostAsJsonAsync(serviceUrl, requestObj);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region PUT METHODS
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Calculates Potential Staff Result
        /// </summary>
        /// <param name="numberOfStaffs">The number of potential staffs</param>
        /// <param name="numberOfShifts">The number of shifts in a job</param>
        /// <returns>Potential Staff Result</returns>
        public double ComputePotentialStaffResult(
            int numberOfStaffs, int numberOfShifts, float ssf)
        {
            if (numberOfShifts == 0)
            {
                throw new DivideByZeroException();
            }
            return (numberOfStaffs * ssf * 10) / numberOfShifts;
        }
        /// <summary>
        /// Calculates the number of Single/Advance requests to be sent
        /// until short notice period arrives
        /// </summary>
        /// <param name="time">Time until short notice period</param>
        /// <param name="rate">The rate to send messages at</param>
        /// <returns></returns>
        public int ComputeNumberOfRequests(
            TimeSpan time, TimeSpan timeUnit, int shiftMultiple)
        {
            if (timeUnit.TotalSeconds == 0.0)
            {
                throw new DivideByZeroException();
            }

            var result = (shiftMultiple * time.TotalSeconds) / 
                            timeUnit.TotalSeconds;
            return Convert.ToInt32(Math.Floor(result));
        }
        /// <summary>
        /// Calculates the number of requests until next ActivityLevel
        /// for Multiple Request Period
        /// </summary>
        /// <param name="currentPSR">PSR of current Activity Lecvel</param>
        /// <param name="nextPSR">PSR of next Activity Level</param>
        /// <returns>Number of requests</returns>
        public int ComputeNumberOfRequests(
            double currentPSR, double nextPSR)
        {
            try
            {
                var result =
                    Convert.ToInt32(Math.Floor(currentPSR - nextPSR));
                return result;
            }
            catch (ArithmeticException)
            {
                throw;
            }
        }
        /// <summary>
        /// Calculates the PSR 
        /// </summary>
        /// <param name="nextAL"></param>
        /// <returns></returns>
        public double CalculateNextPSR(ActivityLevel nextAL)
        {
            if (nextAL == null)
            {
                return 0;
            }
            try
            {
                var result =
                    (double)nextAL.p_staff_result_range_to / 10;
                return result;
            }
            catch (ArithmeticException)
            {
                throw;
            }
        }
        #endregion
        #endregion
        #endregion
        #region CREWGO NOTIFICATIONS SERVICES
        #region GET METHODS
        /// <summary>
        /// Gets PreNotifications which are neither scheduled nor sent.
        /// </summary>
        /// <returns>List of PreNotifications</returns>
        public async Task<IEnumerable<PreNotification>> 
            GetPreNotifications()
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetPreNotifications;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                HttpResponseMessage response = await CustomHttpClient
                                                .CustomHttpClientObj
                                                .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert
                        .DeserializeObject<IEnumerable<PreNotification>>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets User by userId.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>User</returns>
        public async Task<User> GetUserById(long userId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetUserById;
            urlHelper.Parameters.Enqueue(userId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();
            try
            {
                HttpResponseMessage response = await CustomHttpClient
                                                .CustomHttpClientObj
                                                .GetAsync(serviceUrl);

                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<User>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets Job by jobId.
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Job</returns>
        public async Task<Job> GetJobById(long jobId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetJobById;
            urlHelper.Parameters.Enqueue(jobId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();
            try
            {
                HttpResponseMessage response = await CustomHttpClient
                                                .CustomHttpClientObj
                                                .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Job>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets JobShifts by jobShiftId.
        /// </summary>
        /// <param name="jobShiftId">Job Shift Id</param>
        /// <returns>JobShifts</returns>
        public async Task<JobShift> GetJobShiftById(long jobShiftId)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Crewgo;
            urlHelper.ApiPath = ApiPath.GetShiftById;
            urlHelper.Parameters.Enqueue(jobShiftId.ToString());
            var serviceUrl = urlHelper.GenerateServiceUrl();
            try
            {
                HttpResponseMessage response = await CustomHttpClient
                                                .CustomHttpClientObj
                                                .GetAsync(serviceUrl);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<JobShift>(jsonString);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region PUT METHODS
        /// <summary>
        /// Sends "is_scheduled" and "is_sent" values to api respectively
        /// after the notification is scheduled and sent.
        /// </summary>
        /// <param name="prenotificationId">The PreNotificatio id</param>
        /// <param name="isSent">True if notification is sent.</param>
        [DisplayName("Put PreNotification: ID: {0}, Sent: {1}")]
        public void PutPreNotification(int prenotificationId, bool isSent)
        {
            try
            {
                var urlHelper = new UrlHelper();
                urlHelper.ApiType = ApiType.PreNotification;
                urlHelper.ApiPath = ApiPath.GetById;
                urlHelper.Parameters.Enqueue(prenotificationId.ToString());
                var serviceUrl = urlHelper.GenerateServiceUrl();
                var response = CustomHttpClient
                                .CustomHttpClientObj
                                .GetAsync(serviceUrl)
                                .Result;

                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                var jsonString = response.Content.ReadAsStringAsync().Result;
                PreNotification notification = JsonConvert
                    .DeserializeObject<PreNotification>(jsonString);
                notification.NotificationId = prenotificationId;
                notification.SentDate = DateTime.Now;
                notification.IsSent = isSent ? true : false;
                notification.IsScheduled = true;

                urlHelper = new UrlHelper();
                urlHelper.ApiType = ApiType.PreNotification;
                urlHelper.ApiPath = ApiPath.PutPrenotification;
                serviceUrl = urlHelper.GenerateServiceUrl();
                var result = CustomHttpClient
                        .CustomHttpClientObj
                        .PutAsJsonAsync(serviceUrl, notification)
                        .Result;
                if (!result.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)result.StatusCode).ToString(),
                            result.ReasonPhrase
                        )
                    );
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region CUSTOM METHODS

        /// <summary>
        /// Writes algorithm runtime to Log.
        /// </summary>
        /// <param name="sw">The active stopwatch.</param>
        public async Task MaintainLog(Stopwatch sw)
        {
            await Task.Run(() =>
            {
                string content = string.Empty;
                string path = System.AppDomain.CurrentDomain.BaseDirectory +
                    "/Logs/" + DateTime.Now.ToString("ddMMyyyy") + ".txt";

                content = string.Format(System.Environment.NewLine +
                    "Notification Algorithm Runtime [{0}] = {1}ms",
                    DateTime.Now, sw.ElapsedMilliseconds);

                Utility.WriteLogToFileExclusively(path, content);
            });
        }
        #endregion
        #endregion
        #region AUTH SERVICES
        /// <summary>
        /// Calls api to authorise user trying to access hangfire dashboard.
        /// </summary>
        /// <param name="email">Entered email</param>
        /// <param name="password">Entered password</param>
        /// <returns>True if authorised</returns>
        public async Task<bool> AuthenticateDashboardUse(
            string email, string password)
        {
            var urlHelper = new UrlHelper();
            urlHelper.ApiType = ApiType.Auth;
            urlHelper.ApiPath = ApiPath.AuthenticateDashboardUse;
            var serviceUrl = urlHelper.GenerateServiceUrl();

            try
            {
                var obj = new Tuple<string, string>(email, password);
                HttpResponseMessage response = await CustomHttpClient
                                            .CustomHttpClientObj
                                            .PostAsJsonAsync(serviceUrl, obj);
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(
                        String.Format(
                            "{0} : {1}",
                            ((int)response.StatusCode).ToString(),
                            response.ReasonPhrase
                        )
                    );
                }
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<bool>(jsonString);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
    }
}