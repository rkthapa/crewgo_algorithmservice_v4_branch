﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using CREWGO.Service.Common.RequestObject;
using CREWGO.Service.TaskSchedulerFront.Helpers;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.TaskSchedulerFront.Services;
using Hangfire;
using Hangfire.SqlServer;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CREWGO.Service.TaskSchedulerFront.Modules
{
    /// <summary>
    /// Handles fetching and scheduling of PreNotifications.
    /// </summary>
    public class CREWGONotifications : ICREWGONotifications
    {

        #region PRIVATE VARIABLES
        private static string alertBody = string.Empty;
        private static NotificationTypeEnum alertType = 0;
        private static RequestJobDetail requestJobDetail = null;
        private ICREWGOService crewgoService;
        #endregion
        #region PUBLIC VARIABLES
        public string ErrorPath =
            System.AppDomain.CurrentDomain.BaseDirectory
            + "/Logs/ErrorLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
        public string LogPath =
            System.AppDomain.CurrentDomain.BaseDirectory
            + "/Logs/BackgroundTaskLog_" + DateTime.Now.ToString("ddMMyyyy") +
            ".txt";
        #endregion
        #region CONSTRUCTOR
        public CREWGONotifications(ICREWGOService service)
        {
            crewgoService = service;
            Hangfire.JobStorage.Current = new SqlServerStorage("HangfireDB");
        }
        public CREWGONotifications()
        {
            crewgoService = new CREWGOService();
            Hangfire.JobStorage.Current = new SqlServerStorage("HangfireDB");
        }
        #endregion
        #region PUSHNOTIFICATION LOGIC
        /// <summary>
        /// Gets all pending prenotifications.
        /// Fills in necessary details.
        /// Checks if user device tokens are present.
        /// Queues notifications in Hangfire server.
        /// Changes "is_scheduled" field of prenotification to 1.
        /// On completion of scheduled jobs, sets "is_sent" field to 1.
        /// </summary>
        /// <returns>Awaitable async Task</returns>
        public async Task ScheduleNotifications()
        {
            //Get list of notifications from database.
            var preNotifications = 
                await crewgoService.GetPreNotifications();

            if (preNotifications.Count<PreNotification>() <= 0)
            {
                return;
            }
            foreach (var notification in preNotifications)
            {
                //Get respective user
                var responseUser = 
                    await crewgoService.GetUserById(notification.UserId);
                if (responseUser == null)
                {
                    continue;
                }
                try
                {
                    await SetNotificationAttributes(notification);
                }
                catch(Exception)
                {
                    continue;
                }
              

                //List of device tokens [Android|IOS]
                var iosTokens = new List<string>();
                var androidTokens = new List<string>();
                var userDevice = responseUser.UserDevice;
                //If single device token
                if (userDevice != null && !string.IsNullOrEmpty(userDevice.DeviceToken))
                {
                    if (userDevice.DeviceType == (int)DeviceTypeEnum.ANDROID)
                        androidTokens.Add(userDevice.DeviceToken);
                    else
                        iosTokens.Add(userDevice.DeviceToken);
                }

                //If multiple device token
                if (responseUser.UserDevices != null && 
                    responseUser.UserDevices.Count > 0)
                {
                    foreach (var device in responseUser.UserDevices)
                    {
                        if (!string.IsNullOrEmpty(device.DeviceToken))
                        {
                            if (device.DeviceType == (int)DeviceTypeEnum.ANDROID)
                                androidTokens.Add(device.DeviceToken);
                            else
                                iosTokens.Add(device.DeviceToken);
                        }
                    }
                }
                if ((iosTokens != null && iosTokens.Count > 0) || 
                    (androidTokens != null && androidTokens.Count > 0))
                {
                    BackgroundJob.Enqueue(() =>
                        crewgoService
                        .PutPreNotification(notification.NotificationId, false)
                    );
                    try
                    { 
                        Utility.WriteLogToFileExclusively(LogPath,
                            requestJobDetail.jobId.ToString() + "\r\n");
                    }
                    catch(Exception)
                    { }
                    var scheduleID = BackgroundJob.Schedule(() =>
                        ScheduleNotificationsWrapper(
                            1,
                            alertBody,
                            alertType,
                            androidTokens,
                            iosTokens,
                            null,
                            requestJobDetail,
                            null,
                            (UserGroupEnum)responseUser.GroupId,
                            requestJobDetail.jobTitle,
                            requestJobDetail.jobId.ToString()
                        ),
                        notification.ActiveDate - DateTime.UtcNow);

                    BackgroundJob.ContinueWith(scheduleID, () =>
                        crewgoService
                        .PutPreNotification(notification.NotificationId, true)
                    );
                                   
                }
            }
        }
        #endregion
        #region CUSTOM FUNCTIONS
        /// <summary>
        /// Set the necessary notification attributes
        /// </summary>
        /// <param name="notification"></param>
        public static async Task SetNotificationAttributes(
            PreNotification notification)
        {
            alertBody = notification.Message;

            requestJobDetail = new RequestJobDetail();
            requestJobDetail.alertId = notification.NotificationId;
            requestJobDetail.alertMessage = notification.Message;
            requestJobDetail.sentDate = DateTime.Now;

            switch (notification.Type)
            {
                case "JobAlerts":
                    alertType = NotificationTypeEnum.JOBALERTS;
                    break;

                case "JobPosted":
                    alertType = NotificationTypeEnum.JOBPOSTED;
                    requestJobDetail.jobId = notification.SourceId;
                    break;

                case "JobAccepted":
                    alertType = NotificationTypeEnum.JOBACCEPTED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "JobCompleted":
                    alertType = NotificationTypeEnum.JOBCOMPLETED;
                    requestJobDetail.jobId = notification.SourceId;
                    break;

                case "JobCancelled":
                    alertType = NotificationTypeEnum.JOBCANCELLED;
                    requestJobDetail.jobId = notification.SourceId;
                    break;

                case "JobUpdated":
                    alertType = NotificationTypeEnum.JOBUPDATED;
                    requestJobDetail.jobId = notification.SourceId;
                    break;

                case "JobStart":
                    alertType = NotificationTypeEnum.BEFOREJOBSTART;
                    requestJobDetail.jobId = notification.SourceId;
                    break;

                case "StaffLeavesJobSite":
                    alertType = NotificationTypeEnum.STAFFLEFT;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "MissToCompleteJob":
                    alertType = NotificationTypeEnum.MISSEDTOCOMPLETE;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "PaymentMade":
                    alertType = NotificationTypeEnum.PAYMENTMADE;
                    requestJobDetail.jobId = notification.SourceId;
                    break;

                case "TimesheetApproved":
                    alertType = NotificationTypeEnum.TIMESHEETAPPROVED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "TimesheetCompleted":
                    alertType = NotificationTypeEnum.TIMESHEETCOMPLETED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "OnGoingJob":
                    alertType = NotificationTypeEnum.ONGOINGJOB;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "InductionRequired":
                    alertType = NotificationTypeEnum.INDUCTIONREQUIRED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;
               
                case "ShiftUpdated":
                    alertType = NotificationTypeEnum.SHIFTUPDATED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "SupervisorAdded":
                    alertType = NotificationTypeEnum.SUPERVISORADDED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "SupervisorRemoved":
                    alertType = NotificationTypeEnum.SUPERVISORREMOVED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "JobAdded":
                    alertType = NotificationTypeEnum.JOBADDED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "JobRemoved":
                    alertType = NotificationTypeEnum.JOBREMOVED;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                case "AdminJobOffer":
                    alertType = NotificationTypeEnum.ADMINJOBOFFER;
                    requestJobDetail.jobShiftId = notification.SourceId;
                    break;

                default:
                    break;
            }

            if (notification.Type == "JobAccepted" || 
                notification.Type == "StaffLeavesJobSite" || 
                notification.Type == "MissToCompleteJob" ||
                notification.Type == "TimesheetApproved" || 
                notification.Type == "TimesheetCompleted" || 
                notification.Type == "OngoingJob")
            {
                var responseShift = 
                    await new CREWGOService()
                    .GetJobShiftById(requestJobDetail.jobShiftId);
                if (responseShift != null)
                {
                    requestJobDetail.jobId = responseShift.JobId;
                    requestJobDetail.jobTitle = 
                        responseShift.ParentJob.JobTitle;
                    requestJobDetail.jobAddress = 
                        responseShift.ParentJob.JobFullAddress;
                }
            }
            else
            {
                var responseJob = await new CREWGOService()
                        .GetJobById(requestJobDetail.jobId);
                if (responseJob != null)
                {
                    requestJobDetail.jobTitle = responseJob.JobTitle;
                    requestJobDetail.jobAddress = responseJob.JobFullAddress;
                }
            }
        }
        /// <summary>
        /// Wrapper to schedule notifications.
        /// </summary>
        /// <param name="badgeCount">The badge count to be displayed.</param>
        /// <param name="body">Notification body</param>
        /// <param name="type">Notification type</param>
        /// <param name="androidTokens">Android token list</param>
        /// <param name="iosTokens">IOS token list</param>
        /// <param name="message">Message to be included(JSON String)</param>
        /// <param name="jobDetails">The job details</param>
        /// <param name="staffPosition">The staff's position</param>
        /// <param name="groupId">The user's group ID</param>
        /// <param name="title">The notification title</param>
        /// <param name="id">The notification ID</param>
        [DisplayName("Schedule Notifications: Type: {2}, Job: {9} , ID: {10}")]
        public void ScheduleNotificationsWrapper(
            int badgeCount,
            string body,
            NotificationTypeEnum type,
            List<string> androidTokens,
            List<string> iosTokens,
            RequestMessage message,
            RequestJobDetail jobDetails,
            RequestStaffPosition staffPosition,
            UserGroupEnum groupId,
            string title,
            string id)
        {
            

            // Get an instance of PushNotifications class from the IoC Container
            var kernel = KernelHelper.CustomStandardKernel;
            var pushNotification = kernel.Get<IPushNotifications>();

            // Schedule the notification
            pushNotification.SendPushNotification(
                badgeCount,
                body,
                string.Empty,
                type,
                androidTokens,
                iosTokens,
                message,
                jobDetails,
                staffPosition,
                null,
                groupId,
                title,
                id
            );
        }
        #endregion
    }
}