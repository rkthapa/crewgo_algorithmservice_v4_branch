﻿using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using CREWGO.Service.TaskSchedulerFront.Helpers;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.TaskSchedulerFront.Services;
using Hangfire.SqlServer;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace CREWGO.Service.TaskSchedulerFront.Modules
{
    /// <summary>
    /// The CrewGo Crew (CRUVA) Algorithm Class
    /// </summary>
    public class CREWGOCrewAlgorithm : ICREWGOCrewAlgorithm
    {
        #region PROPERTIES
        private ICREWGOService crewgoService;
        private static readonly int MaxRetryCount = 3;
        private TimeSpan timeSpan;
        #endregion
        #region CONSTRUCTOR
        public CREWGOCrewAlgorithm()
        {
            crewgoService = new CREWGOService();
            timeSpan = new TimeSpan(0, 0, 0, 0, 0);
        }
        public CREWGOCrewAlgorithm(ICREWGOService service)
        {
            crewgoService = service;
            timeSpan = new TimeSpan(0, 0, 0, 0, 0);
        }
        #endregion
        #region CREW ALGORITHM LOGIC
        /// <summary>
        /// The CRUVA algorithm logic
        /// </summary>
        /// <param name="shift">JobShift</param>
        public async Task RunCrewAlgorithm(JobShift shift)
        {
            // Generate potential staff list in database
            await crewgoService
                    .GeneratePotentialStaffList(shift.JobShiftId);

            // Load Activity Level table from Database
            var activityLevels = crewgoService.GetAllActivityLevels();

            // Get Shift Sociability Factor (SF) for the shift
            var ssf = crewgoService
                        .GetShiftSociabilityFactor(shift.JobShiftId);

            // Get short notice period 
            var snp = crewgoService.GetShortNoticePeriod();

            // Var retry flag (for retries when user is not online)
            sbyte retryCount = 0;

            // Await activity level list.
            List<ActivityLevel> activityLevelList = await activityLevels;

            // Determine highest and least activity levels.
            ActivityLevel leastAL =
                activityLevelList.OrderByDescending(
                    x => x.p_staff_result_range_from
                ).Last();
            ActivityLevel highestAl =
                activityLevelList
                .Where(x => x.p_staff_result_range_to == 0)
                .FirstOrDefault();

            bool isShiftQuotaFulfilled = false;
            while(!isShiftQuotaFulfilled)
            {
                // Get potential staff count for the job
                int potentialStaffCount = await crewgoService
                    .GetPotentialStaffCount(shift.JobShiftId);

                // If Potential Staff List has been exhausted or doesn't exist
                if (potentialStaffCount == 0)
                {
                    break;
                }

                // Await sociability factor
                float sociabilityFactor = await ssf;

                // Compute Potential Staff Result(PSR)
                var potentialStaffResult = Task.Run(() => 
                    crewgoService.ComputePotentialStaffResult(
                        potentialStaffCount, 
                        shift.RequiredNumber, 
                        sociabilityFactor
                    )
                );

                // Await potential staff result calculation result
                double currentPSR = await potentialStaffResult;

                // Check whether it is Single(Advanced) Request Period
                // or Multiple Request Period
                if (currentPSR < leastAL.p_staff_result_range_from)
                {
                    break;
                }
                ActivityLevel currentAL = new ActivityLevel();
                if (currentPSR > highestAl.p_staff_result_range_from)
                {
                    currentAL = highestAl;
                }
                else
                {
                    currentAL = 
                        activityLevelList
                        .FirstOrDefault(x =>
                            currentPSR >= x.p_staff_result_range_from &&
                            currentPSR <= x.p_staff_result_range_to);
                }
                ActivityLevel nextAL =
                    activityLevelList
                    .FirstOrDefault(x => x.p_staff_result_range_to == 
                        currentAL.p_staff_result_range_from - 1);

                TimeSpan timeToShift = (DateTime)shift.StartTime -
                                            DateTime.UtcNow;
                // Await Short Notice Period
                TimeSpan shortNoticePeriod = await snp;
                // If it is Single Request Period, send 1 request per shift 
                // every 'X' minutes as determined by Request Rate 
                // for this period until Multiple Request Period is reached
                if (timeToShift - timeSpan > shortNoticePeriod)
                {
                    // Calculate number of requests to send
                    int numberOfReq = await Task.Run(() =>
                        crewgoService.ComputeNumberOfRequests(
                            (timeToShift - timeSpan - shortNoticePeriod), 
                            TimeSpan.FromSeconds(
                                currentAL.shift_time_interval
                            ),
                            currentAL.shift_request_number
                        )
                    );

                    // Fetch Potential Staffs
                    List<PotentialStaff> potentialStaffs = 
                        await crewgoService.GetPotentialStaffs(
                            shift.JobShiftId, numberOfReq);

                    // If no staff is returned, retry 3 times
                    if (potentialStaffs.Count == 0)
                    {
                        ++retryCount;
                        // After 3 retries, break algorithm for the shift
                        if (retryCount == MaxRetryCount)
                        {
                            break;
                        }
                        continue;
                    }

                    //reset retry count if staffs are found
                    retryCount = 0;
                    
                    // Schedule Notifications
                    await ScheduleNotifications(
                            potentialStaffs,
                            shift,
                            currentAL.shift_time_interval,
                            currentAL.shift_request_number);
                }
                // If it is Multiple Request Period,
                // Recalculate New Potential Staff Result
                // Determine Activity Level for New Potential Staff result
                // Get Request Rate corresponding to the Activity Level
                // Determine the next Activity Level
                // Determine the number of requests 'N' to be sent 
                // until next Activity Level is reached
                // Send 'N' requests depending upon the request rate
                else
                {
                    double nextPSR = 0;
                    if (currentAL.id != leastAL.id)
                    {
                        nextPSR = await Task.Run(() =>
                            crewgoService.CalculateNextPSR(nextAL));
                    }
                    
                    // Calculate number of requests to send
                    int numberOfReq = await Task.Run(() =>
                        crewgoService.ComputeNumberOfRequests(
                            currentPSR, nextPSR)
                    );

                    // Fetch Potential Staffs
                    List<PotentialStaff> potentialStaffs =
                        await crewgoService.GetPotentialStaffs(
                            shift.JobShiftId, numberOfReq);
                    
                    // If no staff is returned, retry 3 times
                    if (potentialStaffs.Count == 0)
                    {
                        // After 3 retries, break algorithm for the shift
                        if (++retryCount == 3)
                        {
                            break;
                        }
                        continue;
                    }

                    //reset retry count if staffs are found
                    retryCount = 0;

                    // Schedule Notifications
                    await ScheduleNotifications(
                            potentialStaffs, 
                            shift, 
                            currentAL.shift_time_interval,
                            currentAL.shift_request_number);
                }

                // Check if shift quota is fulfilled
                isShiftQuotaFulfilled = await crewgoService
                    .IsShiftQuotaFulfilled(shift.JobShiftId);
            }
        }
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Prepares notifications scheduling to potential staffs
        /// </summary>
        /// <param name="staffs">List of Potential Staffs</param>
        /// <param name="shift">The job shift</param>
        /// <param name="interval">Interval to send notifications in</param>
        /// <param name="count">Number of notifications to send at once</param>
        public async Task ScheduleNotifications(
            List<PotentialStaff> staffs, JobShift shift, 
            int interval, int count)
        {
            if (staffs == null || staffs.Count == 0)
            {
                return;
            }

            int counter = 0;
            foreach( PotentialStaff staff in staffs)
            {
                if (staff.Devices == null || staff.Devices.Count == 0)
                {
                    continue;
                }
                ++counter;
                if (counter == count)
                {
                    counter = 0;
                    timeSpan += TimeSpan.FromSeconds(interval);
                }
                var iosTokens =
                    staff.Devices
                    .Where(
                        x => x.DeviceType == (int)DeviceTypeEnum.IOS)
                    .Select(x => x.DeviceToken)
                    .ToList();
                var androidTokens =
                    staff.Devices
                    .Where(
                        x => x.DeviceType == (int)DeviceTypeEnum.ANDROID)
                    .Select(x => x.DeviceToken)
                    .ToList();

                // If staff is autobookable
                var isStaffAutobookable =
                    await crewgoService.IsStaffAutobookable(staff.UserId);

                // Hangfire job scheduling
                Hangfire.JobStorage.Current = new SqlServerStorage("HangfireDB");

                if (isStaffAutobookable)
                {
                    // Schedule the assigned notification
                    var notificationTaskId = Hangfire.BackgroundJob.Schedule(
                        () =>
                        CheckJobStatusAndScheduleNotifications(
                            androidTokens,
                            iosTokens,
                            shift.ParentJob.JobTitle,
                            shift.ParentJob.JobNumber,
                            shift.JobShiftId,
                            staff.UserId,
                            NotificationTypeEnum.STAFFAUTOASSIGNED
                        ), timeSpan
                    );

                    //Set notification scheduled flag to true
                    crewgoService.UpdatePotentialStaffList(
                        staff.UserId,
                        shift.JobShiftId,
                        false,
                        true
                    );

                    // Post Job Alert 
                    var scheduleAlertId = Hangfire.BackgroundJob.ContinueWith(
                        notificationTaskId, 
                        () => CheckJobStatusAndPostJobAlert(
                            shift.JobShiftId,
                            staff.UserId,
                            shift.ParentJob.JobNumber,
                            JobAlertStatusEnum.ACCEPTED
                        )
                    );

                    // Post Job Staff
                    var postStaffId = Hangfire.BackgroundJob.ContinueWith(
                            scheduleAlertId, 
                            () => GetAlertIdAndPostJobStaff(
                                shift.JobShiftId, 
                                staff.UserId,
                                shift.JobShiftId.ToString(),
                                staff.UserId.ToString()
                                )
                        );
                    
                    continue;
                }

                //Schedule notification
                var scheduleId = Hangfire.BackgroundJob.Schedule(() => 
                    CheckJobStatusAndScheduleNotifications(
                        androidTokens, 
                        iosTokens,
                        shift.ParentJob.JobTitle, 
                        shift.ParentJob.JobNumber, 
                        shift.JobShiftId,
                        staff.UserId,
                        NotificationTypeEnum.JOBALERTS),
                        timeSpan
                    );

                //Set notification scheduled flag to true
                crewgoService.UpdatePotentialStaffList(
                    staff.UserId,
                    shift.JobShiftId,
                    false,
                    true
                );
                //Post job alert 
                Hangfire.BackgroundJob.ContinueWith( 
                    scheduleId, () => 
                        CheckJobStatusAndPostJobAlert(
                            shift.JobShiftId, 
                            staff.UserId,
                            shift.ParentJob.JobNumber,
                            JobAlertStatusEnum.UNREAD
                        )
                );
            }
        }
        /// <summary>
        /// Checks if shift quota is fulfilled 
        /// and schedules notifications if it is not fulfilled
        /// </summary>
        /// <param name="androidTokens">List of Android tokens</param>
        /// <param name="iosTokens">List of iOS tokens</param>
        /// <param name="jobTitle">The ParentJob Title of the shift</param>
        /// <param name="jobID">The Parent JobID for of the shift</param>
        /// <param name="shiftId">The ShiftID to send notifications for</param>
        [DisplayName("Schedule Crew Notifications: Job: {2}, JobID: {3}, Staff: {4}, Type: {5}")]
        public bool CheckJobStatusAndScheduleNotifications(
            List<string> androidTokens, List<string> iosTokens, 
            string jobTitle, string jobNumber, long shiftId, long staffId,
            NotificationTypeEnum type)
        {
            var isQuotaFulfilled = IsShiftQuotaFulfilled(shiftId);
            if (isQuotaFulfilled)
            {
                return false;
            }

            // Get an instance of PushNotifications class from the IoC Container
            var kernel = KernelHelper.CustomStandardKernel;
            var pushNotification = kernel.Get<IPushNotifications>();

            // Schedule the notification
            pushNotification
            .SendPushNotification(
                1,
                string.Empty,
                string.Empty,
                type,
                androidTokens,
                iosTokens,
                null,
                null,
                null,
                null,
                UserGroupEnum.STAFF,
                jobTitle,
                jobNumber);

            // Update potential staff list
            crewgoService.UpdatePotentialStaffList(staffId, shiftId, true, true);
            return true;
        }
        /// <summary>
        /// Sync wrapper for async method IsStaffQuotaFulfilled
        /// To be used with hangfire only
        /// </summary>
        /// <param name="shiftId">The Shift's ID</param>
        /// <returns>True if shift quota is fulfilled</returns>
        public bool IsShiftQuotaFulfilled(long shiftId)
        {
            return crewgoService.IsShiftQuotaFulfilled(shiftId).Result;
        }
        /// <summary>
        /// Checks if shift quota is fulfilled. If it is not, gets inserted job 
        /// alert Id and posts a job staff with that alert id.
        /// </summary>
        /// <param name="shiftId">The job shift id.</param>
        /// <param name="userId">The staff user id.</param>
        /// <param name="shift">Shift id for DisplayNameAttribute only</param>
        /// <param name="staff">Staff id for DisplayNameAttribute only</param>
        [DisplayName("Post JobStaff: Shift: {2}  Staff: {3} for Partner Algorithm")]
        public void GetAlertIdAndPostJobStaff(
            long shiftId, long userId, string shift, string staff)
        {
            // check if staff has been sent a notification
            var status = crewgoService.CheckPSNotificationStatus(userId, shiftId);
            if (!status)
            {
                return;
            }

            // Get inserted JobAlert id
            var jobAlert = crewgoService.GetJobAlert(shiftId, userId);

            // Construct JobStaff
            var jobStaff = crewgoService.SetJobStaff(
                            userId,
                            shiftId,
                            jobAlert.JobAlertId,
                            DateTime.Now);

            // Post Job Staff
            var result = crewgoService.PostJobStaff(
                jobStaff,
                jobStaff.JobShiftId.ToString(),
                jobStaff.StaffUserId.ToString(),
                "PARTNER"
            ).Result;
        }
        /// <summary>
        /// Checks if shift quota is fulfilled and posts job alert if not.
        /// </summary>
        /// <param name="shiftId">The job shift Id.</param>
        /// <param name="userId">The staff user Id.</param>
        /// <param name="jobNumber">The job's job number.</param>
        [DisplayName("Post Job Alert: ID: {2} for Crew Algorithm, Type: {3}")]
        public void CheckJobStatusAndPostJobAlert(
            long shiftId, long userId, string jobNumber, JobAlertStatusEnum type)
        {
            // check if staff has been sent a notification
            var status = crewgoService.CheckPSNotificationStatus(userId, shiftId);
            if (!status)
            {
                crewgoService.UpdatePotentialStaffList(
                    userId, shiftId, false, false);
                return;
            }

            // Post job alert
            crewgoService.PostJobAlert(
                crewgoService.SetJobAlert(
                    shiftId,
                    userId,
                    type
                ),
                jobNumber,
                "CREW"
            );
        }
        #endregion
    }
}
