﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CREWGO.Service.Common;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.TaskSchedulerFront.Services;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.TaskSchedulerFront.Helpers;
using Ninject;
using CREWGO.Service.Common.RequestObject;
using System.ComponentModel;
using Hangfire.SqlServer;

namespace CREWGO.Service.TaskSchedulerFront.Modules
{
    /// <summary>
    /// The partner algorithm class.
    /// </summary>
    public class CREWGOPartnerAlgorithm: ICREWGOPartnerAlgorithm
    {
        #region PROPERTIES
        private ICREWGOService crewgoService;
        #endregion
        #region CONSTRUCTOR
        public CREWGOPartnerAlgorithm()
        {
            crewgoService = new CREWGOService();
            Hangfire.JobStorage.Current = new SqlServerStorage("HangfireDB");
        }
        public CREWGOPartnerAlgorithm(ICREWGOService service)
        {
            crewgoService = service;
            Hangfire.JobStorage.Current = new SqlServerStorage("HangfireDB");
        }
        #endregion
        #region ALGORITHM LOGIC
        /// <summary>
        /// The Algorithm Logic
        /// </summary>
        /// <param name="shift">JobShift</param>
        /// <param name="shiftNoticePeriod">The job shift notice period</param>
        /// <returns></returns>
        public async Task RunPartnerAlgorithm( 
            JobShift shift, int shiftNoticePeriod)
        {
            var lstStaffAndPriority = await SetAvailableStaffAndPriorityType(
                                            shift, 
                                            shiftNoticePeriod
                                        ); 
            var availableStaff = lstStaffAndPriority.Item1;
            var staffPriorityType = lstStaffAndPriority.Item2;

            if (availableStaff != null && availableStaff.Count<User>() > 0)
            {
                foreach (var staff in availableStaff)
                {
                    //Get alert count
                    var alertCount = 
                        await crewgoService.GetJobAlertCount(staff.UserId);

                    if (staff.UserDevice == null || 
                        string.IsNullOrEmpty(staff.UserDevice.DeviceToken))
                    {
                        continue;
                    }
                    var iosToken = new List<string>();
                    var androidToken = new List<string>();

                    if (staff.UserDevice.DeviceType == 
                        (int)DeviceTypeEnum.ANDROID)
                        androidToken.Add(staff.UserDevice.DeviceToken);
                    else
                        iosToken.Add(staff.UserDevice.DeviceToken);

                    // If staff is autobookable
                    var isStaffAutobookable = 
                        await crewgoService.IsStaffAutobookable(staff.UserId);

                    if (isStaffAutobookable)
                    {
                        // Post Job Alert 
                        crewgoService.PostJobAlert(
                            crewgoService.SetJobAlert(
                                shift.JobShiftId,
                                staff.UserId,
                                JobAlertStatusEnum.ACCEPTED
                            ),
                            shift.ParentJob.JobNumber,
                            "PARTNER"
                        );
                       
                        // Get inserted JobAlert id
                        var jobAlert = await Task.Run(() => 
                                            crewgoService.GetJobAlert(
                                                shift.JobShiftId, 
                                                staff.UserId)
                                        );

                        // Construct JobStaff
                        var jobStaff = crewgoService.SetJobStaff(
                                        staff.UserId, 
                                        shift.JobShiftId, 
                                        jobAlert.JobAlertId, 
                                        DateTime.Now);
                        
                        // Post Job Staff
                        await crewgoService.PostJobStaff(
                            jobStaff, 
                            jobStaff.JobShiftId.ToString(), 
                            jobStaff.StaffUserId.ToString(),
                            "PARTNER"
                        );

                        // Schedule the assigned notification
                        Hangfire.BackgroundJob.Enqueue(() =>
                            ScheduleNotificationsWrapper(
                                alertCount,
                                "",
                                NotificationTypeEnum.STAFFAUTOASSIGNED,
                                androidToken,
                                iosToken,
                                null,
                                null,
                                null,
                                (UserGroupEnum)staff.GroupId,
                                shift.ParentJob.JobTitle,
                                shift.ParentJob.JobNumber,
                                "Auto Assigned"
                            )
                        );
                        continue;
                    }

                    // Schedule the notification
                    var enqueId = Hangfire.BackgroundJob.Enqueue(() =>
                        ScheduleNotificationsWrapper(
                            alertCount, 
                            "", 
                            NotificationTypeEnum.JOBALERTS,
                            androidToken,
                            iosToken,
                            null,
                            null,
                            null,
                            (UserGroupEnum)staff.GroupId,
                            shift.ParentJob.JobTitle,
                            shift.ParentJob.JobNumber,
                            "Job Offer Sent"
                        )
                    );
                    //Insert job alert 
                    var continueId = Hangfire.BackgroundJob.ContinueWith(
                        enqueId, () =>
                            crewgoService.PostJobAlert(
                                crewgoService.SetJobAlert(
                                    shift.JobShiftId,
                                    staff.UserId,
                                    JobAlertStatusEnum.UNREAD
                                ),
                                shift.ParentJob.JobNumber,
                                "PARTNER"
                            )
                    );

                    //Update job alert settings
                    UpdateJobAlertSettings(
                        staffPriorityType, continueId, shift, staff);
                        
                    //Maintain log for push notifications sent
                    await crewgoService
                        .MaintainAlertLog(
                            Utility.ReplaceInvalidCharsWithUnderScore(
                                shift.ParentJob.JobTitle),
                            shift.ParentJob.JobNumber,
                            shift.JobShiftId,
                            staffPriorityType.ToString(),
                            shift.ParentJob.LHCUserId,
                            shift.RequiredNumber,
                            shift.SkillId,
                            shift.LevelId,
                            shift.StartTime,
                            shift.EndTime,
                            staff.UserId,
                            staff.Name,
                            staff.Email,
                            staff.UserDevice.DeviceToken
                    );
                }
            }            
        }
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Sets the staff priority type and gets list of users.
        /// </summary>
        /// <param name="shift">The job shift.</param>
        /// <param name="shiftNoticePeriod">The shift notice period.</param>
        /// <returns>Tuple of list of users and staff priority type.</returns>
        private async Task<Tuple<List<User>, StaffPriorityTypeEnum>> 
            SetAvailableStaffAndPriorityType( JobShift shift, 
            int shiftNoticePeriod)
        {
            var availableStaff = new List<User>();
            var staffPriorityType = StaffPriorityTypeEnum.MANUAL;
            bool manualPriorityExists = false;
            bool globalPriorityExists = false;
            manualPriorityExists = 
                await crewgoService.OrderPriorityExists(shift.JobShiftId);
            globalPriorityExists = await crewgoService.GlobalPriorityExists(
                (int)shift.ParentJob.LHCUserId, shift.SkillId, shift.LevelId);

            // Case 1 : Manual Priority
            if (manualPriorityExists)
            {
                availableStaff = await crewgoService
                    .GetStaffByManualPriority(
                    shift.JobShiftId, 
                    shift.RequiredNumber, 
                    shift.StartTime, 
                    shift.EndTime
                );
                staffPriorityType = StaffPriorityTypeEnum.MANUAL;
            }
            // Case 2 : Global Priority
            else if (globalPriorityExists)
            {
                if (DateTime.Now.Subtract(shift.ParentJob.EnteredDate)
                        .TotalMinutes >= shiftNoticePeriod)
                {
                    availableStaff = await crewgoService
                        .GetStaffByGlobalPriority(
                        (int)shift.ParentJob.LHCUserId, 
                        shift.JobShiftId, 
                        shift.SkillId, 
                        shift.LevelId, 
                        shift.RequiredNumber, 
                        shift.StartTime, 
                        shift.EndTime
                    );
                }
                staffPriorityType = StaffPriorityTypeEnum.GLOBAL;
            }
            // Case 3 : General Priority
            else
            {
                if (DateTime.Now.Subtract(shift.ParentJob.EnteredDate)
                    .TotalMinutes >= shiftNoticePeriod)
                {
                    availableStaff = 
                        await crewgoService.GetStaffByGeneralPriority(
                            (int)shift.ParentJob.LHCUserId,
                            shift.JobShiftId,
                            shift.SkillId,
                            shift.LevelId,
                            shift.RequiredNumber,
                            shift.StartTime,
                            shift.EndTime
                        );
                }
                staffPriorityType = StaffPriorityTypeEnum.GENERAL;
            }
            // Case 4 : Global/Manual Priority, but AvailableStaff < Required. 
            // Fills remaining position from general priority.
            // Sets priority type to general.
            if ((manualPriorityExists || globalPriorityExists) 
                && (availableStaff ==  null || 
                availableStaff.Count < shift.RequiredNumber))
            {
                if (DateTime.Now.Subtract(shift.ParentJob.EnteredDate)
                    .TotalMinutes >= shiftNoticePeriod)
                {
                    var staffs = new List<User>();
                    if (availableStaff == null)
                    {
                        availableStaff = new List<User>();
                    }
                    staffs = await 
                        crewgoService
                        .GetStaffByGeneralPriority(
                            (int)shift.ParentJob.LHCUserId, 
                            shift.JobShiftId, 
                            shift.SkillId, 
                            shift.LevelId,
                            shift.RequiredNumber - availableStaff.Count,
                            shift.StartTime, 
                            shift.EndTime
                        );
                    if (staffs != null && staffs.Count > 0)
                    {
                        availableStaff.AddRange(staffs);
                        availableStaff = availableStaff
                                        .GroupBy(x => x.UserId)
                                        .Select(g => g.First())
                                        .ToList();
                    }
                }
                staffPriorityType = StaffPriorityTypeEnum.GENERAL;
            }
            return new Tuple<List<User>, StaffPriorityTypeEnum>(
                availableStaff, 
                staffPriorityType);
        }
        /// <summary>
        /// Wrapper to schedule notifications.
        /// </summary>
        /// <param name="badgeCount">The badge count to be displayed.</param>
        /// <param name="body">Notification body</param>
        /// <param name="type">Notification type</param>
        /// <param name="androidTokens">Android token list</param>
        /// <param name="iosTokens">IOS token list</param>
        /// <param name="message">Message to be included(JSON String)</param>
        /// <param name="jobDetails">The job details</param>
        /// <param name="staffPosition">The staff's position</param>
        /// <param name="groupId">The user's group ID</param>
        /// <param name="title">The notification title</param>
        /// <param name="id">The notification ID</param>
        /// <param name="mode">Auto-assigned/Job Offer Sent</param>
        [DisplayName("Schedule Partner Notifications: Job: {9} , ID: {10} {11}")]
        public void ScheduleNotificationsWrapper(
            int badgeCount,
            string body,
            NotificationTypeEnum type,
            List<string> androidTokens,
            List<string> iosTokens,
            RequestMessage message,
            RequestJobDetail jobDetails,
            RequestStaffPosition staffPosition,
            UserGroupEnum groupId,
            string title,
            string id,
            string mode)
        {


            // Get an instance of PushNotifications class from the IoC Container
            var kernel = KernelHelper.CustomStandardKernel;
            var pushNotification = kernel.Get<IPushNotifications>();

            // Schedule the notification
            pushNotification.SendPushNotification(
                badgeCount,
                body,
                string.Empty,
                type,
                androidTokens,
                iosTokens,
                message,
                jobDetails,
                staffPosition,
                null,
                groupId,
                title,
                id
            );
        }
        /// <summary>
        /// Updates Job ALert Setting to database
        /// </summary>
        /// <param name="staffPriorityType">StaffPriorityTypeEnum</param>
        /// <param name="continueId">Hangfire Job ID to continue after</param>
        /// <param name="shift">JobShift</param>
        /// <param name="staff">Staff(User)</param>
        private void UpdateJobAlertSettings(
            StaffPriorityTypeEnum staffPriorityType,
            string continueId, JobShift shift, User staff)
        {
            switch (staffPriorityType)
            {
                case StaffPriorityTypeEnum.MANUAL:
                    Hangfire.BackgroundJob.ContinueWith(
                        continueId, () =>
                            crewgoService
                            .UpdateAlertSettingsManual(
                                shift.JobShiftId,
                                staff.UserId,
                                shift.ParentJob.JobId
                            )
                    );
                    break;

                case StaffPriorityTypeEnum.GLOBAL:
                    Hangfire.BackgroundJob.ContinueWith(
                        continueId, () =>
                            crewgoService
                            .UpdateAlertSettingsGlobal(
                                (int)shift.ParentJob.LHCUserId,
                                staff.UserId,
                                shift.ParentJob.JobId
                            )
                    );
                    break;

                case StaffPriorityTypeEnum.GENERAL:
                    Hangfire.BackgroundJob.ContinueWith(
                        continueId, () =>
                            crewgoService
                            .UpdateAlertSettingsGeneral(
                                shift.JobShiftId,
                                staff.UserId,
                                (int)shift.ParentJob.LHCUserId,
                                shift.ParentJob.JobId
                            )
                    );
                    break;
            }
        }
        #endregion
    }
}