﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using CREWGO.Service.Common;
using CREWGO.Service.TaskSchedulerFront.Services;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.TaskSchedulerFront.Helpers;

namespace CREWGO.Service.TaskSchedulerFront.Modules
{
    public class CREWGOAlgorithm : ICREWGOAlgorithm
    {
        #region VARIABLES
        private static string errorPath = 
            System.AppDomain.CurrentDomain.BaseDirectory + 
                string.Format(
                    "{0}{1}.txt", 
                    ConfigurationManager
                        .AppSettings["PARTNERMODULE_PATH_ERROR"]
                        .ToString(), 
                    DateTime.Now.ToString("ddMMyyyy")
                );
        private ICREWGOService crewgoService;
        #endregion
        #region CONSTRUCTOR
        public CREWGOAlgorithm()
        {
            crewgoService = new CREWGOService();
        }
        #endregion
        #region ALGORITHM LOGIC
        public async Task RunAlgorithm()
        {
            try
            {
                //Get list of latest shifts
                var jobShifts = crewgoService.GetShifts();

                //Get priority notice period
                //i.e time to set the priority to manual or global
                var shiftNoticePeriod = crewgoService.GetShiftNoticePeriod();

                await jobShifts;
                await shiftNoticePeriod;
                foreach (var shift in jobShifts.Result)
                {
                    //Check if parent job is associated with partner or not
                    if (shift.PartnerGroup.Equals(
                            "CrewServr", StringComparison.OrdinalIgnoreCase))
                    {
                        try
                        {
                            //Run Partner Algorithm
                            var partnerAlgorithm = new CREWGOPartnerAlgorithm();
                            await partnerAlgorithm
                                .RunPartnerAlgorithm(
                                    shift,
                                    shiftNoticePeriod.Result
                                );
                        }
                        catch(Exception ex)
                        {
                            try
                            {
                                Utility.WriteLogToFileExclusively(
                                    errorPath,
                                    ex.Message + "\n" + ex.StackTrace
                                );
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }
                    }
                    else
                    {
                        try 
                        {
                            //Run Crew Algorithm
                            var crewAlgorithm =
                                new CREWGOCrewAlgorithm();
                            await crewAlgorithm.RunCrewAlgorithm(shift);
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                Utility.WriteLogToFileExclusively(
                                    errorPath,
                                    ex.Message + "\n" + ex.StackTrace
                                );
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Utility.WriteLogToFileExclusively(
                        errorPath,
                        ex.Message + "\n" + ex.StackTrace
                    );
                }
                catch(Exception)
                {
                    throw;
                }
            }
        }
        #endregion
    }
}