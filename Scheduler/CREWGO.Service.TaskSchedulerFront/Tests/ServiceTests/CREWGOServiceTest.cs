﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using CREWGO.Service.TaskSchedulerFront.Services;
using CREWGO.Service.BusinessService.Common;
using System.Threading.Tasks;

namespace CREWGO.Service.TaskSchedulerFront.Tests.ServiceTests
{
    [TestFixture]
    public class CREWGOServiceTest
    {
        [Test]
        public void GetShiftsTest()
        {
            var response = CREWGOService.GetShifts();
            Assert.IsNotNull(response);
        }

        [Test]
        public async Task GetStaffByGeneralPriorityTest()
        {
            long partnerId = 1;
            long jobShiftId = 1;
            int skillId = 1;
            int levelId = 1;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await CREWGOService.GetStaffByGeneralPriority(
                partnerId,
                jobShiftId,
                skillId,
                levelId,
                startTime,
                endTime
            );
            Assert.IsNotNull(response);
            Assert.IsInstanceOf<List<User>>(response);


        }

        [Test]
        public async Task GetStaffByGeneralPriorityNullTest()
        {
            long partnerId = 0;
            long jobShiftId = 0;
            int skillId = 0;
            int levelId = 0;
            DateTime startTime = DateTime.Now + TimeSpan.FromDays(2);
            DateTime endTime = DateTime.Now + TimeSpan.FromDays(3);
            var response = await CREWGOService.GetStaffByGeneralPriority(
                partnerId,
                jobShiftId,
                skillId,
                levelId,
                startTime,
                endTime
            );
            Assert.IsEmpty(response);

        }
    }
}