﻿using CREWGO.Service.TaskSchedulerFront.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CREWGO.Service.TaskSchedulerFront.Tests.HelperTests
{
    [TestFixture]
    public class UrlHelperTest
    {
        [Test]
        public void GenerateServiceUrlEqualityTest()
        {
            var urlHelper = new UrlHelper();
            
            urlHelper.ApiType = DataEnums.ApiType.Crewgo;
            urlHelper.ApiPath = DataEnums.ApiPath.UpdatePotentialStaffList;

            urlHelper.Parameters.Enqueue("first");
            urlHelper.Parameters.Enqueue("second");

            var serviceUrl = urlHelper.GenerateServiceUrl();
            var expectedUrl = "Crewgo/UpdatePotentialStaffList/first/second";
            Assert.AreEqual(expectedUrl, serviceUrl);

        }
        [Test]
        public void GenerateServiceUrlNullApiTypeTest()
        {
            var urlHelper = new UrlHelper();

            urlHelper.ApiPath = DataEnums.ApiPath.UpdatePotentialStaffList;

            urlHelper.Parameters.Enqueue("first");
            urlHelper.Parameters.Enqueue("second");

            var serviceUrl = urlHelper.GenerateServiceUrl();
            var expectedUrl = "UpdatePotentialStaffList/first/second";
            Assert.AreEqual(expectedUrl, serviceUrl);

        }
        [Test]
        public void GenerateServiceUrlNullApiPathTest()
        {
            var urlHelper = new UrlHelper();

            urlHelper.ApiType = DataEnums.ApiType.Crewgo;

            urlHelper.Parameters.Enqueue("first");
            urlHelper.Parameters.Enqueue("second");

            var serviceUrl = urlHelper.GenerateServiceUrl();
            var expectedUrl = "Crewgo/first/second";
            Assert.AreEqual(expectedUrl, serviceUrl);

        }
        [Test]
        public void GenerateServiceUrlZeroParameterTest()
        {
            var urlHelper = new UrlHelper();

            urlHelper.ApiType = DataEnums.ApiType.Crewgo;
            urlHelper.ApiPath = DataEnums.ApiPath.UpdatePotentialStaffList;

            var serviceUrl = urlHelper.GenerateServiceUrl();
            var expectedUrl = "Crewgo/UpdatePotentialStaffList";
            Assert.AreEqual(expectedUrl, serviceUrl);

        }
        [Test]
        public void GenerateServiceUrlAllNullTest()
        {
            var urlHelper = new UrlHelper();
            var serviceUrl = urlHelper.GenerateServiceUrl();
            var expectedUrl = "";
            Assert.AreEqual(expectedUrl, serviceUrl);

        }
    }
}