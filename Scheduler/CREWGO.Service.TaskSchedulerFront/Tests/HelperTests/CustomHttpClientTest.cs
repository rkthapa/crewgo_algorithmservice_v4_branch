﻿using CREWGO.Service.TaskSchedulerFront.Helpers;
using NUnit.Framework;
using System.Net.Http;

namespace CREWGO.Service.TaskSchedulerFront.Tests.HelperTests
{
    [TestFixture]
    public class CustomHttpClientTest
    {
        private HttpClient customClient;
        [SetUp]
        public void CustomHttpClientTestSetUp()
        {
            customClient = CustomHttpClient.CustomHttpClientObj;
        }
        [Test]
        public void CustomHttpClientObjNotNullTest()
        {
            Assert.IsNotNull(customClient);
        }
        [Test]
        public void CustomHttpClientObjBaseAddressTest()
        {
            var expectedBaseAddress = "http://192.168.0.145/api/";
            Assert.AreEqual(customClient.BaseAddress, expectedBaseAddress);
        }
        [Test]
        public void CustomHttpClientObjAuthParameterTest()
        {
            var expectedAuthParameters = 
                "apiKey CB6BDLPLWIWF8HZD6207SENO30CHBVZO:" + 
                "W61AE1SSDV0QFAS3TFTH3FD8XRHMJIJATHTCQY9FJI46N6J3HIY6CSVQ23NWTGI5";
            Assert.AreEqual(
                customClient.DefaultRequestHeaders.Authorization.ToString(), 
                expectedAuthParameters
            );
        }
        [Test]
        public void CustomHttpClientObjHeaderAcceptTest()
        {
            var expectedHeaderAccept = "application/json";
            Assert.AreEqual(
                customClient.DefaultRequestHeaders.Accept.ToString(),
                expectedHeaderAccept
            );
        }
    }
}