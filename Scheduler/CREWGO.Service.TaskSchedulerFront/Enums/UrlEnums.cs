﻿namespace CREWGO.Service.TaskSchedulerFront
{
    /// <summary>
    /// API Route Prefixes
    /// </summary>
    public enum ApiType
    {
        Auth,
        Crewgo,
        JobAlert,
        JobAlertSetting,
        JobAlertSettingGlobal,
        PreNotification
    }
    /// <summary>
    /// Api Routes
    /// </summary>
    public enum ApiPath
    {
        // Auth
        GetCredentials,

        // Activity Levels
        GetActivityLevelByValue,
        GetAllActivityLevels,
        GetShortNoticePeriod,

        // Jobs
        GetAllJobs,
        GetJobById,

        // Shifts
        GetShiftById,
        GetAllShifts,
        GetLatestShifts,

        // Job Alert
        GetJobAlertByShiftUserId,

        // Job Staff
        PostJobStaff,

        // Job Alert Setting
        GetJobAlertSettingByShiftUserId,
        GetJobAlertSettingByPartnerUserId,

        // Potential Staff
        GeneratePotentialStaffList,
        GetPotentialStaffCount,
        GetPotentialStaffs,
        UpdatePotentialStaffList,
        GetShiftSociabilityFactor,
        CheckPSNotificationStatus,

        // User
        GetUserById,
        GetAvailableStaffByGlobalPriority,
        GetAvailableStaffByGeneralPriority,
        GetAvailableStaffByManualPriority,

        // PreNotifications
        GetPreNotifications,
        PutPrenotification,

        // Custom
        GetJobAlertCount,
        OrderPriorityExists,
        GlobalPriorityExists,
        IsShiftQuotaFulfilled,
        IsStaffAutobookable,
        GetCurrentDate,
        GetShiftNoticePeriod,
        PushNotification,

        // Job Alert
        PostJobAlert,

        // Job Alert Setting
        GetById,
        PutJobAlertSetting,
        PostJobAlertSetting,

        // Job Alert Global Setting
        PutJobAlertSettingGlobal,

        // Dashboard Authentication
        AuthenticateDashboardUse
    }
}