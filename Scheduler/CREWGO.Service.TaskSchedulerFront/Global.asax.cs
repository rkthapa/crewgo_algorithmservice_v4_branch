﻿
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using CREWGO.Service.TaskSchedulerFront.Handlers;
using CREWGO.Service.TaskSchedulerFront.Helpers;
using System;

namespace CREWGO.Service.TaskSchedulerFront
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            
            // Start Hangfire server instance
            HangfireBootstrapper.Instance.Start();

            // Handle Notification Types
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                new NotificationHandler().SendNotifications();
            }).Start();

            // Handle CREWGO Algorithm [CREW/PARTNER]
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                new AlgorithmHandler().InitiateAlgorithm();
            }).Start();
        }
        protected void Application_End(object sender, EventArgs e)
        {
            // Stop Hangfire server instance
            HangfireBootstrapper.Instance.Stop();
        }
    }
}