﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Web;
using CREWGO.Service.TaskSchedulerFront.Modules;
using CREWGO.Service.TaskSchedulerFront.Interfaces;
using CREWGO.Service.Common;

namespace CREWGO.Service.TaskSchedulerFront.Handlers
{
    /// <summary>
    /// Handles execution of algorithms and logging of execution time.
    /// </summary>
    public class AlgorithmHandler
    {
        #region PRIVATE FIELDS
        private static Stopwatch sw;
        private ICREWGOAlgorithm crewgoAlgorithm;
        //Runtime Log Path
        private static string runtimeLogPath = System.AppDomain.CurrentDomain.BaseDirectory +
                                                    string.Format("{0}{1}#{2}.txt", ConfigurationManager.AppSettings["CREWGO_ALGORITHM_PATH"].ToString(), "CREWGOAlgorithm-RuntimeLog", DateTime.Now.ToString("ddMMyyyy"));
        //Error Log Path
        private static string errorLogPath = System.AppDomain.CurrentDomain.BaseDirectory + 
                                                    string.Format("{0}{1}.txt", ConfigurationManager.AppSettings["PARTNERMODULE_PATH_ERROR"].ToString(), DateTime.Now.ToString("ddMMyyyy"));
        #endregion
        #region CONSTRUCTORS
        public AlgorithmHandler(ICREWGOAlgorithm algorithm)
        {
            crewgoAlgorithm = algorithm;
        }
        public AlgorithmHandler()
        {
            crewgoAlgorithm = new CREWGOAlgorithm();
        }
        #endregion
        #region RUN ALGORITHM
        public async void InitiateAlgorithm()
        {
            sbyte errorCount = 0;
            while (true)
            {
                Thread.Sleep(TimeSpan.FromSeconds(30));
                try
                {
                    Utility.WriteLogToFileExclusively(runtimeLogPath, "\nRun Algorithm| Date: " + DateTime.Now);
                    sw = Stopwatch.StartNew();
                    await crewgoAlgorithm.RunAlgorithm();
                    sw.Stop();
                    
                    Utility.WriteLogToFileExclusively(runtimeLogPath, "Successful Execution\n");
                    Utility.WriteLogToFileExclusively(runtimeLogPath, "Elapsed Time:" + sw.ElapsedMilliseconds + "ms\n");
                }
                catch (Exception ex)
                {
                    sw.Stop();

                    try
                    {
                        Utility.WriteLogToFileExclusively(runtimeLogPath, "Unsuccessful Execution \n");
                        Utility.WriteLogToFileExclusively(runtimeLogPath, "Elapsed Time:" + sw.ElapsedMilliseconds + "ms\n" + ex.Message + "\n" + ex.StackTrace);
                        Utility.WriteLogToFileExclusively(errorLogPath, "Elapsed Time:" + sw.ElapsedMilliseconds + "ms\n" + ex.Message + "\n" + ex.StackTrace);
                        Utility.SendEmail(EmailType.ERRORMAIL, "\r\n" + ex.Message + "\r\n" + ex.StackTrace.ToString());
                    }
                    catch(Exception)
                    { }
                    if (++errorCount == 5)
                        Thread.Sleep(TimeSpan.FromMinutes(5));
                    continue;
                }
            }
        }        
        #endregion
    }
}