﻿using System;
using System.Diagnostics;
using System.Threading;
using CREWGO.Service.TaskSchedulerFront.Modules;
using CREWGO.Service.TaskSchedulerFront.Services;
using CREWGO.Service.Common;

namespace CREWGO.Service.TaskSchedulerFront.Handlers
{
    /// <summary>
    /// Invokes the ScheduleNotifications algorithm.
    /// Maintains a log of algorithm runtime.
    /// </summary>
    public class NotificationHandler
    {
        #region Private Fields
        private static Stopwatch sw;
        private static string ErrorPath =
            System.AppDomain.CurrentDomain.BaseDirectory
            + "/Logs/ErrorLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
        private static string LogPath =
            System.AppDomain.CurrentDomain.BaseDirectory
            + "/Logs/BackgroundTaskLog_" + DateTime.Now.ToString("ddMMyyyy") +
            ".txt";
        #endregion
        /// <summary>
        /// Awaits ScheduleNotifications
        /// Maintains stopwatch
        /// </summary>
        /// <param name="client">The client to make API calls as.</param>
        public async void SendNotifications()
        {
            while (true)
            {
                Thread.Sleep(TimeSpan.FromSeconds(30));
                try
                {
                    sw = Stopwatch.StartNew();
                    var notifications = new CREWGONotifications();
                    await notifications.ScheduleNotifications();
                    sw.Stop();
                    await new CREWGOService().MaintainLog(sw);
                }
                catch (Exception ex)
                {
                    try
                    {
                        Utility.WriteLogToFileExclusively(
                            ErrorPath,
                            "Unsuccessful Execution \r\n" +
                                ex.Message + "\r\n" + ex.StackTrace
                        );
                    }
                    catch(Exception)
                    {}
                }

            }
        }
    }
}