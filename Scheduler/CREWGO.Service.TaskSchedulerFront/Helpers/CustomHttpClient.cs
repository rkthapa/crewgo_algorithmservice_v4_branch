﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Text;

namespace CREWGO.Service.TaskSchedulerFront.Helpers
{
    /// <summary>
    /// Custom HttpClient silgleton class
    /// Used to make all API calls within the algorithm.
    /// </summary>
    public sealed class CustomHttpClient
    {
        private static readonly HttpClient _customHttpClient = new HttpClient();
        /// <summary>
        /// HttpClient initialization. Base address, app id and api key are 
        /// fetched from app settings. AuthenticationHeaderValue's authContent 
        /// is set to apiKey in the authorization header.
        /// Default request timeout is set to 35 sesonds.
        /// </summary>
        static CustomHttpClient() 
        {
            _customHttpClient.BaseAddress = new Uri(
                ConfigurationManager
                .AppSettings["CREWGOWEBAPI"].ToString());
            _customHttpClient.DefaultRequestHeaders.Accept.Clear();

            //Setting up authentication header
            string authContent = 
            String.Format(
                "{0}:{1}",
                ConfigurationManager
                    .AppSettings["CREWGOWEBAPI_APP_ID"].ToString(),
                ConfigurationManager
                    .AppSettings["CREWGOWEBAPI_API_KEY"].ToString()
            );

            _customHttpClient.DefaultRequestHeaders.Authorization = 
                new System.Net.Http.Headers.AuthenticationHeaderValue(
                    "apiKey", authContent
                );

            _customHttpClient.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers
                    .MediaTypeWithQualityHeaderValue("application/json"));

            _customHttpClient.Timeout = TimeSpan.FromSeconds(35);
        }
        public static HttpClient CustomHttpClientObj
        {
            get
            {
                return _customHttpClient;
            }
        }
    }
}