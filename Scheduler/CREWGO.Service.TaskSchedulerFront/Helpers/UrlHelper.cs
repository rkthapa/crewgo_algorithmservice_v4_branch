﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CREWGO.Service.TaskSchedulerFront.Helpers
{
    /// <summary>
    /// Manages URL, API parameters and builds API call service URL.
    /// </summary>
    public class UrlHelper
    {
        /// <summary>
        /// The API Route Prefix
        /// </summary>
        public ApiType? ApiType { get; set; }
        /// <summary>
        /// The API Route
        /// </summary>
        public ApiPath? ApiPath { get; set; }
        /// <summary>
        /// List of API Call Parameters
        /// </summary>
        public Queue<string> Parameters { get; set; }
        /// <summary>
        /// Instantiate 'Parameters' queue
        /// </summary>
        public UrlHelper()
        {
            Parameters = new Queue<string>();
        }
        /// <summary>
        /// Generates the service URL from ApiType, ApiPath and Parameters list
        /// </summary>
        /// <returns>Service URL string</returns>
        public string GenerateServiceUrl()
        {
            StringBuilder serviceUrl = new StringBuilder();
            if (ApiType.ToString() != String.Empty)
            {
                serviceUrl.Append(ApiType.ToString()); 
            }
            if (ApiPath.ToString() != String.Empty)
            {
                if (ApiType.ToString() != String.Empty)
                {
                    serviceUrl.Append("/");
                }
                serviceUrl.Append(ApiPath.ToString());
            }
            if (Parameters == null || Parameters.Count == 0)
            {
                return serviceUrl.ToString();
            }
            var count = Parameters.Count;
            for (int i = 0; i < count; ++i)
            {
                serviceUrl.Append("/");
                serviceUrl.Append(Parameters.Dequeue());
            }
            return serviceUrl.ToString();
        }
    }
}