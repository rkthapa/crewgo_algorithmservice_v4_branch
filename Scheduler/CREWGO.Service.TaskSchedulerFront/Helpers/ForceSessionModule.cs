﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace CREWGO.Service.TaskSchedulerFront.Helpers
{
    /// <summary>
    /// Implements IHttpModule to force session use on every request
    /// because Hangfire dashboard requests originating from job details,
    /// job reschedule, job enqueue and job delete have null session values
    /// by default.
    /// </summary>
    public class ForceSessionModule : IHttpModule
    {
        /// <summary>
        /// Assigns event handler for PostAuthorizeRequest
        /// </summary>
        /// <param name="context">HttpApplication context</param>
        public void Init(HttpApplication context)
        {
            context.PostAuthorizeRequest += OnPostAuthorizeRequest;
        }

        public void Dispose() { }
        /// <summary>
        /// Sets SessionStateBehavior to SessionStateBehavior.Required
        /// </summary>
        private void OnPostAuthorizeRequest(object sender, EventArgs eventArgs)
        {
            var context = ((HttpApplication)sender).Context;
            var request = context.Request;
            if ((request != null
                 && request.AppRelativeCurrentExecutionFilePath != null
                 && request.AppRelativeCurrentExecutionFilePath.StartsWith(
                    "~/", 
                    StringComparison.InvariantCultureIgnoreCase)))
            {
                context.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }
    }
}