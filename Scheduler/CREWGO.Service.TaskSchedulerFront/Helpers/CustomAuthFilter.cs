﻿using CREWGO.Service.TaskSchedulerFront.Services;
using Hangfire.Dashboard;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace CREWGO.Service.TaskSchedulerFront.Helpers
{
    /// <summary>
    /// Authorizes Hangfire Dashboard use
    /// </summary>
    public class CustomDashboardAuthFilter : IDashboardAuthorizationFilter
    {
        /// <summary>
        /// Authorize if session variable ["IsLoggedIn"] has been set to true.
        /// Redirect to login page otherwise.
        /// </summary>
        /// <param name="context">DashboardContext</param>
        /// <returns>True if authorised.</returns>
        public bool Authorize(DashboardContext context)
        {
            if (HttpContext.Current == null)
            {
                HttpContext.Current.Response.Redirect("/Home/Index");
            }
            if (HttpContext.Current.Session == null)
            {
                HttpContext.Current.Response.Redirect("/Home/Index");
            }
            if (HttpContext.Current.Session["IsLoggedIn"] == null)
            {
                HttpContext.Current.Response.Redirect("/Home/Index");
            }
            if (!(bool)HttpContext.Current.Session["IsLoggedIn"])
            {
                HttpContext.Current.Response.Redirect("/Home/Index");
            }
            else
            {
                return true;
            }
            return false;
        }
    }
}