﻿using Hangfire;
using System;
using System.Configuration;
using System.Web.Hosting;

namespace CREWGO.Service.TaskSchedulerFront.Helpers
{
    /// <summary>
    /// Controls the Hangfire Background task server.
    /// </summary>
    public class HangfireBootstrapper : IRegisteredObject
    {
        public static readonly HangfireBootstrapper Instance = 
            new HangfireBootstrapper();

        private readonly object _lockObject = new object();
        private bool _started;

        private BackgroundJobServer _backgroundJobServer;

        private HangfireBootstrapper()
        {
        }

        /// <summary>
        /// Starts an instance of Hangfire Background task server.
        /// </summary>
        public void Start()
        {
            lock (_lockObject)
            {
                if (_started) return;
                _started = true;

                HostingEnvironment.RegisterObject(this);

                ////Use MySQL storage HangfireDB
                GlobalConfiguration.Configuration.UseSqlServerStorage("HangfireDB");

                // Maximum retry attempts: 5
                GlobalJobFilters.Filters.Add(
                    new AutomaticRetryAttribute
                    {
                        Attempts = Convert.ToInt32(
                            ConfigurationManager.AppSettings["HANGFIRE_MAX_RETRY_ATTEMPTS"])
                    });

                // Worker Multiple determines number of worker processes
                int workerMultiple = Convert.ToInt32(
                    ConfigurationManager.AppSettings["HANGFIRE_WORKER_MULTIPLE"]);

                // Number of concurrent workers : ProcessorCount * workerMultiple
                var options = new BackgroundJobServerOptions
                {
                    WorkerCount = Environment.ProcessorCount * workerMultiple,
                    Activator = NinjectJobActivator.Current,
                    ServerName = "CrewgoScheduler"
                };

                _backgroundJobServer = new BackgroundJobServer(options);
            }
        }
        /// <summary>
        /// Stops the instance of Hangfire Background task server.
        /// </summary>
        public void Stop()
        {
            lock (_lockObject)
            {
                if (_backgroundJobServer != null)
                {
                    _backgroundJobServer.Dispose();
                }

                HostingEnvironment.UnregisterObject(this);
            }
        }

        void IRegisteredObject.Stop(bool immediate)
        {
            Stop();
        }
    }
}