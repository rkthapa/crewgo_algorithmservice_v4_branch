﻿using CREWGO.Service.Common;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CREWGO.Service.TaskSchedulerFront.Helpers
{
    /// <summary>
    /// Kernel class for NinjectJobActivator
    /// </summary>
    public class KernelHelper
    {
        private static readonly IKernel kernel = CreateKernel();
        private static IKernel CreateKernel()
        {
            // Inject push notification keys via dependency manager
            var kernel = new StandardKernel();
            kernel.Bind<IPushNotifications>()
                .To<PushNotifications>()
                .WithConstructorArgument(
                    "customerSenderId",
                    PushNotificationKeys.CustomerSenderId)
                .WithConstructorArgument(
                    "customerAuthToken",
                    PushNotificationKeys.CustomerAuthToken)
                .WithConstructorArgument(
                    "supervisorSenderId",
                    PushNotificationKeys.SupervisorSenderId)
                .WithConstructorArgument(
                    "supervisorAuthToken",
                    PushNotificationKeys.SupervisorAuthToken)
                .WithConstructorArgument(
                    "staffSenderId",
                    PushNotificationKeys.StaffSenderId)
                .WithConstructorArgument(
                    "staffAuthToken",
                    PushNotificationKeys.StaffAuthToken)
                .WithConstructorArgument(
                    "appleCertCustomer",
                    PushNotificationKeys.CustomerAppleCert)
                .WithConstructorArgument(
                    "applePassCustomer",
                    PushNotificationKeys.CustomerApplePass)
                .WithConstructorArgument(
                    "appleCertSupervisor",
                    PushNotificationKeys.SupervisorAppleCert)
                .WithConstructorArgument(
                    "applePassSupervisor",
                    PushNotificationKeys.SupervisorApplePass)
                .WithConstructorArgument(
                    "appleCertStaff",
                    PushNotificationKeys.StaffAppleCert)
                .WithConstructorArgument(
                    "applePassStaff",
                    PushNotificationKeys.StaffApplePass);
            return kernel;
        }
        public static IKernel CustomStandardKernel
        {
            get
            {
                return kernel;
            }
        }
    }
}