﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CREWGO.Service.Common;
using CREWGO.Service.BusinessService.Common;

namespace CREWGO.Service.TaskSchedulerFront.Interfaces
{
    /// <summary>
    /// The partner algorithm Interface.
    /// </summary>
    public interface ICREWGOPartnerAlgorithm
    {
        /// <summary>
        /// The Algorithm Logic
        /// </summary>
        /// <param name="shift">JobShift</param>
        /// <param name="shiftNoticePeriod">The job shift notice period</param>
        /// <returns></returns>
        Task RunPartnerAlgorithm(
            JobShift shift, int shiftNoticePeriod);
    }
}