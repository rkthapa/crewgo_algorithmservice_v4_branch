﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using CREWGO.Service.BusinessService;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;
using CREWGO.Service.TaskSchedulerFront.Helpers;
using System.ComponentModel;
using System.Diagnostics;

namespace CREWGO.Service.TaskSchedulerFront.Interfaces
{
    /// <summary>
    /// Service class for all algorithms
    /// </summary>
    public interface ICREWGOService
    {
        #region CREWGO ALGORITHM SERVICES
        #region GET METHODS
        Task<List<JobShift>> GetShifts();
        Task<List<User>> GetStaffByGeneralPriority(
            long partnerId, long jobShiftId, int skillId,
            int levelId, int userLimit, DateTime? startTime, DateTime? endTime);
        Task<List<User>> GetStaffByGlobalPriority(
            long partnerId, long jobShiftId, int skillId, int levelId,
            int userLimit, DateTime? startTime, DateTime? endTime);
        Task<List<User>> GetStaffByManualPriority(
            long jobShiftId, int userLimit,
            DateTime? startTime, DateTime? endTime);
        Task<Int32> GetJobAlertCount(long staffUserId);
        Task<Boolean> OrderPriorityExists(long jobShiftId);
        Task<Boolean>
            GlobalPriorityExists(long partnerId, int skillId, int levelId);
        JobAlert GetJobAlert(long jobShiftId, long staffUserId);
        JobAlertSetting GetJobAlertSettingByShiftUser
            (long jobShiftId, long staffUserId);
        JobAlertSettingGlobal GetJobAlertSettingByPartnerUser(
            long partnerId, long staffUserId);
        JobAlertSetting GetJobManualAlertSetting(
            JobAlertSetting obj);
        JobAlertSettingGlobal GetJobGlobalAlertSetting(
            JobAlertSettingGlobal obj);
        Task<Boolean> IsShiftQuotaFulfilled(long jobShiftId);
        /// <summary>
        /// Checks if staff has autobookable enabled ot not.
        /// </summary>
        /// <param name="staffId">Staff's userId.</param>
        /// <returns>True if autobookable, False otherwise.</returns>
        Task<Boolean> IsStaffAutobookable(long staffId);
        Task<int> GetShiftNoticePeriod();
        #endregion
        #region POST METHODS
        /// <summary>
        /// Posts a JobStaff object to API.
        /// </summary>
        /// <param name="obj">JobStaff object.</param>
        /// <param name="shiftId">Shift ID for DisplayName attribute.</param>
        /// <param name="staffId">Staff ID for DisplayName attribute.</param>
        /// <param name="type">Algorithm type for DisplayName attribure.</param>
        [DisplayName("Post JobStaff: Shift: {1}  Staff: {2} for {3} Algorithm")]
        Task<bool> PostJobStaff(
            JobStaff jobStaff, string shiftId, string staffId, string algorithm);
        /// <summary>
        /// Posts a JobAlert object to API.
        /// </summary>
        /// <param name="obj">JobAlert object</param>
        /// <param name="id">ParentJobID for DisplayName attribute only</param>
        /// <param name="type">Algorithm type for DisplayName attribure</param>
        [DisplayName("Post Job Alert: ID: {1} for {2} Algorithm")]
        void PostJobAlert(
            JobAlert obj, string id, string type);
        /// <summary>
        /// Post job alert settings to database
        /// </summary>
        /// <param name="obj">Manual Alert Setting Object</param>
        void PostJobAlertSetting(JobAlertSetting obj);
        #endregion
        #region PUT METHODS
        void PutJobAlertSetting(JobAlertSetting obj);
        void PutJobAlertSettingGlobal(JobAlertSettingGlobal obj);
        #endregion
        #region SET ENTITYFRAMEWORK OBJECTS
        JobAlert SetJobAlert(
            long jobShiftId, long staffUserId, JobAlertStatusEnum status);
        JobAlertSetting SetJobAlertSetting(
            long jobShiftId, long staffUserId, long partnerId);
        JobStaff SetJobStaff(
            long staffId, long shiftId, long alertId, DateTime acceptedDate);
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Update job alert settings for case: manual
        /// </summary>
        /// <param name="shift">JobSHift</param>
        /// <param name="staff">User (Staff)</param>
        /// <param name="parentJobId">ParentJobID for Display Name Only</param>
        [DisplayName(
            "Update Alert Settings Manual: Parent JobID: {2} , " +
            "StaffID: {1} , ShiftID: {0}"
        )]
        void UpdateAlertSettingsManual(
            long shiftId, long staffId, long parentJobId);
        /// <summary>
        /// Update job alert settings for case: global
        /// </summary>
        /// <param name="shift">JobSHift</param>
        /// <param name="staff">User (Staff)</param>
        /// <param name="parentJobId">ParentJobID for Display Name Only</param>
        [DisplayName(
            "Update Alert Settings Global: Parent JobID: {2} , " +
            "StaffID: {1} , LHCUserID: {0}"
        )]
        void UpdateAlertSettingsGlobal(
            long lhcUserId, long staffId, long parentJobId);
        /// <summary>
        /// Update job alert settings for case: general
        /// </summary>
        /// <param name="shift">JobSHift</param>
        /// <param name="staff">User (Staff)</param>
        /// <param name="parentJobId">ParentJobID for Display Name Only</param>
        [DisplayName(
            "Update Alert Settings General: Parent JobID: {3} , " +
            "ShiftId: {0} , StaffID: {1} , LHCUserID: {2}"
        )]
        void UpdateAlertSettingsGeneral(
            long shiftId, long staffId, long lhcUserId, long parentJobId);
        Task MaintainAlertLog(
            string jobTitle, string jobNumber, long jobShiftId,
            string staffPriorityType, long? partnerId, int shiftReqNumber,
            int skillId, int levelId, DateTime? startTime, DateTime? endTime,
            long staffId, string staffName, string staffEmail,
            string deviceToken);
        #endregion
        #region POTENTIAL STAFF ALGORITHM
        #region GET METHODS
        /// <summary>
        /// Gets the count of potential staffs.
        /// </summary>
        /// <param name="jobShiftId">The Job Shift Id</param>
        /// <returns>Count of Potential Staffs(int)</returns>
        Task<int> GetPotentialStaffCount(long jobShiftId);
        /// <summary>
        /// Fetches shift sociability factor.
        /// </summary>
        /// <param name="jobShiftId">The job shift's Id</param>
        /// <returns>Shift sociability factor (0 <= SSF <= 1)</returns>
        Task<float> GetShiftSociabilityFactor(
            long jobShiftId);
        /// <summary>
        /// Gets all activity levels defined in the admin portal.
        /// </summary>
        /// <returns>ActivityLevelList</returns>
        Task<List<ActivityLevel>> GetAllActivityLevels();
        /// <summary>
        /// Gets list of top potential staffs upto 'count'
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        /// <param name="count">The number of potential staffs to fetch</param>
        /// <returns>List of PotentialStaff</returns>
        Task<List<PotentialStaff>> GetPotentialStaffs(
            long jobShiftId, int count);
        /// <summary>
        /// Gets the value of short notice period (in hours) by API call
        /// </summary>
        /// <returns>Timespan in hours</returns>
        Task<TimeSpan> GetShortNoticePeriod();

        #endregion
        #region POST METHODS
        /// <summary>
        /// API call to generate Potential Staff List for a job in database
        /// </summary>
        /// <param name="jobShiftId">The Job Shift's ID</param>
        Task GeneratePotentialStaffList(long jobShiftId);
        /// <summary>
        /// Updates the potential staff table for a particular shift-user 
        /// combination with value of is_notification_sent as true
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        [DisplayName("Set Notification Sent Flag for user: {0}, shift: {1}")]
        void UpdatePotentialStaffList(
            long userId, long shiftId, bool isSent, bool isScheduled);
        /// <summary>
        /// Checks potential staff's notification status for the job shift.
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <returns>True if notification is sent, false otherwise</returns>
        bool CheckPSNotificationStatus(long userId, long shiftId);
        /// <summary>
        /// Updates the potential staff table for a particular shift-user 
        /// combination with value of is_notification_sent as true
        /// </summary>
        /// <param name="userId">The staff's user id</param>
        /// <param name="shiftId">The job shift id</param>
        /// <param name="isScheduled">If notification is scheduled.</param>
        /// <param name="isSent">If notification is sent.</param>
        Task UpdatePotentialStaffListAsync(
            long userId, long shiftId, bool isSent, bool isScheduled);
        #endregion
        #region PUT METHODS
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Calculates Potential Staff Result
        /// </summary>
        /// <param name="numberOfStaffs">The number of potential staffs</param>
        /// <param name="numberOfShifts">The number of shifts in a job</param>
        /// <returns>Potential Staff Result</returns>
        double ComputePotentialStaffResult(
            int numberOfStaffs, int numberOfShifts, float ssf);
        /// <summary>
        /// Calculates the number of Single/Advance requests to be sent
        /// until short notice period arrives
        /// </summary>
        /// <param name="time">Time until short notice period</param>
        /// <param name="rate">The rate to send messages at</param>
        /// <returns></returns>
        int ComputeNumberOfRequests(
            TimeSpan time, TimeSpan timeUnit, int shiftMultiple);
        /// <summary>
        /// Calculates the number of requests until next ActivityLevel
        /// for Multiple Request Period
        /// </summary>
        /// <param name="currentPSR">PSR of current Activity Lecvel</param>
        /// <param name="nextPSR">PSR of next Activity Level</param>
        /// <returns>Number of requests</returns>
        int ComputeNumberOfRequests(
            double currentPSR, double nextPSR);
        /// <summary>
        /// Calculates the PSR 
        /// </summary>
        /// <param name="nextAL"></param>
        /// <returns></returns>
        double CalculateNextPSR(ActivityLevel nextAL);
        #endregion
        #endregion
        #endregion
        #region CREWGO NOTIFICATIONS SERVICES
        #region GET METHODS
        /// <summary>
        /// Gets PreNotifications which are neither scheduled nor sent.
        /// </summary>
        /// <returns>List of PreNotifications</returns>
        Task<IEnumerable<PreNotification>>
            GetPreNotifications();
        /// <summary>
        /// Gets User by userId.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns>User</returns>
        Task<User> GetUserById(long userId);
        /// <summary>
        /// Gets Job by jobId.
        /// </summary>
        /// <param name="jobId">Job Id</param>
        /// <returns>Job</returns>
        Task<Job> GetJobById(long jobId);
        /// <summary>
        /// Gets JobShifts by jobShiftId.
        /// </summary>
        /// <param name="jobShiftId">Job Shift Id</param>
        /// <returns>JobShifts</returns>
        Task<JobShift> GetJobShiftById(long jobShiftId);
        #endregion
        #region PUT METHODS
        /// <summary>
        /// Sends "is_scheduled" and "is_sent" values to api respectively
        /// after the notification is scheduled and sent.
        /// </summary>
        /// <param name="obj">The PreNotificatio object</param>
        /// <param name="isSent">True if notification is sent.</param>
        void PutPreNotification(
            int prenotificationId, bool isSent);
        #endregion
        #region CUSTOM METHODS
        /// <summary>
        /// Writes algorithm runtime to Log.
        /// </summary>
        /// <param name="sw">The active stopwatch.</param>
        Task MaintainLog(Stopwatch sw);
        #endregion
        #endregion
    }
}