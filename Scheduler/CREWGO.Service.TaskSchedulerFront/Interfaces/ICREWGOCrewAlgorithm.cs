﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel;
using CREWGO.Service.BusinessService.Common;
using CREWGO.Service.Common;

namespace CREWGO.Service.TaskSchedulerFront.Interfaces
{
    /// <summary>
    /// The CrewGo Crew (CRUVA) Algorithm Interface
    /// </summary>
    public interface ICREWGOCrewAlgorithm 
    {
        /// <summary>
        /// The CRUVA algorithm logic
        /// </summary>
        /// <param name="shift">JobShift</param>
        Task RunCrewAlgorithm(JobShift shift);
        /// <summary>
        /// Prepares notifications scheduling to potential staffs
        /// </summary>
        /// <param name="staffs">List of Potential Staffs</param>
        /// <param name="shift">The job shift</param>
        /// <param name="interval">Interval to send notifications in</param>
        /// <param name="count">Number of notifications to send at once</param>
        Task ScheduleNotifications(List<PotentialStaff> staffs, JobShift shift, 
            int interval, int count);
        /// <summary>
        /// Checks if shift quota is fulfilled 
        /// and schedules notifications if it is not fulfilled
        /// </summary>
        /// <param name="androidTokens">List of Android tokens</param>
        /// <param name="iosTokens">List of iOS tokens</param>
        /// <param name="jobTitle">The ParentJob Title of the shift</param>
        /// <param name="jobID">The Parent JobID for of the shift</param>
        /// <param name="shiftID">The ShiftID to send notifications for</param>
        [DisplayName("Schedule Crew Notifications: Job: {2} , JobID: {3}")]
        bool CheckJobStatusAndScheduleNotifications(
            List<string> androidTokens, List<string> iosTokens, 
            string jobTitle, string jobNumber, long shiftId, long userId,
            NotificationTypeEnum type);
    }
}
