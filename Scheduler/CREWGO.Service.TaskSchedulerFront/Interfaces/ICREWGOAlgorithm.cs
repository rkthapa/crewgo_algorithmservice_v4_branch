﻿using System.Threading.Tasks;

namespace CREWGO.Service.TaskSchedulerFront.Interfaces
{
    public interface ICREWGOAlgorithm
    {
        Task RunAlgorithm();
    }
}