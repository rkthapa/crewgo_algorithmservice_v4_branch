﻿using System.Threading.Tasks;

namespace CREWGO.Service.TaskSchedulerFront.Interfaces
{
    public interface ICREWGONotifications
    {
        Task ScheduleNotifications();
    }
}