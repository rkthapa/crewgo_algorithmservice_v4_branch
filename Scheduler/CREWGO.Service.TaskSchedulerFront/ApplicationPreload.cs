﻿using CREWGO.Service.TaskSchedulerFront.Helpers;
using System.Web.Hosting;

namespace CREWGO.Service.TaskSchedulerFront
{
    /// <summary>
    /// ApplicationPreload class for keeping the application always running.
    /// This class is referenced in ApplicationHost.config file in
    /// ../windows/system32/inetsrv/config/ApplicationHost.config
    /// Changing this class name will result in Application Pool failure in IIS.
    /// To avoid this, use the same name in ApplicationHost.config file also.
    /// This class initiates an instance of HangfireBootsteapper class.
    /// </summary>
    public class ApplicationPreload : IProcessHostPreloadClient
    {
        public void Preload(string[] parameters)
        {
           HangfireBootstrapper.Instance.Start();
        }
    }
}